package pageobjects;

import com.codeborne.selenide.Selenide;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 23.08.2017.
 */
public class MailForSpamMainPage extends WebDriverCommands {

    private static MailForSpamMainPage mailForSpamPage = null;

    public static synchronized MailForSpamMainPage getMailForSpamPage() {
        if (mailForSpamPage == null)
            mailForSpamPage = new MailForSpamMainPage();

        return mailForSpamPage;
    }

    public MailForSpamMainPage gotoMailForSpamPage() {
        Selenide.open("https://www.mailforspam.com/");

        return this;
    }

    private final String EMAIL_INPUT_FIELD = "input_box"; //id
    private final String CHECK_MAILBOX_BUTTON = "button"; //id
    private final String BOOKED_ORDER_LETTER = "//a[contains(text(), 'принят в работу')]";
    private final String CLEAR_MAILBOX_BUTTON = "//a[@class = 'button_del']";


    public MailForSpamMainPage fillEmailValue(String emailValue) throws Exception{
        $(byId(EMAIL_INPUT_FIELD)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(emailValue.replace("@mailforspam.com", ""));

        return this;
    }

    public MailForSpamMainPage clickCheckMailboxButton() throws Exception{
        $(byId(CHECK_MAILBOX_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MailForSpamMainPage clickClearMailboxButton() throws Exception{
        $(byXpath(CLEAR_MAILBOX_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MailForSpamLetterPage clickBookedOrderLetter() throws Exception{
        $(byXpath(BOOKED_ORDER_LETTER)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MailForSpamLetterPage();
    }

}
