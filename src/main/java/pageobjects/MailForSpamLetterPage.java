package pageobjects;

import framework.WebDriverCommands;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 23.08.2017.
 */
public class MailForSpamLetterPage extends WebDriverCommands{

    private static MailForSpamLetterPage mailForSpamLetterPage = null;

    public static synchronized MailForSpamLetterPage getMailForSpamLetterPage() {
        if (mailForSpamLetterPage == null)
            mailForSpamLetterPage = new MailForSpamLetterPage();

        return mailForSpamLetterPage;
    }

    private final String EMAIL_SUBJECT_FIELD = "//p[contains(text(), 'принят в работу')]";
    public static String orderNumber;

    public MailForSpamLetterPage getOrderNumber() throws Exception{
        orderNumber = $(byXpath(EMAIL_SUBJECT_FIELD)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D", "");
        return this;

    }

}
