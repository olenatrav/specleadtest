package pageobjects.TOM;

import framework.WebDriverCommands;

import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
/**
 * Created by User on 14.06.2017.
 */
public class PredeparturePage extends WebDriverCommands {
    private static PredeparturePage predeparturePage = null;

    private PredeparturePage() {
    }

    public static synchronized PredeparturePage getPredeparturePage() {
        if (predeparturePage == null)
            predeparturePage = new PredeparturePage();

        return predeparturePage;
    }

    final private String LAST_NAME_VALUE = "//.[contains(@ng-bind, 'lastName')]";
    final private String FIRST_NAME_VALUE = "//.[contains(@ng-bind, 'firstName')]";
    final private String MIDDLE_NAME_VALUE = "//.[contains(@ng-bind, 'middleName')]";
    final private String EMAIL_VALUE = "//.[contains(@ng-bind, 'email')]";
    final private String PHONE_VALUE = "//.[contains(@ng-bind, 'phone')]";

    final private String DEPARTURE_CITY_VALUE = "//.[text() = 'Город вылета']/following-sibling::div";
    final private String COUNRY_VALUE = "//.[text() = 'Страна']/following-sibling::div";
    final private String RESORT_VALUE = "//.[text() = 'Курорт']/following-sibling::div";
    final private String HOTEL_NAME_VALUE = "//.[text() = 'Отель']/following-sibling::div";
    final private String ADULTS_VALUE = "//.[text() = 'Взрослые']/following-sibling::div";
    final private String CHILDREN_VALUE = "//.[text() = 'Дети']/following-sibling::div";
    final private String INFANTS_VALUE = "//.[text() = 'Младенцы']/following-sibling::div";
    final private String DEPARTURE_DATE_VALUE = "//.[text() = 'Дата вылета']/following-sibling::div";
    final private String NIGHTS_VALUE = "//.[text() = 'Ночей']/following-sibling::div";
    final private String TOUR_OPERATOR_VALUE = "//.[text() = 'Оператор']/following-sibling::div";
    final private String MEAL_TYPE_VALUE = "//.[text() = 'Питание']/following-sibling::div";
    final private String ROOM_TYPE_VALUE = "//.[text() = 'Тип номера']/following-sibling::div";
    final private String ACCOMODATION_VALUE = "//.[text() = 'Размещение']/following-sibling::div";
    final private String TRANSFER_VALUE = "//.[text() = 'Трансфер']/following-sibling::div";
    final private String TRANSPORT_VALUE = "//.[text() = 'Транспорт']/following-sibling::div";

    final private String TOUR_PRICE_VALUE = "//.[text() = 'Стоимость тура']/following-sibling::div";
    final private String OIL_TAX_VALUE = "//.[text() = 'Топливный сбор']/following-sibling::div";
    final private String INFANT_TAX_VALUE = "//.[text() = 'Сбор за младенцев']/following-sibling::div";
    final private String DINNER_TAX_VALUE = "//.[text() = 'Доплата за ужин']/following-sibling::div";

    final private String BOOKING_CODE_VALUE = "//.[text() = 'Ид брони']/following-sibling::div";
    final private String BOOKING_DATE_VALUE = "//.[text() = 'Дата']/following-sibling::div";

    final private String VISA_LAST_AND_FIRST_NAME = "//.[text() = 'ФИО']/following-sibling::input";
    final private String VISA_BIRTHDAY = "//.[text() = 'Дата рождения']/following-sibling::input";
    final private String VISA_NATIONALITY = "//.[text() = 'Гражданство']/following-sibling::input";
    final private String VISA_EXPIRE_DATE = "//.[text() = 'Действителен до']/following-sibling::input";
    final private String VISA_ADDRESS = "//.[text() = 'Адрес']/following-sibling::input";
    final private String VISA_PRICE = "//.[text() = 'Стоимость, руб.']/following-sibling::input";
    final private String VISA_DOCUMENTS_TAKEN_FROM_CUSTOMER = "//input[contains(@data-ng-model, 'documentsTakenFromCustomer')]/following-sibling::span";
    final private String VISA_DOCUMENTS_TAKEN_BACK_FROM_TO = "//input[contains(@data-ng-model, 'documentsTakenBackFromTO')]/following-sibling::span";
    final private String VISA_DOCUMENTS_SEND_BACK_TO_CUSTOMER = "//input[contains(@data-ng-model, 'documentsSendBackToCustomer')]/following-sibling::span";
    final private String VISA_TOTAL = "//.[contains(@data-ng-bind, 'SummVisa')]";

    final private String ACTION_NEW_BUTTON = "//.[contains(text(), 'Добавить действие')]";
    final private String ACTION_LIST_DROPDOWN = "//select[contains(@data-ng-model, 'forms.action.action')]";
    final private String ACTION_DATE = "//input[contains(@data-ng-model, 'executedDue')][1]";
    final private String ACTION_TIME = "//input[contains(@data-ng-model, 'executedDue')][2]";
    final private String ACTION_SUBMIT_BUTTON = "//div[contains(@data-ng-click, 'saveNextAction')]";
    final private String ACTION_NAME_VALUE = "//td[text() = '%s']";
    final private String ACTION_DATE_VALUE = "//td[contains(text(), '%s')]";
    final private String ACTION_EXECUTE_BUTTON = "//.[text() = '%s']/following-sibling::td/a";


    final private String LAST_AND_FIRST_NAME = "(//.[contains(@data-ng-repeat, 'touristDocument')]/div/strong)[%s]";
    final private String NATIONALITY = "(//.[contains(@data-ng-repeat, 'touristDocument')]/div/div)[%s]";
    final private String BIRTHDAY = "(//.[contains(@data-ng-repeat, 'touristDocument')]/div/small)[%s]";

    final private String DOCUMENTS_ARE_SENT = "//input[contains(@data-ng-model, 'ticketsSend')]/following-sibling::span";

    final private String NOTIFICATION_TITLE = "//div[@aria-label = '%s']";
    final private String NOTIFICATION_MESSAGE = "//div[@aria-label = '%s']";

    public static String documentsAreSent = "нет";
    public static String nextActionName = "";
    public static String nextActionDate = "";
    public static String lastAction = "";

    public PredeparturePage checkPredepartureData(String lastName, String firstName, String middleName, String email, String phone, String departureCity, String country, String resort,
                                                  String hotel, String adults, String children, String infants, String startDate, String nights, String tourOperator, String mealType, String tourPrice,
                                                  String bookingCode, String bookingDate) throws Exception
    {
        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }


        Assert.assertTrue($(byXpath(LAST_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(lastName.toLowerCase()), "predeparture page: s1");
        Assert.assertTrue($(byXpath(FIRST_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(firstName.toLowerCase()), "predeparture page: s2");
        if (!middleName.equals(""))
        {
            Assert.assertTrue($(byXpath(MIDDLE_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(middleName.toLowerCase()), "predeparture page: s3");
        }

        Assert.assertTrue($(byXpath(EMAIL_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(email.toLowerCase()), "predeparture page: s4");
        Assert.assertTrue($(byXpath(PHONE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("7" + phone.toLowerCase()), "predeparture page: s5");

        Assert.assertTrue($(byXpath(DEPARTURE_CITY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(departureCity.toLowerCase()), "predeparture page: s6");
        Assert.assertTrue($(byXpath(COUNRY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(country.toLowerCase()), "predeparture page: s7");
        Assert.assertTrue($(byXpath(RESORT_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(resort.toLowerCase()), "predeparture page: s8");
        Assert.assertTrue($(byXpath(HOTEL_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(hotel.toLowerCase()), "predeparture page: s9");
        Assert.assertTrue($(byXpath(ADULTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(adults.toLowerCase()), "predeparture page: s10");

        if (!children.equals(""))
        {
            Assert.assertTrue($(byXpath(CHILDREN_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(children.toLowerCase()), "predeparture page: s11");
        }
        if (!infants.equals(""))
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(infants.toLowerCase()), "predeparture page: s12");
        }

        SimpleDateFormat formatter = new SimpleDateFormat("MM.dd.yyyy");
        Date date = formatter.parse($(byXpath(DEPARTURE_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase());
        formatter = new SimpleDateFormat("dd.MM.yyyy");
        Assert.assertTrue(formatter.format(date).equals(startDate.toLowerCase()), "predeparture page: s13");
        Assert.assertTrue($(byXpath(NIGHTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(nights.toLowerCase()), "predeparture page: s14");
        Assert.assertTrue($(byXpath(TOUR_OPERATOR_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(tourOperator.toLowerCase()), "predeparture page: s15");
        Assert.assertTrue($(byXpath(MEAL_TYPE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(mealType.toLowerCase()), "predeparture page: s16");
        Assert.assertTrue($(byXpath(TOUR_PRICE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().replaceAll("\\s", "").replaceAll("\\D*", "").equals(tourPrice.toLowerCase()), "predeparture page: s17");

        Assert.assertTrue($(byXpath(BOOKING_CODE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(bookingCode.toLowerCase()), "predeparture page: s18");
        formatter = new SimpleDateFormat("MM.dd.yyyy");
        date = formatter.parse($(byXpath(BOOKING_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll("\\s", "").toLowerCase());
        formatter = new SimpleDateFormat("dd.MM.yyyy");
        Assert.assertTrue(formatter.format(date).equals(bookingDate.toLowerCase()), "predeparture page: s19");

        return this;
    }
    public PredeparturePage checkTouristsData(String adults, String children, String infants, String lastName, String firstName, String nationality, String adultBirthday, String kidBirthday)
    {
        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }

        for (int i = 0; i < touristsCount; i++)
        {
            Assert.assertTrue($(byXpath(String.format(LAST_AND_FIRST_NAME, Integer.toString(i+1)))).innerHtml().toLowerCase().contains(lastName.toLowerCase() + " " + firstName.toLowerCase()));
            Assert.assertTrue($(byXpath(String.format(NATIONALITY, Integer.toString(i+1)))).innerHtml().toLowerCase().contains(nationality.toLowerCase()));
            if (i <= Integer.parseInt(adults))
            {
                Assert.assertTrue($(byXpath(String.format(BIRTHDAY, Integer.toString(i+1)))).innerHtml().toLowerCase().contains(adultBirthday.toLowerCase()));
            }
            else
            {
                Assert.assertTrue($(byXpath(String.format(BIRTHDAY, Integer.toString(i+1)))).innerHtml().toLowerCase().contains(kidBirthday.toLowerCase()));
            }

        }


        return this;
    }

    public PredeparturePage checkVisaData(String lastAndFirstName, String birthday, String nationality, String expireDate, String price)
    {
        Assert.assertTrue($(byXpath(VISA_LAST_AND_FIRST_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").toLowerCase().contains(lastAndFirstName.toLowerCase()), "predeparture page: s20");
        Assert.assertTrue($(byXpath(VISA_BIRTHDAY)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").toLowerCase().contains(birthday.toLowerCase()), "predeparture page: s21");
        Assert.assertTrue($(byXpath(VISA_NATIONALITY)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").toLowerCase().contains(nationality.toLowerCase()), "predeparture page: s22");
        Assert.assertTrue($(byXpath(VISA_EXPIRE_DATE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").toLowerCase().contains(expireDate.toLowerCase()), "predeparture page: s23");
        Assert.assertTrue($(byXpath(VISA_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").toLowerCase().replaceAll("\\s", "").contains(price.toLowerCase()), "predeparture page: s24");
        //Assert.assertTrue($(byXpath(VISA_TOTAL)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").toLowerCase().replaceAll("\\s", "").contains(OrderPage.visaPrice.toLowerCase()), "predeparture page: s25");

        return this;
    }

    public PredeparturePage addNewAction(String action)
    {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        DateFormat dateFormat2 = new SimpleDateFormat("MM.dd.yyyy");

        $(byXpath(ACTION_NEW_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(ACTION_LIST_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).selectOption(action);
        $(byXpath(ACTION_DATE)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(dateFormat.format(date)).pressEnter();

        nextActionDate = dateFormat.format(date);

        $(byXpath(ACTION_SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Добавление действия"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Добавление действия"), "OrderPAge: notification title 'Добавление действия'");
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Выполнено успешно"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Выполнено успешно"), "OrderPAge: notification message 'Выполнено успешно'");

        String s = dateFormat2.format(date);
        String s11 = $(byXpath(String.format(ACTION_DATE_VALUE, dateFormat2.format(date)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml();
        Assert.assertTrue($(byXpath(String.format(ACTION_NAME_VALUE, action))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(action.toLowerCase()));
        Assert.assertTrue($(byXpath(String.format(ACTION_DATE_VALUE, dateFormat2.format(date)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains(dateFormat2.format(date)));

        nextActionName = action;

        return this;
    }

    public PredeparturePage executeAction(String action)
    {
        $(byXpath(String.format(ACTION_EXECUTE_BUTTON, action))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Обновление действия"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Обновление действия"), "OrderPAge: notification title 'Обновление действия'");
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Действие помечено как выполненное"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Действие помечено как выполненное"), "OrderPAge: notification message 'Действие помечено как выполненное'");

        lastAction = action;

        return this;
    }

    public PredeparturePage selectDocumentsAreSentCheckbox()
    {
        $(byXpath(DOCUMENTS_ARE_SENT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Статус отправки документов"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Статус отправки документов"), "OrderPAge: notification title 'Статус отправки документов'");
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Сохранено успешно"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Сохранено успешно"), "OrderPAge: notification message 'Сохранено успешно'");
        documentsAreSent = "да";
        return this;
    }

}
