package pageobjects.TOM;

import com.codeborne.selenide.Selenide;

import com.codeborne.selenide.WebDriverRunner;
import framework.WebDriverCommands;
import org.testng.Assert;
import pageobjects.Travelata.CheckoutPage;
import pageobjects.Travelata.HotelPage;
import pageobjects.Travelata.MainPage;

import java.io.File;
import java.text.SimpleDateFormat;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static java.lang.Math.toIntExact;

/**
 * Created by User on 14.06.2017.
 */
public class SendingTouristsPage extends WebDriverCommands
{
    private static SendingTouristsPage sendingTouristsPage = null;
    private SendingTouristsPage()
    {
    }
    public static synchronized SendingTouristsPage getSendingTouristsPage()
    {
        if(sendingTouristsPage == null)
            sendingTouristsPage = new SendingTouristsPage();

        return sendingTouristsPage;
    }


    final private String FILTER_DATE_TO_INPUT = "//input[contains(@data-ng-model, 'dateTo')]";
    final private String FILTER_DATE_FROM_INPUT = "//input[contains(@data-ng-model, 'dateFrom')]";
    final private String FILTER_TOUR_OPERATOR = "//span[text() = 'Туроператор']";
    final private String FILTER_TOUR_OPERATOR_INPUT = "//input[@aria-label = 'Туроператор']";
    final private String FILTER_REMOVE_TO = "//a[contains(@aria-label, 'Туроператор')]/i";
    final private String FILTER_COUNTRY = "//span[text() = 'Страна']";
    final private String FILTER_COUNTRY_INPUT = "//input[@aria-label = 'Страна']";
    final private String FILTER_REMOVE_COUNTRY = "//a[contains(@aria-label, 'Страна')]/i";
    final private String FILTER_RESORT = "//span[text() = 'Курорт']";
    final private String FILTER_RESORT_INPUT = "//input[@aria-label = 'Курорт']";
    final private String FILTER_REMOVE_RESORT = "//a[contains(@aria-label, 'Курорт')]/i";
    final private String FILTER_SHOWCASE_DROPDOWN = "//select[contains(@ng-model, 'showCase')]";
    final private String FILTER_ID_INPUT = "//input[contains(@ng-model, 'id')]";
    final private String FILTER_URGENTLY_CHECKBOX = "//.[text() = 'Срочно']/preceding-sibling::input";
    final private String FILTER_SELLER = "//span[text() = 'Менеджер']";
    final private String FILTER_SELLER_INPUT = "//input[@aria-label = 'Менеджер']";
    final private String FILTER_SELLER_REMOVE_BUTTON = "//span[text() = 'Менеджер']/following-sibling::a/i";
    final private String FILTER_DOCUMENTS_ARE_NOT_SENT_CHECKBOX = "//input[contains(@data-ng-model, 'ticketsNotSend')]";
    final private String FILTER_FLIGHT_TIME_IS_NOT_CHECKED_CHECKBOX = "//input[contains(@data-ng-model, 'flightTimeNotChecked')]";
    final private String FILTER_VISA_REQUIRED_CHECKBOX = "//input[contains(@data-ng-model, 'visaRequired')]/following-sibling::span";
    final private String FILTER_VISA_SUPPORT_REQUIRED_CHECKBOX = "//input[contains(@data-ng-model, 'visaSupportRequired')]/following-sibling::span";

    final private String EMPTY_RESULT_BLOCK = "//.[contains(text(), 'Пока не найдено ни одной записи')]";
    final private String PAGINATION_BLOCK = "//div[@class = 'pagination-container']/ul";
    final private String PAGINATION_TOTAL_PAGES_BUTTONS = "//ul[contains(@ng-show, 'totalPages')]/li";
    final private String PAGINATION_LAST_PAGE_BUTTON = "//ul[contains(@ng-show, 'totalPages')]/li[last()-1]/a";
    final private String PAGINATION_NEXT_BUTTON = "//a[text() = 'Next']";
    final private String PAGINATION_FIRST_PAGE_BUTTON = "//a[text() = '1']";
    final private String PAGINATION_LAST_POSSIBLE_PAGE_BUTTON = "//.[text() = 'Next']/../preceding-sibling::li[1]/a";
    final private String ORDERS_LIST = "//tbody/tr";

    final private String FILTER_ORDER_START_DATE = "//tbody/tr[%s]/td[6]";
    final private String FILTER_ORDER_TOUR_OPERATOR = "//tbody/tr[%s]/td[5]";
    final private String FILTER_ORDER_COUNTRY = "//tbody/tr[%s]/td[12]";
    final private String FILTER_ORDER_RESORT = "//tbody/tr[%s]/td[13]";
    final private String FILTER_ORDER_SHOWCASE = "//tbody/tr[%s]/td[10]";
    final private String FILTER_ORDER_SELLER = "//tbody/tr[%s]/td[4]";

    final private String FILTER_ORDER_VISA = "//tbody/tr[1]/td[14]/span[1]";
    final private String FILTER_ORDER_VISA_SUPPORT = "//tbody/tr[1]/td[14]/span[2]";


    private final String EXPORT_DATA_BUTTON = "//div[contains(@data-ng-click, 'openLinkReportFile')]";



    public static String dateFrom = "01.06.2017";
    public static String dateTo = "22.06.2017";


    public SendingTouristsPage goToSendingTouristsPage()
    {
        Selenide.open("http://tom.travellata.ru/#/tourists/sending");

        return this;
    }

    public SendingTouristsPage exportData()
    {
        $(byXpath(EXPORT_DATA_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_5_SECONDS);

        File dir = new File("C:\\Downloads\\Reports\\");
        File[] dirContents = dir.listFiles();

        for(int i = 0; i < dirContents.length; i++)
        {

            if(dirContents[i].getName().matches("\\d{4}-\\d{2}-\\d{2}_\\d{2}-\\d{2}-\\d{2}_tourists_sending.csv"))
            {
                Assert.assertFalse(Integer.toString(toIntExact(dirContents[i].length())).equals("0"));
                dirContents[i].delete();
                break;
            }
        }

        return this;
    }


    public SendingTouristsPage enterDateFrom(String dateFrom)
    {
        $(byXpath(FILTER_DATE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(dateFrom).pressEnter();

        return this;
    }
    public SendingTouristsPage enterDateTo(String dateTo)
    {
        $(byXpath(FILTER_DATE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(dateTo).pressEnter();

        return this;
    }
    public SendingTouristsPage clickSearchButton()
    {
        return this;
    }
    public SendingTouristsPage selectTourOperator(String tourOperator)
    {
        $(byXpath(FILTER_TOUR_OPERATOR)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(FILTER_TOUR_OPERATOR_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(tourOperator).pressEnter();

        return this;
    }
    public SendingTouristsPage enterCountry(String country)
    {
        $(byXpath(FILTER_COUNTRY)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(FILTER_COUNTRY_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(country).pressEnter();

        return this;
    }
    public SendingTouristsPage enterResort(String resort)
    {
        $(byXpath(FILTER_RESORT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(FILTER_RESORT_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(resort).pressEnter();

        return this;
    }
    public SendingTouristsPage selectShowCase(String showCase)
    {
        $(byXpath(FILTER_SHOWCASE_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).selectOption(showCase);

        return this;
    }
    public SendingTouristsPage enterID(String id)
    {
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(id);

        return this;
    }
    public SendingTouristsPage enterSeller(String seller)
    {
        $(byXpath(FILTER_SELLER)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(FILTER_SELLER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(seller).pressEnter();

        return this;
    }
    public SendingTouristsPage clickVisa()
    {
        $(byXpath(FILTER_VISA_REQUIRED_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
    public SendingTouristsPage clickVisaSupport()
    {
        $(byXpath(FILTER_VISA_SUPPORT_REQUIRED_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public SendingTouristsPage checkFilters(String dateFrom, String dateTo, String tourOperator, String country, String resort, String showcase, String id, String seller) throws Exception
    {
        $(byXpath(FILTER_DATE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_DATE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        WebDriverRunner.getWebDriver().navigate().refresh();

        enterDateFrom(dateFrom);
        enterDateTo(dateTo);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("start date");
        $(byXpath(FILTER_DATE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_DATE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        selectTourOperator(tourOperator);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("tour operator");
        $(byXpath(FILTER_REMOVE_TO)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        enterCountry(country);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("country");
        $(byXpath(FILTER_REMOVE_COUNTRY)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        enterResort(resort);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("resort");
        $(byXpath(FILTER_REMOVE_RESORT)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        selectShowCase(showcase);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("showcase");
        selectShowCase("");

        enterID(id);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        Integer orderList = $$(byXpath(ORDERS_LIST)).size();
        Assert.assertTrue(orderList.equals(1), "more than 1 order");
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        WebDriverRunner.getWebDriver().navigate().refresh();

        enterSeller(seller);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("seller");
        $(byXpath(FILTER_SELLER_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        clickVisa();
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("visa");
        clickVisa();

        clickVisaSupport();
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("visa support");
        clickVisaSupport();

        return this;
    }
    public SendingTouristsPage checkSERPwithFilters(String filter) throws Exception
    {
        if($(byXpath(EMPTY_RESULT_BLOCK)).is(not(visible)))
        {
            if($(byXpath(PAGINATION_BLOCK)).is(visible))
            {
                Integer totalPages = $$(byXpath(PAGINATION_TOTAL_PAGES_BUTTONS)).size();
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

                if(totalPages.equals(9))
                {
                    Integer lastPage = Integer.parseInt($(byXpath(PAGINATION_LAST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml());
                    if (lastPage > 15)
                    {
                        lastPage = 15;
                    }
                    for(int i = 1; i < lastPage + 1; i++)
                    {
                        for(int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                        {
                            if(filter.equals("start date"))
                            {
                                Assert.assertTrue((formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                        (formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "start date");
                            }
                            if(filter.equals("tour operator"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_TOUR_OPERATOR, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()), "tour operator");
                            }
                            if(filter.equals("country"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_COUNTRY, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()), "country");
                            }
                            if(filter.equals("resort"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_RESORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()), "resort");
                            }
                            if(filter.equals("showcase"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SHOWCASE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("travelata"), "showcase");
                            }

                            if(filter.equals("seller"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SELLER, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Северина"), "seller");
                            }
                            if(filter.equals("visa"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa");
                            }
                            if(filter.equals("visa support"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA_SUPPORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa support");
                            }



                        }
                        $(byXpath(PAGINATION_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        Selenide.sleep(CONSTANT_1_SECOND);
                    }

                    $(byXpath(PAGINATION_FIRST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
                else
                {
                    for(int i = 1; i < Integer.parseInt($(byXpath(PAGINATION_LAST_POSSIBLE_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()) + 1; i++)
                    {
                        for(int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                        {
                            if(filter.equals("start date"))
                            {
                                Assert.assertTrue((formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                        (formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "start date");
                            }
                            if(filter.equals("tour operator"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_TOUR_OPERATOR, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()),"tour operator");
                            }
                            if(filter.equals("country"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_COUNTRY, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()),"country");
                            }
                            if(filter.equals("resort"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_RESORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()), "resort");
                            }
                            if(filter.equals("showcase"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SHOWCASE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("travelata"), "showcase");
                            }

                            if(filter.equals("seller"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SELLER, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Северина"), "seller");
                            }
                            if(filter.equals("visa"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa");
                            }
                            if(filter.equals("visa support"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA_SUPPORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa support");
                            }

                        }
                        $(byXpath(PAGINATION_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        Selenide.sleep(CONSTANT_1_SECOND);
                    }
                    $(byXpath(PAGINATION_FIRST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }
            else
            {
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

                for(int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                {
                    if(filter.equals("start date"))
                    {
                        Assert.assertTrue((formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                (formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "start date");
                    }
                    if(filter.equals("tour operator"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_TOUR_OPERATOR, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()), "tour operator");
                    }
                    if(filter.equals("country"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_COUNTRY, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()), "country");
                    }
                    if(filter.equals("resort"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_RESORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()), "resort");
                    }
                    if(filter.equals("showcase"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SHOWCASE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("travelata"), "showcase");
                    }

                    if(filter.equals("seller"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SELLER, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Северина"),"seller");
                    }
                    if(filter.equals("visa"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa");
                    }
                    if(filter.equals("visa support"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA_SUPPORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa support");
                    }

                }
            }
        }
        else
        {
            System.console().printf("ничего не найдено");
        }
        return this;
    }


    final private String FILTER_SEARCH_BUTTON = "//.[contains(text(), 'Найти')]";

    final private String ORDER_ID_VALUE = "//.[text() = '%s']";
    final private String BOOKING_ID_VALUE = "//.[text() = '%s']/../following-sibling::td[1]";
    final private String CUSTOMER_NAME_VALUE = "//.[text() = '%s']/../following-sibling::td[2]";
    final private String SELLER_NAME_VALUE = "//.[text() = '%s']/../following-sibling::td[3]";
    final private String TOUR_OPERATOR_VALUE = "//.[text() = '%s']/../following-sibling::td[4]";
    final private String DATE_FROM_VALUE = "//.[text() = '%s']/../following-sibling::td[5]";
    final private String DATE_TO_VALUE = "//.[text() = '%s']/../following-sibling::td[6]";
    final private String NIGHTS_VALUE = "//.[text() = '%s']/../following-sibling::td[7]";
    final private String TYPE_VALUE = "//.[text() = '%s']/../following-sibling::td[8]/span";
    final private String SHOWCASE_VALUE = "//.[text() = '%s']/../following-sibling::td[9]";
    final private String DEPARTURE_DAYS_VALUE = "//.[text() = '%s']/../following-sibling::td[10]";
    final private String COUNTRY_VALUE = "//.[text() = '%s']/../following-sibling::td[11]";
    final private String RESORT_VALUE = "//.[text() = '%s']/../following-sibling::td[12]";
    final private String VISA_VALUE = "//.[text() = '%s']/../following-sibling::td[13]/span";
    final private String CONCRETIZATION_VALUE = "//.[text() = '%s']/../following-sibling::td[14]";
    final private String DOCUMENTS_ARE_SENT_VALUE = "//.[text() = '%s']/../following-sibling::td[15]/div/label";
    final private String DEPARTURE_DATE_IS_CHECKED_VALUE = "//.[text() = '%s']/../following-sibling::td[16]";
    final private String NEXT_VISA_ACTION_VALUE = "//.[text() = '%s']/../following-sibling::td[17]";
    final private String VISA_DO_UNTIL_VALUE = "//.[text() = '%s']/../following-sibling::td[18]";
    final private String LAST_VISA_ACTION_VALUE = "//.[text() = '%s']/../following-sibling::td[19]";
    final private String DONE_VALUE = "//.[text() = '%s']/../following-sibling::td[20]";



    public SendingTouristsPage checkOrder(String orderID) throws Exception
    {
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(orderID);

        $(byXpath(FILTER_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        Assert.assertTrue($(byXpath(String.format(BOOKING_ID_VALUE, orderID))).innerHtml().equals(OrderPage.bookingCode));
        Assert.assertTrue($(byXpath(String.format(CUSTOMER_NAME_VALUE, orderID))).innerHtml().toLowerCase().equals(CheckoutPage.firstNameValue.toLowerCase() + " " + CheckoutPage.lastNameValue.toLowerCase()));
        Assert.assertTrue($(byXpath(String.format(SELLER_NAME_VALUE, orderID))).innerHtml().equals("Ольга Северина"));
        Assert.assertTrue($(byXpath(String.format(TOUR_OPERATOR_VALUE, orderID))).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()));
        Assert.assertTrue($(byXpath(String.format(DATE_FROM_VALUE, orderID))).innerHtml().equals(OrderPage.tourStartDate));
        Assert.assertTrue($(byXpath(String.format(DATE_TO_VALUE, orderID))).innerHtml().equals(getFullDateFrom(OrderPage.tourStartDate, Integer.parseInt(HotelPage.nightsValue))));
        Assert.assertTrue($(byXpath(String.format(NIGHTS_VALUE, orderID))).innerHtml().equals(HotelPage.nightsValue));
        Assert.assertTrue($(byXpath(String.format(TYPE_VALUE, orderID))).innerHtml().toLowerCase().equals("тур"));
        Assert.assertTrue($(byXpath(String.format(SHOWCASE_VALUE, orderID))).innerHtml().toLowerCase().equals("travelata"));
        //Assert.assertTrue($(byXpath(String.format(DEPARTURE_DAYS_VALUE, orderID))).innerHtml().equals("test_booking_code"));
        Assert.assertTrue($(byXpath(String.format(COUNTRY_VALUE, orderID))).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()));
        Assert.assertTrue($(byXpath(String.format(RESORT_VALUE, orderID))).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()));
        Assert.assertTrue($(byXpath(String.format(VISA_VALUE, orderID))).innerHtml().toLowerCase().contains(OrderPage.visa));
        //Assert.assertTrue($(byXpath(String.format(CONCRETIZATION_VALUE, orderID))).innerHtml().equals("test_booking_code"));
        Assert.assertTrue($(byXpath(String.format(DOCUMENTS_ARE_SENT_VALUE, orderID))).innerHtml().toLowerCase().contains(PredeparturePage.documentsAreSent.toLowerCase()));
        //Assert.assertTrue($(byXpath(String.format(DEPARTURE_DATE_IS_CHECKED_VALUE, orderID))).innerHtml().equals("test_booking_code"));
        if(!PredeparturePage.nextActionName.equals(""))
        {
            Assert.assertTrue($(byXpath(String.format(NEXT_VISA_ACTION_VALUE, orderID))).innerHtml().toLowerCase().equals(PredeparturePage.nextActionName.toLowerCase()));
            Assert.assertTrue($(byXpath(String.format(VISA_DO_UNTIL_VALUE, orderID))).innerHtml().toLowerCase().contains(PredeparturePage.nextActionDate.toLowerCase()));
        }
        if(!PredeparturePage.lastAction.equals(""))
        {
            Assert.assertTrue($(byXpath(String.format(LAST_VISA_ACTION_VALUE, orderID))).innerHtml().toLowerCase().equals(PredeparturePage.lastAction.toLowerCase()));
        }
        //Assert.assertTrue($(byXpath(String.format(DONE_VALUE, orderID))).innerHtml().equals("test_booking_code"));



        return this;
    }

    public SendingTouristsPage selectOrder(String orderID)
    {
        $(byXpath(FILTER_DATE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_DATE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(orderID);
        $(byXpath(FILTER_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(String.format(ORDER_ID_VALUE, orderID))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

}
