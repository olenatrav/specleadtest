package pageobjects.TOM;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import framework.WebDriverCommands;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;

import java.io.File;
import java.text.SimpleDateFormat;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static java.lang.Math.toIntExact;

/**
 * Created by User on 09.06.2017.
 */
public class FinancesPage extends WebDriverCommands
{
    private static FinancesPage financesPage = null;
    private FinancesPage()
    {
    }
    public static synchronized FinancesPage getFinancesPage()
    {
        if(financesPage == null)
            financesPage = new FinancesPage();

        return financesPage;
    }
    final private String NOTIFICATION_TITLE = "//div[@aria-label = '%s']";
    final private String NOTIFICATION_MESSAGE = "//div[@aria-label = '%s']";

    final private String FILTER_ID_INPUT = "//input[@placeholder = 'ID']";
    final private String FILTER_SEARCH_BUTTON = "//.[contains(text() , 'Найти')]";
    final private String FILTER_SOLD_DATE_RANGE_FROM_INPUT = "//input[contains(@data-ng-model, 'soldDateRange.from')]";
    final private String FILTER_SOLD_DATE_RANGE_TO_INPUT = "//input[contains(@data-ng-model, 'soldDateRange.to')]";
    final private String FILTER_STATUS_DROPDOWN = "//span[contains(@aria-label, 'Добавьте статус')]/span";
    final private String FILTER_STATUS_VALUE = "//span[text() = '%s']";
    final private String STATUS_LABEL = "//.[contains(@data-ng-repeat, 'statuses')]/./.[contains(text(), '%s')]";
    final private String REMOVE_STATUS_BUTTON = "//.[contains(@data-ng-repeat, 'statuses')]/./.[contains(text(), '%s')]/div/i";
    final private String FILTER_FIRST_PAYMENT_DATE_INPUT = "//input[contains(@data-ng-model, 'firstPayment')]";
    final private String FILTER_LAST_PAYMENT_DATE_INPUT = "//input[contains(@data-ng-model, 'lastPayment')]";
    final private String FILTER_CUSTOMER_NOT_PAID_CHECKBOX = "//input[contains(@data-ng-model, 'customerNotPaid')]/following-sibling::span";
    final private String FILTER_NOT_PAID_TO_TO_CHECKBOX = "//input[contains(@data-ng-model, 'notPaidToTO')]/following-sibling::span";

    final private String ORDER_ID_VALUE = "//.[text() = '%s']";
    final private String ORDER_BOOKING_ID_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'bookingId')]";
    final private String ORDER_CUSTOMER_NAME_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'customerName')]";
    final private String ORDER_DATE_FROM_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'from')]";
    final private String ORDER_DATE_TO_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'to|dateToString')]";
    final private String ORDER_TO_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'tourOperator')]";
    final private String ORDER_COUNTRY_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'country')]";
    final private String ORDER_SOLD_DATE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'soldDate')]";
    final private String ORDER_PRICE_FOR_CUSTOMER_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'priceForCustomer')]";
    final private String ORDER_CUSTOMER_PAID_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'customerPaid')]";
    final private String ORDER_NEED_PAY_TO_TO_VALUE = "//.[text() = '%s']/../following-sibling::td/a[contains(@ng-bind, 'needToPayForTourOperator')]";
    final private String ORDER_EDIT_AMOUNT_INPUT = "//.[@name = 'editAmountForm']/input";
    final private String ORDER_SUBMIT_CHANGE_BUTTON = "//button[contains(text(), 'Изменить')]";

    final private String ORDER_PAID_TO_TO_VALUE = "//.[text() = '%s']/../following-sibling::td/a[contains(@ng-bind, 'paidToTO')]";
    final private String ORDER_AMOUNT = "//.[@name = 'amount']";
    final private String ORDER_TO_DROPDOWN = "//span[text() = 'Туроператор']";
    final private String ORDER_TO = "//.[text() = '%s']";
    final private String ORDER_SAVE_TRANSACTION_BUTTON = "//.[contains(text(), 'Сохранить транзакцию')]";
    final private String TRANSACTION_VALUE = "//.[contains(@data-ng-repeat, 'transactions')]/td[2]";
    final private String OPERATION_TYPE = "//.[contains(@data-ng-repeat, 'transactions')]/td[3]";
    final private String TOUR_OPERATOR = "//.[contains(@data-ng-repeat, 'transactions')]/td[4]";
    final private String SELLER = "//.[contains(@data-ng-repeat, 'transactions')]/td[5]";
    final private String TRANSACTION_DATE = "//.[contains(@data-ng-repeat, 'transactions')]/td[6]";
    final private String CLOSE_BUTTON = "//.[contains(text(), 'Закрыть')]";

    final private String ORDER_EFFECTIVE_MARGIN_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'effectiveMargin')]";
    final private String ORDER_EFFECTIVE_MARGIN_PERCENTAGE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'effectiveMarginPercentage')]";
    final private String ORDER_ADDITIONAL_INSURANCE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'additionalInsurance')]";
    final private String ORDER_PRODUCT_TYPE_VALUE = "//.[text() = '%s']/../following-sibling::td/span[contains(@data-ng-if, 'productType')]";
    final private String ORDER_SHOW_CASE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'showCase')]";
    final private String ORDER_FULL_PRICE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'fullPrice')]";
    final private String ORDER_COMMISSION_TO_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'commissionTO')]";
    final private String ORDER_SELLER_NAME_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'sellerName')]";
    final private String ORDER_STATUS_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'states')]";
    final private String ORDER_SUM_OF_DISCOUNTS_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'sumOfDiscounts')]";
    final private String ORDER_COUNT_OF_DISCOUNTS_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'countOfDiscounts')]";
    final private String ORDER_COUNT_OF_DISCOUNTS_AFFECTED_BONUS_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'countOfDiscountsAffectedBonus')]";
    final private String ORDER_COMMISSION_WITHOUT_ACCESSED_DISCOUNTS_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'commissionWithoutAccessedDiscounts')]";
    final private String ORDER_FIRST_PAYMENT_DATE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'firstPaymentDate')]";
    final private String ORDER_LAST_PAYMENT_DATE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'lastPaymentDate')]";
    final private String ORDER_CREATED_DATE_VALUE = "//.[text() = '%s']/../following-sibling::td[contains(@ng-bind, 'createdDate')]";





    final private String EMPTY_RESULT_BLOCK = "//.[contains(text(), 'Пока не найдено ни одной записи')]";
    final private String PAGINATION_BLOCK = "//div[@class = 'pagination-container']/ul";
    final private String PAGINATION_TOTAL_PAGES_BUTTONS = "//ul[contains(@ng-show, 'totalPages')]/li";
    final private String PAGINATION_LAST_PAGE_BUTTON = "//ul[contains(@ng-show, 'totalPages')]/li[last()-1]/a";
    final private String PAGINATION_NEXT_BUTTON = "//a[text() = 'Next']";
    final private String PAGINATION_FIRST_PAGE_BUTTON = "//a[text() = '1']";
    final private String PAGINATION_LAST_POSSIBLE_PAGE_BUTTON = "//.[text() = 'Next']/../preceding-sibling::li[1]/a";
    final private String ORDERS_LIST = "//tbody/tr";


    final private String FILTER_ORDER_STATUS = "//tbody/tr[%s]/td[21]";
    final private String FILTER_ORDER_FIRST_PAYMENT = "//tbody/tr[%s]/td[26]";
    final private String FILTER_ORDER_LAST_PAYMENT = "//tbody/tr[%s]/td[27]";
    final private String FILTER_ORDER_SOLD_DATE = "//tbody/tr[%s]/td[8]";

    final private String FILTER_ORDER_CUSTOMER_PAYED = "//tbody/tr[%s]/td[10]";
    final private String FILTER_ORDER_PAYED_TO_TO = "//tbody/tr[%s]/td[12]/a";


    private final String EXPORT_DATA_BUTTON = "//div[contains(@data-ng-click, 'openLinkReportFile')]";


public FinancesPage goToFnancesPage()
{
    Selenide.open("http://tom.travellata.ru/#/payments");

    return this;
}

    public FinancesPage exportData()
    {
        $(byXpath(EXPORT_DATA_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_5_SECONDS);
        File file = new File("C:\\Downloads\\Reports\\financial-report.csv");
        Assert.assertTrue(file.isFile());
        Assert.assertFalse(Integer.toString(toIntExact(file.length())).equals("0"));
        file.delete();

        return this;
    }

    public FinancesPage enterID(String order)
    {
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(order);

        return this;
    }
    public FinancesPage clickSearchButton()
    {
        $(byXpath(FILTER_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
    public FinancesPage checkSoldOrder(String order, String bookingCode, String firstName, String lastName, String dateFrom, String dateTo, String tourOperator, String country, String soldDate, String totalPrice,
                                       String needPayToTourOperator, String paidToTourOperator, String orderType, String showcase, String seller, String orderStatus, String createdDate)
    {
        enterID(order);
        clickSearchButton();

        Assert.assertTrue($(byXpath(String.format(ORDER_BOOKING_ID_VALUE, order))).innerHtml().toLowerCase().equals(bookingCode), "Fin page: 1");
        Assert.assertTrue($(byXpath(String.format(ORDER_CUSTOMER_NAME_VALUE, order))).innerHtml().toLowerCase().equals(firstName.toLowerCase() + "  " + lastName.toLowerCase()));
        Assert.assertTrue($(byXpath(String.format(ORDER_CUSTOMER_NAME_VALUE, order))).innerHtml().toLowerCase().equals(firstName.toLowerCase() + "  " + lastName.toLowerCase()), "Fin page: 2");
        Assert.assertTrue($(byXpath(String.format(ORDER_DATE_FROM_VALUE, order))).innerHtml().toLowerCase().equals(dateFrom.toLowerCase()), "Fin page: 3");
        Assert.assertTrue($(byXpath(String.format(ORDER_DATE_TO_VALUE, order))).innerHtml().toLowerCase().equals(dateTo.toLowerCase()), "Fin page: 4");
        Assert.assertTrue($(byXpath(String.format(ORDER_TO_VALUE, order))).innerHtml().toLowerCase().equals(tourOperator.toLowerCase()), "Fin page: 5");
        Assert.assertTrue($(byXpath(String.format(ORDER_COUNTRY_VALUE, order))).innerHtml().toLowerCase().equals(country), "Fin page: 6");
        Assert.assertTrue($(byXpath(String.format(ORDER_SOLD_DATE_VALUE, order))).innerHtml().equals(soldDate), "Fin page: 7");
        Assert.assertTrue($(byXpath(String.format(ORDER_PRICE_FOR_CUSTOMER_VALUE, order))).innerHtml().replaceAll(",.*", "").replaceAll("\\s", "").equals(totalPrice), "Fin page: 8");
        //Assert.assertTrue($(byXpath(String.format(ORDER_CUSTOMER_PAID_VALUE, order))).innerHtml().toLowerCase().equals(""));

        $(byXpath(String.format(ORDER_NEED_PAY_TO_TO_VALUE, order))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(ORDER_EDIT_AMOUNT_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(needPayToTourOperator);
        $(byXpath(ORDER_SUBMIT_CHANGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();



        $(byXpath(String.format(ORDER_PAID_TO_TO_VALUE, order))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(ORDER_AMOUNT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(paidToTourOperator);
        $(byXpath(ORDER_TO_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath("//input[@aria-label = 'Туроператор']")).setValue(tourOperator);
        $(byXpath("//input[@aria-label = 'Туроператор']")).pressEnter();
        $(byXpath(ORDER_SAVE_TRANSACTION_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Добавление транзакции"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Добавление транзакции"), "FIn PAge: notification title 'Добавление транзакции'");
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Выполнено успешно"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Выполнено успешно"), "Fin PAge: notification message 'Выполнено успешносохранены'");

        Assert.assertTrue($(byXpath(TRANSACTION_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(paidToTourOperator));
        Assert.assertTrue($(byXpath(OPERATION_TYPE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Оплата"));
        Assert.assertTrue($(byXpath(TOUR_OPERATOR)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(tourOperator));
        Assert.assertTrue($(byXpath(SELLER)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Северина  Ольга"));
        //Assert.assertTrue($(byXpath(TRANSACTION_DATE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(OrderPage.createdDate));
        $(byXpath(CLOSE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        WebDriverRunner.getWebDriver().navigate().refresh();

        Assert.assertTrue($(byXpath(String.format(ORDER_NEED_PAY_TO_TO_VALUE, order))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(needPayToTourOperator));
        Assert.assertTrue($(byXpath(String.format(ORDER_PAID_TO_TO_VALUE, order))).innerHtml().toLowerCase().equals(paidToTourOperator));

        //Assert.assertTrue($(byXpath(String.format(ORDER_EFFECTIVE_MARGIN_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_EFFECTIVE_MARGIN_PERCENTAGE_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_ADDITIONAL_INSURANCE_VALUE, order))).innerHtml().toLowerCase().equals(""));
        Assert.assertTrue($(byXpath(String.format(ORDER_PRODUCT_TYPE_VALUE, order))).innerHtml().toLowerCase().equals(orderType.toLowerCase()), "Fin page: 9");
        Assert.assertTrue($(byXpath(String.format(ORDER_SHOW_CASE_VALUE, order))).innerHtml().toLowerCase().equals(showcase.toLowerCase()), "Fin page: 10");
        Assert.assertTrue($(byXpath(String.format(ORDER_FULL_PRICE_VALUE, order))).innerHtml().replaceAll(",.*", "").replaceAll(" ", "").equals(totalPrice), "Fin page: 11");
        //Assert.assertTrue($(byXpath(String.format(ORDER_COMMISSION_TO_VALUE, order))).innerHtml().toLowerCase().equals("travelata"));
        Assert.assertTrue($(byXpath(String.format(ORDER_SELLER_NAME_VALUE, order))).innerHtml().toLowerCase().equals(seller.toLowerCase()), "Fin page: 12");
        Assert.assertTrue($(byXpath(String.format(ORDER_STATUS_VALUE, order))).innerHtml().toLowerCase().equals(orderStatus.toLowerCase()), "Fin page: 13");
        //Assert.assertTrue($(byXpath(String.format(ORDER_SUM_OF_DISCOUNTS_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_COUNT_OF_DISCOUNTS_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_COUNT_OF_DISCOUNTS_AFFECTED_BONUS_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_COMMISSION_WITHOUT_ACCESSED_DISCOUNTS_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_FIRST_PAYMENT_DATE_VALUE, order))).innerHtml().toLowerCase().equals(""));
        //Assert.assertTrue($(byXpath(String.format(ORDER_LAST_PAYMENT_DATE_VALUE, order))).innerHtml().toLowerCase().equals(""));
        Assert.assertTrue($(byXpath(String.format(ORDER_CREATED_DATE_VALUE, order))).innerHtml().equals(createdDate), "Fin page: 14");


        return this;
    }

    public FinancesPage enterDateFrom(String dateFrom)
    {
        $(byXpath(FILTER_SOLD_DATE_RANGE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_SOLD_DATE_RANGE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(dateFrom);
        $(byXpath(FILTER_SOLD_DATE_RANGE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).pressEnter();

        return this;
    }
    public FinancesPage enterDateTo(String dateTo)
    {
        $(byXpath(FILTER_SOLD_DATE_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_SOLD_DATE_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(dateTo);
        $(byXpath(FILTER_SOLD_DATE_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).pressEnter();

        return this;
    }
    public FinancesPage selectStatus(String status)
    {
        $(byXpath(FILTER_STATUS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(FILTER_STATUS_VALUE, status))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(STATUS_LABEL, status))).waitUntil(visible, CONSTANT_10_SECONDS);

        return this;
    }

    public FinancesPage enterFirstPaymentDate(String date)
    {
        $(byXpath(FILTER_FIRST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(date);
        $(byXpath(FILTER_FIRST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).pressEnter();

        return this;
    }
    public FinancesPage enterLastPaymentDate(String date)
    {
        $(byXpath(FILTER_LAST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(date);
        $(byXpath(FILTER_LAST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).pressEnter();

        return this;
    }
    public FinancesPage clickCustomerNotPaidCheckBox()
    {
        $(byXpath(FILTER_CUSTOMER_NOT_PAID_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
    public FinancesPage clickNotPaidToTOCheckBox()
    {
        $(byXpath(FILTER_NOT_PAID_TO_TO_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }


    public FinancesPage checkFilters(String dateFrom, String dateTo, String status, String id, String firstPaymentDate, String lastPaymentDate) throws Exception
    {
        $(byXpath(FILTER_SOLD_DATE_RANGE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_SOLD_DATE_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_FIRST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_LAST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        enterDateFrom(dateFrom);
        enterDateTo(dateTo);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("sold date");
        $(byXpath(FILTER_SOLD_DATE_RANGE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(FILTER_SOLD_DATE_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        selectStatus(status);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("status");
        $(byXpath(String.format(REMOVE_STATUS_BUTTON, "Продано"))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Assert.assertTrue($(byXpath(String.format(STATUS_LABEL, "Продано"))).is(not(visible)));

        enterID(id);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        Integer orderList = $$(byXpath(ORDERS_LIST)).size();
        Assert.assertTrue(orderList.equals(1), "more than 1 order");
        $(byXpath(FILTER_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        enterFirstPaymentDate(firstPaymentDate);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("first payment");
        $(byXpath(FILTER_FIRST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        enterLastPaymentDate(lastPaymentDate);
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("last payment");
        $(byXpath(FILTER_LAST_PAYMENT_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();

        clickCustomerNotPaidCheckBox();
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("customer not payed");
        clickCustomerNotPaidCheckBox();

        clickNotPaidToTOCheckBox();
        clickSearchButton();
        Selenide.sleep(CONSTANT_3_SECONDS);
        checkSERPwithFilters("not paid to TO");
        clickNotPaidToTOCheckBox();

        return this;
    }


    public FinancesPage checkSERPwithFilters(String filter) throws Exception
    {
        if ($(byXpath(EMPTY_RESULT_BLOCK)).is(not(visible)))
        {
            if ($(byXpath(PAGINATION_BLOCK)).is(visible))
            {
                Integer totalPages = $$(byXpath(PAGINATION_TOTAL_PAGES_BUTTONS)).size();
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

                if (totalPages.equals(9))
                {
                    Integer lastPage = Integer.parseInt($(byXpath(PAGINATION_LAST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml());
                    if (lastPage > 15)
                    {
                        lastPage = 15;
                    }
                    for (int i = 1; i < lastPage + 1; i++)
                    {
                        for (int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                        {
                            if (filter.equals("status"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_STATUS, y))).innerHtml().toLowerCase().equals("Продано".toLowerCase()), "status");
                            }
                            if (filter.equals("sold date"))
                            {
                                Assert.assertTrue((formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                        formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                        (formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                        formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "sold date");
                            }
                            if (filter.equals("first payment"))
                            {
                                Assert.assertTrue(formatter1.parse($(byXpath(String.format(FILTER_ORDER_FIRST_PAYMENT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom)), "first payment");
                            }
                            if (filter.equals("last payment"))
                            {
                                Assert.assertTrue(formatter1.parse($(byXpath(String.format(FILTER_ORDER_LAST_PAYMENT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom)), "last payment");
                            }
                            if (filter.equals("customer not payed"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_CUSTOMER_PAYED, Integer.toString(y)))).innerHtml().toLowerCase().equals("0,00"), "customer payed");
                            }
                            if (filter.equals("not payed to TO"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_PAYED_TO_TO, Integer.toString(y)))).innerHtml().toLowerCase().equals("0,00"), "TO payed");
                            }
                        }
                        $(byXpath(PAGINATION_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        Selenide.sleep(CONSTANT_1_SECOND);
                    }

                    $(byXpath(PAGINATION_FIRST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
                else
                {
                    for (int i = 1; i < Integer.parseInt($(byXpath(PAGINATION_LAST_POSSIBLE_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()) + 1; i++)
                    {
                        for (int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                        {
                            if (filter.equals("status"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_STATUS, y))).innerHtml().toLowerCase().equals("Продано".toLowerCase()), "status");
                            }
                            if (filter.equals("sold date"))
                            {
                                Assert.assertTrue((formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                        formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                        (formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                        formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "sold date");
                            }
                            if (filter.equals("first payment"))
                            {
                                Assert.assertTrue(formatter1.parse($(byXpath(String.format(FILTER_ORDER_FIRST_PAYMENT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom)), "first payment");
                            }
                            if (filter.equals("last payment"))
                            {
                                Assert.assertTrue(formatter1.parse($(byXpath(String.format(FILTER_ORDER_LAST_PAYMENT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom)), "last payment");
                            }
                            if (filter.equals("customer not payed"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_CUSTOMER_PAYED, Integer.toString(y)))).innerHtml().toLowerCase().equals("0,00"), "payed");
                            }
                            if (filter.equals("not payed to TO"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_PAYED_TO_TO, Integer.toString(y)))).innerHtml().toLowerCase().equals("0,00"), "TO payed");
                            }
                        }

                    }
                    $(byXpath(PAGINATION_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                    Selenide.sleep(CONSTANT_1_SECOND);
                }
                $(byXpath(PAGINATION_FIRST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
            else
            {
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

                for (int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                {
                    if (filter.equals("status"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_STATUS, y))).innerHtml().toLowerCase().equals("Продано".toLowerCase()), "status");
                    }
                    if (filter.equals("sold date"))
                    {
                        Assert.assertTrue((formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                            formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                            (formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                            formatter1.parse($(byXpath(String.format(FILTER_ORDER_SOLD_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "sold date");
                    }
                    if (filter.equals("first payment"))
                    {
                        Assert.assertTrue(formatter1.parse($(byXpath(String.format(FILTER_ORDER_FIRST_PAYMENT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom)), "first payment");
                    }
                    if (filter.equals("last payment"))
                    {
                        Assert.assertTrue(formatter1.parse($(byXpath(String.format(FILTER_ORDER_LAST_PAYMENT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom)), "last payment");
                    }
                    if (filter.equals("customer not payed"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_CUSTOMER_PAYED, Integer.toString(y)))).innerHtml().toLowerCase().equals("0,00"), "payed");
                    }
                    if (filter.equals("not payed to TO"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_PAYED_TO_TO, Integer.toString(y)))).innerHtml().toLowerCase().equals("0,00"), "TO payed");
                    }
                }
            }
        }
        else
        {
            System.console().printf("ничего не найдено");
        }
        return this;
    }

}
