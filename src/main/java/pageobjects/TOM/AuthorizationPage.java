package pageobjects.TOM;


import com.codeborne.selenide.Selenide;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 18.05.2017.
 */
public class AuthorizationPage extends WebDriverCommands
{
    private static AuthorizationPage authorizationPage = null;
    private AuthorizationPage() throws  Exception

    {
    }
    public static synchronized AuthorizationPage getAuthorizationPage() throws  Exception
    {

        if(authorizationPage == null)
        {
            authorizationPage = new AuthorizationPage();
        }


        return authorizationPage;
    }




    final private String EMAIL_INPUT = "email";
        final private String PASSWORD_INPUT = "password";
        final private String SUBMIT_BUTTON = "//button";


    public  AuthorizationPage enterEmail(String email)
    {
        $(byName(EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(email);

        return this;
    }
    public  AuthorizationPage enterPassword(String password)
    {
        $(byName(PASSWORD_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(password);

        return this;
    }
    public AuthorizationPage clickSubmitButton()
    {
        $(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
    //    $(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public AuthorizationPage enterAccountCredentials(String email, String password)
    {

        enterEmail(email);
        enterPassword(password);
        clickSubmitButton();

        Selenide.sleep(CONSTANT_3_SECONDS);

        return this;
    }

    public AuthorizationPage goToAuthorizationPage()
    {
        Selenide.open("http://tom.travellata.ru/login");

        return this;
    }


}
