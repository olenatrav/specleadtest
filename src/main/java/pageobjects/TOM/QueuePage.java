package pageobjects.TOM;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;

import framework.WebDriverCommands;
import pageobjects.MailForSpamLetterPage;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static pageobjects.MailForSpamLetterPage.orderNumber;

/**
 * Created by User on 29.05.2017.
 */
public class QueuePage extends WebDriverCommands
{
    private static QueuePage queuePage = null;
    private QueuePage()
    {
    }
    public static synchronized QueuePage getQueuePage()
    {
        if(queuePage == null)
            queuePage = new QueuePage();

        return queuePage;
    }



    private final String LAST_ORDER = "//tr[contains(@class, 'ng-scope')][last()]";
    private final String ORDER = "(//tr[contains(@class, 'ng-scope')])[%s]";
    private final String ASSIGN_ORDER_BUTTON = "/td/a[@class = 'ng-scope']";

    private final String CERTAIN_ORDER = "//a[contains(text(), '%s')]";
    private final String ASSIGN_CERTAIN_ORDER_BUTTON = "/../following-sibling::td/a[@class = 'ng-scope']";

    private final String SELLERS_DROPDOWN = "//span[text() = 'Менеджер']";
    private final String SELLER_NAME_VALUE = "//span[text() = 'Северина Ольга']";
    private final String SELLER_BOT_NAME_VALUE = "//span[text() =  '1 Seller']";
    private final String LAST_ORDER_BUTTON = "/td/a";
    private final String ORDER_NUMBER_IN_TABLE_LINE = "//tr[%s]/td[1]/a[contains(@data-ng-click, 'app_order_edit')]";
    private final String ASSING_MANAGER_IN_TABLE_LINE = "//tr[%s]/td[13]/a[contains(@data-ng-if, 'sellerSelected')]";




    public QueuePage assignAndRedirectToLastOrder()
    {
        $(byXpath(LAST_ORDER + ASSIGN_ORDER_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(SELLERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(SELLER_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(LAST_ORDER + LAST_ORDER_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public QueuePage assignOrder(int i)
    {

        $(byXpath(String.format(ORDER, i) + ASSIGN_ORDER_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(SELLERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath("html/body/div[5]/input[1]")).waitUntil(visible, CONSTANT_10_SECONDS).setValue("1")
                .pressEnter();

        WebDriverRunner.getWebDriver().navigate().refresh();
        Selenide.sleep(7000);




        //Selenide.sleep(2000);
        /*if($(byXpath("html/body/div[5]/input[1]")).is(not(visible)) || $(byXpath("html/body/div[5]/input[1]")).is(not(exist)))
        {
            WebDriverRunner.getWebDriver().navigate().refresh();
            Selenide.sleep(3000);
            ii = 0;

        }
        else
        {

            if($(byXpath("html/body/div[5]/input[1]")).is(not(visible)) || $(byXpath("html/body/div[5]/input[1]")).is(not(exist)))
            {
                WebDriverRunner.getWebDriver().navigate().refresh();
                Selenide.sleep(3000);
                ii = 0;

            }

                $(byXpath("html/body/div[5]/input[1]")).waitUntil(visible, CONSTANT_10_SECONDS).setValue("1")
                        .pressEnter();


        }*/




        return this;
    }


    public QueuePage assignAndRedirectToCertainOrder()
    {
        $(byXpath(String.format(CERTAIN_ORDER, OrderPage.orderIDValue) + ASSIGN_CERTAIN_ORDER_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(SELLERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(SELLER_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(CERTAIN_ORDER, OrderPage.orderIDValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public OrderPage assignToSellerAndOpenOrder(String orderNumberValue, String managerNameValue) throws Exception{
        int i = 1;
        while($(byXpath(String.format(ORDER_NUMBER_IN_TABLE_LINE, i))).isDisplayed()){
            if($(byXpath(String.format(ORDER_NUMBER_IN_TABLE_LINE, i))).getText().contains(orderNumberValue)){
                $(byXpath(String.format(ASSING_MANAGER_IN_TABLE_LINE, i))).click();
                $(byXpath(SELLERS_DROPDOWN)).setValue(managerNameValue);
                $(byXpath(SELLER_NAME_VALUE)).click();
            }
            i++;
        }
        $(byXpath(String.format(ORDER_NUMBER_IN_TABLE_LINE, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new OrderPage();
    }

    public QueuePage goToQueuePage()
    {
        Selenide.open("http://tom.travellata.ru/#/orders/queue");

        return this;
    }

    public OrderPage gotoCurrentOrderPage() throws Exception{
        $(byXpath(String.format(CERTAIN_ORDER, orderNumber))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new OrderPage();
    }

}
