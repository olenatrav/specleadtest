package pageobjects.TOM;

import com.codeborne.selenide.Selenide;
import framework.SeleniumTestCase;
import org.testng.Assert;

import static java.lang.Math.toIntExact;

import java.io.File;
import java.nio.file.Files;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 03.10.2017.
 */
public class StatisticsPage extends SeleniumTestCase
{
    private static StatisticsPage statisticsPage = null;

    private StatisticsPage() {
    }

    public static synchronized StatisticsPage getStatisticsPage() {
        if (statisticsPage == null)
            statisticsPage = new StatisticsPage();

        return statisticsPage;
    }

    private final String PERIOD_FROM_INPUT = "//input[contains(@data-ng-model, 'dateFrom')]";
    private final String PERIOD_TO_INPUT = "//input[contains(@data-ng-model, 'dateTo')]";
    private final String SUBMIT_SEARCH_BUTTON = "//div[contains(@data-ng-click, 'loadDataReport')]";
    private final String EXPORT_DATA_BUTTON = "//div[contains(@data-ng-click, 'openLinkReportFile')]";


    public StatisticsPage goToStatisticsPage()
    {
        Selenide.open("http://tom.travellata.ru/#/statistic");

        return this;
    }

    public StatisticsPage exportData()
    {
        $(byXpath(EXPORT_DATA_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        File file = new File("C:\\Downloads\\Reports\\consolidate-sold-report.csv");
        Selenide.sleep(CONSTANT_5_SECONDS);
        Assert.assertTrue(file.isFile());

        Assert.assertFalse(Integer.toString(toIntExact(file.length())).equals("0"));
        file.delete();

        return this;
    }
}
