package pageobjects.TOM;

import com.codeborne.selenide.Selenide;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.sun.org.apache.xpath.internal.operations.Or;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
//import pageobjects.CheckoutScreen;
//import pageobjects.HotelPage;
//import pageobjects.MainPage;
import pageobjects.TOM.AuthorizationPage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import org.testng.Assert;
import pageobjects.Travelata.MainPage;
import sun.applet.Main;


/**
 * Created by User on 18.05.2017.
 */
public class OrderPage extends WebDriverCommands
{
    private static OrderPage orderPage = null;
    public OrderPage()
    {
    }
    public static synchronized OrderPage getOrderPage()
    {
        if(orderPage == null)
            orderPage = new OrderPage();

        return orderPage;
    }

    final private String ORDER_ID_HEADER = "//span[contains(@data-copy-dialog, 'customer.orderId') and contains(text(), '%s')]";

    final private String HISTORY_DIALOG_BUTTON = "//.[contains(@class, 'fa-history')]";
    final private String HISTORY_SOLD_DESCRIPTION = "//.[text() = 'Заказ продан']/following-sibling::td[contains(text(), 'Статус:13')]";
    final private String HISTORY_CREATED_TIME = "//.[text() = 'Заказ создан']/following-sibling::td[1]";

    final private String PASSPORT_TAB = "//a[text() = 'Паспорта']";
    final private String TOUR_TAB = "//a[text() = 'Тур']";

    final private String PASSPORT_LAST_NAME_INPUT = "(//input[@name = 'lastName'])[%d]";
    final private String PASSPORT_FIRST_NAME_INPUT = "(//input[@name = 'firstName'])[%d]";
    final private String PASSPORT_MIDDLE_NAME_INPUT = "(//input[@name = 'middleName'])[%d]";
    final private String PASSPORT_BIRTHDAY_INPUT = "(//input[@name = 'birthday'])[%d]";
    final private String PASSPORT_SEX_DROPDOWN = "(//select[@title = 'Пол'])[%d]";
    final private String PASSPORT_TYPE_DROPDOWN = "(//select[@title = 'Тип документа'])[%d]";
    final private String PASSPORT_ISSUE_DATE_INPUT = "(//input[@name = 'issueDate'])[%d]";
    final private String PASSPORT_SERIAL_NUMBER_INPUT = "(//input[@name = 'serialNumber'])[%d]";
    final private String PASSPORT_EXPIRE_DATE_INPUT = "(//input[@name = 'issueExpireDate'])[%d]";
    final private String PASSPORT_ISSUER_INPUT = "(//input[@name = 'issuer'])[%d]";
    final private String PASSPORT_NATIONALITY_INPUT = "(//input[@name = 'nationality'])[%d]";


    final private String TOUR_DEPARTURE_CITY = "//label[contains(text(), 'Город вылета')]/following-sibling::div/div/span/span/span";
    final private String TOUR_COUNTRY = "//label[text() = 'Страна']/following-sibling::div/div/span/span/span";
    final private String TOUR_RESORT = "//label[text() = 'Курорт']/following-sibling::div/div/span/span/span";
    final private String TOUR_HOTEL = "//label[text() = 'Отель']/following-sibling::div/div/span/span/span";
    final private String TOUR_ADULTS = "//input[@title = 'Взрослые']";
    final private String TOUR_CHILDREN = "//input[@title = 'Дети']";
    final private String TOUR_INFANTS = "//input[@title = 'Младенцы']";
    final private String TOUR_FLIGHT_START_DATE = "//input[@title = 'Дата вылета']";
    final private String TOUR_DATE_RANGE_VALUE = "//.[contains(@data-ng-bind, 'dateRange')]";
    final private String TOUR_NIGHTS = "//input[@title = 'Ночей']";
    final private String TOUR_OPERATOR = "//label[text() = 'Оператор']/following-sibling::div/div/span/span/span";
    final private String TOUR_MEALTYPE = "//label[text() = 'Питание']/following-sibling::div/div/span/span/span";
    final private String TOUR_TOUR_PRICE = "//input[@title = 'Стоимость тура']";
    final private String TOUR_OIL_TAX = "//input[@title = 'Топливный сбор']";
    final private String TOUR_BOOKING_BUTTON = "//button[text() = 'Внести номер брони']";
    final private String TOUR_BOOKING_CODE_INPUT = "//input[@placeholder = 'Номер брони']";
    final private String TOUR_BOOKING_DATE_INPUT = "(//input[@placeholder = 'Дата'])[2]";
    final private String TOUR_BOOKING_TODAY_DATE_BUTTON = "//button[contains(@class, 'btn-today')]";
    final private String TOUR_BOOKING_SUBMIT_BUTTON = "//button[text() = 'Сохранить']";

    final private String POSSIBLE_DUPLICATE_ORDERS_BUTTON = "//button[@uib-tooltip = 'Проверить на задвойки']";
    final private String DUPLICATE_ORDERS_BUTTON = "//.[contains(@ng-click, 'openDuplicatesDialog')]";
    final private String DUPLICATE_ORDER_VALUE = "//.[contains(@data-ng-repeat, 'haveDuplicateList')]/*/a";
    final private String DUPLICATE_ORDERS_LIST_BLOCK = "//div[@role = 'document']";
    final private String DUPLICATING_ORDER_CHECKBOX = "//.[text() = '%s']/parent::th/following-sibling::th/input";
    final private String SELECTED_ORDER_BUTTON = "//.[text() = '%s']/parent::th/following-sibling::th/button";
    final private String SELECTED_ORDER_CHECKBOX = "//.[text() = '%s']/parent::th/following-sibling::th/input";
    final private String POSSIBLE_DUPLICATED_ORDER_STATUS = "(//.[text() = '%s']/parent::th/following-sibling::th)[2]";

    final private String PAYMENT_ACTIONS_BUTTON = "//a[contains(text(), 'Действие')]";
    final private String PAYMENT_RELEASE_BUTTON = "//a[contains(text(), 'Отменить платеж')]";
    final private String PAYMENT_SUBMIT_RELEASE_BUTTON = "//button[contains(text(), 'Продолжить')]";
    final private String PAYMENT_CANCEL_RELEASE_BUTTON = "//button[contains(text(), 'Отмена')]";
    final private String PAYMENT_CHARGE_BUTTON = "//a[contains(text(), 'Списать деньги')]";
    final private String PAYMENT_CHARGE_AMOUNT_INPUT = "//input[@data-ng-model = 'amount']";
    final private String PAYMENT_SUBMIT_CHARGE_BUTTON = "//button[contains(text(), 'Списать')]";
    final private String PAYMENT_CANCEL_CHARGE_BUTTON = "//button[contains(text(), 'Отмена')]";
    final private String PAYMENT_STATUS = "//td/span";
    final private String PAYMENT_STATUS_AUTHORIZED_VALUE = "Авторизован";
    final private String PAYMENT_STATUS_RELEASED_VALUE = "Средства разблокированы";
    final private String PAYMENT_CREATE_NEW = "//a[contains(text(), 'новый счет')]";
    final private String PAYMENT_AMOUNT = "//input[@data-ng-model = 'amount']";
    final private String PAYMENT_SUBMIT_NEW_AMOUNT = "//button[contains(@data-ng-click, 'saveOnlineInvoice')]";


    final private String ORDER_ID = "//span[contains(@ng-bind, 'customer.orderId')]";
    final private String ORDER_STATUS_VALUE = "//a[contains(@ng-bind, 'statusName')]";
    final private String ORDER_STATUS_FROM_DROPDOWN = "//.[text() = '%s']";
    final private String ORDER_STATUS_CHANGE_SUBMIT = "//.[text() = 'Вы действительно хотите изменить статус?']/following-sibling::div/button[contains(text(), 'Да')]";
    final private String ORDER_WRONG_STATUS_ISSUE_BUTTON = "//button[contains(text(), 'Клиент многократно не отвечает')]";
    final private String CUSTOMER_PHONE_INPUT = "phoneCustomer";
    final private String CUSTOMER_DATA_SAVE_BUTTON = "//button[contains(@data-ng-click, 'saveCustomerDocument')]";
    final private String NOTIFICATION_TITLE = "//div[@aria-label = '%s']";
    final private String NOTIFICATION_MESSAGE = "//div[@aria-label = '%s']";

//    final private String UPSELL_PRODUCT_ADD = "(//.[contains(@data-ng-click, 'CreatingUpsellProduct')])[1]";

    final private String UPSELL_TABS = "//div[@class = 'upsell-tabs-panel']/div";
    final private String UPSELL_TAB = "(//div[@class = 'upsell-tabs-panel']/div/div/div)[%s]";

    final private String UPSELL_PRODUCT_ADD = "//a[@data-ng-click = 'upSell.toggleCreatingUpsellProductForm()']";
    final private String UPSELL_PRODUCT_DROPDOWN = "(//.[contains(@aria-label, 'Выберите продукт')]/span)[1]";
    final private String UPSELL_PRODUCT_VALUE = "//.[@data-ng-bind = 'item.name']/./.[text() = '%s']";
    final private String UPSELL_CREATE_BUTTON = "//.[contains(@data-ng-click, 'upSell.createUpsell')]";
    final private String UPSELL_SAVE_CHANGES_BUTTON = "//button[contains(@data-ng-click, 'saveUpsellList')]";


    final private String VISA_TOURIST_ADD = "//.[contains(@data-ng-click, 'addNewPositionOfVisa')]";
    final private String TOURISTS_DROPDOWN = "//.[text() = 'Выберите туриста']";
    final private String TOURIST_VALUE = "(//.[contains(@class, 'select-choices')]/./.[@role = 'option'])[1]";
    final private String VISA_PRICE_VALUE = "//input[contains(@data-ng-model, 'upsell')]/./.[@name = 'price']";
    final private String VISA_BLOCK_AND_TAB = "//.[@data-ng-switch = 'upsell.product.type']/div[@data-ng-switch-when = 'VisaProduct']";
    final private String VISA_LAST_AND_FIRST_NAME_VALUE = "//input[contains(@data-ng-model, 'name')]";
    final private String VISA_BIRTHDAY_VALUE = "//input[contains(@data-ng-model, 'birthday')]";
    final private String VISA_NATIONALITY_VALUE = "//input[contains(@data-ng-model, 'nationality')]/.[contains(@data-ng-model, 'upsell')]";
    final private String VISA_EXPIRE_DATE_VALUE = "//input[contains(@data-ng-model, 'issueExpireDate')]/.[contains(@data-ng-model, 'upsell')]";


    final private String INSURANCE_TAB = "//div[@data-ng-switch-when = 'InsuranceProduct']";
    final private String INSURANCE_MEDICINE_TAB_CLOSE = "//span[contains(text(), 'медицинский')]/following-sibling::span/i"; ///i[contains(@сlass, 'fa-close
    final private String INSURANCE_NOT_LIVING_TAB_CLOSE = "//span[contains(text(), 'от невыезда')]/following-sibling::span/i[contains(@сlass, 'fa-close')]";
    final private String INSURANCE_NOT_LIVING_TOURIST_COUNT_INPUT = "touristsCount"; //name
    final private String INSURANCE_NOT_LIVING_PRICE = "//input[@name= 'price' and contains(@data-ng-model, 'upSell')]";
    final private String INSURANCE_NOT_LIVING_COUNT_BUTTON = "//div[contains(text(), 'Рассчитать')]";
    final private String INSURANCE_NOT_LIVING_MANAGER_DROPDOWN = "//span[contains(@aria-label, 'Менеджер')]";
    final private String INSURANCE_NOT_LIVING_MANAGER_DROPDOWN_VALUE = "//span[@data-ng-bind = 'item.name' and contains(text(), '%s')]";
    final private String INSURANCE_NOT_LIVING_STATUS_DROPDOWN = "//span[contains(@aria-label, 'Статус')]";
    final private String INSURANCE_NOT_LIVING_STATUS_DROPDOWN_VALUE = "//span[@data-ng-bind = 'item.name' and contains(text(), '%s')]";
    final private String INSURANCE_NOT_LIVING_SAVE_BUTTON = "//a[contains(@data-ng-click, 'createUpsell')]";
    final private String INSURANCE_CANCELLED_LABEL = "//b[contains(text(), 'Полис анулирован.')]";
    final private String INSURANCE_DELETE_LINK = "//a[contains(@data-ng-click, 'deleteUpsellConfirm')]";
    final private String INSURANCE_DELETE_CONFIRM_BUTTON = "//button[contains(@data-ng-click, 'execute')]";

    final private String CONCIERGE_TAB = "//div[@data-ng-switch-when = 'ConciergeProduct']";

    final private String UPSELL_SAVE_BUTTON = "//button[contains(@data-ng-click, 'saveUpsellList')]";

    final private String ADDITIONAL_PRODUCTS_PRICE = "//strong[text() = 'Доп. продукты: ']/following-sibling::span";
    final private String TOTAL_PRICE = "//strong[text() = 'Итого: ']/following-sibling::span";

    final private String NEXT_ACTION_INPUT = "//input[@data-ng-model = 'nextAction.temp.action.action']";
    final private String NEXT_ACTION_DATE_INPUT = "//input[@data-ng-model = 'nextAction.temp.action.executeAt']";
    final private String NEXT_ACTION_SAVE_BUTTON = "//div[contains(@data-ng-click, 'saveNextAction')]";
    final private String NEXT_ACTION_DELETE_BUTTON = "//a[contains(@data-ng-click, 'removeNextAction')]";

    final private String NOTE = "//span[contains(@ng-bind-html, 'item.note')]";

    public static String tourOperatorValue = "";
    public static String resortValue = "";
    public static String dateFromShortMonth = "";
    public static String dateToShortMonth = "";
    public static String soldDate = "";
    public static String createdDate = "";
    public static String tourPrice = "";
    public static String totalPrice = "";
    public static String tourStartDate = "";
    public static String visa = "нет";
    public static String visaSupport = "нет";
    public static String bookingCode = "test_booking_code";
    public static String bookingDate = "";

    public static String orderIDValue = "";
    public static String duplicatingOrderIDValue = "";

    public static String visaLastAndFirstName = "";
    public static String visaBirthday = "";
    public static String visaNationality = "";
    public static String visaExpireDate = "";
    public static String visaPrice = "";



    public OrderPage checkGenLeadData(String countries, boolean isFlexible, int dayFromCurrent,
                                      String nightsFrom, String nightsTo, String adults, String children, String infants, String hotelClass)
    {
        Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains(MainPage.officeAddressValue));
        //Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Выбран город вылета:" + departureCity));

        String[] countriesArray = countries.split(", ");
        for(int i = 0; i < countriesArray.length; i++)
        {
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Выбрана страна:" + countriesArray[i]));
        }

        if(isFlexible)
        {
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Дата с:" + getFullDateFromCurrent(dayFromCurrent - 3)));
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Дата по:" + getFullDateFromCurrent(dayFromCurrent + 3)));
        }
        else
        {
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Дата с:" + getFullDateFromCurrent(dayFromCurrent)));
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Дата по:" + getFullDateFromCurrent(dayFromCurrent)));
        }


        Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Ночей от:" + nightsFrom));
        Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Ночей до:" + nightsTo));
        Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Количество взрослых:" + adults));
        if(!children.equals(""))
        {
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Количество детей:" + children));
        }
        if(!infants.equals(""))
        {
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Количество младенцев:" + infants));
        }


        String[] hotelClassArray = hotelClass.split(", ");

        for(int i = 0; i < hotelClassArray.length; i++)
        {
            Assert.assertTrue($(byXpath(NOTE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains(hotelClassArray[i] + "*"));
        }



        return this;
    }

    public OrderPage checkOrderPageTourAssertions(String departureCity, String country, String resort, String hotel, String hotelClass, String adults, String children, String infants,
                                              String departureDate, String nights, String mealType) throws Exception
    {
        $(byXpath(TOUR_TAB)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        Assert.assertTrue($(byXpath(TOUR_DEPARTURE_CITY)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(departureCity.toLowerCase()), "OrderPage: departure city");

        Assert.assertTrue($(byXpath(TOUR_COUNTRY)).innerHtml().toLowerCase().equals(country.toLowerCase()), "OrderPage: country");

        if (!resort.equals("")) {
            Assert.assertTrue($(byXpath(TOUR_RESORT)).innerHtml().toLowerCase().contains(resort.toLowerCase()), "OrderPage: resort");
        }
        resortValue = $(byXpath(TOUR_RESORT)).innerHtml().toLowerCase();
        String hotelCategory = $(byXpath(TOUR_HOTEL)).innerHtml().toLowerCase().replaceAll(".*\\s", "").replaceAll("\\*", "");
        String hotelName = $(byXpath(TOUR_HOTEL)).innerHtml().toLowerCase().replaceAll("\\s\\d.*", "");
        String aptsName = $(byXpath(TOUR_HOTEL)).innerHtml().toLowerCase().replaceAll("\\sapts", "");

        if (hotelClass.equals("апартаменты"))
        {
            Assert.assertTrue(aptsName.equals(hotel.toLowerCase()), "OrderPage: hotel name");
        }
        else
        {
            Assert.assertTrue(hotelName.equals(hotel.toLowerCase()), "OrderPage: hotel name");
        }

        if (hotelClass.toLowerCase().equals("апартаменты"))
        {
            Assert.assertTrue(hotelCategory.equals("apts"), "OrderPage: hotel class");
        }
        else
        {
            Assert.assertTrue(hotelCategory.equals(hotelClass), "OrderPage: hotel class");
        }

        Assert.assertTrue($(byXpath(TOUR_ADULTS)).getAttribute("value").equals(adults), "OrderPage: adults");


        if (!children.equals(""))
        {
            Assert.assertTrue($(byXpath(TOUR_CHILDREN)).getAttribute("value").equals(children), "OrderPage: children");
        }
        else
        {
            Assert.assertTrue($(byXpath(TOUR_CHILDREN)).getAttribute("value").equals("0"), "OrderPage: children");
        }

        if (!infants.equals("")) {
            Assert.assertTrue($(byXpath(TOUR_INFANTS)).getAttribute("value").equals(infants), "OrderPage: infants");
        } else {
            Assert.assertTrue($(byXpath(TOUR_INFANTS)).getAttribute("value").equals("0"), "OrderPage: infants");
        }


        Assert.assertTrue($(byXpath(TOUR_FLIGHT_START_DATE)).getAttribute("value").replaceAll("\\d{2}(?=\\d{2}$)", "").contains(departureDate), "OrderPage: date");
        dateFromShortMonth = $(byXpath(TOUR_DATE_RANGE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll("\\s-.*", "");
        dateToShortMonth = $(byXpath(TOUR_DATE_RANGE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll(".*-\\s", "");
        tourStartDate = $(byXpath(TOUR_FLIGHT_START_DATE)).getAttribute("value");

        Assert.assertTrue($(byXpath(TOUR_NIGHTS)).getAttribute("value").equals(nights), "OrderPage: nights");

        tourOperatorValue = $(byXpath(TOUR_OPERATOR)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase();

        Assert.assertTrue($(byXpath(TOUR_MEALTYPE)).innerHtml().toLowerCase().equals(mealType), "OrderPage: meal type");

        tourPrice = $(byXpath(TOUR_TOUR_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value");
        Pattern p1 = Pattern.compile("\\d*");
        Matcher m1 = p1.matcher(tourPrice);
        m1.find();
        Pattern p2 = Pattern.compile("\\d*$");
        Matcher m2 = p2.matcher(tourPrice);
        m2.find();
        tourPrice = m1.group(0) + m2.group(0);

        orderIDValue = $(byXpath(ORDER_ID)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll("#\\s", "");

        totalPrice = $(byXpath(TOTAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll("\\s.\\.", "").replaceAll("&nbsp;", "");

        $(byXpath(HISTORY_DIALOG_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();


        Pattern p = Pattern.compile("\\d{2}\\.\\d{2}\\.\\d{4}(?=\\s\\d)");
        Matcher m = p.matcher($(byXpath(HISTORY_CREATED_TIME)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml());
        m.find();
        String s[] = m.group(0).split("\\.");
        createdDate = s[0] + " " + Constants.monthShortName.get(s[1]) + " " + s[2];
        $(byXpath("//.[@class = 'ngdialog-content']")).waitUntil(visible, CONSTANT_10_SECONDS).pressEscape();


        return this;
    }

    public OrderPage addNextAction(String nextActionTextValue, String nextActionDateValue) throws Exception{
        if ($(byXpath(NEXT_ACTION_DELETE_BUTTON)).isDisplayed())
            $(byXpath(NEXT_ACTION_DELETE_BUTTON)).click();

        $(byXpath(NEXT_ACTION_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(nextActionTextValue);
        $(byXpath(NEXT_ACTION_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(nextActionDateValue);
        $(byXpath(NEXT_ACTION_SAVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public OrderPage getOrderIDValue() throws Exception{
        orderIDValue = $(byXpath(ORDER_ID)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll("#\\s", "");

        return this;
    }

    public OrderPage checkOrderPagePassportAssertions(String adults, String children, String infants, String lastName, String firstName, String serialNumber, String adultBirthday, String kidBirthday, boolean abroad,
                                                      String middleName, String issueDate, String nationality, String expireDate, String sex)
    {
        $(byXpath(PASSPORT_TAB)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }

        for(int i = 1; i < touristsCount + 1; i++)
        {

            Assert.assertTrue($(byXpath(String.format(PASSPORT_LAST_NAME_INPUT, i))).getAttribute("value").toLowerCase().equals(lastName.toLowerCase()), "OrderPage: last name " + Integer.toString(i));
            Assert.assertTrue($(byXpath(String.format(PASSPORT_FIRST_NAME_INPUT, i))).getAttribute("value").toLowerCase().equals(firstName.toLowerCase()), "OrderPage: first name " + Integer.toString(i));
            Assert.assertTrue($(byXpath(String.format(PASSPORT_SERIAL_NUMBER_INPUT, i))).getAttribute("value").toLowerCase().equals(serialNumber.toLowerCase()), "OrderPage: serial number name " + Integer.toString(i));

            if(i <= Integer.parseInt(adults))
            {
                Assert.assertTrue($(byXpath(String.format(PASSPORT_BIRTHDAY_INPUT, i))).getAttribute("value").equals(adultBirthday), "OrderPage: adult birthday " + Integer.toString(i));
            }
            else
            {
                Assert.assertTrue($(byXpath(String.format(PASSPORT_BIRTHDAY_INPUT, i))).getAttribute("value").equals(kidBirthday), "OrderPage: kid birthday " + Integer.toString(i));
            }

            if(abroad == false)
            {
                Assert.assertTrue($(byXpath(String.format(PASSPORT_MIDDLE_NAME_INPUT, i))).getAttribute("value").toLowerCase().equals(middleName.toLowerCase()), "OrderPage: middle name " + Integer.toString(i));
                Assert.assertTrue($(byXpath(String.format(PASSPORT_ISSUE_DATE_INPUT, i))).getAttribute("value").equals(issueDate), "OrderPage: issue date " + Integer.toString(i));
            }
            else
            {
                Assert.assertTrue($(byXpath(String.format(PASSPORT_NATIONALITY_INPUT, i))).getAttribute("value").toLowerCase().equals(nationality.toLowerCase()), "OrderPage: nationality " + Integer.toString(i));
                Assert.assertTrue($(byXpath(String.format(PASSPORT_EXPIRE_DATE_INPUT, i))).getAttribute("value").equals(expireDate), "OrderPage: expire date " + Integer.toString(i));

                if(sex.equals("male"))
                {
                    Assert.assertTrue($(byXpath(String.format(PASSPORT_SEX_DROPDOWN, i))).getSelectedOption().getText().toLowerCase().equals("мужской"), "OrderPage: sex " + Integer.toString(i));
                }
                else
                {
                    Assert.assertTrue($(byXpath(String.format(PASSPORT_SEX_DROPDOWN, i))).getSelectedOption().getText().toLowerCase().equals("женский"), "OrderPage: sex " + Integer.toString(i));
                }

            }

        }
        return this;
    }

    public OrderPage checkAuthorizedPayment()
    {
        Assert.assertTrue($(byXpath(PAYMENT_STATUS)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(Constants.statusPaymentAuthorized), "Order page:  Authorized status failure");

        return this;
    }



    public OrderPage releaseAuthorizedPayment()
    {
        Assert.assertTrue($(byXpath(PAYMENT_STATUS)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(Constants.statusPaymentAuthorized), "Order page:  Authorized status failure");
        $(byXpath(PAYMENT_ACTIONS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(PAYMENT_RELEASE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(PAYMENT_SUBMIT_RELEASE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_1_SECOND);
        Assert.assertTrue($(byXpath(PAYMENT_STATUS)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(Constants.statusPaymentReleased), "Order page:  Released status failure");

        return this;
    }
    public OrderPage duplicateOrder(String duplicatingOrderID, String orderID)
    {
        $(byXpath(POSSIBLE_DUPLICATE_ORDERS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(String.format(DUPLICATING_ORDER_CHECKBOX, duplicatingOrderIDValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(SELECTED_ORDER_CHECKBOX, orderIDValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(SELECTED_ORDER_BUTTON, orderIDValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(POSSIBLE_DUPLICATE_ORDERS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Assert.assertTrue($(byXpath(String.format(POSSIBLE_DUPLICATED_ORDER_STATUS, duplicatingOrderIDValue))).innerHtml().contains("Двойной заказ"));
        Assert.assertTrue($(byXpath(String.format(POSSIBLE_DUPLICATED_ORDER_STATUS, duplicatingOrderIDValue))).innerHtml().contains(orderIDValue));
        $(byXpath(DUPLICATE_ORDERS_LIST_BLOCK)).pressEscape();

        $(byXpath("//div[@class = 'form-group']")).hover();
        Selenide.sleep(CONSTANT_1_SECOND);
        $(byXpath(DUPLICATE_ORDERS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Assert.assertTrue($(byXpath(DUPLICATE_ORDER_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(duplicatingOrderIDValue));
        $(byXpath(DUPLICATE_ORDERS_LIST_BLOCK)).pressEscape();

        return this;
    }
    public OrderPage checkOrderStatus(String statusValue)
    {
        Assert.assertTrue($(byXpath(ORDER_STATUS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals(statusValue));

        return this;
    }
    public OrderPage selectSoldOrderStatus(String statusValue)
    {
        $(byXpath(TOUR_TAB)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        if(statusValue.equals("Продано"))
        {
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date date = new Date();
            bookingDate = dateFormat.format(date);

            $(byXpath(TOUR_BOOKING_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(TOUR_BOOKING_CODE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(bookingCode);
            $(byXpath(TOUR_BOOKING_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(bookingDate);
            $(byXpath(TOUR_BOOKING_SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Данные тура"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Данные тура"), "OrderPAge: notification title 'Данные тура'");
            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Изменения успешно сохранены"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Изменения успешно сохранены"), "OrderPAge: notification message 'Изменения успешно сохранены'");

            $(byXpath(ORDER_STATUS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(String.format(ORDER_STATUS_FROM_DROPDOWN, statusValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(ORDER_STATUS_CHANGE_SUBMIT)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Статус заказа"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Статус заказа"), "OrderPAge: notification title 'Статус заказа'");
            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Статус заказа успешно изменен"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Статус заказа успешно изменен"), "OrderPAge: notification message 'Статус заказа успешно изменен'");

            $(byXpath(HISTORY_DIALOG_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            Pattern p = Pattern.compile("\\d{4}-\\d{2}-\\d{2}(?=\\s\\d)");
            Matcher m = p.matcher($(byXpath(HISTORY_SOLD_DESCRIPTION)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml());
            m.find();
            String s[] = m.group(0).split("-");
            soldDate = s[2] + " " + Constants.monthShortName.get(s[1]) + " " + s[0];
            $(byXpath("//.[@class = 'ngdialog-content']")).waitUntil(visible, CONSTANT_10_SECONDS).pressEscape();

        }

        return this;
    }

    public OrderPage selectOrderStatus(String statusValue) throws Exception{
        Assert.assertFalse($(byXpath(ORDER_STATUS_VALUE)).getText().equals("Новый"), "Невозможно сменить статус Нового заказа");
        $(byXpath(ORDER_STATUS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(ORDER_STATUS_FROM_DROPDOWN, statusValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        if (statusValue.equals("Неверный заказ"))
            $(byXpath(ORDER_WRONG_STATUS_ISSUE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        if(statusValue.equals("Продано") || statusValue.equals("Продано с частичной оплатой"))
            $(byXpath(ORDER_STATUS_CHANGE_SUBMIT)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

//    public OrderPage erasePhoneNumber()
//    {
////        CheckoutScreen.customerPhoneValue = "1111111111";
//        $(byName(CUSTOMER_PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
//        $(byName(CUSTOMER_PHONE_INPUT)).setValue("7" + CheckoutScreen.customerPhoneValue);
//        $(byXpath(CUSTOMER_DATA_SAVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
//        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Данные клиента"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Данные клиента"), "OrderPAge: notification title 'Данные клиента'");
//        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Изменения успешно сохранены"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Изменения успешно сохранены"), "OrderPAge: notification message 'Изменения успешно сохранены'");
//        return this;
//    }

    public OrderPage goToOrderPage(String orderId)
    {
        Selenide.open(String.format("http://tom.travellata.ru/#/orders/%s", orderId));
        $(byXpath(String.format(ORDER_ID_HEADER, orderId))).waitUntil(visible, CONSTANT_10_SECONDS);
//        Selenide.sleep(CONSTANT_1_SECOND);

        return this;
    }

    public OrderPage selectUpsellProduct(String upsellName, String touristCountValue, String insurancePriceValue,
                                         String insuranceManagerNameValue, String insuranceStatusValue)
    {


        $(byXpath(UPSELL_PRODUCT_ADD)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(UPSELL_PRODUCT_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(UPSELL_PRODUCT_VALUE, upsellName))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        if(upsellName.equals("Виза"))
        {
            visaPrice = "12000";

            $(byXpath(VISA_TOURIST_ADD)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(TOURISTS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(TOURIST_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            $(byXpath(UPSELL_CREATE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Добавление продукта"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Добавление продукта"), "OrderPAge: notification title 'Добавление продукта'");
            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Продукт успешно добавлен"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Продукт успешно добавлен"), "OrderPAge: notification message 'Продукт успешно добавлен'");

            $(byXpath(VISA_PRICE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(visaPrice);
            $(byXpath(UPSELL_SAVE_CHANGES_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Дополнительные продукты"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Дополнительные продукты"), "OrderPAge: notification title 'Дополнительные продукты'");
            Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Изменения успешно сохранены"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Изменения успешно сохранены"), "OrderPAge: notification message 'Изменения успешно сохранены'");
            Assert.assertTrue($(byXpath(VISA_BLOCK_AND_TAB)).is(visible));
            Assert.assertTrue($(byXpath(ADDITIONAL_PRODUCTS_PRICE)).innerHtml().replaceAll("\\s.\\.", "").replaceAll("&nbsp;", "").equals(visaPrice));

            visaLastAndFirstName = $(byXpath(VISA_LAST_AND_FIRST_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value");
            visaBirthday = $(byXpath(VISA_BIRTHDAY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value");
            visaNationality = $(byXpath(VISA_NATIONALITY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value");
            visaExpireDate = $(byXpath(VISA_EXPIRE_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value");


            totalPrice = Integer.toString(Integer.parseInt(totalPrice) + Integer.parseInt(visaPrice));
            visa = "да";
        }

        if(upsellName.equals("Страхование")){

            $(byXpath(INSURANCE_MEDICINE_TAB_CLOSE)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byName(INSURANCE_NOT_LIVING_TOURIST_COUNT_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(touristCountValue);
            $(byXpath(INSURANCE_NOT_LIVING_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(insurancePriceValue);
            $(byXpath(INSURANCE_NOT_LIVING_COUNT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(INSURANCE_NOT_LIVING_MANAGER_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(String.format(INSURANCE_NOT_LIVING_MANAGER_DROPDOWN_VALUE, insuranceManagerNameValue))).click();
            $(byXpath(INSURANCE_NOT_LIVING_STATUS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(String.format(INSURANCE_NOT_LIVING_STATUS_DROPDOWN_VALUE, insuranceStatusValue))).click();
            $(byXpath(INSURANCE_NOT_LIVING_SAVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
//            Selenide.sleep(CONSTANT_1_SECOND);

        }

        return this;
    }

    public OrderPage setInsuranceStatus(String insuranceStatusValue) throws Exception{
        Selenide.sleep(CONSTANT_1_SECOND);
        if($(byXpath(INSURANCE_TAB)).exists()){
            $(byXpath(INSURANCE_TAB)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(INSURANCE_NOT_LIVING_STATUS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(String.format(INSURANCE_NOT_LIVING_STATUS_DROPDOWN_VALUE, insuranceStatusValue)))
                    .waitUntil(visible, CONSTANT_10_SECONDS).click();
//            Selenide.sleep(CONSTANT_1_SECOND);
        }

        return this;
    }

    public OrderPage deleteInsurance() throws Exception{
        $(byXpath(INSURANCE_DELETE_LINK)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(INSURANCE_DELETE_CONFIRM_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public OrderPage saveUpsellChanges() throws Exception{
        $(byXpath(UPSELL_SAVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public OrderPage checkIfInsuranceCandcelled() throws Exception{
        if($(byXpath(INSURANCE_TAB)).exists()){
            $(byXpath(INSURANCE_TAB)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            Assert.assertTrue($(byXpath(INSURANCE_CANCELLED_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).isDisplayed(),
                    "Полис не аннулирован");
        }
        return this;
    }
    public OrderPage createNewPayment(String amount)
    {

        $(byXpath(PAYMENT_CREATE_NEW)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(PAYMENT_AMOUNT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(amount);
        $(byXpath(PAYMENT_SUBMIT_NEW_AMOUNT)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Отправить ссылку"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Отправить ссылку"), "OrderPAge: notification title 'Отправить ссылку'");
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Выполнено успешно"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Выполнено успешно"), "OrderPAge: notification message 'Выполнено успешно'");

        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_TITLE, "Новый счет"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Новый счет"), "OrderPAge: notification title 'Новый счет'");
        Assert.assertTrue($(byXpath(String.format(NOTIFICATION_MESSAGE, "Счет успешно добавлен"))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Счет успешно добавлен"), "OrderPAge: notification message 'Счет успешно добавлен'");


        return this;
    }

    public OrderPage checkExistingUpsells(boolean concierge)
    {
        int size =  $$(byXpath(UPSELL_TABS)).size();

        if(concierge)
        {
            boolean match = false;
            for(int i = 1; i < size + 1; i++)
            {
                if($(byXpath(String.format(UPSELL_TAB, i))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Консьерж"))
                {
                    match = true;
                }
            }
            Assert.assertTrue(match);
        }






        return this;
    }



}
