package pageobjects.TOM;

import com.codeborne.selenide.Selenide;
import framework.WebDriverCommands;
import org.testng.Assert;

import java.text.SimpleDateFormat;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 26.06.2017.
 */
public class InsurancePage extends WebDriverCommands {

    private static InsurancePage insurancePage = null;
    private InsurancePage()
    {
    }
    public static synchronized InsurancePage getInsurancePage()
    {
        if(insurancePage == null)
            insurancePage = new InsurancePage();

        return insurancePage;
    }

    public InsurancePage goToInsurancePage()
    {
        Selenide.open("http://tom.travellata.ru/#/predeparture/insurance");

        return this;
    }

    final private String ID_INPUT = "//input[@placeholder= 'ID']";
    final private String STATUS_DROPDOWN = "//span[contains(@aria-label, 'Статус')]";
    final private String STATUS_DROPDOWN_VALUE = "//span[@data-ng-bind = 'item.name' and contains(text(), '%s')]";
    final private String EXTERNAL_ID_INPUT = "//input[@placeholder= 'Внешний ID']";
    final private String CREATE_DATE_FROM_INPUT = "//input[contains(@data-ng-model, 'issuedDate[from]')]";
    final private String CREATE_DATE_TO_INPUT = "//input[contains(@data-ng-model, 'issuedDate[to]')]";
    final private String CANCEL_DATE_FROM_INPUT = "//input[contains(@data-ng-model, 'cancellationDate[from]')]";
    final private String CANCEL_DATE_TO_INPUT = "//input[contains(@data-ng-model, 'cancellationDate[to]')]";
    final private String SEARCH_BUTTON = "//div[contains(text(), 'Найти')]";

    final private String INSURANCE_ROW = "//table/tbody[%s]";
    final private String INSURANCE_RESULT_TABLE = "//table/thead/tr";
    final private String INSURANCE_ROW_ID_VALUE = "//table/tbody[%s]/.//td[1]/a";
    final private String INSURANCE_ROW_STATUS_VALUE = "//table/tbody[%s]/.//td[5]/span";
    final private String INSURANCE_ROW_EXTERNAL_ID_VALUE = "//table/tbody[%s]/.//td[6]/span";
    final private String INSURANCE_ROW_CREATE_DATE_VALUE = "//table/tbody[%s]/.//td[7]/span";
    final private String INSURANCE_ROW_CANCEL_DATE_VALUE = "//table/tbody[%s]/.//td[8]/span";

    final private String INSURANCE_EDIT_EXTERNAL_ID_ICON = "//a[contains(@data-ng-click, 'openSettingExternalIdentityDg')]/i";
    final private String INSURANCE_EXTERNAL_ID_INPUT = "externalNumber"; //name
    final private String INSURANCE_EXTERNAL_ID_SAVE_BUTTON = "//button[contains(@data-ng-click, 'saveExternalNumber')]";
    final private String INSURANCE_EDIT_CREATE_DATE_ICON = "//a[contains(@data-ng-click, 'openSettingIssuedDateDg')]/i";
    final private String INSURANCE_CREATE_DATE_INPUT = "issuedDate"; //name
    final private String INSURANCE_CREATE_DATE_SAVE_BUTTON = "//button[contains(@data-ng-click, 'saveIssuedDate')]";
    final private String INSURANCE_EDIT_STATUS_ICON = "//a[contains(@data-ng-click, 'openSettingStateDg')]/i";
    final private String INSURANCE_CHANGE_STATUS_CONFIRM_BUTTON = "//button[contains(@data-ng-click, 'saveState')]";
    final private String INSURANCE_EDIT_CANCEL_DATE_ICON = "//a[contains(@data-ng-click, 'openSettingCancellationDateDg')]/i";
    final private String INSURANCE_CANCEL_DATE_INPUT = "cancellationDate"; //name
    final private String INSURANCE_CANCEL_DATE_SAVE_BUTTON = "//button[contains(@data-ng-click, 'saveCancellationDate')]";


    public InsurancePage setInsuranceExternalID(String insuranceExternalIDValue) throws Exception{
        $(byXpath(INSURANCE_EDIT_EXTERNAL_ID_ICON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byName(INSURANCE_EXTERNAL_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(insuranceExternalIDValue);
        $(byXpath(INSURANCE_EXTERNAL_ID_SAVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public InsurancePage setInsuranceCreateDate(String insuranceCreateDateValue) throws Exception{
        $(byXpath(INSURANCE_EDIT_CREATE_DATE_ICON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byName(INSURANCE_CREATE_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(insuranceCreateDateValue + "\n");
        $(byXpath(INSURANCE_CREATE_DATE_SAVE_BUTTON)).waitUntil(exist, CONSTANT_10_SECONDS).click();

        return this;
    }

    public InsurancePage setInsuranceCancelDate(String insuranceCancelDateValue) throws Exception{
        $(byXpath(INSURANCE_EDIT_CANCEL_DATE_ICON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byName(INSURANCE_CANCEL_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(insuranceCancelDateValue + "\n");
        $(byXpath(INSURANCE_CANCEL_DATE_SAVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public InsurancePage confirmInsuranceChangedStatus() throws Exception{
        $(byXpath(INSURANCE_EDIT_STATUS_ICON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(INSURANCE_CHANGE_STATUS_CONFIRM_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
//        Selenide.sleep(CONSTANT_1_SECOND);

        return this;
    }

    public InsurancePage fillID(String IDValue) throws Exception{
        $(byXpath(ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(ID_INPUT)).setValue(IDValue);

        return this;
    }

    public InsurancePage fillStatus(String statusValue) throws Exception{
        $(byXpath(STATUS_DROPDOWN)).click();
        $(byXpath(String.format(STATUS_DROPDOWN_VALUE, statusValue))).click();

        return this;
    }

    public InsurancePage fillExternalID(String externalIDValue) throws Exception{
        $(byXpath(EXTERNAL_ID_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(EXTERNAL_ID_INPUT)).setValue(externalIDValue);

        return this;
    }

    public InsurancePage fillCreateDateFrom(String createDateFromValue) throws Exception{
        $(byXpath(CREATE_DATE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(CREATE_DATE_FROM_INPUT)).setValue(createDateFromValue);

        return this;
    }

    public InsurancePage fillCreateDateTo(String createDateToValue) throws Exception{
        $(byXpath(CREATE_DATE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(CREATE_DATE_TO_INPUT)).setValue(createDateToValue);

        return this;
    }

    public InsurancePage fillCancelDateFrom(String cancelDateFromValue) throws Exception{
        $(byXpath(CANCEL_DATE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(CANCEL_DATE_FROM_INPUT)).setValue(cancelDateFromValue);

        return this;
    }

    public InsurancePage fillCancelDateTo(String cancelDateToValue) throws Exception{
        $(byXpath(CANCEL_DATE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byXpath(CANCEL_DATE_TO_INPUT)).setValue(cancelDateToValue);

        return this;
    }

    public InsurancePage clickSearchButton() throws Exception{
        $(byXpath(SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_1_SECOND);

        return this;
    }

    public InsurancePage checkFilteredInsuranceParameters(String IDValue, String statusValue, String externalIDValue,
                                                          String createDateFromValue, String createDateToValue,
                                                          String cancelDateFromValue, String cancelDateToValue)
            throws Exception{

        int rowsCounter = 1;

        Assert.assertTrue($(byXpath(String.format(INSURANCE_ROW, rowsCounter))).waitUntil(visible, CONSTANT_10_SECONDS)
                .isDisplayed(), "Нет результатов поиска");

        while ($(byXpath(String.format(INSURANCE_ROW, rowsCounter))).isDisplayed()){
            if (!IDValue.isEmpty())
                Assert.assertTrue($(byXpath(String.format(INSURANCE_ROW_ID_VALUE, rowsCounter)))
                        .waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(IDValue),
                        "ID заказа не совпадает'");

            if (!statusValue.isEmpty())
                Assert.assertTrue($(byXpath(String.format(INSURANCE_ROW_STATUS_VALUE, rowsCounter)))
                        .waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(statusValue),
                        "Статус страховки не совпадает'");

            if (!externalIDValue.isEmpty())
                Assert.assertTrue($(byXpath(String.format(INSURANCE_ROW_EXTERNAL_ID_VALUE, rowsCounter)))
                        .waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(externalIDValue),
                        "Внешний ID заказа не совпадает'");

            SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
            SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

            if(!createDateFromValue.isEmpty())
                Assert.assertTrue(formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CREATE_DATE_VALUE, rowsCounter)))
                        .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(createDateFromValue)) ||
                        formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CREATE_DATE_VALUE, rowsCounter)))
                                .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(createDateFromValue)),
                        "Дата выписки страховки не совпадает");

            if(!createDateToValue.isEmpty())
                Assert.assertTrue(formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CREATE_DATE_VALUE, rowsCounter)))
                        .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(createDateToValue)) ||
                        formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CREATE_DATE_VALUE, rowsCounter)))
                                .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(createDateToValue)),
                        "Дата выписки страховки не совпадает");

            if(!cancelDateFromValue.isEmpty())
                Assert.assertTrue(formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CANCEL_DATE_VALUE, rowsCounter)))
                                .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(cancelDateFromValue)) ||
                                formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CANCEL_DATE_VALUE, rowsCounter)))
                                        .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(cancelDateFromValue)),
                        "Дата выписки страховки не совпадает");

            if(!cancelDateToValue.isEmpty())
                Assert.assertTrue(formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CANCEL_DATE_VALUE, rowsCounter)))
                                .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(cancelDateToValue)) ||
                                formatter1.parse($(byXpath(String.format(INSURANCE_ROW_CANCEL_DATE_VALUE, rowsCounter)))
                                        .waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(cancelDateToValue)),
                        "Дата аннуляции страховки страховки не совпадает");

            rowsCounter ++;
        }

        return this;
    }

}
