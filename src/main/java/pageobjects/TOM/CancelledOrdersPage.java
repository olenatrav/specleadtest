package pageobjects.TOM;

import com.codeborne.selenide.Selenide;
import framework.WebDriverCommands;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 03.07.2017.
 */
public class CancelledOrdersPage extends WebDriverCommands {

    private static CancelledOrdersPage cancelledOrdersPage = null;

    public static synchronized CancelledOrdersPage getCancelledOrdersPage()
    {
        if(cancelledOrdersPage == null)
            cancelledOrdersPage = new CancelledOrdersPage();

        return cancelledOrdersPage;
    }

    final private String CANCELLED_ORDERS_TABLE = "//div[@data-ng-if = 'data.items.list.length']";
    final private String CANCELLED_ORDERS_ID_ORDERS_ROW_VALUE = "//div[@data-ng-if = 'data.items.list.length']/.//tbody/tr[%s]/td[1]/a";
    final private String CANCELLED_ORDERS_NEXT_PAGE_ORDERS_BUTTON = "Next";
    final private String CANCELLED_ORDERS_N_PAGE_ORDERS_BUTTON = "//dr-pagination/.//li[%s]/a[contains(@ng-click, 'page.number')]";

    public CancelledOrdersPage checkIfOrderIDExistInСancelledOrdersQueue(String orderIDValue) throws Exception{
        $(byXpath(CANCELLED_ORDERS_TABLE)).waitUntil(visible, CONSTANT_10_SECONDS);
        int visiblePagesCounter = 1;
        int rowsCounter;
        boolean isElementPresent = false;
        if ($(byLinkText(CANCELLED_ORDERS_NEXT_PAGE_ORDERS_BUTTON)).exists()){
            while($(byXpath(String.format(CANCELLED_ORDERS_N_PAGE_ORDERS_BUTTON, visiblePagesCounter + 1))).exists())
                visiblePagesCounter ++;
        }
        int pagesCounter = Integer.parseInt($(byXpath(String.format(CANCELLED_ORDERS_N_PAGE_ORDERS_BUTTON, visiblePagesCounter))).getText());

        L1: for(int i = 1; i <= pagesCounter; i++) {
            rowsCounter = 1;
            while ($(byXpath(String.format(CANCELLED_ORDERS_ID_ORDERS_ROW_VALUE, rowsCounter))).isDisplayed()){
                if ($(byXpath(String.format(CANCELLED_ORDERS_ID_ORDERS_ROW_VALUE, rowsCounter))).getText().trim().equals(orderIDValue)){
                    isElementPresent = true;
                    break L1;
                }
                rowsCounter ++;
            }
            $(byLinkText(CANCELLED_ORDERS_NEXT_PAGE_ORDERS_BUTTON)).waitUntil(exist, CONSTANT_10_SECONDS).click();
            $(byXpath(CANCELLED_ORDERS_TABLE)).waitUntil(visible, CONSTANT_10_SECONDS);
        }

        Assert.assertTrue(isElementPresent, "Заказа нет в списке отмененных заказов");

        return this;
    }

}
