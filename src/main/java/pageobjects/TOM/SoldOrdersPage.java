package pageobjects.TOM;

import framework.WebDriverCommands;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 03.07.2017.
 */
public class SoldOrdersPage extends WebDriverCommands {

    private static SoldOrdersPage soldOrdersPage = null;

    public static synchronized SoldOrdersPage getSoldOrdersPage()
    {
        if(soldOrdersPage == null)
            soldOrdersPage = new SoldOrdersPage();

        return soldOrdersPage;
    }

    final private String SOLD_ORDERS_TABLE = "//div[@data-ng-if = 'data.items.list.length']";
    final private String SOLD_ORDERS_ID_ORDERS_ROW_VALUE = "//div[@data-ng-if = 'data.items.list.length']/.//tbody/tr[%s]/td[1]/a";
    final private String SOLD_ORDERS_NEXT_PAGE_ORDERS_BUTTON = "Next";
    final private String SOLD_ORDERS_N_PAGE_ORDERS_BUTTON = "//dr-pagination/.//li[%s]/a[contains(@ng-click, 'page.number')]";

    public SoldOrdersPage checkIfOrderIDExistInXancelledOrdersQueue(String orderIDValue) throws Exception{
        $(byXpath(SOLD_ORDERS_TABLE)).waitUntil(visible, CONSTANT_10_SECONDS);
        int visiblePagesCounter = 1;
        int rowsCounter;
        boolean isElementPresent = false;
        if ($(byLinkText(SOLD_ORDERS_NEXT_PAGE_ORDERS_BUTTON)).exists()){
            while($(byXpath(String.format(SOLD_ORDERS_N_PAGE_ORDERS_BUTTON, visiblePagesCounter + 1))).exists())
                visiblePagesCounter ++;
        }
        int pagesCounter = Integer.parseInt($(byXpath(String.format(SOLD_ORDERS_N_PAGE_ORDERS_BUTTON, visiblePagesCounter))).getText());

        L1: for(int i = 1; i <= pagesCounter; i++) {
            rowsCounter = 1;
            while ($(byXpath(String.format(SOLD_ORDERS_ID_ORDERS_ROW_VALUE, rowsCounter))).isDisplayed()){
                if ($(byXpath(String.format(SOLD_ORDERS_ID_ORDERS_ROW_VALUE, rowsCounter))).getText().trim().equals(orderIDValue)){
                    isElementPresent = true;
                    break L1;
                }
                rowsCounter ++;
            }
            $(byLinkText(SOLD_ORDERS_NEXT_PAGE_ORDERS_BUTTON)).waitUntil(exist, CONSTANT_10_SECONDS).click();
            $(byXpath(SOLD_ORDERS_TABLE)).waitUntil(visible, CONSTANT_10_SECONDS);
        }

        Assert.assertTrue(isElementPresent, "Заказа нет в списке проданных заказов");

        return this;
    }

}
