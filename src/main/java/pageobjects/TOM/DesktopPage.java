package pageobjects.TOM;

import framework.WebDriverCommands;


import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.ElementsCollection.*;

/**
 * Created by User on 18.05.2017.
 */
public class DesktopPage extends WebDriverCommands
{
    private static DesktopPage desktopPage = null;
    private DesktopPage()
    {
    }
    public static synchronized DesktopPage getDesktopPage()
    {
        if(desktopPage == null)
            desktopPage = new DesktopPage();

        return desktopPage;
    }

    final private String SEARCH_FORM_INPUT = "//input[@type = 'search']";
    final private String ORDER_FROM_SEARCH = "ui-select-choices-row-inner";
    final private String SIDE_MENU_QUEUE_PAGE_BUTTON = "//span[text() = '%s']";


    public DesktopPage selectSideMenuOption(String option) throws Exception
    {
        $(byXpath(String.format(SIDE_MENU_QUEUE_PAGE_BUTTON, option))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        
        return this;
    }


}
