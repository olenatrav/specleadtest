package pageobjects.TOM;

import com.codeborne.selenide.Selenide;
import framework.WebDriverCommands;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byLinkText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 03.07.2017.
 */
public class DashboardPage extends WebDriverCommands {

    private static DashboardPage dashboardPage = null;
    private DashboardPage()
    {
    }
    public static synchronized DashboardPage getDashboardPage()
    {
        if(dashboardPage == null)
            dashboardPage = new DashboardPage();

        return dashboardPage;
    }

    public DashboardPage goToDashboardPage()
    {
        Selenide.open("http://tom.travellata.ru/#/dashboard/seller/12");

        return this;
    }

    final private String ORDERS_IN_WORK_SPINNER = "//h5[contains(text(), 'Заказы в работе')]/i[contains(@class, 'fa-spinner')]";

    final private String ACTIONS_TABLE = "//div[@data-ng-if = 'actionList.data.items.actions.length']";
    final private String ACTIONS_ID_ORDERS_ROW_VALUE = "//div[@data-ng-if = 'actionList.data.items.actions.length']/.//tbody/tr[%s]/td[1]/a";
    final private String ACTIONS_NEXT_ACTION_ORDERS_ROW_VALUE = "//div[@data-ng-if = 'actionList.data.items.actions.length']/.//tbody/tr[%s]/td[4]";
    final private String ACTIONS_NEXT_PAGE_ORDERS_BUTTON = "Next";
    final private String ACTIONS_N_PAGE_ORDERS_BUTTON = "//dr-pagination/.//li[%s]/a[contains(@ng-click, 'page.number')]";

    final private String ORDERS_IN_WORK_TABLE = "//div[@data-ng-if = 'job.job.length']";
    final private String ORDERS_IN_WORK_ID_ROW_VALUE = "//div[@data-ng-if = 'job.job.length']/.//tbody/tr[%s]/td[1]/a";

    final private String ORDERS_WAIT_FOR_PAYMENT = "//div[@data-ng-if = 'pay.pay.length']";
    final private String ORDERS_WAIT_FOR_PAYMENT_ID_ROW_VALUE = "//div[@data-ng-if = 'pay.pay.length']/.//tbody/tr[%s]/td[1]/a";

    final private String SOLD_ORDERS_PAGE_LINK = "//a[contains(@href, 'Проданные заказы')]";
    final private String CANCELLED_ORDERS_PAGE_LINK = "//a[contains(@href, 'Отмененные заказы')]";

    public DashboardPage waitForOrdersLoad() throws Exception{
        $(byXpath(ORDERS_IN_WORK_SPINNER)).waitUntil(disappear, CONSTANT_10_SECONDS);
        return this;
    }

    public DashboardPage checkIfOrderIDExistInActionsQueue(String orderIDValue, String nextActionTextValue) throws Exception{
        $(byXpath(ACTIONS_TABLE)).waitUntil(visible, CONSTANT_10_SECONDS);
        int visiblePagesCounter = 1;
        int rowsCounter = 1;
        boolean isElementPresent = false;
        if ($(byLinkText(ACTIONS_NEXT_PAGE_ORDERS_BUTTON)).exists()){
            while($(byXpath(String.format(ACTIONS_N_PAGE_ORDERS_BUTTON, visiblePagesCounter + 1))).exists())
                visiblePagesCounter ++;
        }
        int pagesCounter = Integer.parseInt($(byXpath(String.format(ACTIONS_N_PAGE_ORDERS_BUTTON, visiblePagesCounter))).getText());

        L1: for(int i = 1; i <= pagesCounter; i++) {
            rowsCounter = 1;
            while ($(byXpath(String.format(ACTIONS_ID_ORDERS_ROW_VALUE, rowsCounter))).isDisplayed()){
                if ($(byXpath(String.format(ACTIONS_ID_ORDERS_ROW_VALUE, rowsCounter))).getText().trim().equals(orderIDValue)){
                    isElementPresent = true;
                    break L1;
                }
                rowsCounter ++;
            }
            $(byLinkText(ACTIONS_NEXT_PAGE_ORDERS_BUTTON)).waitUntil(exist, CONSTANT_10_SECONDS).click();
            $(byXpath(ACTIONS_TABLE)).waitUntil(visible, CONSTANT_10_SECONDS);
        }

        Assert.assertTrue(isElementPresent, "Заказа нет в списке действий");
        Assert.assertTrue($(byXpath(String.format(ACTIONS_NEXT_ACTION_ORDERS_ROW_VALUE, rowsCounter))).getText().equals(nextActionTextValue),
                "Следующее действие не совпадает");

        return this;
    }

    public DashboardPage checkIfOrderIDExistInWorkQueue(String orderIDValue) throws Exception{
        $(byXpath(ORDERS_IN_WORK_TABLE)).waitUntil(visible, CONSTANT_20_SECONDS);
        int rowsCounter = 1;
        boolean isElementPresent = false;
        while ($(byXpath(String.format(ORDERS_IN_WORK_ID_ROW_VALUE, rowsCounter))).isDisplayed()){
            if ($(byXpath(String.format(ORDERS_IN_WORK_ID_ROW_VALUE, rowsCounter))).getText().equals(orderIDValue)){
                isElementPresent = true;
                break;
            }
            rowsCounter ++;
        }
        Assert.assertTrue(isElementPresent, "Заказа нет в списке заказов в работе");

        return this;
    }

    public DashboardPage checkIfOrderIDExistInPaymentQueue(String orderIDValue) throws Exception{
        $(byXpath(ORDERS_WAIT_FOR_PAYMENT)).waitUntil(visible, CONSTANT_10_SECONDS);
        int rowsCounter = 1;
        boolean isElementPresent = false;
        while ($(byXpath(String.format(ORDERS_WAIT_FOR_PAYMENT_ID_ROW_VALUE, rowsCounter))).isDisplayed()){
            if ($(byXpath(String.format(ORDERS_WAIT_FOR_PAYMENT_ID_ROW_VALUE, rowsCounter))).getText().equals(orderIDValue)){
                isElementPresent = true;
                break;
            }
            rowsCounter ++;
        }
        Assert.assertTrue(isElementPresent, "Заказа нет в списке заказов на оплату");

        return this;
    }

    public CancelledOrdersPage gotoCancelledOrdersPage() throws Exception{
        $(byXpath(CANCELLED_ORDERS_PAGE_LINK)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new CancelledOrdersPage();
    }

    public SoldOrdersPage gotoSoldOrdersPage() throws Exception{
        $(byXpath(SOLD_ORDERS_PAGE_LINK)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new SoldOrdersPage();
    }

}
