package pageobjects.MDA;

import framework.WebDriverCommands;
import org.openqa.selenium.By;

/**
 * Created by User on 07.09.2016.
 */
public class AttendancePage extends WebDriverCommands
{
    private final String WITHOUT_ATTENDANCE_BUTTON = "//a[contains(text(), 'посещаемости')]";

    public MDAMainPage clickWitoutAttendanceButton()
    {
        By byWithoutAttendanceButton = By.xpath(WITHOUT_ATTENDANCE_BUTTON);
        if (isElementPresent(byWithoutAttendanceButton) == true)
        {
            waitForElementDisplayed(byWithoutAttendanceButton, CONSTANT_3_SECONDS);
            click(byWithoutAttendanceButton);
        }

        return new MDAMainPage();

    }
}
