package pageobjects.MDA;

import framework.WebDriverCommands;
import org.openqa.selenium.By;

/**
 * Created by User on 07.09.2016.
 */
public class OrderSerpPage extends WebDriverCommands
{
    private final String LAST_ORDER = "//a[contains(@title, '�������������� ������')]";
    private final String LAST_ORDER_CHECK_BOX = "massCheckBox_order_list";/*id*/
    private final String ORDER_ACTION_LIST_DROPDOWN = "massCheckBox_order_list";/*id*/
    private final String TRANSMISSION = "//option[text(), '��������']";

    public OrderSerpPage()
    {
        switchToNewTab(2);
    }

    public OrderSerpPage chooseOrder()
    {
        By byChooseOrder = By.id(LAST_ORDER_CHECK_BOX);
        waitForElementDisplayed(byChooseOrder, CONSTANT_3_SECONDS);
        click(byChooseOrder);

        return this;
    }

    public OrderSerpPage selectActionListDropdown()
    {
        By byActionListDropdown = By.xpath(ORDER_ACTION_LIST_DROPDOWN);
        waitForElementDisplayed(byActionListDropdown, CONSTANT_3_SECONDS);
        click(byActionListDropdown);

        return this;
    }


/*

    public OrderPage selectOrder(String customerName, String customerEmail, String customerPhone, String departureCity, String comment)
    {
        String[]  zz2 = {"r", "2"};
        HotelPage hotel = new HotelPage();

        By byLastOrder = By.xpath(LAST_ORDER);
        waitForElementDisplayed(byLastOrder, CONSTANT_3_SECONDS);
        click(byLastOrder);

        String[] data = hotel.data;


        return new OrderPage(hotel.data[0], hotel.data[1], hotel.data[2], hotel.data[3], hotel.data[4], hotel.data[5], hotel.data[6], hotel.data[7], hotel.data[8], hotel.data[9], hotel.data[10],
                hotel.data[11], hotel.data[12], hotel.data[13], hotel.data[14], hotel.data[15], customerName, customerEmail, customerPhone, departureCity, comment);
*/

        /*return new OrderPage("test", hotel.data[1], "811111111111", "autotest@mailforspam.com", hotel.data[2], hotel.data[0], "������", hotel.data[6], hotel.data[7], hotel.data[8], hotel.data[5] + " " + hotel.data[4], "60001.00", "��������",
                "�� ��������", "�� ���������", "7", "08.11.2015", "15.11.2015", "TRAVELATA.RU", "Sunmar RU", "���������", "1");*/
        //hotelValue, countryValue, resortValue, tourOperatorValue, roomValue, roomCodeValue, feedingValue, adultsValue, childrenValue, infantsValue, dateFromValue, dateToValue, finalPriceValue, priceValue, fuelTaxValue
    }

