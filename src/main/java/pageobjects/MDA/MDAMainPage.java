package pageobjects.MDA;

import framework.WebDriverCommands;
import org.openqa.selenium.By;

/**
 * Created by User on 07.09.2016.
 */
public class MDAMainPage extends WebDriverCommands
{
    private final String DESKTOP_DROPDOWN = "//a[contains(text(), '������� ����')]";
    private final String MANAGER_DESKTOP = "//a[contains(text(), '������� ���� ���������')]";

    public MDAMainPage selectDesktopDropdown()
    {
        By byDesktopDropdown = By.xpath(DESKTOP_DROPDOWN);
        waitForElementDisplayed(byDesktopDropdown, CONSTANT_3_SECONDS);
        click(byDesktopDropdown);

        return this;
    }

    public DesktopPage selectManagerDesktopOption()
    {
        By byManagerDesktopOption = By.xpath(MANAGER_DESKTOP);
        waitForElementDisplayed(byManagerDesktopOption, CONSTANT_3_SECONDS);
        click(byManagerDesktopOption);

        return new DesktopPage();
    }
}
