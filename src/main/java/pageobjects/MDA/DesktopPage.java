package pageobjects.MDA;

import framework.WebDriverCommands;
import org.openqa.selenium.By;

/**
 * Created by User on 07.09.2016.
 */
public class DesktopPage extends WebDriverCommands
{
    private final String EMAIL_FIELD = "s_customer_surname_or_email";/*id*/

    private final String SUBMIT_SEARCH_BUTTON = "submit"; /*id*/

    public DesktopPage enterEmail()
    {
        By byEmailField = By.id(EMAIL_FIELD);
        waitForElementDisplayed(byEmailField, CONSTANT_3_SECONDS);
        click(byEmailField);
        clear(byEmailField);
        sendKeys(byEmailField, "autotest@mailforspam.com");

        return this;
    }

    public OrderSerpPage clickLoginButton()
    {
        By byLoginButton = By.id(SUBMIT_SEARCH_BUTTON);
        waitForElementDisplayed(byLoginButton, CONSTANT_3_SECONDS);
        click(byLoginButton);

        return new OrderSerpPage();
    }
}
