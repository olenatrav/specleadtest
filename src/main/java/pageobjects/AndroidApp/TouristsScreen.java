package pageobjects.AndroidApp;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import framework.AndroidTestCase;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
/**
 * Created by User on 07.08.2017.
 */
public class TouristsScreen extends AndroidTestCase
{
    private static TouristsScreen touristsScreen = null;
    private TouristsScreen() throws  Exception

    {
    }
    public static synchronized TouristsScreen getTouristsScreen() throws  Exception
    {

        if(touristsScreen == null)
        {
            touristsScreen = new TouristsScreen();
        }


        return touristsScreen;
    }

    private final String SCREEN_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String SCREEN_TITLE_VALUE = "Кто едет";
    private final String ADULTS_LABEL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_adult_title']";
    private final String ADULTS_ADD_REMOVE_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/btn_adult']";
    private final String CHILDREN_LABEL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_child_title']";
    private final String CHILDREN_ADD_REMOVE_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/btn_child']";
    private final String INFANTS_LABEL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_infants_title']";
    private final String INFANTS_ADD_REMOVE_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/btn_infants']";
    private final String SUBMIT_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_select']";
    private final String BACK_BUTTON = "//android.widget.LinearLayout[@resource-id = 'ru.travelata.app:id/btn_back']";

    public TouristsScreen checkTitle()
    {
        Assert.assertTrue($(byXpath(SCREEN_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(SCREEN_TITLE_VALUE));

        return this;
    }
    public TouristsScreen tapBackButton()
    {
        //$(byXpath(BACK_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(BACK_BUTTON);

        return this;
    }
    public TouristsScreen selectAdults(String adults)
    {
        if(Integer.parseInt($(byXpath(ADULTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) > Integer.parseInt(adults))
        {
            int leftX = $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
            int rightX = leftX + $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth();
            int middleX = ((rightX + leftX) / 2 + leftX) / 2;
            int upperY = $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
            int lowerY = upperY + $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
            int middleY = (upperY + lowerY) / 2;

            while (Integer.parseInt($(byXpath(ADULTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) != Integer.parseInt(adults))
            {
                tapByCoordinates(middleX, middleY);
            }

        }

        if(Integer.parseInt($(byXpath(ADULTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) < Integer.parseInt(adults))
        {
            int leftX = $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
            int rightX = leftX + $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth();
            int middleX = ((rightX + leftX) / 2 + rightX) / 2;
            int upperY = $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
            int lowerY = upperY + $(byXpath(ADULTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
            int middleY = (upperY + lowerY) / 2;

            while (Integer.parseInt($(byXpath(ADULTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) != Integer.parseInt(adults))
            {
                tapByCoordinates(middleX, middleY);
            }

        }


        return this;
    }

    public TouristsScreen selectChildren(String children)
    {
        if(!(children.equals("")))
        {
            if(Integer.parseInt($(byXpath(CHILDREN_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) > Integer.parseInt(children))
            {
                int leftX = $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
                int rightX = leftX + $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth();
                int middleX = ((rightX + leftX) / 2 + leftX) / 2;
                int upperY = $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
                int lowerY = upperY + $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
                int middleY = (upperY + lowerY) / 2;

                while (Integer.parseInt($(byXpath(CHILDREN_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) != Integer.parseInt(children))
                {
                    tapByCoordinates(middleX, middleY);
                }

            }

            if(Integer.parseInt($(byXpath(CHILDREN_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) < Integer.parseInt(children))
            {
                int leftX = $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
                int rightX = leftX + $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth();
                int middleX = ((rightX + leftX) / 2 + rightX) / 2;
                int upperY = $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
                int lowerY = upperY + $(byXpath(CHILDREN_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
                int middleY = (upperY + lowerY) / 2;

                while (Integer.parseInt($(byXpath(CHILDREN_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) != Integer.parseInt(children))
                {
                    tapByCoordinates(middleX, middleY);
                }

            }

        }


        return this;
    }

    public TouristsScreen selectInfants(String infants)
    {
        if(!(infants.equals("")))
        {
            if(Integer.parseInt($(byXpath(INFANTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) > Integer.parseInt(infants))
            {
                int leftX = $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
                int rightX = leftX + $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth();
                int middleX = ((rightX + leftX) / 2 + leftX) / 2;
                int upperY = $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
                int lowerY = upperY + $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
                int middleY = (upperY + lowerY) / 2;

                while (Integer.parseInt($(byXpath(INFANTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) != Integer.parseInt(infants))
                {
                    tapByCoordinates(middleX, middleY);
                }

            }

            if(Integer.parseInt($(byXpath(INFANTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) < Integer.parseInt(infants))
            {
                int leftX = $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
                int rightX = leftX + $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth();
                int middleX = ((rightX + leftX) / 2 + rightX) / 2;
                int upperY = $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
                int lowerY = upperY + $(byXpath(INFANTS_ADD_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
                int middleY = (upperY + lowerY) / 2;

                while (Integer.parseInt($(byXpath(INFANTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "")) != Integer.parseInt(infants))
                {
                    tapByCoordinates(middleX, middleY);
                }

            }

        }

        return this;
    }

    public TouristsScreen selectPassengers(String adults, String children, String infants)
    {
        selectAdults(adults)
        .selectChildren(children)
        .selectInfants(infants)
        .tapSubmitButton();

        $(byXpath(SUBMIT_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public TouristsScreen tapSubmitButton()
    {
        tapElement(SUBMIT_BUTTON);

        return this;
    }


}
