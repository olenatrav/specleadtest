package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 31.07.2017.
 */
public class PreCheckoutScreen extends AndroidTestCase
{
    private static PreCheckoutScreen preCheckoutScreen = null;
    private PreCheckoutScreen() throws  Exception

    {
    }
    public static synchronized PreCheckoutScreen getPreCheckoutScreen() throws  Exception
    {

        if(preCheckoutScreen == null)
        {
            preCheckoutScreen = new PreCheckoutScreen();
        }


        return preCheckoutScreen;
    }

    private final String PROGRESS_ICON = "//android.view.View[@resource-id = 'ru.travelata.app:id/progress']";
    private final String PROGRESS_TEXT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_progress_message']";

    private final String BOOK_TOUR_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_show_tours']";
    private final String PRICE_CHANGE_POP_UP = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/snackbar_text']";
    private final String DISCOUNT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_deals']";
    private final String ROOM = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_room']";
    private final String MEAL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_meal']";
    private final String TO_LOGO = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_operator_logo']";
    private final String DATE_FROM = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_date_forward']";
    private final String DATE_TO = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_date_backward']";
    private final String NIGHTS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_nights']";
    private final String PASSENGERS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_persons']";
    private final String CONSIERGE_CHECKBOX = "//android.widget.CheckBox[@resource-id = 'ru.travelata.app:id/cb_consierge']";
    private final String INFO_ORIGINAL_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_original_price']";
    private final String INFO_DISCOUNT_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_deal_title']";
    private final String INFO_DISCOUNT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_deal']";
    private final String INFO_OIL_TAX = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_oil_tax']";
    private final String FINAL_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_final_price']";




    private final String PAID_ORDER_BLOCK = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_order_complete']";
    private final String PAID_ORDER_TEXT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_tour_is_order']";
    private final String PAID_ORDER_TEXT_VALUE = "Оплачено";

    private final String OLD_TOUR_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_title']";
    private final String OLD_TOUR_TITLE_VALUE = "Тур устарел";
    private final String OLD_TOUR_TEXT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_description']";
    private final String OLD_TOUR_TEXT_VALUE = "К сожалению, данный тур устарел. Пожалуйста, вернитесь на страницу отеля и выберите другой тур.";
    private final String SELECT_NEW_TOUR_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_ok']";

    public static String oilTaxValue;

    public PreCheckoutScreen tapBookTourButton()
    {

        if($(byXpath(PROGRESS_ICON)).is(exist))
        {
            $(byXpath(PROGRESS_ICON)).waitUntil(not(visible), CONSTANT_10_SECONDS);
        }


        if($(byXpath(PRICE_CHANGE_POP_UP)).is(exist) && $(byXpath(PRICE_CHANGE_POP_UP)).is(visible))
        {
            $(byXpath(PRICE_CHANGE_POP_UP)).waitUntil(not(visible), CONSTANT_20_SECONDS);
        }
        //$(byXpath(BOOK_TOUR_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(BOOK_TOUR_BUTTON);
        $(byXpath(BOOK_TOUR_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);


        return this;
    }

    public PreCheckoutScreen checkPaidBlock()
    {
        Assert.assertTrue($(byXpath(PAID_ORDER_BLOCK)).is(visible));
        Assert.assertTrue($(byXpath(PAID_ORDER_TEXT)).getText().equals(PAID_ORDER_TEXT_VALUE));

        return this;
    }

    public PreCheckoutScreen checkPreCheckoutScreen(String room, String meal, String discount, String dateFrom, String dateTo, String nights, String passengers, String originalPrice, String tourPrice)
    {
        Assert.assertTrue($(byXpath(ROOM)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(room));
        Assert.assertTrue($(byXpath(MEAL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(meal));
        Assert.assertTrue($(byXpath(TO_LOGO)).is(exist) && $(byXpath(TO_LOGO)).is(visible));
        if($(byXpath(DISCOUNT)).is(visible))
        {
            Assert.assertTrue($(byXpath(DISCOUNT)).getText().equals(discount));
        }
        Assert.assertTrue($(byXpath(DATE_FROM)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(dateFrom));
        Assert.assertTrue($(byXpath(DATE_TO)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(dateTo));

        swipeFromBottomToTop(1);

        Assert.assertTrue($(byXpath(NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "").equals(nights));
        Assert.assertTrue($(byXpath(PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(passengers));

       /* swipeFromBottomToTop(8);

        if(!discount.equals(""))
        {
            Assert.assertTrue($(byXpath(INFO_ORIGINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "").contains(originalPrice));
            Assert.assertTrue($(byXpath(INFO_DISCOUNT_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(discount));
        }
        else
        {
            Assert.assertTrue($(byXpath(INFO_ORIGINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "").contains(tourPrice));
        }
        oilTaxValue = $(byXpath(INFO_OIL_TAX)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "").replaceAll("\\s.*", "");

        if(!discount.equals(""))
        {
            Assert.assertTrue($(byXpath(FINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "").replaceAll("\\s", "")
                    .equals(Integer.toString(Integer.parseInt(originalPrice) -
                            Integer.parseInt(oilTaxValue) -
                            Integer.parseInt($(byXpath(INFO_DISCOUNT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "").replaceAll("\\s.*", "")))));
        }
        else
        {

            Assert.assertTrue($(byXpath(FINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "").replaceAll("\\s.*", "")
                    .equals(Integer.toString(Integer.parseInt(tourPrice) -
                            Integer.parseInt(oilTaxValue))));
        }
*/

        return this;
    }


}
