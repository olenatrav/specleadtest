package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by User on 26.07.2017.
 */
public class SecondSplashScreen extends AndroidTestCase
{

    private static SecondSplashScreen secondSplashScreen = null;
    private SecondSplashScreen() throws  Exception

    {
    }
    public static synchronized SecondSplashScreen getSecondSplashScreen() throws  Exception
    {

        if(secondSplashScreen == null)
        {
            secondSplashScreen = new SecondSplashScreen();
        }


        return secondSplashScreen;
    }

    private final String LAYOUT = "//android.widget.RelativeLayout[@index = '0']";
    private final String HEADER = "//android.view.View[@index = '0']";

    private final String SKIP_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_break']";
    private final String SKIP_BUTTON_TEXT_VALUE = "ПРОПУСТИТЬ";
    private final String STRING_LABEL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_message']";
    private final String STRING_VALUE = "Поиск лучших цен - экономия вашего времени и денег!";
    private final String CENTRAL_IMAGE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_demo']";
    private final String BOTTOM_INDICATOR = "//android.view.View[@resource-id = 'ru.travelata.app:id/indicator_demo']";


    public SecondSplashScreen checkSplashScreen()
    {
        $(byXpath(HEADER)).waitUntil(visible, CONSTANT_10_SECONDS);
        $(byXpath(LAYOUT)).waitUntil(visible, CONSTANT_10_SECONDS);
        Assert.assertTrue($(byXpath(SKIP_BUTTON)).is(visible), "second SplashScreen: SKIP_BUTTON is not visible");
        Assert.assertTrue($(byXpath(SKIP_BUTTON)).getText().equals(SKIP_BUTTON_TEXT_VALUE), "second SplashScreen: SKIP_BUTTON_TEXT is not correct");
        Assert.assertTrue($(byXpath(STRING_LABEL)).is(visible), "second SplashScreen: STRING_LABEL is not visible");
        Assert.assertTrue($(byXpath(STRING_LABEL)).getText().equals(STRING_VALUE), "second SplashScreen: STRING_VALUE is not correct");
        Assert.assertTrue($(byXpath(CENTRAL_IMAGE)).is(visible), "second SplashScreen: CENTRAL_IMAGE is not visible");
        Assert.assertTrue($(byXpath(BOTTOM_INDICATOR)).is(visible), "second SplashScreen: BOTTOM_INDICATOR is not visible");

        return this;

    }
}
