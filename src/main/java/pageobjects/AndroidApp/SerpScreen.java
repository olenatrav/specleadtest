package pageobjects.AndroidApp;

import com.codeborne.selenide.Selenide;
import framework.AndroidTestCase;
import org.testng.Assert;

import java.util.Random;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 28.07.2017.
 */
public class SerpScreen extends AndroidTestCase
{
    private static SerpScreen serpScreen = null;
    private SerpScreen() throws  Exception

    {
    }
    public static synchronized SerpScreen getSerpScreen() throws  Exception
    {

        if(serpScreen == null)
        {
            serpScreen = new SerpScreen();
        }


        return serpScreen;
    }

    private final String LOADING_IMAGE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_progress']";
    private final String LOADING_TEXT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_filters']";
    private final String LOADING_TEXT_VALUE = "Более точно настраивайте критерии поиска, чтобы мы нашли для вас наиболее подходящие для вас предложения. Это значительно облегчит поиск.";

    private final String BACK_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_back']";
    private final String RESORT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_title_resort']";
    private final String DATE_AND_NIGHTS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_title']";
    private final String MAP_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/btn_map']";
    private final String TOURHUNTER_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_torhunter']";
    private final String HOTEL_BLOCK = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_hotel']";
    private final String HOTEL = "//android.widget.LinearLayout[@resource-id = 'ru.travelata.app:id/ll_price']";
    private final String HOTEL_NAME = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hotel_name']";
    private final String HOTEL_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price']";


    private final String BOTTOM_INFO_POP_UP = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/snackbar_text']";

    private final String EMPTY_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_empty_title']";
    private final String EMPTY_TITLE_VALUE = "Мы не нашли для вас туров";
    private final String EMPTY_DESCRIPTION = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_empty_description']";
    private final String EMPTY_DESCRIPTION_VALUE = "Попробуйте выбрать другие критерии поиска";
    private final String CHANGE_CRITERIA_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_new_criteria']";

    private final String TOUR_HUNTER_CLOSE_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_close']";
    private final String TOUR_HUNTER_EMAIL_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_email']";
    private final String TOUR_HUNTER_DISPATCH_CHECKBOX = "//android.widget.CheckBox[@resource-id = 'ru.travelata.app:id/cb_is_news']";
    private final String TOUR_HUNTER_SUBMIT_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_send']";
    private final String TOUR_HUNTER_SUCCESS_TITLE = "//android.widget.TextView[@text = 'Подписка оформлена!']";
    private final String TOUR_HUNTER_SUCCESS_MESSAGE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_message']";
    private final String TOUR_HUNTER_SUCCESS_DOG_IMAGE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_dog']";
    private final String TOUR_HUNTER_OK_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_ok']";

    private final String COUNTRY_HUNTER_IMAGE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_tourhunter']";
    private final String COUNTRY_HUNTER_BUTTON = "//android.widget.TextView[@text = 'Следить за ценой на страну']";

    private final String SORTING_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_show_sorting']";
    private final String SORT_POPULAR = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_recomend']";
    private final String SORT_CHEAP_FIRST = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_price_up']";
    private final String SORT_EXPENSIVE_FIRST = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_price_down']";
    private final String SORT_RATING = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_rating']";

    private final String FILTERS_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_show_filters']";
    private final String FILTERS_CLEAR_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_clear']";
    private final String FILTERS_NEAR_TO_BEACH = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_near_to_beach']";
    private final String FILTERS_SELECTED_BEACH = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_beach']";
    private final String FILTERS_RESORT = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_resort']";
    private final String FILTERS_RESORT_VALUE = "//android.widget.TextView[@text = '%s']";
    private final String FILTERS_SELECTED_RESORT = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_resort']";
    private final String FILTERS_HOTEL_CLASS = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_hotel_class']";
    private final String FILTERS_HOTEL_CLASS_FIVE_STARS = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_five_stars']";
    private final String FILTERS_HOTEL_CLASS_FOUR_STARS = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_fore_stars']";
    private final String FILTERS_HOTEL_CLASS_THREE_STARS = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_three_stars']";
    private final String FILTERS_HOTEL_CLASS_TWO_STARS = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_two_stars']";
    private final String FILTERS_HOTEL_CLASS_APTS = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_appartaments']";
    private final String FILTERS_SELECTED_HOTEL_CLASS = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_hotel_class']";
    private final String FILTERS_MEAL_TYPE = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_meal']";
    private final String FILTERS_MEAL_UAI = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_uai']";
    private final String FILTERS_MEAL_AI = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_ai']";
    private final String FILTERS_MEAL_BB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_bb']";
    private final String FILTERS_MEAL_FB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_fb']";
    private final String FILTERS_MEAL_HB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hb']";
    private final String FILTERS_MEAL_RO = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_ro']";
    private final String FILTERS_SELECTED_MEAL = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_meal']";
    private final String FILTERS_PRICE = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_price']";
    private final String FILTERS_PRICE_SEEKBAR = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/sb_price']";
    private final String FILTERS_PRICE_INTERVAL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price']";
    private final String FILTERS_SELECTED_PRICE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_price']";
    private final String FILTERS_ATTRIBUTES = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_atributes']";
    private final String FILTERS_ATTRIBUTE_CHILDREN = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_child']";
    private final String FILTERS_ATTRIBUTE_CONDITION = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_condition']";
    private final String FILTERS_ATTRIBUTE_WIFI = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_wifi']";
    private final String FILTERS_ATTRIBUTE_COUPLES = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_couples']";
    private final String FILTERS_ATTRIBUTE_ADULTS_ONLY = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_adults']";
    private final String FILTERS_ATTRIBUTE_SAND_BEACH = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_sand_beach']";
    private final String FILTERS_ATTRIBUTE_PEBBLE_BEACH = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_pebble_beach']";
    private final String FILTERS_ATTRIBUTE_SAND_PEBBLE_BEACH = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_sand_pebble_beach']";
    private final String FILTERS_ATTRIBUTE_WELLNESS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_wellness']";
    private final String FILTERS_SELECTED_ATTRIBUTES = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_atributes']";
    private final String FILTERS_CHILDREN_ATTRIBUTES = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_atributes_kids']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_ANIMATION = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_animation']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_POOL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_pool']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_WATERSLIDES = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_waterslides']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_AQUAPARK = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_aquapark']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_SEA_ENTRANCE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_sea']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_FOOD = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_food']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_BED = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_bed']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_NANNY = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_nanny']";
    private final String FILTERS_CHILDREN_ATTRIBUTES_CLUBS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_clubs']";
    private final String FILTERS_SELECTED_CHILDREN_ATTRIBUTES = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_atributes_kids']";
    private final String FILTERS_RATING = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_rating']";
    private final String FILTERS_RATING_VERY_GOOD = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_very_good']";
    private final String FILTERS_RATING_GOOD = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_good']";
    private final String FILTERS_RATING_NORMAL = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_normal']";
    private final String FILTERS_RATING_BAD = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_bad']";
    private final String FILTERS_SELECTED_RATING = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_selected_rating']";
    private final String FILTERS_SUBMIT_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_select']";


    public static String tourHunterPriceValue = "";
    public static String tourHunterHotelName = "";
    private static int swipeCount = 1;

    public static boolean nearBeachIsSelected = false;
    public static boolean resortIsSelected = false;
    public static boolean hotelClassIsSelected = false;
    public static boolean mealTypeIsSelected = false;
    public static String priceValue = "";
    public static boolean attributesIsSelected = false;
    public static boolean childrenAttributesIsSelected = false;
    public static String ratingValue = "";


    public SerpScreen tapFiltersButton()
    {
        tapElement(FILTERS_BUTTON);

        return this;
    }
    public SerpScreen tapFiltersAcceptButton()
    {
        tapElement(FILTERS_SUBMIT_BUTTON);
        Selenide.sleep(CONSTANT_2_SECONDS);

        return this;
    }
    public SerpScreen tapClearFiltersButton()
    {
        tapElement(FILTERS_CLEAR_BUTTON);

        return this;
    }
    public SerpScreen selectFilterNearBeach(boolean bool)
    {
        if(bool)
        {
            tapElement(FILTERS_NEAR_TO_BEACH);

            Assert.assertTrue($(byXpath(FILTERS_SELECTED_BEACH)).is(visible));

            nearBeachIsSelected = true;
        }

        return this;
    }
    public SerpScreen selectFilterResorts(String[] resorts)
    {
        if(resorts.length > 0)
        {
            tapElement(FILTERS_RESORT);

            int limit = 0;
            tapElement(FILTERS_CLEAR_BUTTON);
            for(int i = 0; i < resorts.length; i++)
            {
                while($(byXpath(String.format(FILTERS_RESORT_VALUE, resorts[i]))).is(not(visible)))
                {
                    swipeFromBottomToTop(1);
                    limit++;
                }
                //$(byXpath(String.format(FILTERS_RESORT_VALUE, resorts[i]))).click();
                tapElement(String.format(FILTERS_RESORT_VALUE, resorts[i]));
                if(!(i == resorts.length - 1))
                {
                    swipeFromTopToBottom(limit);
                    limit = 0;
                }
            }

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_RESORT)).is(visible));

            resortIsSelected = true;
        }



        return this;
    }
    public SerpScreen selectFilterHotelClass(String[] hotelClass)
    {
        if(hotelClass.length > 0)
        {
            tapElement(FILTERS_HOTEL_CLASS);

            tapElement(FILTERS_CLEAR_BUTTON);
            for(int i = 0; i < hotelClass.length; i++)
            {
                if(hotelClass[i].equals("5"))
                {
                    tapElement(FILTERS_HOTEL_CLASS_FIVE_STARS);
                }
                else
                if(hotelClass[i].equals("4"))
                {
                    tapElement(FILTERS_HOTEL_CLASS_FOUR_STARS);
                }
                else
                if(hotelClass[i].equals("3"))
                {
                    tapElement(FILTERS_HOTEL_CLASS_THREE_STARS);
                }
                else
                if(hotelClass[i].equals("2"))
                {
                    tapElement(FILTERS_HOTEL_CLASS_TWO_STARS);
                }
                else
                if(hotelClass[i].equals("apts"))
                {
                    tapElement(FILTERS_HOTEL_CLASS_APTS);
                }

            }

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_HOTEL_CLASS)).is(visible));

            hotelClassIsSelected = true;
        }



        return this;
    }
    public SerpScreen selectFilterMealType(String[] mealType)
    {
        if(mealType.length > 0)
        {
            tapElement(FILTERS_MEAL_TYPE);

            tapElement(FILTERS_CLEAR_BUTTON);

            for(int i = 0; i < mealType.length; i++)
            {
                if(mealType[i].equals("UAI"))
                {
                    tapElement(FILTERS_MEAL_UAI);
                }
                else
                if(mealType[i].equals("AI"))
                {
                    tapElement(FILTERS_MEAL_AI);
                }
                else
                if(mealType[i].equals("BB"))
                {
                    tapElement(FILTERS_MEAL_BB);
                }
                else
                if(mealType[i].equals("FB"))
                {
                    tapElement(FILTERS_MEAL_FB);
                }
                else
                if(mealType[i].equals("HB"))
                {
                    tapElement(FILTERS_MEAL_HB);
                }
                else
                if(mealType[i].equals("RO"))
                {
                    tapElement(FILTERS_MEAL_RO);
                }
            }

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_MEAL)).is(visible));

            mealTypeIsSelected = true;
        }



        return this;
    }

    public SerpScreen selectFilterPrice(boolean bool)
    {
        if(bool)
        {
            tapElement(FILTERS_PRICE);

            int xleft = $(byXpath(FILTERS_PRICE_SEEKBAR)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
            int upperY = $(byXpath(FILTERS_PRICE_SEEKBAR)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
            int lowerY = upperY + $(byXpath(FILTERS_PRICE_SEEKBAR)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
            int middleY = (upperY + lowerY) / 2;

            Random r = new Random();
            double dd = 0.1 + 0.1 * r.nextDouble();
            double d =  xleft + ($(byXpath(FILTERS_PRICE_SEEKBAR)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth() * dd);
            int x = (int) d;

            tapByCoordinates(x, middleY);

            String minPrice = $(byXpath(FILTERS_PRICE_INTERVAL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s-.*", "");

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_PRICE)).is(visible));

            priceValue = minPrice;

        }

        return this;
    }

    public SerpScreen selectFilterAttribute(String[] attributes)
    {
        if(attributes.length > 0)
        {
            tapElement(FILTERS_ATTRIBUTES);

            tapElement(FILTERS_CLEAR_BUTTON);

            swipeFromBottomToTop(1);

            for(int i = 0; i < attributes.length; i++)
            {
                if(attributes[i].equals("children"))
                {
                    tapElement(FILTERS_ATTRIBUTE_CHILDREN);
                }
                else
                if(attributes[i].equals("condition"))
                {
                    tapElement(FILTERS_ATTRIBUTE_CONDITION);
                }
                else
                if(attributes[i].equals("wifi"))
                {
                    tapElement(FILTERS_ATTRIBUTE_WIFI);
                }
                else
                if(attributes[i].equals("couples"))
                {
                    tapElement(FILTERS_ATTRIBUTE_COUPLES);
                }
                else
                if(attributes[i].equals("adults"))
                {
                    tapElement(FILTERS_ATTRIBUTE_ADULTS_ONLY);
                }
                if(attributes[i].equals("sand"))
                {
                    tapElement(FILTERS_ATTRIBUTE_SAND_BEACH);
                }
                else
                if(attributes[i].equals("pebble"))
                {
                    tapElement(FILTERS_ATTRIBUTE_PEBBLE_BEACH);
                }
                else
                if(attributes[i].equals("sand_and_pebble"))
                {
                    tapElement(FILTERS_ATTRIBUTE_SAND_PEBBLE_BEACH);
                }
                else
                if(attributes[i].equals("wellness"))
                {
                    tapElement(FILTERS_ATTRIBUTE_WELLNESS);
                }
            }

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_ATTRIBUTES)).is(visible));

            attributesIsSelected = true;
        }

        return this;
    }

    public SerpScreen selectFilterChildrenAttribute(String[] childrenAttributes)
    {
        if(childrenAttributes.length > 0)
        {
            tapElement(FILTERS_CHILDREN_ATTRIBUTES);

            Selenide.sleep(CONSTANT_3_SECONDS);

            swipeFromBottomToTop(1);

            for(int i = 0; i < childrenAttributes.length; i++)
            {
                if (childrenAttributes[i].equals("animation"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_ANIMATION);
                }
                else
                if (childrenAttributes[i].equals("pool"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_POOL);
                }
                else
                if (childrenAttributes[i].equals("waterslides"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_WATERSLIDES);
                }
                else
                if (childrenAttributes[i].equals("aquapark"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_AQUAPARK);
                }
                else
                if (childrenAttributes[i].equals("sea_entrance"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_SEA_ENTRANCE);
                }
                else
                if (childrenAttributes[i].equals("food"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_FOOD);
                }
                else
                if (childrenAttributes[i].equals("bed"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_BED);
                }
                else
                if (childrenAttributes[i].equals("nanny"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_NANNY);
                }
                else
                if (childrenAttributes[i].equals("clubs"))
                {
                    tapElement(FILTERS_CHILDREN_ATTRIBUTES_CLUBS);
                }
            }

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_CHILDREN_ATTRIBUTES)).is(visible));

            childrenAttributesIsSelected = true;
        }

        return this;
    }

    public SerpScreen selectFilterRating(String rating)
    {
        if(!rating.equals(""))
        {
            tapElement(FILTERS_RATING);

            if(rating.equals("4.5"))
            {
                tapElement(FILTERS_RATING_VERY_GOOD);
                ratingValue = "4.5";
            }
            else
            if(rating.equals("4.0"))
            {
                tapElement(FILTERS_RATING_GOOD);
                ratingValue = "4";
            }
            else
            if(rating.equals("3.5"))
            {
                tapElement(FILTERS_RATING_NORMAL);
                ratingValue = "3.5";
            }
            else
            if(rating.equals("3.0"))
            {
                tapElement(FILTERS_RATING_BAD);
                ratingValue = "3";
            }

            tapElement(FILTERS_SUBMIT_BUTTON);
            Selenide.sleep(CONSTANT_4_SECONDS);
            Assert.assertTrue($(byXpath(FILTERS_SELECTED_RATING)).is(visible));


        }

        return this;
    }


    public SerpScreen tapBackButton()
    {
        swipeFromTopToBottom(1);
        tapElement(BACK_BUTTON);

        return this;
    }

    public SerpScreen waitUntilResultsShown(/*String adults, String children, String infants*/)
    {
        /*$(byXpath(LOADING_IMAGE)).waitUntil(visible, CONSTANT_10_SECONDS);
        $(byXpath(LOADING_TEXT)).waitUntil(visible, CONSTANT_10_SECONDS);*/
        if($(byXpath(LOADING_IMAGE)).exists())
        {
            $(byXpath(LOADING_IMAGE)).waitUntil(not(visible), CONSTANT_20_SECONDS);
            $(byXpath(LOADING_TEXT)).waitUntil(not(visible), CONSTANT_20_SECONDS);
        }

        $(byXpath(HOTEL_BLOCK)).waitUntil(visible, CONSTANT_10_SECONDS);
        Selenide.sleep(CONSTANT_2_SECONDS);

        /*Assert.assertTrue($(byXpath(BOTTOM_INFO_POP_UP)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(adults + " взросл"));

        if(children.equals("") && !infants.equals(""))
        {
            Assert.assertTrue($(byXpath(BOTTOM_INFO_POP_UP)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(infants + " младен"));
        }
        if(infants.equals("") && children.equals("1"))
        {
            Assert.assertTrue($(byXpath(BOTTOM_INFO_POP_UP)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(children + " ребенка"));
        }
        if(infants.equals("") && Integer.parseInt(children) > 1)
        {
            Assert.assertTrue($(byXpath(BOTTOM_INFO_POP_UP)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(children + " детей"));
        }
        if(Integer.parseInt(children) + Integer.parseInt(infants) > 0)
        {
            Assert.assertTrue($(byXpath(BOTTOM_INFO_POP_UP)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(Integer.toString(Integer.parseInt(children) + Integer.parseInt(infants)) + " детей"));
        }*/

        return this;
    }

    public SerpScreen checkCriteria(String country, String[] resorts, int dayFromCurrent, boolean flexibleDate, String nightsFrom, String nightsTo)
    {
        if(resorts.length > 0)
        {
            if(resorts.length > 1)
            {
                Assert.assertTrue($(byXpath(RESORT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(country + ", " + resorts.length + " курорта"));
            }
            else
            {
                Assert.assertTrue($(byXpath(RESORT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(country + ", " + resorts[0]));
            }
        }
        else
        {
            Assert.assertTrue($(byXpath(RESORT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(country));
        }

        if(!flexibleDate)
        {
            Assert.assertTrue($(byXpath(DATE_AND_NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(getDateFromCurrentNoYear(dayFromCurrent) + ", " + nightsFrom + " - " + nightsTo + " ночей"));
        }
        else
        {
            Assert.assertTrue($(byXpath(DATE_AND_NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(getDateFromCurrentNoYear(dayFromCurrent - 3) + " - " + getDateFromCurrentNoYear(dayFromCurrent + 3) + ", " + nightsFrom + " - " + nightsTo + " ночей"));
        }

        return this;
    }


    public SerpScreen selectHotel()
    {
        //$(byXpath(HOTEL)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(HOTEL);

        $(byXpath(HOTEL)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public SerpScreen addToTourHunter(String email)
    {
        tapTourHunterButton();
        enterTourHunterEmail(email);
        tapAcceptDispatch();
        tapTourHunterSubmitButton();
        checkTourHunterSuccess();

        return this;
    }
    public SerpScreen enterTourHunterEmail(String email)
    {
        $(byXpath(TOUR_HUNTER_EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(email);
        //driver.pressKeyCode(111);


        return this;
    }

    public SerpScreen tapAcceptDispatch()
    {
        tapElement(TOUR_HUNTER_DISPATCH_CHECKBOX);

        return this;
    }

    public SerpScreen tapTourHunterSubmitButton()
    {
        tapElement(TOUR_HUNTER_SUBMIT_BUTTON);

        return this;
    }

    public SerpScreen checkTourHunterSuccess()
    {
        $(byXpath(TOUR_HUNTER_SUCCESS_DOG_IMAGE)).waitUntil(visible, CONSTANT_10_SECONDS);
        Assert.assertTrue($(byXpath(TOUR_HUNTER_SUCCESS_MESSAGE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals("Вы начали следить за ценой на отель"));
        Assert.assertTrue($(byXpath(TOUR_HUNTER_SUCCESS_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals("Подписка оформлена!"));
        tapElement(TOUR_HUNTER_OK_BUTTON);

        return this;
    }
    public SerpScreen checkCountryHunterSuccess()
    {
        $(byXpath(TOUR_HUNTER_SUCCESS_DOG_IMAGE)).waitUntil(visible, CONSTANT_10_SECONDS);
        Assert.assertTrue($(byXpath(TOUR_HUNTER_SUCCESS_MESSAGE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals("Вы начали следить за ценой на направление"));
        Assert.assertTrue($(byXpath(TOUR_HUNTER_SUCCESS_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals("Подписка оформлена!"));
        tapElement(TOUR_HUNTER_OK_BUTTON);

        return this;
    }

    public SerpScreen tapTourHunterButton()
    {
        tourHunterHotelName = $(byXpath(HOTEL_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText();
        tourHunterPriceValue = $(byXpath(HOTEL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText();
        tapElement(TOURHUNTER_BUTTON);

        return this;
    }

    public SerpScreen tapCountryHunter()
    {

        while($(byXpath(COUNTRY_HUNTER_BUTTON)).is(not(visible)))
        {
            swipeFromBottomToTop(1);
            swipeCount++;
        }
        swipeFromBottomToTop(1);
        Assert.assertTrue($(byXpath(COUNTRY_HUNTER_IMAGE)).is(visible));
        tapElement(COUNTRY_HUNTER_BUTTON);


        return this;
    }

    public SerpScreen addCountryToTourHunter(String email)
    {
        tapCountryHunter();
        enterTourHunterEmail(email);
        tapAcceptDispatch();
        tapTourHunterSubmitButton();
        checkCountryHunterSuccess();

        swipeFromTopToBottom(swipeCount + 1);
        tapSortingButton();
        selectSort("cheapFirst");
        tourHunterPriceValue = $(byXpath(HOTEL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText();

        return this;
    }

    public SerpScreen tapSortingButton()
    {
        tapElement(SORTING_BUTTON);

        return this;
    }

    public SerpScreen selectSort(String sort)
    {
        if(sort.equals("popular"))
        {
            tapElement(SORT_POPULAR);
        }
        else
        if(sort.equals("cheapFirst"))
        {
            tapElement(SORT_CHEAP_FIRST);
        }
        else
        if(sort.equals("expensiveFirst"))
        {
            tapElement(SORT_EXPENSIVE_FIRST);
        }
        else
        if(sort.equals("rating"))
        {
            tapElement(SORT_RATING);
        }

        return this;
    }




}
