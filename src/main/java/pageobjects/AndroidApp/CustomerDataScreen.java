package pageobjects.AndroidApp;

import com.codeborne.selenide.WebDriverRunner;
import framework.AndroidTestCase;
import framework.WebDriverCommands;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 31.07.2017.
 */
public class CustomerDataScreen extends AndroidTestCase
{
    private static CustomerDataScreen customerDataScreen= null;
    private CustomerDataScreen() throws  Exception

    {
    }
    public static synchronized CustomerDataScreen getCustomerDataScreen() throws  Exception
    {

        if(customerDataScreen == null)
        {
            customerDataScreen = new CustomerDataScreen();
        }


        return customerDataScreen;
    }

    private final String EMAIL_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_email']";
    private final String PHONE_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_phone']";
    private final String SUBMIT_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_specific']";

    public CustomerDataScreen editEmail(String email)
    {

        //$(byXpath(EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        //$(byXpath(EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(email);
        tapElement(EMAIL_INPUT);
        translateToKeyCode(email);

        return this;
    }
    public CustomerDataScreen editPhone(String phone)
    {
        //$(byXpath(PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        //$(byXpath(PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(phone);

        tapElement(PHONE_INPUT);


        while (!$(byXpath(PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals("+7 " + phone))
        {
            $(byXpath(PHONE_INPUT)).clear();
            translateToKeyCode(phone);
        }

        return this;
    }
    public CustomerDataScreen tapSubmitButton()
    {
        //$(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(SUBMIT_BUTTON);

        return this;
    }
}
