package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 27.07.2017.
 */
public class CountryScreen extends AndroidTestCase
{
    private static CountryScreen countryScreen = null;
    private CountryScreen() throws  Exception

    {
    }
    public static synchronized CountryScreen getCountryScreen() throws  Exception
    {

        if(countryScreen == null)
        {
            countryScreen = new CountryScreen();
        }


        return countryScreen;
    }

    final private String BACK_BUTTON = "//android.widget.LinearLayout[@resource_id = 'ru.travelata.app:id/btn_back']";
    final private String WITHOUT_VISA_FILTER_BUTTON = "//android.widget.TextView[@resource_id = 'ru.travelata.app:id/tv_visad']";
    final private String COUNTRY = "//android.widget.TextView[@text = '%s' and @resource-id = 'ru.travelata.app:id/tv_country']";
    final private String VISA = "//android.widget.TextView[@text = '%s']/following-sibling::android.widget.TextView";
    private final String SCREEN_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String SCREEN_TITLE_VALUE = "Куда";

    public static boolean countryVisa;

    public CountryScreen selectCountry(String country)
    {
        int limit = 1;
        while ($(byXpath(String.format(COUNTRY, country))).is(not(visible)))
        {
            swipeFromBottomToTop(1);
            limit++;
            if(limit > 25)
            {
                Assert.assertTrue($(byXpath(String.format(COUNTRY, country))).is(visible));
            }
        }
        tapElement(String.format(COUNTRY, country));
        $(byXpath(String.format(COUNTRY, country))).waitUntil(not(visible), CONSTANT_10_SECONDS);


        return this;
    }



    public CountryScreen checkTitle()
    {
        Assert.assertTrue($(byXpath(SCREEN_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(SCREEN_TITLE_VALUE));

        return this;
    }
    public CountryScreen tapBackButton()
    {
        //$(byXpath(BACK_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(BACK_BUTTON);

        return this;
    }



}
