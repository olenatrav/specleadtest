package pageobjects.AndroidApp;

import com.codeborne.selenide.Selenide;
import framework.AndroidTestCase;
import framework.Constants;
import io.appium.java_client.service.local.AppiumDriverLocalService;

import org.testng.Assert;
import org.testng.Assert.*;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
/**
 * Created by User on 28.07.2017.
 */
public class HotelScreen extends AndroidTestCase
{
    private static HotelScreen hotelScreen = null;
    private HotelScreen() throws  Exception

    {
    }
    public static synchronized HotelScreen getHotelScreen() throws  Exception
    {

        if(hotelScreen == null)
        {
            hotelScreen = new HotelScreen();
        }


        return hotelScreen;
    }

    private final String BACK_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_back']";
    private final String HOTEL_NAME = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hotel_name']";
    private final String COUNTRY_AND_RESORT_NAME = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_resort_name']";
    private final String LOWEST_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price']";
    private final String ONE_STAR_RATING = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_stars_one']";
    private final String TWO_STAR_RATING = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_stars_two']";
    private final String THREE_STAR_RATING = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_stars_three']";
    private final String FOUR_STAR_RATING = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_stars_fore']";
    private final String FIVE_STAR_RATING = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_stars_five']";
    private final String DISCOUNT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_deals']";
    private final String DISTANCE_TO_BEACH = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_beach_distance']";
    private final String SHOW_TOURS_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_show_tours']";
    private final String HOTEL_DETAILED_INFO_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hotel_description']";
    private final String HOTEL_RATING = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_rating']";
    private final String COUPLE_ICON = "//android.widget.TextView[@text = 'Для\n" + "влюбленных']";
    private final String ADULTS_ONLY_ICON = "//android.widget.TextView[@text = 'Только для\n" + "взрослых']";

    private final String TOUR_PRICE = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_price']/android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price']";
    private final String HOTEL_NAME_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String TOUR_NIGHTS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_nights']";
    private final String TOUR_PASSENGERS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_persons']";
    private final String TOUR_ROOM = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_room']";
    private final String TOUR_MEAL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_meal']";
    private final String TOUR_DATE_FROM = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_fly_from']";
    private final String TOUR_DATE_TO = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_fly_to']";
    private final String TOUR_DISCOUNT_IMAGE = "//android.widget.LinearLayout[@resource-id = 'ru.travelata.app:id/ll_deal']/android.widget.ImageView";
    private final String TOUR_DISCOUNT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_deal']";
    private final String TOUR_LOGO = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_operator_logo']";
    private final String TOUR_ORIGINAL_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_original_price']";

    private final String TOUR_TABS = "//android.widget.LinearLayout[@resource-id = 'ru.travelata.app:id/ll_tabs']";
    //private final String TOUR_TABS = "//android.widget.HorizontalScrollView[@resource-id = 'ru.travelata.app:id/sv_tabs']";
    private final String ACTION_BAR = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/ll_actionbar']";
    private final String TOUR_TAB = "(//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_filters'])[%s]";
    private final String TOUR_TAB_DATE = "(//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_date'])[%d]";
    private final String TOUR_TAB_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price']";
    private final String TOUR_TAB_SELECTION = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_date' and @text = '%s']/../following-sibling::android.view.View";
    private final String TOUR_TAB_SELECTED_DATE = "//android.view.View[@resource-id = 'ru.travelata.app:id/selection']/preceding-sibling::android.widget.LinearLayout/android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_date']";

    private final String HOTEL_ATTRIBUTES_BACK_BUTTON = "//android.widget.LinearLayout[@resource-id = 'ru.travelata.app:id/btn_back']";
    private final String HOTEL_ATTRIBUTES_HOTEL_FEATURES = "//android.widget.TextView[@text = 'Особенности отеля']/following-sibling::android.widget.TextView";
    private final String HOTEL_ATTRIBUTES_CHILDREN = "//android.widget.TextView[@text = 'Услуги для детей']";
    private final String HOTEL_ATTRIBUTES_WIFI = "//android.widget.TextView[@text = 'Услуги в отеле']/following-sibling::android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_description']";
    private final String HOTEL_ATTRIBUTES_CONDITION = "//android.widget.TextView[@text = 'Удобства в номерах']/following-sibling::android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_description']";
    private final String HOTEL_ATTRIBUTES_COUPLES = "//android.widget.TextView[@text = 'Удобства в номерах']/following-sibling::android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_description']";


    public static String hotelNameValue;
    public static String lowestPriceValue;
    public static String nightsValue;
    public static String adultsValue;
    public static String childrenValue;
    public static String infantsValue;
    public static String passengersValue;
    public static String roomValue;
    public static String mealValue;
    public static String dateFromValue;
    public static String dateToValue;
    public static String discountValue = "";
    public static String originalPriceValue;
    public static String tourPriceValue;
    public static String ratingValue;

    public HotelScreen checkTabs(boolean flexibleDate, int dayFromCurrent)
    {
        if($(byXpath(TOUR_TABS)).is(visible))
        {
            if(!flexibleDate)
            {
                Assert.assertTrue($(byXpath(String.format(TOUR_TAB_SELECTION, getDateFromCurrentMonthINEnglish(dayFromCurrent)))).is(visible));
                Assert.assertTrue($(byXpath(TOUR_TAB_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "").equals($(byXpath(TOUR_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText()));
            }
            else
            {

                int xleft = $(byXpath(TOUR_TABS)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
                int xright = xleft + $(byXpath(TOUR_TABS)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth() - 1;

                int upperY = $(byXpath(TOUR_TABS)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
                int lowerY = upperY + $(byXpath(TOUR_TABS)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();


                int middleY = (upperY + lowerY) / 2;

                swipeFromLeftToRight(xleft, middleY, xright, middleY);
                //swipeFromLeftToRight(1, 160, 479, 160);
                int i = 1;
                String[] st = new String[7];

                while ($(byXpath(String.format(TOUR_TAB, i))).is(visible))
                {
                    st[i] = $(byXpath(TOUR_TAB_DATE)).waitUntil(visible, CONSTANT_10_SECONDS).getText();
                    i++;
                }
                swipeFromRightToLeft(xright, middleY, xleft, middleY);
                //swipeFromRightToLeft(479, 160, 1 , 160);
                //$(byXpath(TOUR_TAB_SELECTED_DATE)).waitUntil(visible, CONSTANT_10_SECONDS).getText();

            }
        }

        return this;
    }



    public HotelScreen scrollToTours()
    {
        tapElement(SHOW_TOURS_BUTTON);

        return this;
    }

    public HotelScreen selectTour() {

        tapElement(TOUR_PRICE);
        $(byXpath(TOUR_PRICE)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public HotelScreen checkFilters(boolean nearBeach, String[] resorts, String[] hotelClass, String[] mealType, String price, String rating)
    {
        if(nearBeach)
        {
            Assert.assertTrue($(byXpath(DISTANCE_TO_BEACH)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains("\\d" + " метров до пляжа"), "HotelScreen: fail beach distance");
        }
        if(resorts.length != 0)
        {
            boolean resortMatch = false;
            for (int i = 0; i < resorts.length; i++)
            {
                if($(byXpath(COUNTRY_AND_RESORT_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(resorts[i]))
                {
                    resortMatch = true;
                    break;
                }
            }
            Assert.assertTrue(resortMatch);
        }
        if(hotelClass.length != 0)
        {
            boolean hotelClassMatch = false;
            for(int i = 0; i < hotelClass.length; i++)
            {
                if($(byXpath(FIVE_STAR_RATING)).is(exist) &&
                        $(byXpath(FIVE_STAR_RATING)).is(visible))
                {
                    Assert.assertTrue("5".equals(hotelClass[i]));
                    hotelClassMatch = true;
                    break;
                }
                else
                if($(byXpath(FOUR_STAR_RATING)).is(exist) &&
                        $(byXpath(FOUR_STAR_RATING)).is(visible) &&
                        $(byXpath(FIVE_STAR_RATING)).is(not(visible)))
                {
                    Assert.assertTrue("4".equals(hotelClass[i]));
                    hotelClassMatch = true;
                    break;
                }
                else
                if($(byXpath(THREE_STAR_RATING)).is(exist) &&
                        $(byXpath(THREE_STAR_RATING)).is(visible) &&
                        $(byXpath(FOUR_STAR_RATING)).is(not(visible)))
                {
                    Assert.assertTrue("3".equals(hotelClass[i]));
                    hotelClassMatch = true;
                    break;
                }
                else
                if($(byXpath(TWO_STAR_RATING)).is(exist) &&
                        $(byXpath(TWO_STAR_RATING)).is(visible) &&
                        $(byXpath(THREE_STAR_RATING)).is(not(visible)))
                {
                    Assert.assertTrue("2".equals(hotelClass[i]));
                    hotelClassMatch = true;
                    break;
                }
                else
                if($(byXpath(ONE_STAR_RATING)).is(exist) &&
                        $(byXpath(ONE_STAR_RATING)).is(visible) &&
                        $(byXpath(TWO_STAR_RATING)).is(not(visible)))
                {
                    Assert.assertTrue("2".equals(hotelClass[i]));
                    hotelClassMatch = true;
                    break;
                }
                else
                {
                    Assert.assertTrue("apts".equals(hotelClass[i]));
                    hotelClassMatch = true;
                    break;
                }
            }
            Assert.assertTrue(hotelClassMatch);

        }

        if(!rating.equals(""))
        {
            Double d = Double.parseDouble(rating);
            Double dd = Double.parseDouble($(byXpath(HOTEL_RATING)).waitUntil(visible, CONSTANT_10_SECONDS).getText());
            Assert.assertTrue(dd >= d);
        }

        scrollToTours();

        if(mealType.length != 0)
        {
            boolean mealTypeMatch = false;

            for(int i = 0; i < mealType.length; i++)
            {
                if($(byXpath(TOUR_MEAL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(Constants.mealTypeFullName.get(mealType[i])))
                {
                    mealTypeMatch = true;
                    break;
                }
            Assert.assertTrue(mealTypeMatch);
            }
        }

        if(!price.equals(""))
        {
            Assert.assertTrue(Integer.parseInt($(byXpath(TOUR_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",", "")) >= Integer.parseInt(price.replaceAll(",", "")));
        }




        return this;
    }

    public HotelScreen checkHotelScreen(String country, String[] resorts)
    {
        hotelNameValue = $(byXpath(HOTEL_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText();
        //lowestPriceValue = $(byXpath(LOWEST_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText();

        if(resorts.length == 0)
        {
            Assert.assertTrue($(byXpath(COUNTRY_AND_RESORT_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll(",.*", "").equals(country));
        }
        else
        {
        boolean resortMatch = false;
            for (int i = 0; i < resorts.length; i++)
            {
                if($(byXpath(COUNTRY_AND_RESORT_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(country + ", " + resorts[i]))
                {
                    resortMatch = true;
                    break;
                }
            }
            Assert.assertTrue(resortMatch);
        }

        if($(byXpath(FIVE_STAR_RATING)).is(exist) &&
                $(byXpath(FIVE_STAR_RATING)).is(visible))
        {
            ratingValue = "5";
        }
        else
        if($(byXpath(FOUR_STAR_RATING)).is(exist) &&
                $(byXpath(FOUR_STAR_RATING)).is(visible) &&
                $(byXpath(FIVE_STAR_RATING)).is(not(visible)))
        {
            ratingValue = "4";
        }
        else
        if($(byXpath(THREE_STAR_RATING)).is(exist) &&
                $(byXpath(THREE_STAR_RATING)).is(visible) &&
                $(byXpath(FOUR_STAR_RATING)).is(not(visible)))
        {
            ratingValue = "3";
        }
        else
        if($(byXpath(TWO_STAR_RATING)).is(exist) &&
                $(byXpath(TWO_STAR_RATING)).is(visible) &&
                $(byXpath(THREE_STAR_RATING)).is(not(visible)))
        {
            ratingValue = "2";
        }
        else
        if($(byXpath(ONE_STAR_RATING)).is(exist) &&
                $(byXpath(ONE_STAR_RATING)).is(visible) &&
                $(byXpath(TWO_STAR_RATING)).is(not(visible)))
        {
            ratingValue = "1";
        }
        else
        {
            ratingValue = "";
        }


        return this;
    }
    public HotelScreen checkTour(String nightsFrom, String nightsTo, int dayFromCurrent, boolean flexibleDate, String adults, String children, String infants)
    {

  //      Assert.assertTrue($(byXpath(HOTEL_NAME_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(hotelNameValue));

        if($(byXpath(TOUR_DISCOUNT)).is(visible))
        {
            discountValue = $(byXpath(TOUR_DISCOUNT)).getText();
            originalPriceValue = $(byXpath(TOUR_ORIGINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "").replaceAll(",", "");
            Assert.assertTrue($(byXpath(TOUR_DISCOUNT_IMAGE)).is(visible));
        }

        nightsValue = $(byXpath(TOUR_NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D*", "");
        boolean nightsMatch = false;
        for(int i = Integer.parseInt(nightsFrom); i <= Integer.parseInt(nightsTo); i++)
        {
            if(nightsValue.equals(Integer.toString(i)))
            {
                nightsMatch = true;
                break;
            }
        }
        Assert.assertTrue(nightsMatch);

        roomValue = $(byXpath(TOUR_ROOM)).waitUntil(visible, CONSTANT_10_SECONDS).getText();
        mealValue = $(byXpath(TOUR_MEAL)).waitUntil(visible, CONSTANT_10_SECONDS).getText();

        dateToValue = $(byXpath(TOUR_DATE_TO)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "");
        dateFromValue = $(byXpath(TOUR_DATE_FROM)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "");
        if(!flexibleDate)
        {
            Assert.assertTrue((dateFromValue).equals(getDateFromCurrentNoYear(dayFromCurrent)));
        }
        else
        {
            boolean dateMatch = false;
            for(int i = 0; i <= 6; i++)
            {
                if(dateFromValue.equals(getDateFromCurrentNoYear(dayFromCurrent - 3 + i)))
                {
                    dateMatch = true;
                    break;
                }

            }
            Assert.assertTrue(dateMatch);
        }

        Assert.assertTrue($(byXpath(TOUR_PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(adults + " взросл"));


        if(children.equals("") && infants.equals(""))
        {

        }
        else
        if(children.equals("") && !infants.equals(""))
        {
            Assert.assertTrue($(byXpath(TOUR_PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(infants + " младен"));
        }
        else
        if(infants.equals("") && children.equals("1"))
        {
            Assert.assertTrue($(byXpath(TOUR_PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(children + " ребен"));
        }
        else
        if(infants.equals("") && !children.equals("") && Integer.parseInt(children) > 1)
        {
            Assert.assertTrue($(byXpath(TOUR_PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(children + " детей"));
        }
        else
        if(Integer.parseInt(children) + Integer.parseInt(infants) > 0)
        {
            Assert.assertTrue($(byXpath(TOUR_PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(Integer.toString(Integer.parseInt(children) + Integer.parseInt(infants)) + " детей"));
        }

        passengersValue = $(byXpath(TOUR_PASSENGERS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\n", " ");

        tourPriceValue = $(byXpath(TOUR_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "").replaceAll(",", "");

        Assert.assertTrue($(byXpath(TOUR_LOGO)).is(exist) && $(byXpath(TOUR_LOGO)).is(visible));



        return this;
    }

}
