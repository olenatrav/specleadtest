package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by User on 26.07.2017.
 */
public class ThirdSplashScreen extends AndroidTestCase
{
    private static ThirdSplashScreen thirdSplashScreen = null;
    private ThirdSplashScreen() throws  Exception

    {
    }
    public static synchronized ThirdSplashScreen getThirdSplashScreen() throws  Exception
    {

        if(thirdSplashScreen == null)
        {
            thirdSplashScreen = new ThirdSplashScreen();
        }


        return thirdSplashScreen;
    }

    private final String LAYOUT = "//android.widget.RelativeLayout[@index = '0']";
    private final String HEADER = "//android.view.View[@index = '0']";

    private final String STRING_LABEL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_message']";
    private final String STRING_VALUE = "Сервис Турхантер сообщит, когда цена упадёт ещё ниже!";
    private final String CENTRAL_IMAGE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_demo']";
    private final String START_SEARCH_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_next']";
    private final String START_BUTTON_TEXT_VALUE = "НАЙТИ ЛУЧШИЙ ТУР";

    public ThirdSplashScreen checkSplashScreen()
    {
        $(byXpath(HEADER)).waitUntil(visible, CONSTANT_10_SECONDS);
        $(byXpath(LAYOUT)).waitUntil(visible, CONSTANT_10_SECONDS);
        Assert.assertTrue($(byXpath(STRING_LABEL)).is(visible), "third SplashScreen: STRING_LABEL is not visible");
        Assert.assertTrue($(byXpath(STRING_LABEL)).getText().equals(STRING_VALUE), "third SplashScreen: STRING_VALUE is not correct");
        Assert.assertTrue($(byXpath(CENTRAL_IMAGE)).is(visible), "third SplashScreen: CENTRAL_IMAGE is not visible");
        Assert.assertTrue($(byXpath(START_SEARCH_BUTTON)).is(visible), "third SplashScreen: CENTRAL_IMAGE is not visible");
        Assert.assertTrue($(byXpath(START_SEARCH_BUTTON)).getText().equals(START_BUTTON_TEXT_VALUE), "third SplashScreen: START_BUTTON_TEXT_VALUE is not correct");

        return this;

    }

    public ThirdSplashScreen clickStartSearchButton()
    {
        $(byXpath(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

}
