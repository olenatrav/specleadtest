package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 27.07.2017.
 */
public class DepartureCityScreen extends AndroidTestCase
{

    private static DepartureCityScreen departureCityScreen = null;
    private DepartureCityScreen() throws  Exception

    {
    }
    public static synchronized DepartureCityScreen getDepartureCityScreen() throws  Exception
    {

        if(departureCityScreen == null)
        {
            departureCityScreen = new DepartureCityScreen();
        }


        return departureCityScreen;
    }


    final private String DEPARTURE_CITY_VALUE = "//android.widget.TextView[@text = '%s' and @resource-id = 'ru.travelata.app:id/tv_city']";
    final private String BACK_BUTTON = "//android.widget.LinearLayout[@resource_id = 'ru.travelata.app:id/btn_back']";
    private final String SCREEN_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String SCREEN_TITLE_VALUE = "Откуда";

    public DepartureCityScreen selectDepartureCity(String departureCity)
    {
        int i = 1;
        while($(byXpath(String.format(DEPARTURE_CITY_VALUE, departureCity))).is(not(visible)))
        {
            swipeFromBottomToTop(1);
            i++;
            if(i > 7)
            {
                Assert.assertTrue($(byXpath(String.format(DEPARTURE_CITY_VALUE, departureCity))).is(visible));
            }

        }

        tapElement(String.format(DEPARTURE_CITY_VALUE, departureCity));
        $(byXpath(String.format(DEPARTURE_CITY_VALUE, departureCity))).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public DepartureCityScreen checkTitle()
    {
        Assert.assertTrue($(byXpath(SCREEN_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(SCREEN_TITLE_VALUE));

        return this;
    }
    public DepartureCityScreen tapBackButton()
    {
        tapElement(BACK_BUTTON);

        return this;
    }

}
