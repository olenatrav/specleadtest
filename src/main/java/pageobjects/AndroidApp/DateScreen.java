package pageobjects.AndroidApp;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import framework.AndroidTestCase;
import framework.Constants;
import org.testng.Assert;

import java.util.Date;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 28.07.2017.
 */
public class DateScreen extends AndroidTestCase
{
    private static DateScreen dateScreen = null;
    private DateScreen() throws  Exception

    {
    }
    public static synchronized DateScreen getDateScreen() throws  Exception
    {

        if(dateScreen == null)
        {
            dateScreen = new DateScreen();
        }


        return dateScreen;
    }

    private final String BACK_BUTTON = "//android.widget.LinearLayout[@resource-id = 'ru.travelata.app:id/btn_back']";
    private final String FLEXIBLE_DATE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_three_days']";
    private final String MONTH_ARROW_LEFT = "//android.widget.RelativeLayout[@index = '0']/android.widget.ImageView[@index = '0']";
    private final String MONTH_ARROW_RIGHT = "//android.widget.RelativeLayout[@index = '0']/android.widget.ImageView[@index = '2']";
    private final String MONTH_AND_YEAR_VALUE = "//android.widget.RelativeLayout[@index = '0']/android.widget.TextView[@index = '1']";
    private final String SUBMIT_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_select']";
    private final String DAY_VALUE = "//android.widget.CheckedTextView[@text = '%s']";
    private final String SCREEN_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String SCREEN_TITLE_VALUE = "Дата вылета";

    public DateScreen checkTitle()
    {
        Assert.assertTrue($(byXpath(SCREEN_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(SCREEN_TITLE_VALUE));

        return this;
    }
    public DateScreen tapBackButton()
    {
        //$(byXpath(BACK_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(BACK_BUTTON);

        return this;
    }
    public DateScreen tapFlexibleDate()
    {
        tapElement(FLEXIBLE_DATE);
        return this;
    }

    public DateScreen selectDate(int dayFromCurrent, boolean flexibleDate)
    {
        if(flexibleDate)
        {
            tapFlexibleDate();
        }

        String flightDate = getDateFromCurrentShortMonth(dayFromCurrent);

        String[] flightDateArray = flightDate.split("-");
        String monthAndYear = Constants.fullMonthName.get(flightDateArray[0]) + " " + flightDateArray[1];
        while (!$(byXpath(MONTH_AND_YEAR_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().toLowerCase().equals(monthAndYear))
        {
            //$(byXpath(MONTH_ARROW_RIGHT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            tapElement(MONTH_ARROW_RIGHT);
            Selenide.sleep(CONSTANT_2_SECONDS);
        }

        //$(byXpath(String.format(DAY_VALUE, flightDateArray[2].replaceFirst("^0", "")))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(String.format(DAY_VALUE, flightDateArray[2].replaceFirst("^0", "")));


        tapSubmitButton();

        $(byXpath(SUBMIT_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }
    public DateScreen tapSubmitButton()
    {
        //$(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(SUBMIT_BUTTON);

        return this;
    }


}
