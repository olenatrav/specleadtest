package pageobjects.AndroidApp;

import framework.AndroidTestCase;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 31.07.2017.
 */
public class ConciergeScreen extends AndroidTestCase
{
    private static ConciergeScreen conciergeScreen = null;
    private ConciergeScreen() throws  Exception

    {
    }
    public static synchronized ConciergeScreen getConciergeScreen() throws  Exception
    {

        if(conciergeScreen == null)
        {
            conciergeScreen = new ConciergeScreen();
        }


        return conciergeScreen;
    }

    private final String ADD_CONCIERGE_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_tour_with_concierge']";
    private final String TOUR_WITH_CONCIERGE_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price_with_concierge']";
    private final String NOT_ADD_CONCIERGE_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_tour_without_concierge']";
    private final String TOUR_WITHOUT_CONCIERGE_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_price_without_concierge']";

    private final String SUBMIT_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_ok']";


    public ConciergeScreen addConcierge(boolean concierge)
    {
        swipeFromBottomToTop(2);
        if(concierge)
        {
            //$(byXpath(ADD_CONCIERGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            tapElement(ADD_CONCIERGE_BUTTON);
        }
        else
        {
            //$(byXpath(NOT_ADD_CONCIERGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            tapElement(NOT_ADD_CONCIERGE_BUTTON);
        }

        return this;
    }

}
