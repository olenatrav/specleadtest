package pageobjects.AndroidApp;

import com.codeborne.selenide.Selenide;
import framework.AndroidTestCase;
import framework.Constants;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 31.07.2017.
 */
public class CheckoutScreen extends AndroidTestCase
{
    private static CheckoutScreen checkoutScreen = null;
    private CheckoutScreen() throws  Exception

    {
    }
    public static synchronized CheckoutScreen getCheckoutScreen() throws  Exception
    {

        if(checkoutScreen == null)
        {
            checkoutScreen = new CheckoutScreen();
        }


        return checkoutScreen;
    }

    private final String FOREIGN_FIRST_NAME_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_name_foreign']";
    private final String INTERNAL_FIRST_NAME_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_name']";
    private final String INTERNAL_LAST_NAME_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_lastname']";
    private final String INTERNAL_MIDDLE_NAME_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_middlename']";
    private final String INTERNAL_BIRTHDAY_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_birsday']";
    private final String INTERNAL_SERIAL_NUMBER_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_passport_number']";
    private final String INTERNAL_ISSUE_DATE_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_passport_date']";

    private final String LAST_NAME_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_lastname_foreign']";
    private final String BIRTHDAY_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_birsday_foreign']";
    private final String SEX_CHECKBOX = "//android.widget.RadioButton[@resource-id = 'ru.travelata.app:id/rb_%s']";
    private final String SERIAL_NUMBER_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_passport_number_foreign']";
    private final String EXPIRE_DATE_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_expire_date']";

    private final String CARD_NUMBER_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_pan']";
    private final String CARD_MONTH_AND_YEAR_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_date']";
    private final String CARD_CVV_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_cvv']";
    private final String CARD_CARDHOLDER_INPUT = "//android.widget.EditText[@resource-id = 'ru.travelata.app:id/et_cardholder']";
    private final String AGREEMENT_CHECKBOX = "//android.widget.CheckBox[@resource-id = 'ru.travelata.app:id/cb_ofert']";
    private final String SUBMIT_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_next']";

    private final String PAYMENT_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_title']";
    private final String PAYMENT_SUCCESS_TITLE_VALUE = "Оплата прошла успешно!";
    private final String PAYMENT_PROGRESS_TITLE_VALUE = "Проверяем статус оплаты…";
    private final String PAYMENT_TEXT = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_description']";
    private final String PAYMENT_SUCCESS_TEXT_VALUE = "Оплата прошла успешно и деньги зачислены. Ваш тур забронирован и договор выслан на указанный вами email.";
    private final String PAYMENT_PROGRESS_TEXT_VALUE = "Это может занять несколько минут.";
    private final String PAYMENT_SUBMIT_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_ok']";
    private final String PAYMENT_PROGRESS_ICON = "//android.widget.ProgressBar[@resource-id = 'ru.travelata.app:id/pb_check']";
    private final String PAYMENT_SUCCESS_ICON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_true']";


    public CheckoutScreen enterTouristData(String country, String adults, String children, String infants, String firstNameForeign,
                                           String lastNameForeign, String firstNameInternal, String lastNameInternal, String middleName,
                                           String adultBirthday, String kidBirthday, String sex, String serialNumber, String expireDate, String issueDate)
    {
        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }

        if(Constants.countryVisa.get(country))
        {
            swipeFromBottomToTop(3);
        }
        else
        {
            swipeFromBottomToTop(2);
        }

        if(!country.equals("Россия"))
        {
            for(int i = 1; i != touristsCount + 1; i++)
            {
                tapElement(FOREIGN_FIRST_NAME_INPUT);
                translateToKeyCode(firstNameForeign);

                driver.pressKeyCode(66);

                translateToKeyCode(lastNameForeign);

                driver.pressKeyCode(66);

                String[] adultBirthdayDate = adultBirthday.split(" ");
                String[] kidBirthdayDate = kidBirthday.split(" ");

                if(i > Integer.parseInt(adults))
                {
                    while (!$(byXpath(BIRTHDAY_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(kidBirthdayDate[0] + "/" + kidBirthdayDate[1] + "/" + kidBirthdayDate[2]))
                    {
                        $(byXpath(BIRTHDAY_INPUT)).clear();
                        translateToKeyCode(kidBirthday);
                    }
                }
                else
                {
                    while (!$(byXpath(BIRTHDAY_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(adultBirthdayDate[0] + "/" + adultBirthdayDate[1] + "/" + adultBirthdayDate[2]))
                    {
                        $(byXpath(BIRTHDAY_INPUT)).clear();
                        translateToKeyCode(adultBirthday);
                    }
                }
                driver.pressKeyCode(66);

                swipeFromBottomToTop(1);

                tapElement(String.format(SEX_CHECKBOX, sex));

                tapElement(SERIAL_NUMBER_INPUT);

                translateToKeyCode(serialNumber);


                driver.pressKeyCode(66);

                translateToKeyCode(expireDate);


                if(i != touristsCount)
                {
                    driver.pressKeyCode(66);
                    if(Constants.countryVisa.get(country))
                    {
                        swipeFromBottomToTop(3);
                    }
                }


            }
        }
        else
        {
            for(int i = 1; i != touristsCount + 1; i++)
            {
                tapElement(INTERNAL_FIRST_NAME_INPUT);
                //translateToKeyCode(firstNameInternal);
                $(byXpath(INTERNAL_FIRST_NAME_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(firstNameInternal);

                driver.pressKeyCode(66);

                //translateToKeyCode(lastNameInternal);
                $(byXpath(INTERNAL_LAST_NAME_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(lastNameInternal);

                driver.pressKeyCode(66);

                $(byXpath(INTERNAL_MIDDLE_NAME_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(middleName);
                //translateToKeyCode(middleName);

                driver.pressKeyCode(66);

                String[] adultBirthdayDate = adultBirthday.split(" ");
                String[] kidBirthdayDate = kidBirthday.split(" ");

                if(i > Integer.parseInt(adults))
                {
                    while (!$(byXpath(INTERNAL_BIRTHDAY_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(kidBirthdayDate[0] + "/" + kidBirthdayDate[1] + "/" + kidBirthdayDate[2]))
                    {
                        $(byXpath(INTERNAL_BIRTHDAY_INPUT)).clear();
                        translateToKeyCode(kidBirthday);
                    }
                }
                else
                {
                    while (!$(byXpath(INTERNAL_BIRTHDAY_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(adultBirthdayDate[0] + "/" + adultBirthdayDate[1] + "/" + adultBirthdayDate[2]))
                    {
                        $(byXpath(INTERNAL_BIRTHDAY_INPUT)).clear();
                        translateToKeyCode(adultBirthday);
                    }
                }
                driver.pressKeyCode(66);

                //$(byXpath(INTERNAL_SERIAL_NUMBER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(serialNumber);
                //tapElement(SERIAL_NUMBER_INPUT);


                while (!$(byXpath(INTERNAL_SERIAL_NUMBER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(serialNumber))
                {
                    $(byXpath(INTERNAL_SERIAL_NUMBER_INPUT)).clear();
                    translateToKeyCode(serialNumber);
                }


                if(i <= Integer.parseInt(adults))
                {
                    driver.pressKeyCode(66);

                    while (!$(byXpath(INTERNAL_ISSUE_DATE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(issueDate.replaceAll(" ", "/")))
                    {
                        $(byXpath(INTERNAL_ISSUE_DATE_INPUT)).clear();
                        translateToKeyCode(issueDate);
                    }



                }


                if(i != touristsCount) {
                    driver.pressKeyCode(66);

                }


            }
        }


        return this;
    }

    public CheckoutScreen enterCardData(String cardNumber, String date, String cvv, String cardholder)
    {
        swipeFromBottomToTop(6);

        //$(byXpath(CARD_NUMBER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(CARD_NUMBER_INPUT);
        //$(byXpath(CARD_NUMBER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(cardNumber);
        translateToKeyCode(cardNumber);
        driver.pressKeyCode(66);

        //$(byXpath(CARD_MONTH_AND_YEAR_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(date);
        translateToKeyCode(date);
        driver.pressKeyCode(66);

        //$(byXpath(CARD_CVV_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(cvv);
        translateToKeyCode(cvv);
        driver.pressKeyCode(66);

        //$(byXpath(CARD_CARDHOLDER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(cardholder);
        translateToKeyCode(cardholder);
        //driver.pressKeyCode(111);

        swipeFromBottomToTop(2);

        //$(byXpath(AGREEMENT_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(AGREEMENT_CHECKBOX);

        //$(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(SUBMIT_BUTTON);

        return this;
    }

    public CheckoutScreen checkPayment()
    {
        $(byXpath(PAYMENT_PROGRESS_ICON)).waitUntil(visible, CONSTANT_10_SECONDS);
        $(byXpath(PAYMENT_PROGRESS_ICON)).waitUntil(not(visible), CONSTANT_10_SECONDS);
        $(byXpath(PAYMENT_SUCCESS_ICON)).waitUntil(visible, CONSTANT_10_SECONDS);

        Assert.assertTrue($(byXpath(PAYMENT_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(PAYMENT_SUCCESS_TITLE_VALUE));
        Assert.assertTrue($(byXpath(PAYMENT_TEXT)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(PAYMENT_SUCCESS_TEXT_VALUE));
        //$(byXpath(PAYMENT_SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(PAYMENT_SUBMIT_BUTTON);

        return this;
    }

}
