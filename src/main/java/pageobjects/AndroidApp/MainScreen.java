package pageobjects.AndroidApp;

import com.codeborne.selenide.Selenide;
import framework.AndroidTestCase;
import org.openqa.selenium.By;
import org.testng.Assert;
import sun.applet.Main;


import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
/**
 * Created by User on 26.07.2017.
 */
public class MainScreen extends AndroidTestCase {
    private static MainScreen mainScreen = null;

    private MainScreen() throws Exception

    {
    }

    public static synchronized MainScreen getMainScreen() throws Exception {

        if (mainScreen == null) {
            mainScreen = new MainScreen();
        }


        return mainScreen;
    }

    private final String DIALOG_CONTAINER = "//android.widget.LinearLayout[@resource-id = 'com.android.packageinstaller:id/dialog_container']";
    private final String DIALOG_PERMISSION_MESSAGE_BLOCK = "//android.widget.TextView[@resource-id = 'com.android.packageinstaller:id/permission_message']";
    private final String DEVICE_ACCESS_LOCATION_MESSAGE = "Allow Travelata.ru to access this device's location?";
    private final String DENY_BUTTON = "//android.widget.Button[@resource-id = 'com.android.packageinstaller:id/permission_deny_button']";
    private final String ALLOW_BUTTON = "//android.widget.Button[@resource-id = 'com.android.packageinstaller:id/permission_allow_button']";
    private final String LOCATION_ICON = "//android.widget.ImageView[@resource-id = 'com.android.packageinstaller:id/permission_icon']";

    private final String ALERT_TITLE = "//android.widget.TextView[@resource-id = 'android:id/alertTitle']";
    private final String ALERT_TITLE_VALUE = "Новая версия";
    private final String ALERT_MESSAGE = "//android.widget.TextView[@resource-id = 'android:id/message']";
    private final String ALERT_MESSAGE_VALUE = "Доступна новая версия приложения!";
    private final String LATER_BUTTON = "//android.widget.Button[@resource-id = 'android:id/button2']";
    private final String UPDATE_BUTTON = "//android.widget.Button[@resource-id = 'android:id/button1']";

    private final String INFO_MENU_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/btn_info']";
    private final String TOURHUNTER_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/btn_tourhunter']";
    private final String TOURHUNTER_COUNTER = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_tourhanter_count']";
    private final String TOUR_SEARCH_TAB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_tour_search']";
    private final String HOT_TOUR_SEARCH_TAB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hot_deals']";
    private final String DEPARTURE_CITY_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_citie_from']";
    private final String DESTINATION_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_to']";
    private final String DATE_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_date']";
    private final String NIGHTS_SLIDER = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/sb_nights']";
    private final String NIGHTS = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_nights_value']";
    private final String PASSENGERS_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_subject']";
    private final String START_SEARCH_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_search_tours']";

    public static String date = "";

    public MainScreen getDate()
    {
        date = $(byXpath(DATE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getText();

        return this;
    }

    public MainScreen tapTourHunterButton()
    {
        tapElement(TOURHUNTER_BUTTON);

        return this;
    }

    public MainScreen selectHotTourTab()
    {
        //$(byXpath(HOT_TOUR_SEARCH_TAB)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(HOT_TOUR_SEARCH_TAB);

        return this;
    }

    public MainScreen checkDeviceLocationAccessDialog()
    {
        if($(byXpath(DIALOG_CONTAINER)).is(visible))
        {

            Assert.assertTrue($(byXpath(DIALOG_PERMISSION_MESSAGE_BLOCK)).is(visible));
            Assert.assertTrue($(byXpath(DIALOG_PERMISSION_MESSAGE_BLOCK)).getText().equals(DEVICE_ACCESS_LOCATION_MESSAGE));
            Assert.assertTrue($(byXpath(DENY_BUTTON)).is(visible));
            Assert.assertTrue($(byXpath(ALLOW_BUTTON)).is(visible));
            Assert.assertTrue($(byXpath(LOCATION_ICON)).is(visible));
        }


        return this;
    }
    public MainScreen selectDeviceLocationAccess(boolean access)
    {
        if(access == true)
        {
            //$(byXpath(ALLOW_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            tapElement(ALLOW_BUTTON);
            $(byXpath(ALLOW_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);
        }
        else
        {
            //$(byXpath(DENY_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            tapElement(DENY_BUTTON);
            $(byXpath(DENY_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);
        }

        return this;
    }

    public MainScreen skipGooglePlay()
    {
        driver.pressKeyCode(4);

        return this;
    }

    public MainScreen selectUpdate(boolean update)
    {
        //String s = $(byXpath(ALERT_TITLE)).getText();
        if($(byXpath(ALERT_TITLE)).is(visible) && $(byXpath(ALERT_TITLE)).getText().equals(ALERT_TITLE_VALUE))
        {
            if(update == true)
            {
                //$(byXpath(UPDATE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                tapElement(UPDATE_BUTTON);
                $(byXpath(UPDATE_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);
            }
            else
            {
                //$(byXpath(LATER_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                tapElement(LATER_BUTTON);
                $(byXpath(LATER_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);
            }
        }

        return this;
    }

    public MainScreen skipDialogs()
    {
        //Selenide.sleep(CONSTANT_1_SECOND);
        selectDeviceLocationAccess(false);
        //Selenide.sleep(CONSTANT_1_SECOND);
        selectUpdate(false);
        //Selenide.sleep(CONSTANT_1_SECOND);
        skipGooglePlay();

        return this;
    }



    public MainScreen selectDepartureCity()
    {
        //$(byXpath(DEPARTURE_CITY_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(DEPARTURE_CITY_BUTTON);
        $(byXpath(DEPARTURE_CITY_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }
    public MainScreen selectDestination()
    {
        //$(byXpath(DESTINATION_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(DESTINATION_BUTTON);
        $(byXpath(DESTINATION_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }
    public MainScreen selectDate()
    {
        //$(byXpath(DATE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(DATE_BUTTON);
        $(byXpath(DATE_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }
    public MainScreen selectPassengers()
    {
        //$(byXpath(PASSENGERS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(PASSENGERS_BUTTON);
        $(byXpath(PASSENGERS_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }
    public MainScreen tapStartSearchButton()
    {
        //$(byXpath(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(START_SEARCH_BUTTON);
        $(byXpath(START_SEARCH_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public MainScreen selectNights(String nightsFrom, String nightsTo)
    {
        int xleft = $(byXpath(NIGHTS_SLIDER)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
        int xright = xleft + $(byXpath(NIGHTS_SLIDER)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getWidth() - 1;

        int upperY = $(byXpath(NIGHTS_SLIDER)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();
        int lowerY = upperY + $(byXpath(NIGHTS_SLIDER)).waitUntil(visible, CONSTANT_10_SECONDS).getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;

        tapByCoordinates(xleft, middleY);
        tapByCoordinates(xright, middleY);

        int startStep = xleft;
        int nextStep = 0;

        while (!($(byXpath(NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\D.*", "").equals(nightsFrom)))
        {
            driver.swipe(startStep, middleY, nextStep + xleft, middleY, 1000);
            startStep = nextStep + xleft;
            nextStep += 10;
        }

        startStep = xright;
        nextStep = 0;

        while (!($(byXpath(NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\d*\\D", "").equals(nightsTo)))
        {
            driver.swipe(startStep, middleY, xright - nextStep, middleY, 1000);
            startStep = xright - nextStep;
            nextStep += 10;
        }


        return this;
    }


}
