package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;


import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 15.08.2017.
 */
public class TourHunterScreen extends AndroidTestCase
{
    private static TourHunterScreen tourHunterScreen = null;

    private TourHunterScreen() throws Exception
    {
    }

    public static synchronized TourHunterScreen getTourHunterScreen() throws Exception {

        if (tourHunterScreen == null) {
            tourHunterScreen = new TourHunterScreen();
        }

        return tourHunterScreen;
    }

    private final String SCREEN_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String SCREEN_TITLE_VALUE = "Турхантер";

    private final String HOTEL_TAB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hotels']";
    private final String COUNTRY_TAB = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_resorts']";

    private final String HOTEL_NAME = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_hotel_name']";
    private final String TOUR_CRITERIA = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_tour_criteria']";
    private final String ORIGINAL_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_original_price']";
    private final String CURRENT_PRICE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_current_price']";
    private final String NO_PRICE_CHANGE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_empty']";
    private final String NO_PRICE_CHANGE_VALUE = "Цена на этот отель пока не менялась.";
    private final String REMOVE_BUTTON = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_delete']";

    private final String COUNTRY_NAME = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_country_name']";


    public TourHunterScreen checkHotel(String hotelName, String hotelPrice, int dayFromCurrent, boolean flexibleDate, String nightsFrom, String nightsTo, String adults, String children, String infants)
    {
        Assert.assertTrue($(byXpath(HOTEL_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(hotelName));
        Assert.assertTrue($(byXpath(ORIGINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "").equals(hotelPrice));

        if(flexibleDate)
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(getDateFromCurrentNoYear(dayFromCurrent - 3) + " - " + getDateFromCurrentNoYear(dayFromCurrent + 3)));
        }
        else
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(getDateFromCurrentNoYear(dayFromCurrent)));
        }

        Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(nightsFrom + " - " + nightsTo));
        Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(adults + " взр"));


/*        if(children.equals("") && infants.equals(""))
        {}
        else*/
        if(children.equals("") && !infants.equals("") && Integer.parseInt(infants) > 1)
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(infants + " дет"));
        }
        else
        if(!children.equals("") &&Integer.parseInt(children) > 1 &&  infants.equals(""))
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(children + " дет"));
        }
        else
        if(!children.equals("") && !infants.equals(""))
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(Integer.toString(Integer.parseInt(children) + Integer.parseInt(infants)) + " дет"));
        }
        else
        if(children.equals("1") || infants.equals("1"))
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains("1 реб"));
        }


        return this;
    }
    
    public TourHunterScreen tapCountryTab()
    {
        tapElement(COUNTRY_TAB);

        return this;
    }

    public TourHunterScreen checkCountry(String country, String hotelPrice, int dayFromCurrent, boolean flexibleDate, String nightsFrom, String nightsTo, String adults, String children, String infants)
    {
        Assert.assertTrue($(byXpath(COUNTRY_NAME)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(country));
        Assert.assertTrue($(byXpath(ORIGINAL_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().replaceAll("\\s.*", "").equals(hotelPrice));

        if(flexibleDate)
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(getDateFromCurrentNoYear(dayFromCurrent - 3) + " - " + getDateFromCurrentNoYear(dayFromCurrent + 3)));
        }
        else
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(getDateFromCurrentNoYear(dayFromCurrent)));
        }

        Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(nightsFrom + " - " + nightsTo));
        Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(adults + " взр"));


        if(children.equals("") && !infants.equals("") && Integer.parseInt(infants) > 1)
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(infants + " дет"));
        }
        else
        if(!children.equals("") &&Integer.parseInt(children) > 1 &&  infants.equals(""))
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(children + " дет"));
        }
        else
        if(!children.equals("") && !infants.equals(""))
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(Integer.toString(Integer.parseInt(children) + Integer.parseInt(infants)) + " дет"));
        }
        else
        if(children.equals("1") || infants.equals("1"))
        {
            Assert.assertTrue($(byXpath(TOUR_CRITERIA)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains("1 реб"));
        }


        return this;
    }









}
