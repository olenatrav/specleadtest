package pageobjects.AndroidApp;

import framework.AndroidTestCase;

import java.lang.reflect.Array;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;


/**
 * Created by User on 04.08.2017.
 */
public class CountryHotToursScreen extends AndroidTestCase
{

    private static CountryHotToursScreen countryHotToursScreen = null;
    private CountryHotToursScreen() throws  Exception

    {
    }
    public static synchronized CountryHotToursScreen getCountryHotToursScreen() throws  Exception
    {

        if(countryHotToursScreen == null)
        {
            countryHotToursScreen = new CountryHotToursScreen();
        }


        return countryHotToursScreen;
    }
    private final String MAX_DESTINATION_POP_UP = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_message']";
    private final String FIRST_SELECTED_COUNTRY = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_country_name_first']";
    private final String FIRST_SELECTED_COUNTRY_REMOVE_BUTTON = "//android.widget.RelativeLayout[@resource-id = 'ru.travelata.app:id/rl_selected_counry_first']/android.widget.ImageView";
    private final String SECOND_SELECTED_COUNTRY = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_country_name_second']";
    private final String THIRD_SELECTED_COUNTRY = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_country_name_third']";
    private final String COUNTRY = "//android.widget.TextView[@text = '%s']";
    private final String SUBMIT_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_select']";

    public CountryHotToursScreen selectCountries(String[] countries)
    {
        Arrays.sort(countries);

        for(int i = 0; i != 3; i++)
        {
            if($(byXpath(FIRST_SELECTED_COUNTRY_REMOVE_BUTTON)).is(exist))
            {
                $(byXpath(FIRST_SELECTED_COUNTRY_REMOVE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
            else
            {
                break;
            }
        }

        for(int i = 0; i != countries.length; i++)
        {
            if($(byXpath(String.format(COUNTRY, countries[i]))).is(not(visible)))
            {
                swipeFromBottomToTop(1);

                if($(byXpath(String.format(COUNTRY, countries[i]))).is(not(visible)))
                {
                    swipeFromBottomToTop(1);
                    if($(byXpath(String.format(COUNTRY, countries[i]))).is(not(visible)))
                    {
                        swipeFromBottomToTop(1);
                        $(byXpath(String.format(COUNTRY, countries[i]))).click();
                    }
                    else
                    {
                        $(byXpath(String.format(COUNTRY, countries[i]))).click();
                    }

                }
                else
                {
                    $(byXpath(String.format(COUNTRY, countries[i]))).click();
                }
            }
            else
            {
                $(byXpath(String.format(COUNTRY, countries[i]))).click();
            }

        }

        $(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();



        return this;
    }
}
