package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 24.07.2017.
 */
public class FirstSplashScreen extends AndroidTestCase
{
    private static FirstSplashScreen firstSplashScreen = null;
    private FirstSplashScreen() throws  Exception

    {
    }
    public static synchronized FirstSplashScreen getFirstSplashScreen() throws  Exception
    {

        if(firstSplashScreen == null)
        {
            firstSplashScreen = new FirstSplashScreen();
        }


        return firstSplashScreen;
    }

    private final String LAYOUT = "//android.widget.RelativeLayout[@index = '0']";
    private final String HEADER = "//android.view.View[@index = '0']";

    private final String SKIP_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_break']";
    private final String SKIP_BUTTON_TEXT_VALUE = "ПРОПУСТИТЬ";
    private final String STRING_LABEL = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_message']";//Сравните предложения от 120 туроператоров; Поиск лучших цен - экономия вашего времени и денег; Сервис Турхантер сообщит, когда цена упадёт ещё ниже!
    private final String STRING_VALUE = "Сравните предложения от 120 туроператоров";
    private final String CENTRAL_IMAGE = "//android.widget.ImageView[@resource-id = 'ru.travelata.app:id/iv_demo']";
    private final String BOTTOM_INDICATOR = "//android.view.View[@resource-id = 'ru.travelata.app:id/indicator_demo']";


    public FirstSplashScreen checkSplashScreen()
    {

        $(byXpath(HEADER)).waitUntil(visible, CONSTANT_20_SECONDS);
        $(byXpath(LAYOUT)).waitUntil(visible, CONSTANT_20_SECONDS);
        Assert.assertTrue($(byXpath(SKIP_BUTTON)).is(visible), "first SplashScreen: SKIP_BUTTON is not visible");
        Assert.assertTrue($(byXpath(SKIP_BUTTON)).getText().equals(SKIP_BUTTON_TEXT_VALUE), "first SplashScreen: SKIP_BUTTON_TEXT is not correct");
        Assert.assertTrue($(byXpath(STRING_LABEL)).is(visible), "first SplashScreen: STRING_LABEL is not visible");
        Assert.assertTrue($(byXpath(STRING_LABEL)).getText().equals(STRING_VALUE), "first SplashScreen: STRING_VALUE is not correct");
        Assert.assertTrue($(byXpath(CENTRAL_IMAGE)).is(visible), "first SplashScreen: CENTRAL_IMAGE is not visible");
        Assert.assertTrue($(byXpath(BOTTOM_INDICATOR)).is(visible), "first SplashScreen: BOTTOM_INDICATOR is not visible");

        return this;

    }

    public FirstSplashScreen skipSplash()
    {

        //$(byXpath(SKIP_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(SKIP_BUTTON);
        $(byXpath(SKIP_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

}
