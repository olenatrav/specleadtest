package pageobjects.AndroidApp;

import framework.AndroidTestCase;
import org.testng.Assert;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;


/**
 * Created by User on 27.07.2017.
 */
public class ResortScreen extends AndroidTestCase {
    private static ResortScreen resortScreen = null;

    private ResortScreen() throws Exception

    {
    }

    public static synchronized ResortScreen getResortScreen() throws Exception {

        if (resortScreen == null) {
            resortScreen = new ResortScreen();
        }


        return resortScreen;
    }

    private final String RESORT_VALUE = "//android.widget.TextView[@text = '%s']";
    private final String SUBMIT_BUTTON = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tv_select']";
    final private String BACK_BUTTON = "//android.widget.LinearLayout[@resource_id = 'ru.travelata.app:id/btn_back']";
    private final String SCREEN_TITLE = "//android.widget.TextView[@resource-id = 'ru.travelata.app:id/tvTitle']";
    private final String SCREEN_TITLE_VALUE = "Выбор курорта";

    public ResortScreen selectResorts(String[] resorts)
    {
        if(resorts.length > 0)
        {
            int limit = 1;

            if(resorts.length != 0)
            {
                for(int i = 0; i < resorts.length; i++)
                {
                    while ($(byXpath(String.format(RESORT_VALUE, resorts[i]))).is(not(visible)))
                    {
                        swipeFromBottomToTop(1);
                        limit++;
                        if (limit > 25)
                        {
                            Assert.assertTrue($(byXpath(String.format(RESORT_VALUE, resorts[i]))).is(visible));
                        }
                    }
                    //$(byXpath(String.format(RESORT_VALUE, resorts[i]))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                    tapElement(String.format(RESORT_VALUE, resorts[i]));

                    swipeFromTopToBottom(limit);
                    limit = 1;


                }
            }

        }

        tapSubmitButton();

        $(byXpath(SUBMIT_BUTTON)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public  ResortScreen tapSubmitButton()
    {
        //$(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(SUBMIT_BUTTON);

        return this;
    }
    public ResortScreen checkTitle()
    {
        Assert.assertTrue($(byXpath(SCREEN_TITLE)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(SCREEN_TITLE_VALUE));

        return this;
    }
    public ResortScreen tapBackButton()
    {
        //$(byXpath(BACK_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        tapElement(BACK_BUTTON);

        return this;
    }

}
