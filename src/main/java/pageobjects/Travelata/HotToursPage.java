package pageobjects.Travelata;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import framework.WebDriverCommands;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by User on 29.06.2017.
 */
public class HotToursPage extends WebDriverCommands
{
    private final String EMPTY_SEARCH_RESULT = "//div[contains(@class, 'searchEmpty')]";
    private final String OPERATORS_SEARCH_BLOCK = "//div[@class = 'searchProgressOperators']";

    private final String DEPARTURE_CITY_DROPDOWN = "//div[@id = 'departureCitySelect']/div/a/span";
    private final String DEPARTURE_CITY_VALUE = "//div[text() = '%s']";

    private final String COUNTRIES_DROPDOWN = "(//div[@class = 'formInputPlace']/p)[1]";
    private final String COUNTRY_VALUE = "//span[text() = '%s']";

    private final String DATE_DROPDOWN = "(//div[@class = 'formInputPlace']/p)[2]";

    private final String NIGHTS_DROPDOWN = "(//div[@class = 'formInputPlace']/p)[3]";
    private final String NIGHTS_DROPDOWN_OPEN_BLOCK = "//div[contains(@class, 'nights open')]";
    private final String NIGHTS_PLUS_BUTTON = "/following-sibling::a[contains(@class, 'ui-spinner-up')]";
    private final String NIGHTS_MINUS_BUTTON = "/following-sibling::a[contains(@class, 'ui-spinner-down')]";
    private final String NIGHTS_FROM_VALUE = "//input[contains(@class, 'duration tour-duration-from')]";
    private final String NIGHTS_TO_VALUE = "//input[contains(@class, 'duration tour-duration-to')]";

    private final String PASSENGERS_DROPDOWN = "(//div[@class = 'formInputPlace']/p)[4]";
    private final String PASSENGERS_DROPDOWN_OPEN_BLOCK = "//div[contains(@class, 'touristGroup open')]";
    private final String ADULTS_AMOUNT_VALUE = "//label[contains(@for,'adults_buttonset_%s')]/span";
    private final String CHILDREN_AMOUNT_VALUE = "//label[contains(@for,'kids_buttonset_%s')]/span";
    private final String INFANTS_AMOUNT_VALUE = "//label[contains(@for,'infants_buttonset_%s')]/span";

    private final String START_SEARCH_BUTTON = "startSearch"; //id


    public HotToursPage waitForResultShown() throws Exception
    {
        $(byXpath(OPERATORS_SEARCH_BLOCK)).waitUntil(not(visible), CONSTANT_20_SECONDS);
        Assert.assertFalse($(byXpath(EMPTY_SEARCH_RESULT)).is(visible), "Serp Page: search doesn't have results");

        return this;
    }

    public HotToursPage selectDepartureCity(String departureCity) throws Exception
    {
        Selenide.sleep(CONSTANT_3_SECONDS);

        $(byXpath(DEPARTURE_CITY_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(DEPARTURE_CITY_VALUE, departureCity))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public HotToursPage selectCountry(String country) throws Exception
    {
        $(byXpath(COUNTRIES_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(COUNTRY_VALUE, country))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public HotToursPage selectDate()
    {
        return this;
    }
    public HotToursPage selectNights(String from, String to)
    {
        if($(byXpath(NIGHTS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(NIGHTS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }


        SelenideElement nightsFrom = $(byXpath(NIGHTS_FROM_VALUE));
        int nightsFromValue = Integer.parseInt(nightsFrom.getAttribute("aria-valuenow"));

        SelenideElement nightsTo = $(byXpath(NIGHTS_TO_VALUE));
        int nightsToValue = Integer.parseInt(nightsTo.getAttribute("aria-valuenow"));

        SelenideElement nightFromMinusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightFromPlusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_PLUS_BUTTON));

        int intFrom = Integer.parseInt(from);

        if(intFrom > nightsFromValue)
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromPlusButton.click();
            }
        }
        else
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromMinusButton.click();
            }
        }


        SelenideElement nightToMinusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightToPlusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_PLUS_BUTTON));

        int intTo = Integer.parseInt(to);

        if(intTo > nightsToValue)
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToPlusButton.click();
            }
        }
        else
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToMinusButton.click();
            }
        }

        return this;
    }

    public HotToursPage selectAdults(String adultsAmountValue) throws Exception
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        $(byXpath(String.format(ADULTS_AMOUNT_VALUE, adultsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
    public HotToursPage selectChildren(String childrenAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(CHILDREN_AMOUNT_VALUE, childrenAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public HotToursPage selectInfants(String infantsAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(INFANTS_AMOUNT_VALUE, infantsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public HotToursPage clickStartButton() throws Exception
    {

        $(byId(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
}
