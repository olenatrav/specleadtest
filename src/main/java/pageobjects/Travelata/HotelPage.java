 package pageobjects.Travelata;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;

import framework.Constants;
import framework.WebDriverCommands;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

 /**
 * Created by User on 01.09.2016.
 */
public class HotelPage extends WebDriverCommands
{
    private static HotelPage hotelPage = null;
    private HotelPage()
    {
    }
    public static synchronized HotelPage getHotelPage()
    {
        if(hotelPage == null)
            hotelPage = new HotelPage();

        return hotelPage;
    }



    private final String DEPARTURE_CITY_VALUE = "//div[@id = 'departureCitySelect']/div/a/span";
    private final String HOTEL_NAME_VALUE = "hotelTitle";
    private final String HOTEL_EX_NAME_VALUE = "hotelExTitle";
    private final String DATE_VALUE = "//span[@class = 'datesLabel']/b";
    private final String TOUR_DEPARTURE_DATE_VALUE = "(//*[@class = 'HtlDate'])[%s]";
    private final String TOUR_NIGHTS_VALUE = "(//*[@class = 'tourNights'])[%s]";
    private final String TOUR_PRICE_VALUE = "(//span[contains(@class, 'HtlBtnSelect')])[%s]/span";
    private final String TOUR_MEAL_TYPE_VALUE= "(//span[@class = 'HtlMealDescr'])[%s]";

    private final String IS_FLEXIBLE_VALUE = "//span[@class = 'datesLabel']";
    private final String NIGHTS_VALUE = "//div[@class = 'bottom center']/b";
    private final String ADULTS_VALUE = "//div[@id = 'adults_buttonset']/label[@aria-pressed = 'true']/span";
    private final String KIDS_VALUE = "//div[@id = 'kids_buttonset']/label[@aria-pressed = 'true']/span";
    private final String INFANTS_VALUE = "//div[@id = 'infants_buttonset']/label[@aria-pressed = 'true']/span";

   // private final String MEAL_TYPE_VALUE = "(//span[@class = 'HtlMealDescr'])[%s]";
    private final String HOTEL_CLASS_VALUE = "//span[@class = 'hotelCategory']/i";
    private final String HOTEL_CLASS_APTS = "//span[@class = 'hotelCategory']";
    private final String TO_LOGOS = "//div[@id = 'tab-content-tours']//div[@id = 'hotelToursList']//span[@class = 'operatorLogoWrapper']/img";
    private final String TO_LOGO = "(//div[@id = 'tab-content-tours']//div[@id = 'hotelToursList']//span[@class = 'operatorLogoWrapper'])[%s]/img";

    private final String EMPTY_SEARCH_RESULT = "//div[contains(@class, 'searchEmpty')]";
    private final String TOURS_LIST_BLOCK = "//div[@class = 'hotelToursListData']";

    private final String SELECT_TOUR_BUTTON = "(//*[@id='tourProductTemplate']/.//span[contains(@class, 'HtlBtnSelect ')])[%s]";
    private final String SELECT_TOUR_BUTTON_BY_PRICE = "//*[@id='tourProductTemplate']/.//span[contains(@class, 'HtlBtnSelect ')]/span[text() = '%s']";
    private final String INACTIVE_BUTTON = "//div[contains(@class, 'btnGray')]";
    private final String ORDER_TOUR_BUTTON = "orderButtonStep1"; //id

    private final String CUSTOMER_EMAIL_INPUT = "customerEmail";
    private final String CUSTOMER_PHONE_INPUT = "customerPhone";
    private final String CONTINUE_BUTTON = "//div[@id='basicCustomerCreateOrderButton']/span";
    private final String CONTINUE_BUTTON_LOADER = "(//span[@class = 'btnLoader'])[1]";
    private final String HOT_TOUR_DATE_AND_NIGHTS = "//span[text() = 'Даты тура:']/following-sibling::span";
    private final String PASSENGERS_VALUE = "//span[text() = 'Кто едет:']/following-sibling::span";
    private final String MEAL_TYPE_VALUE = "//span[text() = 'Питание:']/following-sibling::span";


    public static String hotelValue = "";
    public static String departureDateValue = "";
    public static String nightsValue = "";
    public static String hotelClassValue = "";
    public static String mealTypeValue = "";
    public static String tourPrice = "";

    public static String customerEmailValue = "";


    private static int dayFromCurrentValue = 0;
    private static int tourDepartureDay = 0;
    private static int acceptableTour = 0;
    boolean acceptableTO = false;
    private static String anexTour = "AnexTour";
    private static String biblioGlobus = "Biblio Globus";

    public HotelPage checkHotelPageTourAssertions(int dayFromCurrent, String departureCity, boolean isFlexible, String[] dates, String[] nights,
                                                  String adults, String children, String infants, String mealType, String hotelClass) throws Exception
    {
        dayFromCurrentValue = dayFromCurrent;
        hotelValue = $(byClassName(HOTEL_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().replaceAll("&\\w.*", "").replaceAll("\\s*$", "");
        //hotelValue = $(byClassName(HOTEL_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().replaceAll("\\s*$", "");
        if($(byClassName(HOTEL_EX_NAME_VALUE)).is(exist))
        {
            hotelValue = hotelValue + " " + $(byClassName(HOTEL_EX_NAME_VALUE)).innerHtml().toLowerCase();
        }
        Assert.assertTrue($(byXpath(DEPARTURE_CITY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(departureCity.toLowerCase()), "hotel page: departure city failure");

        String flightDates = $(byXpath(DATE_VALUE)).innerHtml().toLowerCase();
        if(isFlexible)
        {
            String[] flexibleDateMinus;
            String[] flexibleDatePlus;
            String[] dateValue;

            dateValue = flightDates.split(" по ");

            flexibleDateMinus = getFullDateFromCurrent(dayFromCurrentValue - 3).split("-");
            Assert.assertTrue(dateValue[0].equals(flexibleDateMinus[2] + "." + flexibleDateMinus[1]), "hotel page: flexible date minus failure");

            flexibleDatePlus = getFullDateFromCurrent(dayFromCurrentValue + 3).split("-");
            Assert.assertTrue(dateValue[1].equals(flexibleDatePlus[2] + "." + flexibleDatePlus[1]), "hotel page: flexible date plus failure");

            String[] flexibleDate;
            boolean departureDateMatch = false;
            for(int i = 0; i <= 6 ; i++)
            {
                flexibleDate = getFullDateFromCurrent(dayFromCurrentValue - 3 + i).split("-");

                if(acceptableTour % 2 != 0)
                {
                    tourDepartureDay = acceptableTour ;
                }
                else
                {
                    tourDepartureDay = acceptableTour + 1;
                }


                if($(byXpath(String.format(TOUR_DEPARTURE_DATE_VALUE, Integer.toString(tourDepartureDay)))).innerHtml().replaceAll("<.*", "").equals(flexibleDate[2] + "." + flexibleDate[1] + "." + flexibleDate[0].replaceAll("^\\d{2}", "")))
                {
                    departureDateMatch = true;
                    departureDateValue = $(byXpath(String.format(TOUR_DEPARTURE_DATE_VALUE, Integer.toString(tourDepartureDay)))).innerHtml().replaceAll("<.*", "");
                    break;
                }

            }
            Assert.assertTrue(departureDateMatch, "hotel page: tour departure date failure");
        }
        else
        {
            Assert.assertTrue(flightDates.equals(dates[2] + "." + dates[1]), "hotel page: flight dates failure");
            Assert.assertTrue($(byClassName(TOUR_DEPARTURE_DATE_VALUE)).innerHtml().replaceAll("<.*", "").equals(dates[2] + "." + dates[1] + "." + dates[0].replaceAll("^\\d{2}", "")), "hotel page: tour departure date failure");
            departureDateValue = $(byClassName(TOUR_DEPARTURE_DATE_VALUE)).innerHtml().replaceAll("<.*", "");
        }



        String flightNights = $(byXpath(NIGHTS_VALUE)).innerHtml().toLowerCase().replaceAll(" н.*", "");
        if (!nights[0].equals(nights[1]))
        {
            Assert.assertTrue(flightNights.equals(nights[0] + " - " + nights[1]), "hotel page: nights criteria failure");

            boolean nightsMatch = false;
            for(int i = 0; i <= (Integer.parseInt(nights[1]) - Integer.parseInt(nights[0])) ;i++)
            {

                if($(byXpath(String.format(TOUR_NIGHTS_VALUE, Integer.toString(acceptableTour)))).innerHtml().replaceAll("^.*? ", "").replaceAll(" .*", "").equals(Integer.toString(Integer.parseInt(nights[0]) + i)))
                {
                    nightsMatch = true;
                    nightsValue = $(byXpath(String.format(TOUR_NIGHTS_VALUE, Integer.toString(acceptableTour)))).innerHtml().replaceAll("^.*? ", "").replaceAll(" .*", "");
                }
            }
            Assert.assertTrue(nightsMatch, "hotel page: tour nights failure");

        }
        else
        {
            Assert.assertTrue(flightNights.equals(nights[0]));
            Assert.assertTrue(flightNights.equals(nights[1]));
            Assert.assertTrue($(byClassName(TOUR_NIGHTS_VALUE)).innerHtml().replaceAll("^.*? ", "").replaceAll(" .*", "").equals(Integer.toString(Integer.parseInt(nights[0]))));
            nightsValue = $(byXpath(String.format(TOUR_NIGHTS_VALUE, Integer.toString(acceptableTour)))).innerHtml().replaceAll("^.*? ", "").replaceAll(" .*", "");
        }



        Assert.assertTrue($(byXpath(ADULTS_VALUE)).innerHtml().toLowerCase().equals(adults));

        if(children.equals(""))
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals("нет"));
        }
        else
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals(children));
        }

        if(infants.equals(""))
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals("нет"));
        }
        else
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals(infants));
        }


        if(!mealType.equals(""))
        {
            String[] mealTypeArray = mealType.split(", ");
            boolean mealTypeMatch = false;
            for(int i = 0; i != mealTypeArray.length; i++)
            {
                if($(byXpath(String.format(TOUR_MEAL_TYPE_VALUE, Integer.toString(acceptableTour)))).innerHtml().equals(Constants.mealTypeFullName.get(mealTypeArray[i])))
                {
                    mealTypeValue = $(byXpath(String.format(TOUR_MEAL_TYPE_VALUE, Integer.toString(acceptableTour)))).innerHtml().toLowerCase();
                    mealTypeMatch = true;

                }
            }

            Assert.assertTrue(mealTypeMatch);

        }
        else
        {
            mealTypeValue = $(byXpath(String.format(TOUR_MEAL_TYPE_VALUE, Integer.toString(acceptableTour)))).innerHtml().toLowerCase();
        }

        if(!hotelClass.equals(""))
        {
            String[] hotelClassArray = hotelClass.split(", ");

            boolean hotelClassMatch = false;

            for(int i = 0; i != hotelClassArray.length; i++)
            {
                if(hotelClassArray[i].equals("2"))
                {

                    if(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals(hotelClassArray[i]) ||
                            Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals("1") ||
                            $(byXpath(HOTEL_CLASS_APTS)).innerHtml().equals("Апартаменты"))
                    {
                        if($(byXpath(HOTEL_CLASS_VALUE)).is(visible))
                        {
                            hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
                        }
                        else
                        {
                            hotelClassValue = $(byXpath(HOTEL_CLASS_APTS)).innerHtml().toLowerCase();
                        }
                        hotelClassMatch = true;
                    }
                }


                if(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals(hotelClassArray[i]))
                {
                    hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
                    hotelClassMatch = true;

                }

            }
            Assert.assertTrue(hotelClassMatch);

        }
        else
        {
            if($(byXpath(HOTEL_CLASS_VALUE)).is(visible))
            {
                hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
            }
            else
            {
                hotelClassValue = $(byXpath(HOTEL_CLASS_APTS)).innerHtml().toLowerCase();
            }

        }

        return this;
    }

    public HotelPage checkHotelPageHotelAssertions(int startDayFromCurrent, int endDayFromStart, String[] dates, String[] nights, String adults, String children, String infants, String mealType, String hotelClass) throws Exception
    {

        hotelValue = $(byClassName(HOTEL_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().replaceAll("&\\w.*", "").replaceAll("\\s*$", "");
        if($(byClassName(HOTEL_EX_NAME_VALUE)).is(exist))
        {
            hotelValue = hotelValue + " " + $(byClassName(HOTEL_EX_NAME_VALUE)).innerHtml().toLowerCase();
        }

        String flightDate = getFullDateFromCurrent(startDayFromCurrent);

        String[] startFlightDateArray = flightDate.split("-");

        /*SelenideElement startFlightDate = $(byXpath(String.format(FLIGHT_DATE_FROM_VALUE, startFlightDateArray[0],
                Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(startFlightDateArray[1])) - 1), deleteFirstZeroFromDate(startFlightDateArray[2]))));*/


        Assert.assertTrue($(byXpath(ADULTS_VALUE)).innerHtml().toLowerCase().equals(adults));

        if(children.equals(""))
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals("нет"));
        }
        else
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals(children));
        }

        if(infants.equals(""))
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals("нет"));
        }
        else
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals(infants));
        }


        if(!mealType.equals(""))
        {
            String[] mealTypeArray = mealType.split(", ");
            boolean mealTypeMatch = false;
            for(int i = 0; i != mealTypeArray.length; i++)
            {
                if($(byXpath(String.format(TOUR_MEAL_TYPE_VALUE, Integer.toString(acceptableTour)))).innerHtml().equals(Constants.mealTypeFullName.get(mealTypeArray[i])))
                {
                    mealTypeValue = $(byXpath(String.format(TOUR_MEAL_TYPE_VALUE, Integer.toString(acceptableTour)))).innerHtml().toLowerCase();
                    mealTypeMatch = true;

                }
            }

            Assert.assertTrue(mealTypeMatch);

        }
        else
        {
            mealTypeValue = $(byXpath(String.format(TOUR_MEAL_TYPE_VALUE, Integer.toString(acceptableTour)))).innerHtml().toLowerCase();
        }

        if(!hotelClass.equals(""))
        {
            String[] hotelClassArray = hotelClass.split(", ");

            boolean hotelClassMatch = false;

            for(int i = 0; i != hotelClassArray.length; i++)
            {
                if(hotelClassArray[i].equals("2"))
                {

                    if(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals(hotelClassArray[i]) ||
                            Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals("1") ||
                            $(byXpath(HOTEL_CLASS_APTS)).innerHtml().equals("Апартаменты"))
                    {
                        if($(byXpath(HOTEL_CLASS_VALUE)).is(visible))
                        {
                            hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
                        }
                        else
                        {
                            hotelClassValue = $(byXpath(HOTEL_CLASS_APTS)).innerHtml().toLowerCase();
                        }
                        hotelClassMatch = true;
                    }
                }


                if(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals(hotelClassArray[i]))
                {
                    hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
                    hotelClassMatch = true;

                }

            }
            Assert.assertTrue(hotelClassMatch);

        }
        else
        {
            if($(byXpath(HOTEL_CLASS_VALUE)).is(visible))
            {
                hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
            }
            else
            {
                hotelClassValue = $(byXpath(HOTEL_CLASS_APTS)).innerHtml().toLowerCase();
            }

        }

        return this;
    }



    public HotelPage checkTourAcceptability() throws Exception
    {

        checkIfUnsuccessfulSearch();
        JavascriptExecutor jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();
        jse.executeScript("window.scroll(0, 1200)", "");
        for(acceptableTour = 1; acceptableTour < $$(byXpath(TO_LOGOS)).size() + 1; acceptableTour++)
        {
            String currentTO = $(byXpath(String.format(TO_LOGO, Integer.toString(acceptableTour)))).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("title");
            if(!currentTO.equals(anexTour) && !currentTO.equals(biblioGlobus))
            {
                acceptableTO = true;
                break;
            }
        }
        if(!acceptableTO)
        {
            SearchPage.hotelCounter++;
            WebDriverRunner.getWebDriver().close();
            switchToNewTab(0);
            SearchPage.getSearchPage().clickMoreTours();

        }



        return this;

    }

    public HotelPage waitForResultsShown() throws Exception
    {
        Selenide.sleep(CONSTANT_5_SECONDS);
        $(byXpath(TOURS_LIST_BLOCK)).waitUntil(visible, CONSTANT_20_SECONDS);

        return this;
    }

    public HotelPage checkIfUnsuccessfulSearch() throws Exception
    {
        waitForResultsShown();
        if($(byXpath(EMPTY_SEARCH_RESULT)).is(visible))
        {
            SearchPage.hotelCounter++;
            WebDriverRunner.getWebDriver().close();
            switchToNewTab(0);
            SearchPage.getSearchPage().selectHotTour();
        }

        Assert.assertFalse($(byXpath(EMPTY_SEARCH_RESULT)).is(visible), "Hotel Page: Search doesn't have results");

        return this;
    }

    public  HotelPage clickSelectTourButton()  throws Exception
    {

        if(tourPrice.equals(""))
        {
            $(byXpath(String.format(SELECT_TOUR_BUTTON, Integer.toString(acceptableTour)))).waitUntil(visible, CONSTANT_10_SECONDS).click();
            tourPrice = $(byXpath(String.format(TOUR_PRICE_VALUE, Integer.toString(acceptableTour)))).innerHtml();
        }
        else
        {
            if(!($(byXpath(String.format(TOUR_PRICE_VALUE, Integer.toString(acceptableTour)))).innerHtml().equals(tourPrice)))
            {
                $(byXpath(String.format(SELECT_TOUR_BUTTON_BY_PRICE, tourPrice))).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
            else
            {
                $(byXpath(String.format(SELECT_TOUR_BUTTON, Integer.toString(acceptableTour)))).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
        }


        return this;
    }

    public HotelPage generateEmail()
    {

        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        Date currentDate = new Date();
        customerEmailValue = "autotest" + dateFormat.format(currentDate) + "@no-spam.ws";


        return this;
    }
    public HotelPage enterCustomerEmail(String customerEmail)
    {
        $(byId(CUSTOMER_EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(customerEmail);

        return this;
    }
    public HotelPage enterCustomerPhone(String customerPhone)
    {
        $(byId(CUSTOMER_PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(customerPhone);

        return this;
    }
    public HotelPage clickContinueButton()
    {
        $(byXpath(CONTINUE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(CONTINUE_BUTTON_LOADER)).waitUntil(not(visible), CONSTANT_20_SECONDS);

        return this;
    }

    public HotelPage enterCustomerData(String email, String phone)
    {
        enterCustomerEmail(email);
        enterCustomerPhone(phone);
        clickContinueButton();

        return this;
    }

    public HotelPage checkHotelPageHotTourAssertions(String departureCity, String dateRange, String nightsFrom, String nightsTo, String adults, String children, String infants, String hotelClass, String mealType) throws Exception
    {
        hotelValue = $(byClassName(HOTEL_NAME_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().replaceAll("&\\w.*", "").replaceAll("\\s*$", "");
        if($(byClassName(HOTEL_EX_NAME_VALUE)).is(exist))
        {
            hotelValue = hotelValue + " " + $(byClassName(HOTEL_EX_NAME_VALUE)).innerHtml().toLowerCase();
        }
        Assert.assertTrue($(byXpath(DEPARTURE_CITY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(departureCity.toLowerCase()), "hotel page: departure city failure");

        String[] date;
        String month;
        String day;
        String year;
        boolean dateMatch = false;
        int maxDays = 0;
        if(dateRange.equals("3 дня"))
        {
            maxDays = 3;
        }
        else
        if(dateRange.equals("неделя"))
        {
            maxDays = 7;
        }
        else
        if(dateRange.equals("2 недели"))
        {
            maxDays = 14;
        }

            for(int i = 1; i < maxDays + 1; i++)
            {
                date = getFullDateFromCurrent(i).split("-");
                year = date[0];
                month = date[1];
                day = date[2];

                String s = $(byXpath(HOT_TOUR_DATE_AND_NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll(" -.*", "");
                String ss = day + " " + Constants.monthName.get(month);
                if($(byXpath(HOT_TOUR_DATE_AND_NIGHTS)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll(" -.*", "").equals(deleteFirstZeroFromDate(day) + " " + Constants.monthName.get(month)))
                {
                    dateMatch = true;
                    departureDateValue = day + "." + month ;
                    break;
                }

            }

        Assert.assertTrue(dateMatch);



        String flightNights = $(byXpath(NIGHTS_VALUE)).innerHtml().toLowerCase().replaceAll(" н.*", "");
        if (!nightsFrom.equals(nightsTo))
        {
            Assert.assertTrue(flightNights.equals(nightsFrom + " - " + nightsTo), "hotel page: nights criteria failure");
        }
        else
        {
            Assert.assertTrue(flightNights.equals(nightsFrom));
            Assert.assertTrue(flightNights.equals(nightsTo));
        }

        boolean nightsMatch = false;
        for(int i = Integer.parseInt(nightsFrom); i < Integer.parseInt(nightsTo) + 1; i++)
        {
            if($(byXpath(HOT_TOUR_DATE_AND_NIGHTS)).innerHtml().replaceAll(".*\\( ", "").replaceAll("\\s.*", "").equals(Integer.toString(i)))
            {
                nightsMatch = true;
                nightsValue = Integer.toString(i);
                break;
            }
        }
        Assert.assertTrue(nightsMatch);

        String passengers = $(byXpath(PASSENGERS_VALUE)).innerHtml();
        Assert.assertTrue(passengers.contains(adults + " взросл"), "Hotel page: adults");

        if (!children.equals("") && !infants.equals(""))
        {
            Assert.assertTrue(passengers.contains(Integer.parseInt(children) + Integer.parseInt(infants) + " детей"), "Hotel page: kids failure");
        }
        else
        {
            if (!children.equals("") && children.equals("1"))
            {
                Assert.assertTrue(passengers.contains(children + " ребен"), "Hotel page: children");
            }
            else
            {
                Assert.assertTrue(passengers.contains(children + " детей"), "Hotel page: children");
            }

            if (!infants.equals(""))
            {
                Assert.assertTrue(passengers.contains(infants + " младен"), "Hotel page: infants");
            }
        }



        Assert.assertTrue($(byXpath(ADULTS_VALUE)).innerHtml().toLowerCase().equals(adults));

        if(children.equals(""))
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals("нет"));
        }
        else
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals(children));
        }

        if(infants.equals(""))
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals("нет"));
        }
        else
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals(infants));
        }



        if(!hotelClass.equals(""))
        {
            String[] hotelClassArray = hotelClass.split(", ");

            boolean hotelClassMatch = false;

            for(int i = 0; i != hotelClassArray.length; i++)
            {
                if(hotelClassArray[i].equals("2"))
                {

                    if(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals(hotelClassArray[i]) ||
                            Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals("1") ||
                            $(byXpath(HOTEL_CLASS_APTS)).innerHtml().equals("Апартаменты"))
                    {
                        if($(byXpath(HOTEL_CLASS_VALUE)).is(visible))
                        {
                            hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
                        }
                        else
                        {
                            hotelClassValue = $(byXpath(HOTEL_CLASS_APTS)).innerHtml().toLowerCase();
                        }
                        hotelClassMatch = true;
                    }
                }


                if(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size()).equals(hotelClassArray[i]))
                {
                    hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
                    hotelClassMatch = true;

                }

            }
            Assert.assertTrue(hotelClassMatch);

        }
        else
        {
            if($(byXpath(HOTEL_CLASS_VALUE)).is(visible))
            {
                hotelClassValue = Integer.toString($$(byXpath(HOTEL_CLASS_VALUE)).size());
            }
            else
            {
                hotelClassValue = $(byXpath(HOTEL_CLASS_APTS)).innerHtml().toLowerCase();
            }

        }


        if(!mealType.equals(""))
        {
            String[] mealTypeArray = mealType.split(", ");

            boolean mealTypeMatch = false;
            for(int i = 0; i != mealTypeArray.length; i++)
            {
                if($(byXpath(MEAL_TYPE_VALUE)).innerHtml().equals(Constants.mealTypeFullName.get(mealTypeArray[i])))
                {
                    mealTypeValue = $(byXpath(MEAL_TYPE_VALUE)).innerHtml().toLowerCase();
                    mealTypeMatch = true;
                    break;

                }
            }

            Assert.assertTrue(mealTypeMatch);

        }
        else
        {
            mealTypeValue = $(byXpath(MEAL_TYPE_VALUE)).innerHtml().toLowerCase();
        }



        return this;
    }


}
