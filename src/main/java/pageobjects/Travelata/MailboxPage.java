package pageobjects.Travelata;

import com.codeborne.selenide.Selenide;
import framework.Constants;
import framework.WebDriverCommands;
import pageobjects.TOM.OrderPage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 07.06.2017.
 */
public class MailboxPage extends WebDriverCommands {
    private static MailboxPage mailboxPage = null;

    private MailboxPage() {
    }

    public static synchronized MailboxPage getMailboxPage() {
        if (mailboxPage == null)
            mailboxPage = new MailboxPage();

        return mailboxPage;
    }

    private final String MAIL_URL = "http://no-spam.ws/";
    private final String TEST_MAIL_INPUT = "//input[@name= 'login']";
    private final String TEST_MAIL_SUBMIT_BUTTON = "//span[text() = 'Go!']";
    private final String EMAIL = "//.[contains(text(), '%s')]";
    private final String CHECK_AND_PAY_BUTTON = "//p[contains(@style, 'center')]/a";
    private final String EMAIL_ORDER_IS_WAITING_FOR_BEING_PAID = "//.[contains(text(), 'Завершите оплату онлайн по заказу')]";
    private final String GEN_LEAD_EMAIL_ORDER_ID = "//b[contains(text(), 'Номер Вашего заказа:')]";




    public MailboxPage goToMailboxPage()
    {
        Selenide.open(MAIL_URL);


        return this;
    }

    public MailboxPage enterTestEmail(String testEmailValue) {
        $(byXpath(TEST_MAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(testEmailValue);


        return this;
    }
    public MailboxPage clickSubmitButton()
    {
        $(byXpath(TEST_MAIL_SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MailboxPage selectEmail(String emailName)
    {
        $(byXpath(String.format(EMAIL, String.format(Constants.emailName.get(emailName), OrderPage.orderIDValue)))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MailboxPage getOrderIDFromMail()
    {
        //String s = $(byXpath(EMAIL_ORDER_IS_WAITING_FOR_BEING_PAID)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml();
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher($(byXpath(EMAIL_ORDER_IS_WAITING_FOR_BEING_PAID)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml());
        m.find();
        OrderPage.orderIDValue = m.group(0);

        return this;
    }

    public MailboxPage getOrderIDFromGenLeadMail()
    {
        OrderPage.orderIDValue = $(byXpath(GEN_LEAD_EMAIL_ORDER_ID)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().replaceAll("\\D*", "");

        return this;
    }



    public MailboxPage goToCheckoutFromEmail()
    {
        $(byXpath(CHECK_AND_PAY_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

}
