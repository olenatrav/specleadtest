package pageobjects.Travelata;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;

/**
 * Created by User on 30.06.2017.
 */
public class HotelSearchPage extends WebDriverCommands
{
    private static HotelSearchPage hotelSearchPage = null;
    private HotelSearchPage()
    {
    }
    public static synchronized HotelSearchPage getHotelSearchPage()
    {
        if(hotelSearchPage == null)
            hotelSearchPage = new HotelSearchPage();

        return hotelSearchPage;
    }
    private final String EMPTY_SEARCH_RESULT = "//div[contains(@class, 'searchEmpty')]";
    private final String OPERATORS_SEARCH_BLOCK = "//div[@class = 'searchProgressOperators']";

    private final String COUNTRY_AND_RESORT_DROPDOWN = "//input[@name = 'destination']";
    private final String COUNTRY_VALUE = "//span[text() = '%s']";
    private final String RESORT_VALUE = "//span[text() = '%s, ']";

    private final String CALENDAR_FROM_DROPDOWN = "//input[@name = 'dateFrom']";
    private final String CALENDAR_TO_DROPDOWN = "//input[@name = 'dateTo']";
    private final String FLIGHT_DATE_FROM_VALUE = "(//td[@data-year='%s' and @data-month='%s']/a[text()='%s'])";
    private final String FLIGHT_DATE_TO_VALUE = "(//td[@data-year='%s' and @data-month='%s']/a[text()='%s'])";
    private final String CALENDAR_NEXT_MONTH_ARROW_FROM = "(//a[@data-handler='next']/span)[1]";
    private final String CALENDAR_NEXT_MONTH_ARROW_TO = "(//a[@data-handler='next']/span)[2]";
    private final String CALENDAR_ERROR_MESSAGE = "errorMessage";

    private final String PASSENGERS_DROPDOWN = "(//div[@class = 'formInputPlace']/p)[2]";
    private final String PASSENGERS_DROPDOWN_OPEN_BLOCK = "//div[contains(@class, 'touristGroup open')]";
    private final String ADULTS_AMOUNT_VALUE = "//label[contains(@for,'adults_buttonset_%s')]/span";
    private final String CHILDREN_AMOUNT_VALUE = "//label[contains(@for,'kids_buttonset_%s')]/span";
    private final String INFANTS_AMOUNT_VALUE = "//label[contains(@for,'infants_buttonset_%s')]/span";

    private final String HOTEL_CLASS = "//ul[contains(@class, 'hotelCategory')]/li/label/input[@value = '%s']";
    private final String HOTEL_CLASS_CHECKBOX = "/following-sibling::div";
    private final String MEAL_TYPE = "//ul[contains(@class, 'meal')]/li/label/input[@value = '%s']";
    private final String MEAL_TYPE_CHECKBOX = "/following-sibling::div";

    private final String SHOW_HOTEL_TOURS = "(//div[@class = 'details-bx__title']/a)[%s]";
    private final String HOTEL = "(//a[contains(@style, 'background-image')])[%s]";

    private final String START_SEARCH_BUTTON = "startSearch"; //id

    public Pattern pattern = Pattern.compile("\\(([^\\s]+)\\)");
    protected static int hotelCounter = 1;


    public HotelSearchPage waitForResultShown() throws Exception
    {
        $(byXpath(OPERATORS_SEARCH_BLOCK)).waitUntil(not(visible), CONSTANT_20_SECONDS);
        Assert.assertFalse($(byXpath(EMPTY_SEARCH_RESULT)).is(visible), "Serp Page: search doesn't have results");

        return this;
    }

    public HotelSearchPage selectCountryAndResort(String country, String resort) throws Exception
    {
        $(byXpath(COUNTRY_AND_RESORT_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(COUNTRY_VALUE, country))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        if (!resort.equals(""))
        {
            $(byXpath(COUNTRY_AND_RESORT_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            $(byXpath(String.format(RESORT_VALUE, resort))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        return this;
    }

    public HotelSearchPage selectDate(int start, int end)
    {
        String flightDate = getFullDateFromCurrent(start);

        $(byXpath(CALENDAR_FROM_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        String[] startFlightDateArray = flightDate.split("-");

        SelenideElement startFlightDate = $(byXpath(String.format(FLIGHT_DATE_FROM_VALUE, startFlightDateArray[0],
                Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(startFlightDateArray[1])) - 1), deleteFirstZeroFromDate(startFlightDateArray[2]))));

        while(startFlightDate.is(not(visible)))
        {
            $(byXpath(CALENDAR_NEXT_MONTH_ARROW_FROM)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        startFlightDate.click();



        $(byXpath(CALENDAR_TO_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        SelenideElement endFlightDate;

        if(end > 29)
        {
            flightDate = getFullDateFromCurrent(start + end);

            String[] endFlightDateArray = flightDate.split("-");

            endFlightDate = $(byXpath(String.format(FLIGHT_DATE_TO_VALUE, endFlightDateArray[0],
                    Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(endFlightDateArray[1])) - 1), deleteFirstZeroFromDate(endFlightDateArray[2]))));

            while(endFlightDate.is(not(visible)))
            {
                $(byXpath(CALENDAR_NEXT_MONTH_ARROW_TO)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }

            endFlightDate.click();

            Assert.assertTrue($(byClassName(CALENDAR_ERROR_MESSAGE)).is(visible));

            flightDate = getFullDateFromCurrent(start + 29);

            endFlightDateArray = flightDate.split("-");

            endFlightDate = $(byXpath(String.format(FLIGHT_DATE_TO_VALUE, endFlightDateArray[0],
                    Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(endFlightDateArray[1])) - 1), deleteFirstZeroFromDate(endFlightDateArray[2]))));

            Assert.assertTrue($(byXpath("(//td[contains(@class, 'current-day')]/a)[2]")).innerHtml().equals(endFlightDate.innerHtml()));
        }
        else
        {
            flightDate = getFullDateFromCurrent(start + end);

            String[] endFlightDateArray = flightDate.split("-");

            endFlightDate = $(byXpath(String.format(FLIGHT_DATE_TO_VALUE, endFlightDateArray[0],
                    Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(endFlightDateArray[1])) - 1), deleteFirstZeroFromDate(endFlightDateArray[2]))));

            while(endFlightDate.is(not(visible)))
            {
                $(byXpath(CALENDAR_NEXT_MONTH_ARROW_TO)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }

            endFlightDate.click();
            Assert.assertTrue($(byClassName(CALENDAR_ERROR_MESSAGE)).is(not(visible)));

        }




        return this;
    }


    public HotelSearchPage selectAdults(String adultsAmountValue) throws Exception
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        $(byXpath(String.format(ADULTS_AMOUNT_VALUE, adultsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
    public HotelSearchPage selectChildren(String childrenAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(CHILDREN_AMOUNT_VALUE, childrenAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public HotelSearchPage selectInfants(String infantsAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(INFANTS_AMOUNT_VALUE, infantsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public HotelSearchPage clickSearchButton() throws Exception
    {

        $(byId(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
    public HotelSearchPage selectHotelClass(String hotelClass) throws Exception
    {
        JavascriptExecutor jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();
        jse.executeScript("window.scroll(0, 400)", "");
        $(byXpath(String.format(HOTEL_CLASS, Constants.hotelClass.get("2")) + HOTEL_CLASS_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_1_SECOND);
        jse.executeScript("window.scroll(0, 400)", "");
        $(byXpath(String.format(HOTEL_CLASS, Constants.hotelClass.get("")) + HOTEL_CLASS_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_1_SECOND);

        if(!hotelClass.equals(""))
        {
            String[] hotelClassArray = hotelClass.split(", ");
            jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();

            for (int i = 0; i != hotelClassArray.length; i++) {

                jse.executeScript("window.scroll(0, 400)", "");

                $(byXpath(String.format(HOTEL_CLASS, Constants.hotelClass.get(hotelClassArray[i])) + HOTEL_CLASS_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                Selenide.sleep(CONSTANT_1_SECOND);
            }
            jse.executeScript("window.scroll(0, 0)", "");
        }

        return this;
    }

    public HotelSearchPage selectMealType(String mealType) throws Exception
    {
        if(!mealType.equals(""))
        {
            String[] mealTypeArray = mealType.split(", ");
            JavascriptExecutor jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();


            for (int i = 0; i != mealTypeArray.length; i++) {
                jse.executeScript("window.scroll(0, 700)", "");

                $(byXpath(String.format(MEAL_TYPE, Constants.mealType.get(mealTypeArray[i])) + MEAL_TYPE_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                Selenide.sleep(CONSTANT_1_SECOND);
            }

            jse.executeScript("window.scroll(0, 0)", "");

        }


        return this;
    }
    public HotelSearchPage clickMoreHotelOffers() throws Exception
    {

        Matcher matcher = pattern.matcher($(byXpath(String.format(HOTEL, Integer.toString(hotelCounter)))).getAttribute("style"));

        matcher.find();
        while (matcher.group(1).equals(""))
        {
            hotelCounter++;
        }
        if(hotelCounter > 1)
        {
            $(byXpath(String.format(SHOW_HOTEL_TOURS, Integer.toString(hotelCounter - 1)))).scrollTo();
        }
        $(byXpath(String.format(SHOW_HOTEL_TOURS, Integer.toString(hotelCounter)))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_3_SECONDS);


        return this;
    }
}
