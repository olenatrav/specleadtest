package pageobjects.Travelata;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;

import framework.Constants;
import framework.WebDriverCommands;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;


/**
 * Created by User on 30.08.2016.
 */
public class SearchPage extends WebDriverCommands
{

    private static SearchPage searchPage = null;
    private SearchPage()
    {
    }
    public static synchronized SearchPage getSearchPage()
    {
        if(searchPage == null)
            searchPage = new SearchPage();

        return searchPage;
    }

    private final String HEADER = "//div[@class = 'headerContainer']";

    private final String HOTEL = "(//a[contains(@style, 'background-image')])[%s]";
    private final String LOADING = "//div[contains(@class, 'loadingInProgress')]";


    private final String DEPARTURE_CITY_VALUE = "//div[@id = 'departureCitySelect']/div/a/span";
    private final String DEPARTURE_CITY_DROPDOWN = "//div[@id = 'departureCitySelect']/div/a/span";
    private final String DEPARTURE_CITY_DROPDOWN_VALUE = "//div[text() = '%s']";

    private final String LOCATION_DROPDOWN = "//input[@name = 'destination']";

    private final String HOT_TOURS_LOCATION_DROPDOWN = "(//div[@class = 'formInputPlace'])[1]";
    private final String SELECTED_COUNTRY_HOT_TOURS = "//ul[@class = 'selected']/li/div/label/div/div";
    private final String COUNTRY_DROPDOWN_VALUE = "//span[text() = '%s']";

    private final String DATE_DROPDOWN = "(//div[@class = 'formInputPlace'])[2]";
    private final String CLOSEST_THREE_DAYS_HOT_TOUR = "//li[text() = 'ближайшие 3 дня']";
    private final String CLOSEST_WEEK_HOT_TOUR = "//li[text() = 'ближайшая неделя']";
    private final String CLOSEST_TWO_WEEKS_HOT_TOUR = "//li[text() = 'ближайшие 2 недели']";

    private final String NIGHTS_DROPDOWN = "(//div[@class = 'formInputPlace'])[3]";
    private final String NIGHTS_PLUS_BUTTON = "/following-sibling::a[contains(@class, 'ui-spinner-up')]";
    private final String NIGHTS_MINUS_BUTTON = "/following-sibling::a[contains(@class, 'ui-spinner-down')]";
    private final String NIGHTS_FROM_VALUE = "//input[contains(@class, 'duration tour-duration-from')]";
    private final String NIGHTS_TO_VALUE = "//input[contains(@class, 'duration tour-duration-to')]";

    private final String PASSENGERS_DROPDOWN = "(//div[@class = 'formInputPlace'])[4]";
    private final String PASSENGERS_DROPDOWN_OPEN_BLOCK = "//div[contains(@class, 'touristGroup open')]";
    private final String ADULTS_AMOUNT_VALUE = "//label[contains(@for,'adults_buttonset_%s')]/span";
    private final String CHILDREN_AMOUNT_VALUE = "//label[contains(@for,'kids_buttonset_%s')]/span";
    private final String INFANTS_AMOUNT_VALUE = "//label[contains(@for,'infants_buttonset_%s')]/span";

    private final String START_SEARCH_BUTTON = "startSearch"; //id

    private final String EMPTY_SEARCH_RESULT = "//div[contains(@class, 'searchEmpty')]";
    private final String SHOW_HOTEL_TOURS = "(//div[@class = 'details-bx__title']/a)[%s]";
    private final String TOUR_BUTTON = "(//div[@class = 'details-bx__destination-hotelName']/a)[%s]";
    private final String OPERATORS_SEARCH_BLOCK = "//div[@class = 'searchProgressOperators']";
    private final String HOTEL_BLOCK_CLASS = "//span[contains(@class, 'stars')]";

    private final String DEPARTURE_CITY_DROPDOWN_RUSSIA = "//label[text() = 'Город вылета']/following-sibling::div";
    private final String RESORT_DROPDOWN_RUSSIA = "//label[text() = 'Курорт']/following-sibling::div";
    private final String DATE_DROPDOWN_RUSSIA = "//label[text() = 'Дата вылета']/following-sibling::div";
    private final String PASSENGERS_DROPDOWN_RUSSIA = "//label[text() = 'Кто едет']/following-sibling::div";
    private final String RESORT_VALUE = "//div[text() = '%s']";
    private final String CALENDAR_DATE_VALUE = "//div[@class = 'formInputPlace']/input[contains(@class, 'calendarInput')]";
    private final String IS_FLEXIBLE_DATE_CHECKBOX = "//div[@class = 'dateFlexible']/label/span";
    private final String FLEX_FLAG_RUSSIA = "//span[@class = 'flex']";
    private final String FLIGHT_DATE_VALUE = "//td[@data-year='%s' and @data-month='%s']/a[text()='%s']";
    private final String CALENDAR_NEXT_MONTH_ARROW = "//a[@data-handler='next']/span";


    private final String COUNTRY_VALUE = "//input[@checked = '']/following-sibling::span";
    private final String RESORTS_VALUE = "(//input[@checked = '']/following-sibling::span)[i]";
    private final String ADULTS_VALUE = "//div[@id = 'adults_buttonset']/label[@aria-pressed = 'true']/span";
    private final String KIDS_VALUE = "//div[@id = 'kids_buttonset']/label[@aria-pressed = 'true']/span";
    private final String INFANTS_VALUE = "//div[@id = 'infants_buttonset']/label[@aria-pressed = 'true']/span";
    private final String IS_FLEXIBLE_VALUE = "//span[@class = 'datesLabel']";
    private final String DATE_VALUE = "//span[@class = 'datesLabel']/b";
    private final String NIGHTS_VALUE = "//div[@class = 'bottom center']/b";
    private final String HOTEL_CLASS = "//ul[contains(@class, 'hotelCategory')]/li/label/input[@value = '%s']";
    private final String HOTEL_CLASS_CHECKBOX = "/following-sibling::div";
    private final String MEAL_TYPE = "//ul[contains(@class, 'meal')]/li/label/input[@value = '%s']";
    private final String MEAL_TYPE_CHECKBOX = "/following-sibling::div";

    private final String FILTERS_RESORTS = "//div[@class = 'resortsSelected']";

    protected static int hotelCounter = 1;

    public static String[] datesValue;

    public static String[] nightsValue = new String[2];
    protected static boolean isFlexible;

    protected static String hotelClassValue = "";
    protected static String mealTypeValue = "";

    public Pattern pattern = Pattern.compile("\\(([^\\s]+)\\)");


    public SearchPage waitForResultShown() throws Exception
    {
        $(byXpath(OPERATORS_SEARCH_BLOCK)).waitUntil(not(visible), CONSTANT_20_SECONDS);
        Assert.assertFalse($(byXpath(EMPTY_SEARCH_RESULT)).is(visible), "Serp Page: search doesn't have results");

        return this;
    }
    public SearchPage checkSearchPageAssertions(String departureCity, String country, String resort, String[] dates, boolean isFlexible, String[] nights, String adults, String children, String infants) throws Exception
    {
        Assert.assertTrue($(byXpath(DEPARTURE_CITY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(departureCity.toLowerCase()), "SERP Page: departure cities are not equal");

        SelenideElement countryAndResortDropdown = $(byXpath(LOCATION_DROPDOWN));
        countryAndResortDropdown.waitUntil(visible, CONSTANT_10_SECONDS).click();

        if(!resort.equals(""))
        {

            Assert.assertTrue($(byXpath(COUNTRY_VALUE + "/i")).innerHtml().toLowerCase().equals(country.toLowerCase()), "SERP Page: Countries are not equal");
            Assert.assertTrue($(byXpath(String.format(RESORTS_VALUE, 1))).innerHtml().toLowerCase().replaceAll(",.*", "").equals(resort.toLowerCase()), "SERP Page: resorts are not equal");

        }

        if(MainPage.resortValue.equals(""))
        {
            Assert.assertTrue($(byXpath(COUNTRY_VALUE)).innerHtml().toLowerCase().contains(country.toLowerCase()), "SERP Page: countries are not equal");

        }

        $(byXpath(HEADER)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        String flightDates = $(byXpath(DATE_VALUE)).innerHtml().toLowerCase();
        Assert.assertTrue(flightDates.equals(dates[2] + "." + dates[1]), "SERP Page: flight dates are not equal");

        boolean isFlexibleValue = false;
        if($(byXpath(IS_FLEXIBLE_VALUE)).innerHtml().toLowerCase().replaceAll(".*\\(", "").equals("± 3 дня)"))
        {
            isFlexibleValue = true;
        }
        Assert.assertTrue(isFlexibleValue == isFlexible, "SERP Page: flexible dates are not equal");


        String flightNights = $(byXpath(NIGHTS_VALUE)).innerHtml().toLowerCase().replaceAll(" н.*", "");
        if (!nights[0].equals(nights[1]))
        {
            Assert.assertTrue(flightNights.equals(nights[0] + " - " + nights[1]), "SERP Page: nights are not equal");
        }
        else
        {
            Assert.assertTrue(flightNights.equals(nights[0]), "SERP Page: nights are not equal");
            Assert.assertTrue(flightNights.equals(nights[1]), "SERP Page: nights are not equal");
        }


        Assert.assertTrue($(byXpath(ADULTS_VALUE)).innerHtml().toLowerCase().equals(adults), "SERP Page: adults are not equal");

        if(children.equals(""))
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals("нет"), "SERP Page: children are not equal 'нет'");
        }
        else
        {
            Assert.assertTrue($(byXpath(KIDS_VALUE)).innerHtml().toLowerCase().equals(children), "SERP Page: children are not equal");
        }

        if(infants.equals(""))
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals("нет"), "SERP Page: infants are not equal 'нет'");
        }
        else
        {
            Assert.assertTrue($(byXpath(INFANTS_VALUE)).innerHtml().toLowerCase().equals(infants), "SERP Page: infants failure");
        }

        return this;
    }
    public SearchPage selectHotelClass(String hotelClass) throws Exception
    {

        JavascriptExecutor jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();
        jse.executeScript("window.scroll(0, 400)", "");
        $(byXpath(String.format(HOTEL_CLASS, Constants.hotelClass.get("2")) + HOTEL_CLASS_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_1_SECOND);
        jse.executeScript("window.scroll(0, 400)", "");
        $(byXpath(String.format(HOTEL_CLASS, Constants.hotelClass.get("")) + HOTEL_CLASS_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_1_SECOND);

        if(!hotelClass.equals(""))
        {
            String[] hotelClassArray = hotelClass.split(", ");
            jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();

            for (int i = 0; i != hotelClassArray.length; i++) {

                jse.executeScript("window.scroll(0, 400)", "");

                $(byXpath(String.format(HOTEL_CLASS, Constants.hotelClass.get(hotelClassArray[i])) + HOTEL_CLASS_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                Selenide.sleep(CONSTANT_1_SECOND);
            }
            jse.executeScript("window.scroll(0, 0)", "");
        }

        return this;
    }

    public SearchPage selectMealType(String mealType) throws Exception
    {
        if(!mealType.equals(""))
        {
            String[] mealTypeArray = mealType.split(", ");
            JavascriptExecutor jse = (JavascriptExecutor) WebDriverRunner.getWebDriver();


            for (int i = 0; i != mealTypeArray.length; i++) {
                jse.executeScript("window.scroll(0, 700)", "");

                $(byXpath(String.format(MEAL_TYPE, Constants.mealType.get(mealTypeArray[i])) + MEAL_TYPE_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                Selenide.sleep(CONSTANT_1_SECOND);
            }

            jse.executeScript("window.scroll(0, 0)", "");

        }


        return this;
    }
    public SearchPage changeMinimalTourPriceTo40k() throws Exception
    {
        Selenide.open(WebDriverRunner.getWebDriver().getCurrentUrl().replaceAll("6000", "50000"));
        Selenide.sleep(CONSTANT_3_SECONDS);
        $(byXpath(LOADING)).waitUntil(not(visible), CONSTANT_10_SECONDS);
        Assert.assertFalse($(byXpath(EMPTY_SEARCH_RESULT)).is(visible), "Search doesn't have results");

        return this;
    }

    public SearchPage clickMoreTours() throws Exception
    {

        Matcher matcher = pattern.matcher($(byXpath(String.format(HOTEL, Integer.toString(hotelCounter)))).getAttribute("style"));

        matcher.find();
        while (matcher.group(1).equals(""))
        {
            hotelCounter++;
        }
        while($(byXpath(String.format(SHOW_HOTEL_TOURS, Integer.toString(hotelCounter)))).is(not(visible)))
        {
            hotelCounter++;
        }

        if(hotelCounter > 1)
        {
            $(byXpath(String.format(SHOW_HOTEL_TOURS, Integer.toString(hotelCounter - 1)))).scrollTo();
        }
        $(byXpath(String.format(SHOW_HOTEL_TOURS, Integer.toString(hotelCounter)))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_3_SECONDS);

        switchToNewTab(1);
        HotelPage.getHotelPage().checkTourAcceptability();



        return this;
    }

    public SearchPage selectHotTour() throws Exception
    {
        while($(byXpath(String.format(TOUR_BUTTON, hotelCounter))).is(not(visible)))
        {
            hotelCounter++;
        }

        $(byXpath(String.format(TOUR_BUTTON, hotelCounter))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_3_SECONDS);

        switchToNewTab(1);
        HotelPage.getHotelPage().checkIfUnsuccessfulSearch();


        return this;
    }

    public SearchPage selectRussianTour() throws Exception
    {
        while($(byXpath(String.format(SHOW_HOTEL_TOURS, hotelCounter))).is(not(visible)))
        {
            hotelCounter++;
        }

        $(byXpath(String.format(SHOW_HOTEL_TOURS, hotelCounter))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        Selenide.sleep(CONSTANT_3_SECONDS);

        switchToNewTab(1);
        HotelPage.getHotelPage().checkIfUnsuccessfulSearch();


        return this;
    }

    public SearchPage selectDepartureCity(String departureCity) throws Exception
    {
        Selenide.sleep(CONSTANT_3_SECONDS);

        $(byXpath(DEPARTURE_CITY_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(DEPARTURE_CITY_DROPDOWN_VALUE, departureCity))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public SearchPage selectHotToursCountries(String countries)
    {
        String[] countriesArray = countries.split(", ");
        $(byXpath(HOT_TOURS_LOCATION_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        for(int i = 0; i < $$(byXpath(SELECTED_COUNTRY_HOT_TOURS)).size(); i++)
        {
            $(byXpath(SELECTED_COUNTRY_HOT_TOURS)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        for(int i = 0; i < countriesArray.length; i++)
        {
            $(byXpath(String.format(COUNTRY_DROPDOWN_VALUE, countriesArray[i]))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        return this;
    }

    public SearchPage selectHotToursDate(String date)
    {
        $(byXpath(DATE_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        if(date.equals("3 дня"))
        {
            $(byXpath(CLOSEST_THREE_DAYS_HOT_TOUR)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        else
        if(date.equals("неделя"))
        {
            $(byXpath(CLOSEST_WEEK_HOT_TOUR)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        else
        if(date.equals("2 недели"))
        {
            $(byXpath(CLOSEST_TWO_WEEKS_HOT_TOUR)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        return this;
    }

    public SearchPage selectNights(String from, String to)
    {
        $(byXpath(NIGHTS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        SelenideElement nightsFrom = $(byXpath(NIGHTS_FROM_VALUE));
        int nightsFromValue = Integer.parseInt(nightsFrom.getAttribute("aria-valuenow"));

        SelenideElement nightsTo = $(byXpath(NIGHTS_TO_VALUE));
        int nightsToValue = Integer.parseInt(nightsTo.getAttribute("aria-valuenow"));

        SelenideElement nightFromMinusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightFromPlusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_PLUS_BUTTON));

        int intFrom = Integer.parseInt(from);

        if(intFrom > nightsFromValue)
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromPlusButton.click();
            }
        }
        else
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromMinusButton.click();
            }
        }


        SelenideElement nightToMinusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightToPlusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_PLUS_BUTTON));

        int intTo = Integer.parseInt(to);

        if(intTo > nightsToValue)
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToPlusButton.click();
            }
        }
        else
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToMinusButton.click();
            }
        }


        nightsValue[0] = from;
        nightsValue[1] = to;

        return this;
    }


        public SearchPage selectAdults(String adultsAmountValue) throws Exception
        {
            if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
            {
                $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }

            $(byXpath(String.format(ADULTS_AMOUNT_VALUE, adultsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();


            return this;
        }
        public SearchPage selectChildren(String childrenAmountValue)
        {
            if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
            {
                $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
            $(byXpath(String.format(CHILDREN_AMOUNT_VALUE, childrenAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

            return this;
        }

        public SearchPage selectInfants(String infantsAmountValue)
        {
            if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
            {
                $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
            $(byXpath(String.format(INFANTS_AMOUNT_VALUE, infantsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

            return this;
        }

        public SearchPage clickStartButton() throws Exception
        {

            $(byId(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

            return this;
        }


    public SearchPage selectDepartureCityRussia(String departureCity)
    {
        $(byXpath(DEPARTURE_CITY_DROPDOWN_RUSSIA)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(String.format(DEPARTURE_CITY_DROPDOWN_VALUE, departureCity))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public SearchPage selectResortRussia(String resort)
    {
        $(byXpath(RESORT_DROPDOWN_RUSSIA)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(String.format(RESORT_VALUE, resort))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public SearchPage selectDateRussia(int dayFromCurrent) throws Exception
    {
        String flightDate = getFullDateFromCurrent(dayFromCurrent);
        $(byXpath(DATE_DROPDOWN_RUSSIA)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        String[] flightDateArray = flightDate.split("-");

        SelenideElement currentFlightDate = $(byXpath(String.format(FLIGHT_DATE_VALUE, flightDateArray[0],
                Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(flightDateArray[1])) - 1), deleteFirstZeroFromDate(flightDateArray[2]))));


        while(currentFlightDate.is(not(visible)))
        {
            $(byXpath(CALENDAR_NEXT_MONTH_ARROW)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        currentFlightDate.click();


        datesValue = flightDateArray;

        return this;
    }

    public SearchPage selectNightsRussia(String from, String to) throws Exception
    {

        SelenideElement nightsFrom = $(byXpath(NIGHTS_FROM_VALUE));
        int nightsFromValue = Integer.parseInt(nightsFrom.getAttribute("aria-valuenow"));

        SelenideElement nightsTo = $(byXpath(NIGHTS_TO_VALUE));
        int nightsToValue = Integer.parseInt(nightsTo.getAttribute("aria-valuenow"));

        SelenideElement nightFromMinusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightFromPlusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_PLUS_BUTTON));

        int intFrom = Integer.parseInt(from);

        if(intFrom > nightsFromValue)
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromPlusButton.click();
            }
        }
        else
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromMinusButton.click();
            }
        }


        SelenideElement nightToMinusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightToPlusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_PLUS_BUTTON));

        int intTo = Integer.parseInt(to);

        if(intTo > nightsToValue)
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToPlusButton.click();
            }
        }
        else
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToMinusButton.click();
            }
        }

        nightsValue[0] = from;
        nightsValue[1] = to;

        return this;
    }

    public SearchPage selectFlexibleDatesRussia(boolean isFlexibleDays)
    {
        if(isFlexibleDays)
        {
            $(byXpath(IS_FLEXIBLE_DATE_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }


        return this;
    }

    public SearchPage selectAdultsRussia(String adultsAmountValue) throws Exception
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN_RUSSIA)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        $(byXpath(String.format(ADULTS_AMOUNT_VALUE, adultsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public SearchPage selectChildrenRussia (String childrenAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN_RUSSIA)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(CHILDREN_AMOUNT_VALUE, childrenAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public SearchPage selectInfantsRussia(String infantsAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN_RUSSIA)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(INFANTS_AMOUNT_VALUE, infantsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }



}
