package pageobjects.Travelata;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import framework.WebDriverCommands;
import org.openqa.selenium.JavascriptExecutor;
import pageobjects.TOM.AuthorizationPage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
/**
 * Created by User on 16.05.2017.
 */
public class SuccessPage extends WebDriverCommands
{


    protected static String countryValue = "";
    protected static String resortValue = "";
    protected static String hotelValue = "";
    protected static String departureCityValue = "";
    protected static String departureDateValue = "";
    protected static String nightsValue = "";
    protected static String adultsValue = "";
    protected static String childrenValue = "";
    protected static String infantsValue = "";
    protected static String hotelClassValue = "";
    protected static String mealTypeValue = "";

    public SuccessPage(String country, String resort, String hotel, String departureCity, String departureDate, String nights, String adults, String children, String infants, String mealType, String hotelClass) throws Exception
    {
        countryValue = country;
        resortValue = resort;
        hotelValue = hotel;
        departureCityValue = departureCity;
        departureDateValue = departureDate;
        nightsValue = nights;
        adultsValue = adults;
        childrenValue = children;
        infantsValue = infants;
        hotelClassValue = hotelClass;
        mealTypeValue = mealType;
        //WebDriverRunner.getWebDriver().close();
        Selenide.close();
    }
/*
    public AuthorizationPage authorizationPage()
    {
        Selenide.open("http://tom.travellata.ru");


        return new AuthorizationPage();
    }*/


}
