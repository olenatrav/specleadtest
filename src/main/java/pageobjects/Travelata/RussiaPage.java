package pageobjects.Travelata;

/**
 * Created by User on 27.09.2017.
 */

import com.codeborne.selenide.WebDriverRunner;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;

public class RussiaPage extends WebDriverCommands
{
    private static RussiaPage russiaPage = null;
    private RussiaPage()
    {
    }
    public static synchronized RussiaPage getRussiaPage()
    {
        if(russiaPage == null)
            russiaPage = new RussiaPage();

        return russiaPage;
    }

    final private String RUSSIA_RESORT = "//.[@class = 'h3']";

    public RussiaPage selectResort()
    {
        $(byXpath(RUSSIA_RESORT)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }
}
