package pageobjects.Travelata;



import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import framework.WebDriverCommands;
import sun.applet.Main;
//import org.apache.xalan.xsltc.dom.SimpleResultTreeImpl;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;


/**
 * Created by User on 26.08.2016.
 */
public final class MainPage  extends WebDriverCommands
{
    private static MainPage mainPage = null;
    private MainPage()
    {
    }
    public static synchronized MainPage getMainPage()
    {
        if(mainPage == null)
            mainPage = new MainPage();

        return mainPage;
    }




    private final String REGION_DROPDOWN = "//div[@class = 'region']/span";
    private final String REGION_VALUE = "//a[contains(@title, '%s')]";

    private final String HEADER = "//div[@class = 'headerContainer']";
    private final String TOP_MENU_BUTTON = "//a[@href = '%s']";
    private final String TOP_MENU_MORE_BUTTON = "//a[text() = 'еще...']";


    private final String DEPARTURE_CITY_DROPDOWN = "//div[@id = 'departureCitySelect']/div/a/span";
    private final String DEPARTURE_CITY_VALUE = "//div[text() = '%s']";

    private final String COUNTRY_AND_RESORT_DROPDOWN = "//input[@name = 'destination']";
    private final String CLEAR_COUNTRY_BUTTON = "//div[@class='formInputPlace']/i[1]";
    private final String SELECTED_COUNTRY_VALUE = "//input[@checked = '']/following-sibling::span";
    private final String COUNTRY_VALUE = "//span[text() = '%s']";
    private final String RESORT_VALUE = "//span[text() = '%s, ']";

    private final String CALENDAR_DROPDOWN = "//div[@class = 'formInput']/i[contains(@class, 'date')]";
    private final String CALENDAR_DATE_VALUE = "//div[@class = 'formInputPlace']/input[contains(@class, 'calendarInput')]";
    private final String IS_FLEXIBLE_DATE_CHECKBOX = "//div[@class = 'dateFlexible']/label/span";
    private final String FLIGHT_DATE_VALUE = "//td[@data-year='%s' and @data-month='%s']/a[text()='%s']";
    private final String CALENDAR_NEXT_MONTH_ARROW = "//a[@data-handler='next']/span";


    private final String NIGHTS_DROPDOWN = "//div[@class = 'formInput']/i[contains(@class, 'nights')]";
    private final String NIGHTS_DROPDOWN_OPEN_BLOCK = "//div[contains(@class, 'nights open')]";
    private final String NIGHTS_PLUS_BUTTON = "/following-sibling::a[contains(@class, 'ui-spinner-up')]";
    private final String NIGHTS_MINUS_BUTTON = "/following-sibling::a[contains(@class, 'ui-spinner-down')]";
    private final String NIGHTS_FROM_VALUE = "//input[contains(@class, 'duration tour-duration-from')]";
    private final String NIGHTS_TO_VALUE = "//input[contains(@class, 'duration tour-duration-to')]";


    private final String PASSENGERS_DROPDOWN = "//div[@class = 'formInput']/i[contains(@class, 'who')]";
    private final String PASSENGERS_DROPDOWN_OPEN_BLOCK = "//div[contains(@class, 'touristGroup open')]";
    private final String ADULTS_AMOUNT_VALUE = "//label[contains(@for,'adults_buttonset_%s')]/span";
    private final String CHILDREN_AMOUNT_VALUE = "//label[contains(@for,'kids_buttonset_%s')]/span";
    private final String INFANTS_AMOUNT_VALUE = "//label[contains(@for,'infants_buttonset_%s')]/span";

    private final String START_SEARCH_BUTTON = "startSearch"; //id

    private final String GENERAL_LEAD_BUTTON = "//a[contains(@class, 'openGeneralLeadForm')]";
    private final String GENERAL_LEAD_COUNTRY_DROPDOWN = "//select[@class = 'countriesSelect']/following-sibling::button";
    private final String GENERAL_LEAD_COUNTRY_DROPDOWN_VALUE = "//input[@title = '%s']/following-sibling::span";
    private final String GENERAL_LEAD_SELECTED_COUNTRIES = "//input[@aria-selected = 'true']/following-sibling::span";
    private final String GENERAL_LEAD_SELECTED_COUNTRY = "(//input[@aria-selected = 'true']/following-sibling::span)[%s]";
    private final String GENERAL_LEAD_FROM_WHERE_TO_WHEN_NEXT_BUTTON = "(//div[@class = 'pageNav']/div[contains(text(), 'Далее')])[2]";

    private final String GENERAL_LEAD_DATE_DROPDOWN = "//div[@class = 'leadFromToDatePicker']";
    private final String GENERAL_LEAD_DATE_BACK_BUTTON = "//div[@class = 'periodPickerBackButton']";
    private final String GENERAL_LEAD_NEXT_ARROW_BUTTON = "(//a[@data-handler='next'])[2]";
    private final String GENERAL_LEAD_FLIGHT_DATE = "(//td[@data-year='%s' and @data-month='%s']/a[text()='%s'])[2]";
    private final String GENERAL_LEAD_FLEXIBLE_DATE_BUTTON = "//div[@class = 'plusminusButton']";
    private final String GENERAL_LEAD_NIGHTS_FROM = "(//input[@name = 'nightFrom'])[2]";
    private final String GENERAL_LEAD_NIGHTS_TO = "(//input[@name = 'nightTo'])[2]";
    private final String GENERAL_LEAD_FROM_MINUS_BUTTON = "//div[@id = 'nightFromMinus']";
    private final String GENERAL_LEAD_FROM_PLUS_BUTTON = "//div[@id = 'nightFromPlus']";
    private final String GENERAL_LEAD_TO_MINUS_BUTTON = "//div[@id = 'nightToMinus']";
    private final String GENERAL_LEAD_TO_PLUS_BUTTON = "//div[@id = 'nightToPlus']";
    private final String GENERAL_LEAD_FROM_WHEN_TO_WHOM_NEXT_BUTTON = "(//div[@class = 'pageNav']/div[contains(text(), 'Далее')])[3]";

    private final String GENERAL_LEAD_ADULTS_VALUE = "(//input[@name = 'adults'])[5]";
    private final String GENERAL_LEAD_CHILDREN_VALUE = "//input[@name = 'children']";
    private final String GENERAL_LEAD_INFANTS_VALUE = "(//input[@name = 'infants'])[5]";
    private final String GENERAL_LEAD_ADULTS_MINUS_BUTTON = "//div[@id='adultsMinus']";
    private final String GENERAL_LEAD_ADULTS_PLUS_BUTTON = "//div[@id='adultsPlus']";
    private final String GENERAL_LEAD_CHILDREN_MINUS_BUTTON = "//div[@id='childrenMinus']";
    private final String GENERAL_LEAD_CHILDREN_PLUS_BUTTON = "//div[@id='childrenPlus']";
    private final String GENERAL_LEAD_INFANTS_MINUS_BUTTON = "//div[@id='infantsMinus']";
    private final String GENERAL_LEAD_INFANTS_PLUS_BUTTON = "//div[@id='infantsPlus']";
    private final String GENERAL_LEAD_FROM_WHOM_TO_HOTEL_CLASS_NEXT_BUTTON = "(//div[@class = 'pageNav']/div[contains(text(), 'Далее')])[4]";

    private final String GENERAL_LEAD_FIVE_STARS = "(//div[@class = 'classStars'])[1]/preceding-sibling::div";
    private final String GENERAL_LEAD_FOUR_STARS = "(//div[@class = 'classStars'])[2]/preceding-sibling::div";
    private final String GENERAL_LEAD_THREE_STARS = "(//div[@class = 'classStars'])[3]/preceding-sibling::div";
    private final String GENERAL_LEAD_TWO_STARS = "(//div[@class = 'classStars'])[4]/preceding-sibling::div";
    private final String GENERAL_LEAD_ANY_STARS = "(//div[@class = 'classStars'])[5]/preceding-sibling::div";
    private final String GENERAL_LEAD_FROM_HOTEL_CLASS_TO_OFFICE_NEXT_BUTTON = "(//div[@class = 'pageNav']/div[contains(text(), 'Далее')])[5]";

    private final String GENERAL_LEAD_LIST_MODE_BUTTON = "//span[@id = 'listMode']";
    private final String GENERAL_LEAD_FIRST_OFFICE = "(//td[@class = 'officeCircle'])[1]";
    private final String GENERAL_LEAD_FIRST_OFFICE_ADDRESS = "(//td[@class = 'officeAddress']/span)[1]";
    private final String GENERAL_LEAD_FIRST_OFFICE_TIME = "(//td[@class = 'officeTime'])[1]";
    private final String GENERAL_LEAD_FROM_OFFICE_TO_CONTACTS_NEXT_BUTTON = "//div[@class = 'pageNav']/div/span[contains(text(), 'Далее')]";

    private final String GENERAL_LEAD_NAME_INPUT = "(//input[@name = 'name'])[2]";
    private final String GENERAL_LEAD_PHONE_INPUT = "(//input[@name = 'phone'])[2]";
    private final String GENERAL_LEAD_EMAIL_INPUT = "(//input[@name = 'email'])[3]";
    private final String GENERAL_LEAD_SEND_DATA_BUTTON = "//div[@class = 'pageNav']/div[contains(text(), 'Отправить')]";


    public static String departureCityValue = "";
    public static String countryValue = "";
    public static boolean isFlexible;
    public static String resortValue = "";
    public static String[] datesValue ;
    public static String[] nightsValue = new String[2];
    public static String adultsValue = "";
    public static String childrenValue = "";
    public static String infantsValue = "";

    public static String mealTypeValue = "";

    public static String officeAddressValue = "";
    public static String officeTimeValue = "";
    public static String customerEmailValue = "";


    public MainPage clickGenLeadButton()
    {
        $(byXpath(GENERAL_LEAD_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MainPage selectDestinationGenLead(String countries)
    {
        String[] destinationArray = countries.split(", ");

        $(byXpath(GENERAL_LEAD_COUNTRY_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        for(int j = 1; j < $$(byXpath(GENERAL_LEAD_SELECTED_COUNTRIES)).size() + 1; j++)
        {
            $(byXpath(String.format(GENERAL_LEAD_SELECTED_COUNTRY, j))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        int s = destinationArray.length;

        for(int i = 0; i < destinationArray.length; i++)
        {
            $(byXpath(String.format(GENERAL_LEAD_COUNTRY_DROPDOWN_VALUE, destinationArray[i]))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }


        return this;
    }

    public MainPage clickFromWhereToWhenButton()
    {
        $(byXpath(GENERAL_LEAD_FROM_WHERE_TO_WHEN_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;

    }

    public MainPage selectDateGenLead(int dayFromCurrent, boolean isFlexible)
    {

        $(byXpath(GENERAL_LEAD_DATE_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        if(isFlexible)
        {
            $(byXpath(GENERAL_LEAD_FLEXIBLE_DATE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }


        String flightDate = getFullDateFromCurrent(dayFromCurrent);

        String[] flightDateArray = flightDate.split("-");

        SelenideElement currentFlightDate = $(byXpath(String.format(GENERAL_LEAD_FLIGHT_DATE, flightDateArray[0],
                Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(flightDateArray[1])) - 1), deleteFirstZeroFromDate(flightDateArray[2]))));


        while(currentFlightDate.is(not(visible)))
        {
            $(byXpath(GENERAL_LEAD_NEXT_ARROW_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        currentFlightDate.click();

        datesValue = flightDateArray;

        return this;
    }
    public MainPage selectNightsGenLead(String from, String to)
    {
        SelenideElement nightsFrom = $(byXpath(GENERAL_LEAD_NIGHTS_FROM));
        int nightsFromValue = Integer.parseInt(nightsFrom.getAttribute("value"));

        SelenideElement nightsTo = $(byXpath(GENERAL_LEAD_NIGHTS_TO));
        int nightsToValue = Integer.parseInt(nightsTo.getAttribute("value"));

        SelenideElement nightFromMinusButton = $(byXpath(GENERAL_LEAD_FROM_MINUS_BUTTON));
        SelenideElement nightFromPlusButton = $(byXpath(GENERAL_LEAD_FROM_PLUS_BUTTON));

        int intFrom = Integer.parseInt(from);

        if(intFrom > nightsFromValue)
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("value")))
            {
                nightFromPlusButton.click();
            }
        }
        else
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("value")))
            {
                nightFromMinusButton.click();
            }
        }


        SelenideElement nightToMinusButton = $(byXpath(GENERAL_LEAD_TO_MINUS_BUTTON));
        SelenideElement nightToPlusButton = $(byXpath(GENERAL_LEAD_TO_PLUS_BUTTON));

        int intTo = Integer.parseInt(to);

        if(intTo > nightsToValue)
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("value")))
            {
                nightToPlusButton.click();
            }
        }
        else
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("value")))
            {
                nightToMinusButton.click();
            }
        }


        return this;
    }

    public MainPage clickFromWhenToWhomButton()
    {
        $(byXpath(GENERAL_LEAD_FROM_WHEN_TO_WHOM_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MainPage selectAdultsGenLead(String adults)
    {
        if(Integer.parseInt($(byXpath(GENERAL_LEAD_ADULTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value")) < Integer.parseInt(adults))
        {
            while(!$(byXpath(GENERAL_LEAD_ADULTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(adults))
            {
                $(byXpath(GENERAL_LEAD_ADULTS_PLUS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
        }
        else
        {
            while(!$(byXpath(GENERAL_LEAD_ADULTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(adults))
            {
                $(byXpath(GENERAL_LEAD_ADULTS_MINUS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
        }


        return this;
    }

    public MainPage selectChildrenGenLead(String children)
    {
        if(!children.equals(""))
        {
            if(Integer.parseInt($(byXpath(GENERAL_LEAD_CHILDREN_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value")) < Integer.parseInt(children))
            {
                while(!$(byXpath(GENERAL_LEAD_CHILDREN_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(children))
                {
                    $(byXpath(GENERAL_LEAD_CHILDREN_PLUS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }
            else
            {
                while(!$(byXpath(GENERAL_LEAD_CHILDREN_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(children))
                {
                    $(byXpath(GENERAL_LEAD_CHILDREN_MINUS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }

        }



        return this;
    }


    public MainPage selectInfantsGenLead(String infants)
    {
        if(!infants.equals(""))
        {
            if(Integer.parseInt($(byXpath(GENERAL_LEAD_INFANTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value")) < Integer.parseInt(infants))
            {
                while(!$(byXpath(GENERAL_LEAD_INFANTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(infants))
                {
                    $(byXpath(GENERAL_LEAD_INFANTS_PLUS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }
            else
            {
                while(!$(byXpath(GENERAL_LEAD_INFANTS_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(infants))
                {
                    $(byXpath(GENERAL_LEAD_INFANTS_MINUS_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }
        }


        return this;
    }

    public MainPage clickFromWhomToHotelClassButton()
    {
        $(byXpath(GENERAL_LEAD_FROM_WHOM_TO_HOTEL_CLASS_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MainPage selectHotelClassGenLead(String hotelClass)
    {
        if(!$(byXpath(GENERAL_LEAD_ANY_STARS)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("class").contains("checked"))
        {
            $(byXpath(GENERAL_LEAD_ANY_STARS)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        if(!hotelClass.equals(""))
        {
            String[] hotelClassArray = hotelClass.split(", ");

            for(int i = 0; i < hotelClassArray.length; i++)
            {
                if(hotelClassArray[i].equals("5"))
                {
                    $(byXpath(GENERAL_LEAD_FIVE_STARS)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
                else
                if(hotelClassArray[i].equals("4"))
                {
                    $(byXpath(GENERAL_LEAD_FOUR_STARS)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
                else
                if(hotelClassArray[i].equals("3"))
                {
                    $(byXpath(GENERAL_LEAD_THREE_STARS)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
                else
                if(hotelClassArray[i].equals("2"))
                {
                    $(byXpath(GENERAL_LEAD_TWO_STARS)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }


        }


        return this;
    }


    public MainPage clickFromHotelClassToOfficeButton()
    {
        $(byXpath(GENERAL_LEAD_FROM_HOTEL_CLASS_TO_OFFICE_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MainPage selectOfficeGenLead()
    {
        Selenide.sleep(CONSTANT_3_SECONDS);
        $(byXpath(GENERAL_LEAD_LIST_MODE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(GENERAL_LEAD_FIRST_OFFICE)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        officeAddressValue = $(byXpath(GENERAL_LEAD_FIRST_OFFICE_ADDRESS)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml();
        officeTimeValue = $(byXpath(GENERAL_LEAD_FIRST_OFFICE_TIME)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml();

        return this;
    }

    public MainPage clickFromOfficeToContactsNextButton()
    {
        $(byXpath(GENERAL_LEAD_FROM_OFFICE_TO_CONTACTS_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MainPage enterNameGenLead(String name)
    {
        $(byXpath(GENERAL_LEAD_NAME_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(name);

        return this;
    }

    public MainPage enterPhoneGenLead(String phone)
    {
        $(byXpath(GENERAL_LEAD_PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(phone);

        return this;
    }

    public MainPage enterEmailGenLead(String email)
    {
        $(byXpath(GENERAL_LEAD_EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(email);

        return this;
    }

    public MainPage submitDataGenLead()
    {
        $(byXpath(GENERAL_LEAD_SEND_DATA_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }




    public MainPage clickTopMenuOption(String option)
    {
        if(option.equals("/russia"))
        {
            $(byXpath(TOP_MENU_MORE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).hover();
        }

        $(byXpath(String.format(TOP_MENU_BUTTON, option))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }


    public MainPage selectRegion(String region) throws Exception
    {
      String regionValue = region.substring(0, 1).toUpperCase() + region.substring(1).toLowerCase();

        $(byXpath(REGION_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(REGION_VALUE, regionValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        if(!$(byXpath(REGION_DROPDOWN)).innerHtml().equals(region))
        {
            Selenide.sleep(CONSTANT_3_SECONDS);
        }

        return this;

    }
    public MainPage selectDepartureCity(String departureCity) throws Exception
    {
        Selenide.sleep(CONSTANT_3_SECONDS);

        $(byXpath(DEPARTURE_CITY_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(DEPARTURE_CITY_VALUE, departureCity))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        departureCityValue = departureCity.toLowerCase();

        return this;
    }

    public MainPage selectCountryAndResorts(String country, String resort) throws Exception
    {

        SelenideElement countryAndResortDropdown = $(byXpath(COUNTRY_AND_RESORT_DROPDOWN));
        countryAndResortDropdown.waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(String.format(COUNTRY_VALUE, country))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(HEADER)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        if (!resort.equals(""))
        {
            countryAndResortDropdown.waitUntil(visible, CONSTANT_10_SECONDS).click();

            $(byXpath(String.format(RESORT_VALUE, resort))).waitUntil(visible, CONSTANT_10_SECONDS).click();

            resortValue = resort.toLowerCase();
        }

        countryValue = country.toLowerCase();

        return this;
    }

    public MainPage selectDate(int dayFromCurrent) throws Exception
    {
        String flightDate = getFullDateFromCurrent(dayFromCurrent);
        $(byXpath(CALENDAR_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        String[] flightDateArray = flightDate.split("-");

        SelenideElement currentFlightDate = $(byXpath(String.format(FLIGHT_DATE_VALUE, flightDateArray[0],
                Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(flightDateArray[1])) - 1), deleteFirstZeroFromDate(flightDateArray[2]))));


        while(currentFlightDate.is(not(visible)))
        {
            $(byXpath(CALENDAR_NEXT_MONTH_ARROW)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        currentFlightDate.click();


        datesValue = flightDateArray;

        return this;
    }

    public MainPage selectFlexibleDates(boolean isFlexibleDays)
    {
        if(isFlexibleDays)
        {
            if(!($(byXpath(CALENDAR_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").contains("по") ||
                    $(byXpath(CALENDAR_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").contains("дня")))
            {
                $(byXpath(CALENDAR_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                $(byXpath(IS_FLEXIBLE_DATE_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
        }
        if(!isFlexibleDays)
        {
            if($(byXpath(CALENDAR_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").contains("по") ||
                    $(byXpath(CALENDAR_DATE_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").contains("дня"))
            {
                $(byXpath(CALENDAR_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                $(byXpath(IS_FLEXIBLE_DATE_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
        }
        isFlexible = isFlexibleDays;

        return this;
    }

    public MainPage selectNights(String from, String to) throws Exception
    {
        if($(byXpath(NIGHTS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(NIGHTS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }


        SelenideElement nightsFrom = $(byXpath(NIGHTS_FROM_VALUE));
        int nightsFromValue = Integer.parseInt(nightsFrom.getAttribute("aria-valuenow"));

        SelenideElement nightsTo = $(byXpath(NIGHTS_TO_VALUE));
        int nightsToValue = Integer.parseInt(nightsTo.getAttribute("aria-valuenow"));

        SelenideElement nightFromMinusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightFromPlusButton = $(byXpath(NIGHTS_FROM_VALUE + NIGHTS_PLUS_BUTTON));

        int intFrom = Integer.parseInt(from);

        if(intFrom > nightsFromValue)
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromPlusButton.click();
            }
        }
        else
        {
            while(intFrom != Integer.parseInt(nightsFrom.getAttribute("aria-valuenow")))
            {
                nightFromMinusButton.click();
            }
        }


        SelenideElement nightToMinusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_MINUS_BUTTON));
        SelenideElement nightToPlusButton = $(byXpath(NIGHTS_TO_VALUE + NIGHTS_PLUS_BUTTON));

        int intTo = Integer.parseInt(to);

        if(intTo > nightsToValue)
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToPlusButton.click();
            }
        }
        else
        {
            while(intTo != Integer.parseInt(nightsTo.getAttribute("aria-valuenow")))
            {
                nightToMinusButton.click();
            }
        }


        nightsValue[0] = from;
        nightsValue[1] = to;

        return this;
    }
    public MainPage selectAdults(String adultsAmountValue) throws Exception
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }

        $(byXpath(String.format(ADULTS_AMOUNT_VALUE, adultsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();


        adultsValue = adultsAmountValue;



        return this;
    }
    public MainPage selectChildren(String childrenAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(CHILDREN_AMOUNT_VALUE, childrenAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        childrenValue = childrenAmountValue;

        return this;
    }

    public MainPage selectInfants(String infantsAmountValue)
    {
        if($(byXpath(PASSENGERS_DROPDOWN_OPEN_BLOCK)).is(not(visible)))
        {
            $(byXpath(PASSENGERS_DROPDOWN)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        $(byXpath(String.format(INFANTS_AMOUNT_VALUE, infantsAmountValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        infantsValue = infantsAmountValue;

        return this;
    }

    public MainPage clickStartButton() throws Exception
    {

        $(byId(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MainPage generateEmail()
    {

        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        Date currentDate = new Date();
        customerEmailValue = "autotest" + dateFormat.format(currentDate) + "@no-spam.ws";


        return this;
    }




}
