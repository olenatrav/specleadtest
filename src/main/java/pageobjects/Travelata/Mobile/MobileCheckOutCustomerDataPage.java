package pageobjects.Travelata.Mobile;

import framework.WebDriverCommands;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 01.08.2017.
 */
public class MobileCheckOutCustomerDataPage extends WebDriverCommands {

    private static MobileCheckOutCustomerDataPage mobileCheckOutCustomerDataPage = null;

    public static synchronized MobileCheckOutCustomerDataPage getCheckOutCustomerDataPage()
    {
        if(mobileCheckOutCustomerDataPage == null)
            mobileCheckOutCustomerDataPage = new MobileCheckOutCustomerDataPage();

        return mobileCheckOutCustomerDataPage;
    }

    final private String CUSTOMER_EMAIL_INPUT = "//input[@id='customerEmail']";
    final private String CUSTOMER_PHONE_INPUT = "//input[@id='customerPhone']";
    final private String CREATE_ORDER_BUTTON = "//div[@id='basicCustomerCreateOrderButton']";

    public MobileCheckOutCustomerDataPage setCustomerEmail(String customerEmailValue) throws Exception{
        $(byXpath(CUSTOMER_EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(customerEmailValue);

        return this;
    }

    public MobileCheckOutCustomerDataPage setCustomerPhone(String customerPhoneValue) throws Exception{
        $(byXpath(CUSTOMER_PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(customerPhoneValue);

        return this;
    }

    public MobileCheckOutTouristDataPage gotoCheckOutPage() throws Exception{
        $(byXpath(CREATE_ORDER_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileCheckOutTouristDataPage();
    }

}