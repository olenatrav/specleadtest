package pageobjects.Travelata.Mobile;

import framework.WebDriverCommands;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

/**
 * Created by Lena on 28.07.2017.
 */
public class MobileTourDetailsPage extends WebDriverCommands {

    private static MobileTourDetailsPage mobileTourDetailsPage = null;

    public static synchronized MobileTourDetailsPage getMobileTourDetailsPage()
    {
        if(mobileTourDetailsPage == null)
            mobileTourDetailsPage = new MobileTourDetailsPage();

        return mobileTourDetailsPage;
    }

    final private String HOTEL_TOURS_LOADER = "//div[@class = 'hotelSearchLoader']";
    final private String TOUR_DATES_LABEL = "//div[@class='tourNights']/div";
    final private String TOUR_NIGHTS_AMOUNT_LABEL = "//div[@class='tourNights']/span";
    final private String TOUR_TOURIST_GROUP_ADULTS = "//div[@class='touristGroup']/span[1]";
    final private String TOUR_TOURIST_GROUP_CHILDREN = "//div[@class='touristGroup']/span[2]";
    final private String ORDER_TOUR_BUTTON = "//a[contains(@class, 'openTourRequestForm')]";

    public MobileCheckOutCustomerDataPage gotoCheckOutPage() throws Exception{
        $(byXpath(ORDER_TOUR_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return new MobileCheckOutCustomerDataPage();
    }

    public MobileTourDetailsPage waitForTourLoaderDissapear() throws Exception{
        $(byId(HOTEL_TOURS_LOADER)).waitUntil(disappear, CONSTANT_20_SECONDS);

        return this;
    }

    public MobileTourDetailsPage checkTouristGroup(String touristGroupValue) throws Exception{
        String[] resortsToArr = touristGroupValue.split(", ");
        int childrenAmount = Integer.parseInt(resortsToArr[1]) + Integer.parseInt(resortsToArr[2]);
        Assert.assertTrue($(byXpath(TOUR_TOURIST_GROUP_ADULTS)).getText().contains(resortsToArr[0]),
                    "Количество взрослых не совпадает");
        Assert.assertTrue($(byXpath(TOUR_TOURIST_GROUP_CHILDREN)).getText().contains(Integer.toString(childrenAmount)),
                    "Количество детей не совпадает");

        return this;
    }

}
