package pageobjects.Travelata.Mobile.Ski;

import com.codeborne.selenide.SelenideElement;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 17.08.2017.
 */
public class MobileSkiMainPage extends WebDriverCommands{

    private static MobileSkiMainPage mobileSkiMainPage = null;

    public static synchronized MobileSkiMainPage getMobileSkiMainPage()
    {
        if(mobileSkiMainPage == null)
            mobileSkiMainPage = new MobileSkiMainPage();

        return mobileSkiMainPage;
    }

    final private String FROM_CITY_SELECT = "fromCity"; //id
    final private String FROM_CITY_VALUE = "//*[@id='formFromCity']/.//span[contains(text(), '%s')]";

    final private String TO_COUNTRY_SELECT = "country"; //id
    final private String TO_COUNTRY_SELECT_VALUE = "//div[@id = 'formToCountry']/.//span[@class='value' and text() = '%s']";
    final private String TO_RESORT_CHECKBOX_VALUE = "//input[@name = 'resort']/following-sibling::span[text() = '%s']";
    final private String RESORT_SELECT_BUTTON = "//div[contains(@class, 'backToHome')]";

    final private String DATES_SELECT = "tourDate"; //id
    final private String FLEXIBLE_3_DATES_BUTTON = "//span[text() = '3 дня']";
    final private String FLEXIBLE_3_DATES_BUTTON_LABEL = "//span[text() = '3 дня']/ancestor::label";
    final private String FLEXIBLE_5_DATES_BUTTON = "//span[text() = '5 дней']";
    final private String FLEXIBLE_5_DATES_BUTTON_LABEL = "//span[text() = '5 дней']/ancestor::label";
    final private String MONTH_VALUE_CALENDAR = "ui-datepicker-month"; //classname
    final private String YEAR_VALUE_CALENDAR = "ui-datepicker-year"; //classname
    final private String NEXT_MONTH_CLICK_BUTTON = "//a[@title = 'След>']";
    final private String PREV_MONTH_CLICK_BUTTON = "//a[@title = '<Пред']";
    final private String CURRENT_DAY_SELECT = "//td[@data-handler = 'selectDay']/a[text() = '%s']";
    private final String FLIGHT_DATE_VALUE = "//td[@data-year='%s' and @data-month='%s']/a[text()='%s']";
    final private String NIGHT_RANGE_FROM_INPUT = "nightRange-a"; //id
    final private String NIGHT_RANGE_TO_INPUT = "nightRange-b"; //id
    final private String NIGHTS_SLIDER_FROM = "//a[@aria-labelledby = 'nightRange-a-label' and @aria-valuenow = '%s']";
    final private String NIGHTS_SLIDER_TO = "//a[@aria-labelledby = 'nightRange-b-label' and @aria-valuenow = '%s']";
    final private String DATE_SELECT_BUTTON = "//span[text() = 'Выбрать']";

    final private String TOURIST_GROUP_SELECT = "//span[@class = 'formPrInput adults']";
    final private String ADULTS_AMOUNT_LABEL = "//label[@class = 'whoTotal_label' and @for = 'adult']/font";
    final private String ADULTS_PLUS_BUTTON = "//input[@id = 'adults']/../following-sibling::div[contains(@class, 'ui-icon-plus')]";
    final private String ADULTS_MINUS_BUTTON = "//input[@id = 'adults']/../preceding-sibling::div[contains(@class, 'ui-icon-minus')]";
    final private String CHILDREN_AMOUNT_LABEL = "//label[@class = 'whoTotal_label' and @for = 'children']/font";
    final private String CHILDREN_PLUS_BUTTON = "//input[@id = 'kids']/../following-sibling::div[contains(@class, 'ui-icon-plus')]";
    final private String CHILDREN_MINUS_BUTTON = "//input[@id = 'kids']/../preceding-sibling::div[contains(@class, 'ui-icon-minus')]";
    final private String INFANTS_AMOUNT_LABEL = "//label[@class = 'whoTotal_label' and @for = 'baby']/font";
    final private String INFANTS_PLUS_BUTTON = "//input[@id = 'infants']/../following-sibling::div[contains(@class, 'ui-icon-plus')]";
    final private String INFANTS_MINUS_BUTTON = "//input[@id = 'infants']/../preceding-sibling::div[contains(@class, 'ui-icon-minus')]";
    final private String TOURIST_GROUP_SELECT_BUTTON = "//span[contains(@class, 'backToHome')]";


    final private String START_SEARCH_BUTTON = "startSearch"; //id

    final private String NAV_PANEL_BUTTON = "//a[@href = '#nav-panel']";
    final private String HOT_TOURS_SELECT = "//a[@href = '/tury']";

    public MobileSkiMainPage selectCityFrom(String cityFromValue) throws Exception{
        $(byId(FROM_CITY_SELECT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(FROM_CITY_VALUE, cityFromValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MobileSkiMainPage selectCountryResortsTo(String countryToValue, String resortsValue) throws Exception{
        $(byId(TO_COUNTRY_SELECT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(String.format(TO_COUNTRY_SELECT_VALUE, countryToValue))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        String[] resortsToArr = resortsValue.split(", ");
        for(int i=0; i< resortsToArr.length; i++)
            $(byXpath(String.format(TO_RESORT_CHECKBOX_VALUE, resortsToArr[i]))).waitUntil(visible, CONSTANT_10_SECONDS).click();
        $(byXpath(RESORT_SELECT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MobileSkiMainPage selectDate(boolean isFlexibleDays, String cityFromValue, int dayFromCurrent) throws Exception{
        $(byId(DATES_SELECT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        if(isFlexibleDays){
            if (cityFromValue.equals("Москва") || cityFromValue.equals("Санкт-Петербург")){
                if ($(byXpath(FLEXIBLE_3_DATES_BUTTON_LABEL)).getAttribute("class").contains("ui-checkbox-off"))
                    $(byXpath(FLEXIBLE_3_DATES_BUTTON)).click();
            }
            else {
                if ($(byXpath(FLEXIBLE_5_DATES_BUTTON_LABEL)).getAttribute("class").contains("ui-checkbox-off"))
                    $(byXpath(FLEXIBLE_5_DATES_BUTTON)).click();
            }
        }
        else{
            if (cityFromValue.equals("Москва") || cityFromValue.equals("Санкт-Петербург")){
                if ($(byXpath(FLEXIBLE_3_DATES_BUTTON_LABEL)).getAttribute("class").contains("ui-checkbox-on"))
                    $(byXpath(FLEXIBLE_3_DATES_BUTTON)).click();
            }
            else {
                if ($(byXpath(FLEXIBLE_5_DATES_BUTTON_LABEL)).getAttribute("class").contains("ui-checkbox-on"))
                    $(byXpath(FLEXIBLE_5_DATES_BUTTON)).click();
            }
        }

        String flightDate = getFullDateFromCurrent(dayFromCurrent);

        String[] flightDateArray = flightDate.split("-");

        SelenideElement currentFlightDate = $(byXpath(String.format(FLIGHT_DATE_VALUE, flightDateArray[0],
                Integer.toString(Integer.parseInt(deleteFirstZeroFromDate(flightDateArray[1])) - 1), deleteFirstZeroFromDate(flightDateArray[2]))));


        while(currentFlightDate.is(not(visible)))
        {
            $(byXpath(NEXT_MONTH_CLICK_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        currentFlightDate.click();

        return this;
    }

    public MobileSkiMainPage selectNightsAmount(String nightsFromValue, String nightsToValue) throws Exception{
        $(byId(NIGHT_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byId(NIGHT_RANGE_TO_INPUT)).setValue(Integer.toString(29));
        $(byXpath(String.format(NIGHTS_SLIDER_TO, Integer.toString(29)))).click();
        $(byId(NIGHT_RANGE_FROM_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byId(NIGHT_RANGE_FROM_INPUT)).setValue(nightsFromValue);
        $(byXpath(String.format(NIGHTS_SLIDER_FROM, nightsFromValue))).click();
        $(byId(NIGHT_RANGE_TO_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).clear();
        $(byId(NIGHT_RANGE_TO_INPUT)).setValue(nightsToValue);
        $(byXpath(String.format(NIGHTS_SLIDER_TO, nightsToValue))).click();

        return this;
    }

    public MobileSkiMainPage clickSelectDatesNightsButton() throws Exception{
        $(byXpath(DATE_SELECT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MobileSkiMainPage selectTouristGroup(String touristGroupValue) throws Exception{
        $(byXpath(TOURIST_GROUP_SELECT)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        String[] resortsToArr = touristGroupValue.split(", ");
        while(!$(byXpath(ADULTS_AMOUNT_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().substring(0, 1).equals(resortsToArr[0])){
            if(Integer.parseInt($(byXpath(ADULTS_AMOUNT_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().substring(0, 1)) < Integer.parseInt(resortsToArr[0]))
                $(byXpath(ADULTS_PLUS_BUTTON)).click();
            else
                $(byXpath(ADULTS_MINUS_BUTTON)).click();
        }
        while(!$(byXpath(CHILDREN_AMOUNT_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().substring(0, 1).equals(resortsToArr[1])){
            if(Integer.parseInt($(byXpath(CHILDREN_AMOUNT_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().substring(0, 1)) < Integer.parseInt(resortsToArr[1]))
                $(byXpath(CHILDREN_PLUS_BUTTON)).click();
            else
                $(byXpath(CHILDREN_MINUS_BUTTON)).click();
        }
        while(!$(byXpath(INFANTS_AMOUNT_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().substring(0, 1).equals(resortsToArr[2])){
            if(Integer.parseInt($(byXpath(INFANTS_AMOUNT_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().substring(0, 1)) < Integer.parseInt(resortsToArr[2]))
                $(byXpath(INFANTS_PLUS_BUTTON)).click();
            else
                $(byXpath(INFANTS_MINUS_BUTTON)).click();
        }
        $(byXpath(TOURIST_GROUP_SELECT_BUTTON)).click();

        return this;
    }

    public MobileSkiSearchPage clickStartSearchToursButton() throws Exception{
        $(byId(START_SEARCH_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileSkiSearchPage();
    }

}
