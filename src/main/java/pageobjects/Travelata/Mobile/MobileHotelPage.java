package pageobjects.Travelata.Mobile;

import framework.WebDriverCommands;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 18.07.2017.
 */
public class MobileHotelPage extends WebDriverCommands{

    private static MobileHotelPage mobileHotelPage = null;

    public static synchronized MobileHotelPage getMobileHotelPage()
    {
        if(mobileHotelPage == null)
            mobileHotelPage = new MobileHotelPage();

        return mobileHotelPage;
    }

    final private String HOTEL_TOURS_LOADER = "//div[@class = 'hotelSearchLoader']";
    final private String TOUR_BLOCK = "//div[@id='tourProductTemplate']/div[%s]/div[@class='tourInfo']";
    final private String TOUR_BLOCK_START_DATE_LABEL = "//div[%s]/.//div[@class='datetime-fly']";
    final private String TOUR_BLOCK_TOUR_NIGHTS_LABEL = "//div[%s]/.//div[@class='tourNights']";
    final private String TOUR_BLOCK_TOURIST_GROUP_ADULTS = "//div[%s]/.//div[@class='touristGroup']/div[1]";
    final private String TOUR_BLOCK_TOURIST_GROUP_CHILDREN = "//div[%s]/.//div[@class='touristGroup']/div[2]";
    final private String TOUR_BLOCK_PRICE_LABEL = "//div[%s]/.//div[@class ='priceBlock']/span";
    final private String TOUR_BLOCK_LOGO = "//div[%s]/.//div[contains(@class, 'tourPrice_logo')]/img";
    final private String TOUR_MENU_ICON = "//div[%s]/.//i[@class='icon icon-i16_info']";

    public MobileHotelPage checkStartDate(boolean isFlexibleDays, int dayFromCurrent, String cityFromValue) throws Exception
    {
        int flexibleDaysAmount = 0;
        if(isFlexibleDays){
            if(cityFromValue.equals("Москва") || cityFromValue.equals("Санкт-Петербург"))
                flexibleDaysAmount = 3;
            else
                flexibleDaysAmount = 5;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM");
        Date flightDateFrom = formatter.parse(getFullDateFromCurrent(dayFromCurrent - flexibleDaysAmount));
        Date flightDateTo = formatter.parse(getFullDateFromCurrent(dayFromCurrent + flexibleDaysAmount));

        int i = 1;
        while ($(byXpath(String.format(TOUR_BLOCK, i))).exists()){

            if (flightDateFrom.equals(flightDateTo))
                Assert.assertTrue($(byXpath(String.format(TOUR_BLOCK_START_DATE_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 5).equals(formatter2.format(flightDateFrom)),
                        "Дата вылета не совпадает");

            else {
                String s = $(byXpath(String.format(TOUR_BLOCK_START_DATE_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 5);
                Assert.assertTrue($(byXpath(String.format(TOUR_BLOCK_START_DATE_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 5).equals(formatter2.format(flightDateFrom)) ||
                                formatter2.parse($(byXpath(String.format(TOUR_BLOCK_START_DATE_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 5)).after(flightDateFrom),
                        "Даты вылета не совпадают");
                Assert.assertTrue($(byXpath(String.format(TOUR_BLOCK_START_DATE_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 5).equals(formatter2.format(flightDateTo)) ||
                                formatter2.parse($(byXpath(String.format(TOUR_BLOCK_START_DATE_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 5)).before(flightDateTo),
                        "Даты вылета не совпадают");
            }

            i++;
        }

        return this;
    }

    public MobileHotelPage checkNightsAmount(String nightsFromValue, String nightsToValue) throws Exception{
        int i = 1;
        while ($(byXpath(String.format(TOUR_BLOCK, i))).exists()){
            if(nightsFromValue.equals(nightsToValue))
                Assert.assertTrue($(byXpath(String .format(TOUR_BLOCK_TOUR_NIGHTS_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().contains(nightsFromValue),
                        "Количество ночей не совпадает");
            else{
                Assert.assertTrue(Integer.parseInt($(byXpath(String .format(TOUR_BLOCK_TOUR_NIGHTS_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 2).trim()) >= Integer.parseInt(nightsFromValue) &&
                                Integer.parseInt($(byXpath(String .format(TOUR_BLOCK_TOUR_NIGHTS_LABEL, i))).waitUntil(exist, CONSTANT_10_SECONDS).innerText().substring(0, 2).trim()) <= Integer.parseInt(nightsToValue),
                        "Количество ночей не совпадает");
            }


            i++;
        }


        return this;
    }

    public MobileHotelPage checkTouristGroup(String touristGroupValue) throws Exception{
        String[] resortsToArr = touristGroupValue.split(", ");
        int childrenAmount = Integer.parseInt(resortsToArr[1]) + Integer.parseInt(resortsToArr[2]);
        int i = 1;
        while ($(byXpath(String.format(TOUR_BLOCK, i))).exists()){
            Assert.assertTrue($(byXpath(String.format(TOUR_BLOCK_TOURIST_GROUP_ADULTS, i))).innerText().contains(resortsToArr[0]),
                    "Количество взрослых не совпадает");
            Assert.assertTrue($(byXpath(String.format(TOUR_BLOCK_TOURIST_GROUP_CHILDREN, i))).innerText().contains(Integer.toString(childrenAmount)),
                    "Количество детей не совпадает");

            i++;
        }

        return this;
    }

    public MobileTourDetailsPage clickFirstTour() throws Exception{
        $(byXpath(String.format(TOUR_BLOCK, 1))).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileTourDetailsPage();
    }

    public MobileHotelPage waitForTourLoaderDissapear() throws Exception{
        $(byId(HOTEL_TOURS_LOADER)).waitUntil(disappear, CONSTANT_20_SECONDS);

        return this;
    }

}
