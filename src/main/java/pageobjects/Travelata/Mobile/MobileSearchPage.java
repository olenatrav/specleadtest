package pageobjects.Travelata.Mobile;

import framework.WebDriverCommands;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 11.07.2017.
 */
public class MobileSearchPage extends WebDriverCommands{

    private static MobileSearchPage mobileSearchPage = null;

    public static synchronized MobileSearchPage getMobileMainPage()
    {
        if(mobileSearchPage == null)
            mobileSearchPage = new MobileSearchPage();

        return mobileSearchPage;
    }

    final private String TOURS_LOADER = "loadingCanvas"; //id
    final private String TO_COUNTRY_LABEL = "//span[contains(@data-bind, 'toCountryLabel')]";
    final private String TO_RESORTS_LABEL = "//span[contains(@data-bind, 'resortsLabel')]";
    final private String DATE_LABEL = "//span[contains(@data-bind, 'dateLabel')]";
    final private String NIGHTS_LABEL = "//span[contains(@data-bind, 'html:nightsLabel')]";
    final private String HOTEL_CARD_NAME = "//div[%s]/.//div[@class='serpHotelName']";
    final private String HOTEL_CARD_PLACE = "//div[%s]/.//div[@class='serpHotelPlace']";
    final private String FIRST_HOTEL_CARD_PRICE = "//div[1]/.//div[@class='serpHotelPrice']";

    public MobileSearchPage waitForLoaderDissapear() throws Exception{
        $(byId(TOURS_LOADER)).waitUntil(disappear, CONSTANT_20_SECONDS);

        return this;
    }

    public MobileSearchPage checkToCountry(String countryToValue) throws Exception{
        Assert.assertTrue($(byXpath(TO_COUNTRY_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(countryToValue),
                "Страна не совпадает");

        return this;
    }

    public MobileSearchPage checkToResorts(String resortsToValue) throws Exception{
        String[] resortsToArr = resortsToValue.split(", ");
        if(resortsToArr.length == 1)
            Assert.assertTrue($(byXpath(TO_RESORTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(resortsToValue),
                    "Курорт не совпадает");
        else
            Assert.assertTrue($(byXpath(TO_RESORTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(Integer.toString(resortsToArr.length)),
                    "Количество курортов не совпадает");

        return this;
    }

    public MobileSearchPage checkDatesTo(boolean isFlexibleDays, int dayFromCurrent, String cityFromValue) throws Exception{
        int flexibleDaysAmount = 0;
        if(isFlexibleDays){
            if(cityFromValue.equals("Москва") || cityFromValue.equals("Санкт-Петербург"))
                flexibleDaysAmount = 3;
            else
                flexibleDaysAmount = 5;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM");
        Date flightDateFrom = formatter.parse(getFullDateFromCurrent(dayFromCurrent - flexibleDaysAmount));
        Date flightDateTo = formatter.parse(getFullDateFromCurrent(dayFromCurrent + flexibleDaysAmount));

        if (flightDateFrom.equals(flightDateTo))
            Assert.assertTrue($(byXpath(DATE_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(formatter2.format(flightDateFrom)),
                    "Дата вылета не совпадает");

        else
            Assert.assertTrue($(byXpath(DATE_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(formatter2.format(flightDateFrom) + " — " + formatter2.format(flightDateTo)),
                    "Даты вылета не совпадают");


         return this;
    }

    public MobileSearchPage checkNightsAmount(String nightsFromValue, String nightsToValue) throws Exception{
        if(nightsFromValue.equals(nightsToValue))
            Assert.assertTrue($(byXpath(NIGHTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(nightsFromValue + " ночей"),
                    "Количество ночей не совпадает");
        else
            Assert.assertTrue($(byXpath(NIGHTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(nightsFromValue + " - " + nightsToValue +" ночей"),
                    "Количество ночей не совпадает");

        return this;
    }

    public MobileSearchPage checkCountryResortByHotel(String countryToValue) throws Exception{
        int hotelCardCounter = 1;
        while($(byXpath(String.format(HOTEL_CARD_NAME, hotelCardCounter))).isDisplayed()){
            Assert.assertTrue($(By.xpath(String.format(HOTEL_CARD_PLACE, hotelCardCounter))).getText().contains(countryToValue),
                    "Страна не совпадает");
            hotelCardCounter ++;
        }

        return this;
    }

    public MobileHotelPage gotoFirstSerpMobileHotelPage() throws Exception{
        $(byXpath(FIRST_HOTEL_CARD_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileHotelPage();
    }

}
