package pageobjects.Travelata.Mobile;

import framework.WebDriverCommands;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 03.08.2017.
 */
public class MobileCheckOutSuccessPage extends WebDriverCommands {

    private static MobileCheckOutSuccessPage mobileCheckOutSuccessPage = null;

    public static synchronized MobileCheckOutSuccessPage getMobileCheckOutSuccessPage()
    {
        if(mobileCheckOutSuccessPage == null)
            mobileCheckOutSuccessPage = new MobileCheckOutSuccessPage();

        return mobileCheckOutSuccessPage;
    }

    private final String SUCCESS_MESSAGE = "//div[text() = 'Спасибо! Мы получили ваш платеж.']";
    private final String BACK_TO_MAIN_PAGE_BUTTON = "//a[contains(@class, 'goBackToHotel')]";

    public MobileCheckOutSuccessPage checkIfPaymentSuccessful() throws Exception{
        $(byXpath(BACK_TO_MAIN_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS);
        Assert.assertTrue($(byXpath(SUCCESS_MESSAGE)).isDisplayed(), "Платеж не прошел");

        return this;
    }

}
