package pageobjects.Travelata.Mobile;

import framework.WebDriverCommands;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.by;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 01.08.2017.
 */
public class MobileCheckOutTouristDataPage extends WebDriverCommands {

    private static MobileCheckOutTouristDataPage mobileCheckOutTouristDataPage = null;

    public static synchronized MobileCheckOutTouristDataPage getMobileCheckOutTouristDataPage()
    {
        if(mobileCheckOutTouristDataPage == null)
            mobileCheckOutTouristDataPage = new MobileCheckOutTouristDataPage();

        return mobileCheckOutTouristDataPage;
    }

    //foreign
    //adult
    final private String TOURIST_DATA_HEADER = "//div[@class = 'personalInfo'][%s]/.//div[@class = 'personalInfo_headTitle']/h4";
    final private String TOURIST_FOREIGN_LAST_NAME_INPUT = "//input[@name = 'touristDocumentForeignLastName_%s']";
    final private String TOURIST_FOREIGN_FIRST_NAME_INPUT = "//input[@name = 'touristDocumentForeignFirstName_%s']";
    final private String TOURIST_FOREIGN_BIRTHDAY_DATE_INPUT = "(//input[@name = 'touristDocumentBirthday_input_%s'])[%s]";
    final private String TOURIST_FOREIGN_MALE_SEX_CHECKBOX = "//label[@for = 'male-sex-%s']";
    final private String TOURIST_FOREIGN_FEMALE_SEX_CHECKBOX = "//label[@for = 'female-sex-%s']";
    final private String TOURIST_FOREIGN_PASSPORT_NUMBER_INPUT = "//input[@name = 'touristDocumentForeignSerialNumber_%s']";
    final private String TOURIST_FOREIGN_PASSPORT_EXPIRED_DATE_INPUT = "//input[@name = 'touristDocumentForeignIssueExpireDate_input_%s']";
    final private String TOURIST_FOREIGN_NATIONALITY_INPUT = "//input[@name = 'touristDocumentForeignNationality_%s']";
    final private String TOURIST_DATA_LABEL = "//div[@class = 'personalInfo_headTitle']";

    //child
    final private String TOURIST_FOREIGN_CHILD_WITH_PASSPORT_SELECT = "//select[@name = 'touristChildWithPassport_%s']";
    final private String TOURIST_FOREIGN_CHILD_WITH_NO_PASSPORT_INPUT = "//input[@name = 'touristDocumentForeignSerialNumberNoPassport_%s']";

    //internal
    final private String TOURIST_INTERNAL_PASSPORT_TYPE_SELECT = "//select[@name = 'touristDocumentType_%s']";
    final private String TOURIST_INTERNAL_LAST_NAME_INPUT = "//input[@name = 'touristDocumentInternalLastName_%s']";
    final private String TOURIST_INTERNAL_FIRST_NAME_INPUT = "//input[@name = 'touristDocumentInternalFirstName_%s']";
    final private String TOURIST_INTERNAL_MIDDLE_NAME_INPUT = "//input[@name = 'touristDocumentInternalMiddleName_%s']";
    final private String TOURIST_INTERNAL_BIRTHDAY_DATE_INPUT = "(//input[@name = 'touristDocumentBirthday_input_%s'])[%s]";
    final private String TOURIST_INTERNAL_PASSPORT_NUMBER_INPUT = "(//input[@name = 'touristDocumentInternalSerialNumber_%s'])[%s]";
    final private String TOURIST_INTERNAL_PASSPORT_ISSUED_DATE_INPUT = "//input[@name = 'touristDocumentInternalIssueDate_input_%s']";

    final private String COUPON_INPUT = "//input[@class = 'couponInput']";

    final private String OPTIONAL_FIELDS_ALERT_CLOSE_BUTTON = "//div[contains(@class, 'optionalFieldsAlertCloseButton')]";

    final private String CARD_NUMBER_INPUT = "//input[@name = 'cardDataPan']";
    final private String CARD_EXPIRED_MONTH_INPUT = "//input[@name = 'cardDataMonth']";
    final private String CARD_EXPIRED_YEAR_INPUT = "//input[@name = 'cardDataYear']";
    final private String CARD_CVV_INPUT = "//input[@name = 'cardDataCvv']";
    final private String CARD_CARDHOLDER_NAME_INPUT = "//input[@name = 'cardDataCardholder']";

    final private String SUBMIT_BUTTON = "//span[text() = 'Оплатить и забронировать']";

    final private String POPUP_3DS_FORM = "//div[@id='3ds']";
    final private String GET_PASSWORD_BUTTON = "//input[@name = 'GET_PWD']";
    final private String PASSWORD_TEXT = "//td[contains(text(), 'password \"sended\" to phone:')]";
    final private String PASSWORD_INPUT = "//input[@name = 'PWD']";
    final private String PASSWORD_SUBMIT_BUTTON = "//input[@name = 'SEND']";

    public MobileCheckOutTouristDataPage closeOptionalFieldsAlert() throws Exception{
        $(byXpath(OPTIONAL_FIELDS_ALERT_CLOSE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public MobileCheckOutTouristDataPage fillTouristData(String countryToValue, String foreignLastNameValue,
                                                         String foreignFirstNameValue, String birthdayDateValue,
                                                         String foreignSexValue, String passportNumberValue,
                                                         String foreignPassportExpiredDateValue, boolean ifChildWithPassport,
                                                         String foreignNationalityValue,
                                                         String passportType, String internalLastNameValue, String internalFirstNameValue,
                                                         String internalMiddleNameInput, String internalPassportIssuedDateValue) throws Exception{
        $(byXpath(TOURIST_DATA_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS);
        if(!(countryToValue.equals("Россия") || countryToValue.equals("Абхазия"))){
            int i=1;
            while ($(byXpath(String.format(TOURIST_DATA_HEADER, i))).isDisplayed()){
                $(byXpath(String.format(TOURIST_FOREIGN_LAST_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(foreignLastNameValue);
                $(byXpath(String.format(TOURIST_FOREIGN_FIRST_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(foreignFirstNameValue);
                $(byXpath(String.format(TOURIST_FOREIGN_BIRTHDAY_DATE_INPUT, i, 1))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(birthdayDateValue);
                if($(byXpath(String.format(TOURIST_DATA_HEADER, i))).getText().contains("ребенок")){
                    if(ifChildWithPassport){
                        $(byXpath(String.format(TOURIST_FOREIGN_CHILD_WITH_PASSPORT_SELECT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                                .selectOption("У ребенка есть свой паспорт");
                        if(foreignSexValue.equals("мужской"))
                            $(byXpath(String.format(TOURIST_FOREIGN_MALE_SEX_CHECKBOX, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        else
                            $(byXpath(String.format(TOURIST_FOREIGN_FEMALE_SEX_CHECKBOX, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        $(byXpath(String.format(TOURIST_FOREIGN_PASSPORT_NUMBER_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                                .setValue(passportNumberValue);
                        $(byXpath(String.format(TOURIST_FOREIGN_PASSPORT_EXPIRED_DATE_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                                .setValue(foreignPassportExpiredDateValue);
                        $(byXpath(String.format(TOURIST_FOREIGN_NATIONALITY_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                                .setValue(foreignNationalityValue);
                    }
                    else{
                        $(byXpath(String.format(TOURIST_FOREIGN_CHILD_WITH_PASSPORT_SELECT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                                .selectOption("У ребенка нет паспорта");
                        $(byXpath(String.format(TOURIST_FOREIGN_CHILD_WITH_NO_PASSPORT_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                                .setValue(passportNumberValue);
                    }
                }
                else{
                    if(foreignSexValue.equals("мужской"))
                        $(byXpath(String.format(TOURIST_FOREIGN_MALE_SEX_CHECKBOX, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                    else
                        $(byXpath(String.format(TOURIST_FOREIGN_FEMALE_SEX_CHECKBOX, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                    $(byXpath(String.format(TOURIST_FOREIGN_PASSPORT_NUMBER_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                            .setValue(passportNumberValue);
                    $(byXpath(String.format(TOURIST_FOREIGN_PASSPORT_EXPIRED_DATE_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                            .setValue(foreignPassportExpiredDateValue);
                    $(byXpath(String.format(TOURIST_FOREIGN_NATIONALITY_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                            .setValue(foreignNationalityValue);
                }
                i++;
            }
        }
        else if (passportType.equals("Российский паспорт")){
            int i = 1;
            while ($(byXpath(String.format(TOURIST_DATA_HEADER, i))).isDisplayed()){
                $(byXpath(String.format(TOURIST_INTERNAL_PASSPORT_TYPE_SELECT, i))).selectOption(passportType);
                $(byXpath(String.format(TOURIST_INTERNAL_LAST_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(internalLastNameValue);
                $(byXpath(String.format(TOURIST_INTERNAL_FIRST_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(internalFirstNameValue);
                $(byXpath(String.format(TOURIST_INTERNAL_MIDDLE_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(internalMiddleNameInput);
                $(byXpath(String.format(TOURIST_INTERNAL_BIRTHDAY_DATE_INPUT, i, 2))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(birthdayDateValue);
                if(!$(byXpath(String.format(TOURIST_DATA_HEADER, i))).getText().contains("ребенок")){
                    $(byXpath(String.format(TOURIST_INTERNAL_PASSPORT_ISSUED_DATE_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                            .setValue(internalPassportIssuedDateValue);
                    $(byXpath(String.format(TOURIST_INTERNAL_PASSPORT_NUMBER_INPUT, i, 2))).waitUntil(visible, CONSTANT_10_SECONDS)
                            .setValue(passportNumberValue);
                }
                else
                    $(byXpath(String.format(TOURIST_INTERNAL_PASSPORT_NUMBER_INPUT, i, 1))).waitUntil(visible, CONSTANT_10_SECONDS)
                            .setValue(passportNumberValue);


                i++;
            }
        }
        else{
            int i = 1;
            while ($(byXpath(String.format(TOURIST_DATA_HEADER, i))).isDisplayed()){
                $(byXpath(String.format(TOURIST_INTERNAL_PASSPORT_TYPE_SELECT, i))).selectOption(passportType);
                $(byXpath(String.format(TOURIST_FOREIGN_LAST_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(foreignLastNameValue);
                $(byXpath(String.format(TOURIST_FOREIGN_FIRST_NAME_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(foreignFirstNameValue);
                $(byXpath(String.format(TOURIST_FOREIGN_BIRTHDAY_DATE_INPUT, i, 3))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(birthdayDateValue);
                if(foreignSexValue.equals("мужской"))
                    $(byXpath(String.format(TOURIST_FOREIGN_MALE_SEX_CHECKBOX, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                else
                    $(byXpath(String.format(TOURIST_FOREIGN_FEMALE_SEX_CHECKBOX, i))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                $(byXpath(String.format(TOURIST_FOREIGN_PASSPORT_NUMBER_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(passportNumberValue);
                $(byXpath(String.format(TOURIST_FOREIGN_PASSPORT_EXPIRED_DATE_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(foreignPassportExpiredDateValue);
                $(byXpath(String.format(TOURIST_FOREIGN_NATIONALITY_INPUT, i))).waitUntil(visible, CONSTANT_10_SECONDS)
                        .setValue(foreignNationalityValue);

                i++;
            }
        }

        return this;
    }

    public MobileCheckOutTouristDataPage fillCardData(String cardNumberValue, String cardExpiredMonthValue,
                                                      String cardExpiredYearValue, String cardCvvValue, String cardHolderValue) throws Exception{
        $(byXpath(CARD_NUMBER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(cardNumberValue);
        $(byXpath(CARD_EXPIRED_MONTH_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardExpiredMonthValue);
        $(byXpath(CARD_EXPIRED_YEAR_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardExpiredYearValue);
        $(byXpath(CARD_CVV_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardCvvValue);
        $(byXpath(CARD_CARDHOLDER_NAME_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardHolderValue);

        return this;
    }

    public MobileCheckOutSuccessPage submitCheckOutPage() throws Exception{
        $(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileCheckOutSuccessPage();
    }

    public MobileCheckOutSuccessPage set3dsPassword() throws Exception{
        $(byXpath(POPUP_3DS_FORM)).waitUntil(visible, CONSTANT_10_SECONDS);
        $(byXpath(GET_PASSWORD_BUTTON)).waitUntil(exist, CONSTANT_10_SECONDS).click();
        String s = $(byXpath(PASSWORD_TEXT)).waitUntil(visible, CONSTANT_10_SECONDS).getValue();
        $(byXpath(PASSWORD_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(s);
        $(byXpath(PASSWORD_SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileCheckOutSuccessPage();
    }

    public MobileCheckOutTouristDataPage fillCoupon(String couponValue) throws Exception{
        if(!couponValue.isEmpty())
            $(byXpath(COUPON_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(couponValue);

        return this;
    }

}
