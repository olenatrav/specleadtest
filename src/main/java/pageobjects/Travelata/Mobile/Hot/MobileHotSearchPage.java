package pageobjects.Travelata.Mobile.Hot;

import framework.WebDriverCommands;
import org.openqa.selenium.By;
import org.testng.Assert;
import pageobjects.Travelata.Mobile.MobileTourDetailsPage;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by Lena on 15.08.2017.
 */
public class MobileHotSearchPage extends WebDriverCommands {

    private static MobileHotSearchPage mobileHotSearchPage = null;

    public static synchronized MobileHotSearchPage getMobileHotSearchPage()
    {
        if(mobileHotSearchPage == null)
            mobileHotSearchPage = new MobileHotSearchPage();

        return mobileHotSearchPage;
    }

    final private String TOURS_LOADER = "loadingCanvas"; //id
    final private String TO_COUNTRY_LABEL = "//span[contains(@data-bind, 'toCountryLabel')]";
    final private String TO_RESORTS_LABEL = "//span[contains(@data-bind, 'resortsLabel')]";
    final private String DATE_LABEL = "//span[contains(@data-bind, 'dateLabel')]";
    final private String NIGHTS_LABEL = "//span[contains(@data-bind, 'html:nightsLabel')]";
    final private String NOT_FOUND_LABEL = "//div[@class = 'notFound_top']";
    final private String HOTEL_CARD_NAME = "//div[%s]/.//div[@class='serpHotelName']";
    final private String HOTEL_CARD_PLACE = "//div[%s]/.//div[@class='serpHotelPlace']";
    final private String FIRST_HOTEL_CARD_PRICE = "//div[1]/.//div[@class='serpHotelPrice']";

    public MobileHotSearchPage waitForLoaderDissapear() throws Exception{
        $(byId(TOURS_LOADER)).waitUntil(disappear, CONSTANT_20_SECONDS);

        return this;
    }

    public MobileHotSearchPage checkToCountry(String countryToValue) throws Exception{
        Assert.assertTrue($(byXpath(TO_COUNTRY_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(countryToValue),
                "Страна не совпадает");

        return this;
    }

    public MobileHotSearchPage checkToResorts(String resortsToValue) throws Exception{
        String[] resortsToArr = resortsToValue.split(", ");
        if(resortsToArr.length == 1)
            Assert.assertTrue($(byXpath(TO_RESORTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(resortsToValue),
                    "Курорт не совпадает");
        else
            Assert.assertTrue($(byXpath(TO_RESORTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().contains(Integer.toString(resortsToArr.length)),
                    "Количество курортов не совпадает");

        return this;
    }

    public MobileHotSearchPage checkDatesTo(boolean isFlexibleDays, int dayFromCurrent, String cityFromValue) throws Exception{
        int flexibleDaysAmount = 0;
        if(isFlexibleDays){
            if(cityFromValue.equals("Москва") || cityFromValue.equals("Санкт-Петербург"))
                flexibleDaysAmount = 3;
            else
                flexibleDaysAmount = 5;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM");
        Date flightDateFrom = formatter.parse(getFullDateFromCurrent(dayFromCurrent - flexibleDaysAmount));
        Date flightDateTo = formatter.parse(getFullDateFromCurrent(dayFromCurrent + flexibleDaysAmount));

        if (flightDateFrom.equals(flightDateTo))
            Assert.assertTrue($(byXpath(DATE_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(formatter2.format(flightDateFrom)),
                    "Дата вылета не совпадает");

        else
            Assert.assertTrue($(byXpath(DATE_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(formatter2.format(flightDateFrom) + " — " + formatter2.format(flightDateTo)),
                    "Даты вылета не совпадают");


        return this;
    }

    public MobileHotSearchPage checkNightsAmount(String nightsFromValue, String nightsToValue) throws Exception{
        if(nightsFromValue.equals(nightsToValue))
            Assert.assertTrue($(byXpath(NIGHTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(nightsFromValue + " ночей"),
                    "Количество ночей не совпадает");
        else
            Assert.assertTrue($(byXpath(NIGHTS_LABEL)).waitUntil(visible, CONSTANT_10_SECONDS).getText().equals(nightsFromValue + " - " + nightsToValue +" ночей"),
                    "Количество ночей не совпадает");

        return this;
    }

    public MobileHotSearchPage checkToursAvailability() throws Exception{
        Assert.assertFalse($(byXpath(NOT_FOUND_LABEL)).isDisplayed(), "Туры по заданным критериям не найдены");

        return this;
    }

    public MobileHotSearchPage checkCountryResortByHotel(String countryToValue) throws Exception{
        int hotelCardCounter = 1;
        while($(byXpath(String.format(HOTEL_CARD_NAME, hotelCardCounter))).isDisplayed()){
            Assert.assertTrue($(By.xpath(String.format(HOTEL_CARD_PLACE, hotelCardCounter))).getText().contains(countryToValue),
                    "Страна не совпадает");
            hotelCardCounter ++;
        }

        return this;
    }

    public MobileTourDetailsPage gotoFirstSerpMobileHotelPage() throws Exception{
        $(byXpath(FIRST_HOTEL_CARD_PRICE)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        return new MobileTourDetailsPage();
    }

}
