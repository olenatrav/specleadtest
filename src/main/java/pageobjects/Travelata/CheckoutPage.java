package pageobjects.Travelata;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import framework.Constants;
import framework.WebDriverCommands;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 02.09.2016.
 */
public class CheckoutPage extends WebDriverCommands {
    private static CheckoutPage checkoutPage = null;

    private CheckoutPage() {
    }

    public static synchronized CheckoutPage getCheckoutPage() {
        if (checkoutPage == null)
            checkoutPage = new CheckoutPage();

        return checkoutPage;
    }

    private final String COUNTRY_VALUE = "countryName";
    private final String RESORT_VALUE = "resortName";
    private final String HOTEL_VALUE = "hotelTitle";
    private final String DATES_AND_NIGHTS_VALUE = "//span[text() = 'Даты тура:']/following-sibling::span";
    private final String PASSENGERS_VALUE = "//span[text() = 'Кто едет:']/following-sibling::span";
    private final String MEAL_TYPE_VALUE = "//span[text() = 'Питание:']/following-sibling::span";
    private final String HOTEL_CLASS_VALUE = "//div[contains(@class, 'hotelCategory')]";

    private final String CUSTOMER_EMAIL_INPUT = "customerEmail";
    private final String CUSTOMER_PHONE_INPUT = "customerPhone";
    private final String CONTINUE_BUTTON = "//div[@id='basicCustomerCreateOrderButton']/span";
    private final String CONTINUE_BUTTON_LOADER = "(//span[@class = 'btnLoader'])[1]";

    private final String FOREIGN_LAST_NAME_INPUT = "touristDocumentForeignLastName_%s";
    private final String FOREIGN_FIRST_NAME_INPUT = "touristDocumentForeignFirstName_%s";
    private final String FOREIGN_BIRTHDAY_INPUT = "touristDocumentBirthday_input_%s";
    private final String SEX_BUTTON = "//label[@for = '%s-sex-%s']/span";
    private final String FOREIGN_NATIONALITY_INPUT = "touristDocumentForeignNationality_%s";
    private final String FOREIGN_SERIAL_NUMBER_INPUT = "(//input[@name = 'touristDocumentForeignSerialNumber_%s'])[2]";
    private final String FOREIGN_SERIAL_NUMBER_CHECK_INPUT = "//input[@name = 'touristDocumentForeignSerialNumber_%s']";
    private final String FOREIGN_ISSUE_EXPIRE_DATE_INPUT = "(//input[@name = 'touristDocumentForeignIssueExpireDate_input_%s'])[2]";
    private final String FOREIGN_ISSUE_EXPIRE_DATE_CHECK_INPUT = "//input[@name = 'touristDocumentForeignIssueExpireDate_input_%s']";

    private final String INTERNAL_LAST_NAME_INPUT = "touristDocumentInternalLastName_%s";
    private final String INTERNAL_FIRST_NAME_INPUT = "touristDocumentInternalFirstName_%s";
    private final String INTERNAL_MIDDLE_NAME_INPUT = "touristDocumentInternalMiddleName_%s";
    private final String INTERNAL_BIRTHDAY_INPUT = "(//input[@name = 'touristDocumentBirthday_input_%s'])[2]";
    private final String INTERNAL_SERIAL_NUMBER_INPUT = "(//input[@name = 'touristDocumentInternalSerialNumber_%s'])[2]";
    private final String INTERNAL_ISSUE_DATE_INPUT = "//input[@name = 'touristDocumentInternalIssueDate_input_%s']";
    private final String INTERNAL_BIRTH_CERTIFICATE_INPUT = "//input[@name = 'touristDocumentInternalSerialNumber_%s']";

    private final String CONCIERGE_CHECKBOX = "//input[@name = 'travelСoncierge']/following-sibling::span";
    private final String CONCIERGE_CHECKED = "//span[@class = 'checkboxStyled checked']";
    private final String TOUR_WITH_CONCIERGE_BUTTON = "//div[@class = 'travelСonciergePopup__add']/span/b";
    private final String TOUR_WITHOUT_CONCIERGE_BUTTON = "//div[@class = 'travelСonciergePopup__close2']/span/b";

    private final String CARD_NUMBER_INPUT = "cardDataPan";
    private final String CARD_MONTH_INPUT = "cardDataMonth";
    private final String CARD_YEAR_INPUT = "cardDataYear";
    private final String CARD_CARDHOLDER_INPUT = "cardDataCardholder";
    private final String CARD_CVV_INPUT = "cardDataCvv";

    private final String SUBMIT_BUTTON = "//div[text() = 'Оплатить и забронировать']";

    final private String PAYMENT_CARD_BLOCK = "paymentCardInfo";

    public static String countryValue = "";

    public static String customerEmailValue = "";
    public static String customerPhoneValue = "";


    public static String firstNameValue = "";
    public static String lastNameValue = "";
    public static String middleNameValue = "";
    public static String birthdayAdultValue = "";
    public static String birthdayKidValue = "";
    public static String issueDateValue = "";
    public static String serialNumberValue = "";
    public static String sexValue = "";
    public static String nationalityValue = "";
    public static String expireDateValue = "";
 //   public static int touristsCount = 0;
    public static boolean abroad = true;




    public CheckoutPage checkCheckoutPageAssertions(String country, String resort, String hotel, String departureDate, String nights, String adults, String children, String infants,
                                                    String mealType, String hotelClass) throws Exception
    {
        String[] countryArray = country.split(", ");
        if(countryArray.length == 1)
        {
            Assert.assertTrue($(byClassName(COUNTRY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(country.toLowerCase()), "Checkout page: country");
            countryValue = country;
        }
        else
        {
            boolean countryMatch = false;
            for(int i = 0; i < countryArray.length; i++)
            {
                if($(byClassName(COUNTRY_VALUE)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(countryArray[i].toLowerCase()))
                {
                    countryMatch = true;
                    countryValue = countryArray[i];
                    break;
                }

            }
            Assert.assertTrue(countryMatch);
        }




        if (!resort.equals(""))
        {
            Assert.assertTrue($(byClassName(RESORT_VALUE)).innerHtml().toLowerCase().contains(resort.toLowerCase()), "Checkout page: resort");
        }

        Assert.assertTrue($(byClassName(HOTEL_VALUE)).innerHtml().toLowerCase().equals(hotel.toLowerCase()), "Checkout page: hotel");

        String departureDates[] = departureDate.split("\\.");
        Assert.assertTrue($(byXpath(DATES_AND_NIGHTS_VALUE)).innerHtml().replaceAll("\\s-.*", "").contains(departureDates[0] + " " + Constants.monthName.get(departureDates[1])), "Checkout page: date");

        Assert.assertTrue($(byXpath(DATES_AND_NIGHTS_VALUE)).innerHtml().toLowerCase().replaceAll("^\\s", "").replaceAll(".*\\(", "").replaceAll("\\s.*", "").equals(nights), "Checkout page: nights");

        String passengers = $(byXpath(PASSENGERS_VALUE)).innerHtml();
        Assert.assertTrue(passengers.contains(adults + " взросл"), "Checkout page: adults");

        if (!children.equals("") && !infants.equals(""))
        {
            Assert.assertTrue(passengers.contains(children + " ребен"), "Checkout page: children failure");
            Assert.assertTrue(passengers.contains(infants + " младен"), "Checkout page: infants");
        }
        else
        {
            if (!children.equals(""))
            {
                Assert.assertTrue(passengers.contains(children + " ребен"), "Checkout page: children");
            }

            if (!infants.equals(""))
            {
                Assert.assertTrue(passengers.contains(infants + " младен"), "Checkout page: infants");
            }
        }

        Assert.assertTrue($(byXpath(MEAL_TYPE_VALUE)).innerHtml().toLowerCase().contains(mealType), "Checkout page: meal type");

        if (hotelClass.equals("апартаменты"))
        {
            Assert.assertTrue($(byXpath(HOTEL_CLASS_VALUE)).innerHtml().toLowerCase().equals(hotelClass), "Checkout page: hotel class apartments");
        }
        else
        {
            Assert.assertTrue(Integer.toString($$(byXpath(HOTEL_CLASS_VALUE + "/i")).size()).equals(hotelClass), "Checkout page: hotel class");
        }

        return this;
    }

    public CheckoutPage generateEmail()
    {

        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        Date currentDate = new Date();
        customerEmailValue = "autotest" + dateFormat.format(currentDate) + "@no-spam.ws";


        return this;
    }
    public CheckoutPage enterCustomerEmail(String customerEmail)
    {
        $(byId(CUSTOMER_EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(customerEmail);

        return this;
    }
    public CheckoutPage enterCustomerPhone(String customerPhone)
    {
        $(byId(CUSTOMER_PHONE_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(customerPhone);

        return this;
    }
    public CheckoutPage clickContinueButton()
    {
        $(byXpath(CONTINUE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        $(byXpath(CONTINUE_BUTTON_LOADER)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }
    public CheckoutPage enterCustomerData(String email, String phone)
    {
        enterCustomerEmail(email);
        enterCustomerPhone(phone);
        clickContinueButton();

        return this;
    }



    public CheckoutPage enterTouristData(String country, String adults, String children, String infants, String internalLastName, String internalFirstName, String internalMiddleName, String adultBirthday, String kidBirthday,
                                         String internalIssueDate, String serialNumber,String foreignLastName, String foreignFirstName, String sex, String nationality, String expireDate)
    {
        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }

        if (country.toLowerCase().equals("россия") || country.toLowerCase().equals("абхазия"))
        {
            abroad = false;
        }

        if (!abroad)
        {

            for (int i = 1; i < touristsCount + 1; i++)
            {
                $(byName(String.format(INTERNAL_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(internalLastName);
                $(byName(String.format(INTERNAL_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(internalFirstName);
                $(byName(String.format(INTERNAL_MIDDLE_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(internalMiddleName);
                $(byXpath(String.format(INTERNAL_ISSUE_DATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(internalIssueDate);

                if (i <= Integer.parseInt(adults))
                {
                    $(byXpath(String.format(INTERNAL_SERIAL_NUMBER_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(serialNumber);
                    $(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(adultBirthday);
                }
                else
                {
                    $(byXpath(String.format(INTERNAL_BIRTH_CERTIFICATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(serialNumber);
                    $(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(kidBirthday);
                }

            }

            lastNameValue = internalLastName.toLowerCase();
            firstNameValue = internalFirstName.toLowerCase();
            middleNameValue = internalMiddleName.toLowerCase();
            birthdayAdultValue = adultBirthday;
            birthdayKidValue = kidBirthday;
            issueDateValue = internalIssueDate;
            serialNumberValue = serialNumber.toLowerCase();
        }
        else
        {
            for (int i = 1; i < touristsCount + 1; i++)
            {
                $(byName(String.format(FOREIGN_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(foreignLastName);
                $(byName(String.format(FOREIGN_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(foreignFirstName);
                $(byXpath(String.format(SEX_BUTTON, sex, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).click();
                $(byName(String.format(FOREIGN_NATIONALITY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(nationality);
                $(byXpath(String.format(FOREIGN_SERIAL_NUMBER_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(serialNumber);
                $(byXpath(String.format(FOREIGN_ISSUE_EXPIRE_DATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(expireDate);

                //if (i <= Integer.parseInt(MainPage.adultsValue))
                if (i <= Integer.parseInt(adults))
                {
                    $(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(adultBirthday);
                }
                else
                {
                    $(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(kidBirthday);
                }

            }

            lastNameValue = foreignLastName.toLowerCase();
            firstNameValue = foreignFirstName.toLowerCase();
            birthdayAdultValue = adultBirthday;
            birthdayKidValue = kidBirthday;
            serialNumberValue = serialNumber.toLowerCase();
            expireDateValue = expireDate;
            sexValue = sex;
            nationalityValue = nationality.toLowerCase();
        }


        return this;
    }


    public CheckoutPage selectConciergeCheckBox(boolean concierge)
    {
        if(concierge)
        {
            $(byXpath(CONCIERGE_CHECKBOX)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            $(byXpath(CONCIERGE_CHECKED)).waitUntil(visible, CONSTANT_10_SECONDS);
        }


        return this;
    }
    public  CheckoutPage selectConciergeAfterPay(boolean before, boolean after)
    {
        if(!before)
        {
            String with = $(byXpath(TOUR_WITH_CONCIERGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("innerHTML").replaceAll("\\s", "");
            String without = $(byXpath(TOUR_WITHOUT_CONCIERGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("innerHTML").replaceAll("\\s", "");
            Assert.assertTrue((Integer.parseInt(with) - Integer.parseInt(without)) == 590);

            if(after)
            {
                $(byXpath(TOUR_WITH_CONCIERGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
            else
            {
                $(byXpath(TOUR_WITHOUT_CONCIERGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
            }
        }

        return this;
    }





    public CheckoutPage enterCardNumber(String cardNumber)
    {
        $(byName(CARD_NUMBER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardNumber);

        return this;
    }
    public CheckoutPage enterCardMonth(String cardMonth)
    {
        $(byName(CARD_MONTH_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardMonth);

        return this;
    }
    public CheckoutPage enterCardYear(String cardYear)
    {
        $(byName(CARD_YEAR_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardYear);

        return this;
    }
    public CheckoutPage enterCardCardholder(String cardCardholder)
    {
        $(byName(CARD_CARDHOLDER_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).setValue(cardCardholder);

        return this;
    }
    public CheckoutPage enterCardCVV(String cardCVV)
    {
        $(byName(CARD_CVV_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(cardCVV);

        return this;
    }
    public CheckoutPage clickSubmitButton(boolean conciergeBeforePay, boolean conciergeAfterPay)
    {
        $(byXpath(SUBMIT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();

        selectConciergeAfterPay(conciergeBeforePay, conciergeAfterPay);

        $(byClassName(PAYMENT_CARD_BLOCK)).waitUntil(not(visible), CONSTANT_10_SECONDS);

        return this;
    }

    public CheckoutPage enterCardData(String cardNumber, String cardMonth, String cardYear, String cardholder, String CVV, boolean conciergeBeforePay, boolean conciergeAfterPay) throws Exception
    {

        enterCardNumber(cardNumber);
        enterCardMonth(cardMonth);
        enterCardYear(cardYear);
        enterCardCardholder(cardholder);
        enterCardCVV(CVV);
        clickSubmitButton(conciergeBeforePay, conciergeAfterPay);

        return this;
    }

    public CheckoutPage checkTestUrl(String url)
    {
        Pattern p = Pattern.compile("(?<=payment\\.)\\w*(?!travellata)");
        Matcher m = p.matcher(WebDriverRunner.getWebDriver().getCurrentUrl());
        m.find();
        if (!m.group(0).equals(url)) {
            Selenide.open(WebDriverRunner.getWebDriver().getCurrentUrl().replaceFirst("(?<=payment\\.)\\w*(?!travellata)", url));
        }

        return this;
    }



    public CheckoutPage checkTouristData(String email, boolean abroad, String adults, String children, String infants,
                                         String lastName, String firstName, String middleName, String issueDate, String serialNumber, String adultBirthday, String kidBirthday,
                                         String sex, String nationality, String expireDate)
    {
        Assert.assertTrue($(byId(CUSTOMER_EMAIL_INPUT)).waitUntil(visible, CONSTANT_10_SECONDS).getAttribute("value").equals(email));
        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }

        if (!abroad)
        {
            for (int i = 1; i < touristsCount + 1; i++)
            {

                Assert.assertTrue($(byName(String.format(INTERNAL_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(lastName));
                Assert.assertTrue($(byName(String.format(INTERNAL_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(firstName));
                Assert.assertTrue($(byName(String.format(INTERNAL_MIDDLE_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(middleName));
                Assert.assertTrue($(byXpath(String.format(INTERNAL_ISSUE_DATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(issueDate));

                if (i <= Integer.parseInt(adults))
                {
                    Assert.assertTrue($(byXpath(String.format(INTERNAL_SERIAL_NUMBER_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(serialNumber));
                    Assert.assertTrue($(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().equals(adultBirthday));
                }
                else
                {
                    Assert.assertTrue($(byXpath(String.format(INTERNAL_BIRTH_CERTIFICATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(serialNumber));
                    Assert.assertTrue($(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().equals(kidBirthday));
                }

            }

        }
        else
        {

            for (int i = 1; i < touristsCount + 1; i++) {
                Assert.assertTrue($(byName(String.format(FOREIGN_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(lastName));
                Assert.assertTrue($(byName(String.format(FOREIGN_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(firstName));

                if (sex.equals("female"))
                {
                    Assert.assertTrue($(byXpath(String.format(SEX_BUTTON, sex, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("Ж"));
                }
                else
                {
                    Assert.assertTrue($(byXpath(String.format(SEX_BUTTON, sex, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().equals("М"));
                }

                Assert.assertTrue($(byName(String.format(FOREIGN_NATIONALITY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(nationality));
                Assert.assertTrue($(byXpath(String.format(FOREIGN_SERIAL_NUMBER_CHECK_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(serialNumber));
                Assert.assertTrue($(byXpath(String.format(FOREIGN_ISSUE_EXPIRE_DATE_CHECK_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().toLowerCase().equals(expireDate));

                if (i <= Integer.parseInt(adults))
                {
                    Assert.assertTrue($(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().equals(adultBirthday));
                }
                else
                {
                    Assert.assertTrue($(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).getValue().equals(kidBirthday));
                }


            }
        }

        return this;

    }

    public CheckoutPage editTouristData(String adults, String children, String infants, boolean abroad, String lastName, String firstName, String middleName, String issueDate, String serialNumber,
                                        String adultBirthday, String kidBirthday, String sex, String nationality, String expireDate)
    {
        int touristsCount = Integer.parseInt(adults);

        if (!children.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(children);

        }
        if (!infants.equals(""))
        {
            touristsCount = touristsCount + Integer.parseInt(infants);
        }
        if (!abroad)
        {

            for (int i = 1; i < touristsCount + 1; i++)
            {

                $(byName(String.format(INTERNAL_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byName(String.format(INTERNAL_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(lastName);

                $(byName(String.format(INTERNAL_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byName(String.format(INTERNAL_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(firstName);

                $(byName(String.format(INTERNAL_MIDDLE_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byName(String.format(INTERNAL_MIDDLE_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(middleName);

                $(byXpath(String.format(INTERNAL_ISSUE_DATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byXpath(String.format(INTERNAL_ISSUE_DATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(issueDate);

                if (i <= Integer.parseInt(adults))
                {
                    $(byXpath(String.format(INTERNAL_SERIAL_NUMBER_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                    $(byXpath(String.format(INTERNAL_SERIAL_NUMBER_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(serialNumber);

                    $(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                    $(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(adultBirthday);
                }
                else
                {
                    $(byXpath(String.format(INTERNAL_BIRTH_CERTIFICATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                    $(byXpath(String.format(INTERNAL_BIRTH_CERTIFICATE_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(serialNumber);

                    $(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                    $(byXpath(String.format(INTERNAL_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(kidBirthday);
                }

            }
            lastNameValue = lastName;
            firstNameValue = firstName;
            middleNameValue = middleName;
            birthdayAdultValue = adultBirthday;
            birthdayKidValue = kidBirthday;
            issueDateValue = issueDate;
            serialNumberValue = serialNumber;

        }
        else
        {
            for (int i = 1; i < touristsCount + 1; i++)
            {
                $(byName(String.format(FOREIGN_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byName(String.format(FOREIGN_LAST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(lastName);

                $(byName(String.format(FOREIGN_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byName(String.format(FOREIGN_FIRST_NAME_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(firstName);

                $(byXpath(String.format(SEX_BUTTON, sexValue, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).click();

                $(byName(String.format(FOREIGN_NATIONALITY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byName(String.format(FOREIGN_NATIONALITY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(nationality);

                $(byXpath(String.format(FOREIGN_SERIAL_NUMBER_CHECK_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byXpath(String.format(FOREIGN_SERIAL_NUMBER_CHECK_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(serialNumber);

                $(byXpath(String.format(FOREIGN_ISSUE_EXPIRE_DATE_CHECK_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                $(byXpath(String.format(FOREIGN_ISSUE_EXPIRE_DATE_CHECK_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(expireDate);

                if (i <= Integer.parseInt(adults))
                {
                    $(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                    $(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(adultBirthday);
                }
                else
                {
                    $(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).clear();
                    $(byName(String.format(FOREIGN_BIRTHDAY_INPUT, Integer.toString(i)))).waitUntil(visible, CONSTANT_10_SECONDS).setValue(kidBirthday);
                }
            }
            lastNameValue = lastName;
            firstNameValue = firstName;
            birthdayAdultValue = adultBirthday;
            birthdayKidValue = kidBirthday;
            serialNumberValue = serialNumber;
            expireDateValue = expireDate;
            sexValue = sex;
            nationalityValue = nationality;

        }
        return this;
    }
}