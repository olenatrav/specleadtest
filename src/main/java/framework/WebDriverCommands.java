package framework;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import io.appium.java_client.android.AndroidDriver;

/**
 * Created by User on 26.08.2016.
 */
public class WebDriverCommands
{
    public final static int CONSTANT_1_SECOND = 1000;
    public final static int CONSTANT_3_SECONDS = 3000;
    public final static int CONSTANT_5_SECONDS = 5000;
    public final static int CONSTANT_10_SECONDS = 10000;
    public final static int CONSTANT_20_SECONDS = 20000;

    FirefoxProfile profile = createCustomFireFoxProfile();
    public static WebDriver driver;

    public FirefoxProfile createCustomFireFoxProfile()
    {
        FirefoxProfile profile = new FirefoxProfile();

        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", "C:\\Downloads\\Reports\\");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.manager.showAlertOnComplete", false);
        profile.setPreference("browser.download.manager.closeWhenDone", false);


        return profile;
    }


    public String fromJsonpToJsonParse(String JsonpResponse)
    {
        String json = JsonpResponse.replaceFirst(".*?\\(", "").replaceFirst("\\);", "");

        return json;
    }

    public String inventoryErrorSource(String inventoryUrl)
    {
        Matcher matcher;
        Pattern errorSourcePattern = Pattern.compile("inventory\\w*");
        matcher = errorSourcePattern.matcher(inventoryUrl);
        matcher.find();

        return matcher.group(0);
    }

    public String getKeyFromValue(Map hm, Object value)
    {
        Set ref = hm.keySet();
        Iterator iterator = ref.iterator();
        String key = new String();

        while (iterator.hasNext())
        {
            Object o = iterator.next();
            if (hm.get(o).equals(value))
            {
                key = o.toString();
            }
        }
        return key;
    }


    /**
     * Method waits for timeout interval
     * @param secsToWait   Integer Interval in sec to wait
     */
    public void waitTime(int secsToWait) {

        try {
            for (int second = 0; ; second++) {
                if ((second > secsToWait)) {
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }

    }

    /**
     * Method switch to new browser tab
     */
    public void switchToNewTab(int tabNumber){ //tab number starts from 0

        ArrayList<String> newTab = new ArrayList<String>(WebDriverRunner.getWebDriver().getWindowHandles());
//        newTab.remove(oldTab);
        // change focus to new tab
        WebDriverRunner.getWebDriver().switchTo().window(newTab.get(tabNumber));
    }

    public void closeOldTab()
    {
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(0));
        driver.close();
        tab.remove(tab.size());
    }

    /**
     * Method clear element
     * @param by By Element
     */
    public void clear(By by){
        if (isElementPresent(by)){
            driver.findElement(by).clear();
        }
    }

    /**
     * Method send keys to element
     * @param by    By Element
     * @param value String value
     */
    public void sendKeys(By by, String value){
        if (isElementPresent(by)){
            driver.findElement(by).sendKeys(value);
        }
    }

    public static String  getFullDateFromCurrent(int daysCount){
        Calendar c = Calendar.getInstance();
        Date dt = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        c.setTime(dt);
        c.add(Calendar.DATE, daysCount);

        return dateFormat.format(c.getTime());
    }
    public static String getFullDateFrom(String startDate, int nights) throws Exception
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date date = formatter.parse(startDate);
        c.setTime(date);
        c.add(Calendar.DATE, nights);

        return formatter.format(c.getTime());
    }

    public String deleteFirstZeroFromDate(String dayValue){
        dayValue = dayValue.replaceFirst("^0", "");
        return dayValue;
    }

    final private String EMPTY_RESULT_BLOCK = "//.[contains(text(), 'Пока не найдено ни одной записи')]";
    final private String PAGINATION_BLOCK = "//div[@class = 'pagination-container']/ul";
    final private String PAGINATION_TOTAL_PAGES_BUTTONS = "//ul[contains(@ng-show, 'totalPages')]/li";
    final private String PAGINATION_LAST_PAGE_BUTTON = "//ul[contains(@ng-show, 'totalPages')]/li[last()-1]/a";
    final private String PAGINATION_NEXT_BUTTON = "//a[text() = 'Next']";
    final private String PAGINATION_FIRST_PAGE_BUTTON = "//a[text() = '1']";
    final private String PAGINATION_LAST_POSSIBLE_PAGE_BUTTON = "//.[text() = 'Next']/../preceding-sibling::li[1]/a";
    final private String ORDERS_LIST = "//tbody/tr";

    final private String FILTER_ORDER_START_DATE = "//tbody/tr[%s]/td[6]";
    final private String FILTER_ORDER_TOUR_OPERATOR = "//tbody/tr[%s]/td[5]";
    final private String FILTER_ORDER_COUNTRY = "//tbody/tr[%s]/td[12]";
    final private String FILTER_ORDER_RESORT = "//tbody/tr[%s]/td[13]";
    final private String FILTER_ORDER_SHOWCASE = "//tbody/tr[%s]/td[10]";
    final private String FILTER_ORDER_SELLER = "//tbody/tr[%s]/td[4]";



    final private String FILTER_ORDER_STATUS = "//tbody/tr[%s]/td[21]";
    final private String FILTER_ORDER_FIRST_PAYMENT = "//tbody/tr[%s]/td[26]";
    final private String FILTER_ORDER_LAST_PAYMENT = "//tbody/tr[%s]/td[27]";
    final private String FILTER_ORDER_SOLD_DATE = "//tbody/tr[%s]/td[8]";
    final private String FILTER_ORDER_VISA = "//tbody/tr[1]/td[14]/span[1]";
    final private String FILTER_ORDER_VISA_SUPPORT = "//tbody/tr[1]/td[14]/span[2]";
    final private String FILTER_ORDER_CUSTOMER_PAYED = "//tbody/tr[%s]/td[10]";
    final private String FILTER_ORDER_PAYED_TO_TO = "//tbody/tr[%s]/td[12]/a";

    /*public void checkSERPwithFilters(String filter) throws Exception
    {
        if($(byXpath(EMPTY_RESULT_BLOCK)).is(not(visible)))
        {
            if($(byXpath(PAGINATION_BLOCK)).is(visible))
            {
                Integer totalPages = $$(byXpath(PAGINATION_TOTAL_PAGES_BUTTONS)).size();
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

                if(totalPages.equals(9))
                {
                    Integer lastPage = Integer.parseInt($(byXpath(PAGINATION_LAST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml());
                    if (lastPage > 15)
                    {
                        lastPage = 15;
                    }
                    for(int i = 1; i < lastPage + 1; i++)
                    {
                        for(int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                        {
                            if(filter.equals("start date"))
                            {
                                Assert.assertTrue((formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                        (formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "start date");
                            }
                            if(filter.equals("tour operator"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_TOUR_OPERATOR, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()), "tour operator");
                            }
                            if(filter.equals("country"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_COUNTRY, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()), "country");
                            }
                            if(filter.equals("resort"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_RESORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()), "resort");
                            }
                            if(filter.equals("showcase"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SHOWCASE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("travelata"), "showcase");
                            }

                            if(filter.equals("seller"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SELLER, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Северина"), "seller");
                            }
                            if(filter.equals("visa"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa");
                            }
                            if(filter.equals("visa support"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA_SUPPORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa support");
                            }



                        }
                        $(byXpath(PAGINATION_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        Selenide.sleep(CONSTANT_1_SECOND);
                    }

                    $(byXpath(PAGINATION_FIRST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
                else
                {
                    for(int i = 1; i < Integer.parseInt($(byXpath(PAGINATION_LAST_POSSIBLE_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()) + 1; i++)
                    {
                        for(int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                        {
                            if(filter.equals("start date"))
                            {
                                Assert.assertTrue((formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                        (formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                        formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "start date");
                            }
                            if(filter.equals("tour operator"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_TOUR_OPERATOR, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()),"tour operator");
                            }
                            if(filter.equals("country"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_COUNTRY, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()),"country");
                            }
                            if(filter.equals("resort"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_RESORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()), "resort");
                            }
                            if(filter.equals("showcase"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SHOWCASE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("travelata"), "showcase");
                            }

                            if(filter.equals("seller"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SELLER, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Северина"), "seller");
                            }
                            if(filter.equals("visa"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa");
                            }
                            if(filter.equals("visa support"))
                            {
                                Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA_SUPPORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa support");
                            }

                        }
                        $(byXpath(PAGINATION_NEXT_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                        Selenide.sleep(CONSTANT_1_SECOND);
                    }
                    $(byXpath(PAGINATION_FIRST_PAGE_BUTTON)).waitUntil(visible, CONSTANT_10_SECONDS).click();
                }
            }
            else
            {
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy");

                for(int y = 1; y < $$(byXpath(ORDERS_LIST)).size() + 1; y++)
                {
                    if(filter.equals("start date"))
                    {
                        Assert.assertTrue((formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).after(formatter2.parse(SendingTouristsPage.dateFrom)) ||
                                formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateFrom))) &&
                                (formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).before(formatter2.parse(SendingTouristsPage.dateTo))) ||
                                formatter2.parse($(byXpath(String.format(FILTER_ORDER_START_DATE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml()).equals(formatter2.parse(SendingTouristsPage.dateTo)), "start date");
                    }
                    if(filter.equals("tour operator"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_TOUR_OPERATOR, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.tourOperatorValue.toLowerCase()), "tour operator");
                    }
                    if(filter.equals("country"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_COUNTRY, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(MainPage.countryValue.toLowerCase()), "country");
                    }
                    if(filter.equals("resort"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_RESORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals(OrderPage.resortValue.toLowerCase()), "resort");
                    }
                    if(filter.equals("showcase"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SHOWCASE, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().equals("travelata"), "showcase");
                    }

                    if(filter.equals("seller"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_SELLER, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().contains("Северина"),"seller");
                    }
                    if(filter.equals("visa"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa");
                    }
                    if(filter.equals("visa support"))
                    {
                        Assert.assertTrue($(byXpath(String.format(FILTER_ORDER_VISA_SUPPORT, Integer.toString(y)))).waitUntil(visible, CONSTANT_10_SECONDS).innerHtml().toLowerCase().contains("да".toLowerCase()), "visa support");
                    }

                }
            }
        }
        else
        {
            System.console().printf("ничего не найдено");
        }
    }*/


    /**
     * Click some element
     * @param by    By  Element that is needed to be clicked
     */
    public void click(By by)
    {
        if (isElementDisplayed(by))
        {
            driver.findElement(by).click();
        }

    }

    /**
     * Check if element is displayed on page
     * @param by    By  Element that is needed to be checked
     * @return  true:   if element is displayed
     *          false:  otherwise
     */
    public boolean isElementDisplayed(By by)
    {
        return driver.findElement(by).isDisplayed();
    }

    /**
     * Wait for element not to be visible
     * @param by        By  Needed element to be waited for
     * @param timeOut   Integer Time limit for element not to be visible
     */
    public void waitForElementNotVisible(final By by, int timeOut)
    {
        (new WebDriverWait(driver, timeOut))
                .until(ExpectedConditions
                        .invisibilityOfElementLocated(by));
    }

    /**
     * Check if element is present
     * @param by    By  Needed element to be checked
     * @return  true:   element is present
     *          false:  otherwise
     */
    public boolean isElementPresent(By by)
    {
        try
        {
            driver.findElement(by);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e)
        {
            return false;
        }
    }

    /*public void waitForElementPresent(By by, int timeOut)
    {
        for (int i=0; i<timeOut; i++){
            if (isElementPresent(by)) break;
        }
    }*/

    /**
     * Wait for element to be displayed
     * @param by        By  Needed element to be waited for
     * @param timeOut   Integer Time limit for element to be displayed
     */
    public void waitForElementDisplayed(final By by, int timeOut)
    {
        try
        {
            (new WebDriverWait(driver, timeOut)).until(ExpectedConditions.visibilityOfElementLocated(by));
        }
        catch (org.openqa.selenium.TimeoutException timeout)
        {
            String currentUrl = driver.getCurrentUrl();
            System.out.println("Error source: " + currentUrl);
        }

    }


    /**
     * Find element on page
     * @param by    By  Needed element to be found
     * @return  WebElement object
     */
    public WebElement findElement (By by)
    {
        return driver.findElement(by);
    }

    public static String getCurrentDate(){
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy");


         return formatForDateNow.format(dateNow);
    }


}
