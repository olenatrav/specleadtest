package framework;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * Created by User on 26.07.2017.
 */
public class AndroidActions
{
    public final static int CONSTANT_1_SECOND = 1000;
    public final static int CONSTANT_3_SECONDS = 3000;
    public final static int CONSTANT_4_SECONDS = 4000;
    public final static int CONSTANT_2_SECONDS = 2000;
    public final static int CONSTANT_5_SECONDS = 5000;
    public final static int CONSTANT_10_SECONDS = 10000;
    public final static int CONSTANT_20_SECONDS = 20000;

    public static AndroidDriver driver;
    //public AppiumDriver driver;

    /*public void swipeFromRightToLeft()
    {
        org.openqa.selenium.Dimension dimension = WebDriverRunner.getWebDriver().manage().window().getSize();

        driver.swipe(dimension.width - 25, dimension.height / 2, 25, dimension.height / 2, 1000);

    }*/
    public void swipeFromRightToLeft(int startX, int startY, int endX, int endY)
    {
        driver.swipe(startX, startY, endX, endY, 500);
    }

    public void swipeFromLeftToRight(int startX, int startY, int endX, int endY)
    {
        driver.swipe(startX, startY, endX, endY, 500);
    }

    public void swipeFromBottomToTop(int times)
    {
        org.openqa.selenium.Dimension dimension = WebDriverRunner.getWebDriver().manage().window().getSize();
        for(int i = 1; i != times + 1; i++)
        {
            driver.swipe(dimension.width / 2, 400, dimension.width / 2, 200, 1000);
        }

    }
    public void swipeShortFromBottomToTop(int times)
    {
        org.openqa.selenium.Dimension dimension = WebDriverRunner.getWebDriver().manage().window().getSize();
        for(int i = 1; i != times + 1; i++)
        {
            driver.swipe(dimension.width / 2, 400, dimension.width / 2, 350, 1000);
        }

    }
    public void swipeFromTopToBottom(int times)
    {
        org.openqa.selenium.Dimension dimension = WebDriverRunner.getWebDriver().manage().window().getSize();
        for(int i = 1; i != times + 1; i++)
        {
            driver.swipe(dimension.width / 2, 200, dimension.width / 2, 400, 1000);
        }

    }

    public static String  getDateFromCurrentShortMonth(int daysCount){
        Calendar c = Calendar.getInstance();
        Date dt = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy-dd");
        c.setTime(dt);
        c.add(Calendar.DATE, daysCount);

        return dateFormat.format(c.getTime());
    }

    public static String  getDateFromCurrentNoYear(int daysCount){
        Calendar c = Calendar.getInstance();
        Date dt = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM");
        c.setTime(dt);
        c.add(Calendar.DATE, daysCount);

        return dateFormat.format(c.getTime());
    }

    public static String getDateFromCurrentMonthINEnglish(int daysCount)
    {
        Calendar c = Calendar.getInstance();
        Date dt = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM", Locale.US);
        c.setTime(dt);
        c.add(Calendar.DATE, daysCount);

        return dateFormat.format(c.getTime());
    }

    public void tapElement(String xpath)
    {
       // Selenide.sleep(500);
        int x = $(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX();
        int y = $(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY();

        driver.tap(1, x + 1, y + 1, 500);

        //driver.tap(1, $(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).toWebElement(), 500);

    }
    public void tapByCoordinates(int xCoordinate, int yCoordinate)
    {
        driver.tap(1, xCoordinate, yCoordinate, 500);
    }

    public void translateToKeyCode(String text)
    {
        String[] splittedText = text.split("");

        for(int i = 0; i < splittedText.length; i++)
        {
            driver.pressKeyCode(Constants.charCode.get(splittedText[i]));
            Selenide.sleep(200);
        }

    }
    public void splitBeforeSendKeys(String xpath, String text)
    {
        String[] splittedText = text.split("");
        for(int i = 0; i < splittedText.length; i++)
        {
            $(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).sendKeys(splittedText[i]);
        }
    }


}
