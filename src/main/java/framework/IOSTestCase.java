package framework;

import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;

import java.net.URL;

/**
 * Created by User on 29.08.2017.
 */
public class IOSTestCase
{
    //@BeforeMethod
    public void setUp() throws Exception
    {
        AppiumDriver driver;

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "iPhone Simulator");
        //capabilities.setCapability("browserName", "");
        capabilities.setCapability("platformVersion", "6.0");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("newCommandTimeout", 500);

        capabilities.setCapability("unicodeKeyboard", "true");

        driver = new IOSDriver(new URL("http://5.149.215.116/wd/hub/"), capabilities);

        WebDriverRunner.setWebDriver(driver);
    }
}
