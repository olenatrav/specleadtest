package framework;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by User on 27.09.2016.
 */
public interface Constants
{
    Map<String, String> hotelClass = new HashMap<String, String>()
    {{
            put("", "all");
            put("5", "7");
            put("4", "4");
            put("3", "3");
            put("2", "2");

    }};

    Map<String, String> mealType = new HashMap<String, String>()
    {{
            put("", "all");
            put("AI", "1");
            put("BB", "2");
            put("FB", "3");
            put("HB", "5");
            put("RO", "7");

        }};

    Map<String, String> mealTypeFullName = new HashMap<String, String>()
    {{
            /*put("Всё включено", "AI");
            put("Завтрак", "BB");
            put("Завтрак, обед, ужин", "FB");
            put("Завтрак+ужин", "HB");
            put("Без питания", "RO");*/

            put("AI", "Всё включено");
            put("BB", "Завтрак");
            put("FB", "Завтрак, обед, ужин");
            put("HB", "Завтрак+ужин");
            put("RO", "Без питания");

        }};

    Map<String, String> monthName = new HashMap<String, String>()
    {{
            put("01", "января");
            put("02", "февраля");
            put("03", "марта");
            put("04", "апреля");
            put("05", "мая");
            put("06", "июня");
            put("07", "июля");
            put("08", "августа");
            put("09", "сентября");
            put("10", "октября");
            put("11", "ноября");
            put("12", "декабря");

        }};
    Map<String, String> monthShortName = new HashMap<String, String>()
    {{

            put("01", "янв");
            put("02", "фев");
            put("03", "мар");
            put("04", "апр");
            put("05", "май");
            put("06", "июн");
            put("07", "июл");
            put("08", "авг");
            put("09", "сен");
            put("10", "окт");
            put("11", "ноя");
            put("12", "дек");

        }};
    Map<String, String> fullMonthName = new HashMap<String, String>()
    {{
            put("янв", "январь");
            put("фев", "февраль");
            put("мар", "март");
            put("апр", "апрель");
            put("май", "май");
            put("июн", "июнь");
            put("июл", "июль");
            put("авг", "август");
            put("сен", "сентябрь");
            put("окт", "октябрь");
            put("ноя", "ноябрь");
            put("дек", "декабрь");
        }};
    Map<String, Boolean> countryVisa = new HashMap<String, Boolean>()
    {{
            put("Абхазия", false);
            put("Австрия", true);
            put("Азербайджан", false);
            put("Андорра", true);
            put("Армения", false);
            put("Беларусь", false);
            put("Болгария", true);
            put("Венгрия", true);
            put("Вьетнам", false);
            put("Германия", true);
            put("Греция", true);
            put("Грузия", false);
            put("Дания", true);
            put("Доминикана", false);
            put("Израиль", false);
            put("Индия", true);
            put("Индонезия", false);
            put("Иордания", false);
            put("Ирландия", true);
            put("Испания", true);
            put("Италия", true);
            put("Камбоджа", false);
            put("Кения", false);
            put("Кипр", true);
            put("Китай", true);
            put("Коста-Рика", false);
            put("Куба", false);
            put("Латвия", true);
            put("Литва", true);
            put("Маврикий", false);
            put("Мальдивы", false);
            put("Мальта", true);
            put("Марокко", false);
            put("Мексика", true);
            put("Нидерланды", true);
            put("Норвегия", true);
            put("ОАЭ", false);
            put("Португалия", true);
            put("Россия", false);
            put("Румыния", true);
            put("Сейшелы", false);
            put("Сербия", false);
            put("Сингапур", true);
            put("Словения", true);
            put("Таиланд", false);
            put("Танзания", false);
            put("Тунис", false);
            put("Турция", false);
            put("Филиппины", false);
            put("Финляндия", true);
            put("Франция", true);
            put("Хорватия", true);
            put("Черногория", false);
            put("Чехия", true);
            put("Швеция", true);
            put("Шри-Ланка", true);
            put("Эстония", true);
            put("Южная Корея",false );
            put("Ямайка", false);

        }};

   Map<String, Integer> charCode = new HashMap<String, Integer>()
   {
       {
           put("a", 29);
           put("b", 30);
           put("c", 31);
           put("d", 32);
           put("e", 33);
           put("f", 34);
           put("g", 35);
           put("h", 36);
           put("i", 37);
           put("j", 38);
           put("k", 39);
           put("l", 40);
           put("m", 41);
           put("n", 42);
           put("o", 43);
           put("p", 44);
           put("q", 45);
           put("r", 46);
           put("s", 47);
           put("t", 48);
           put("u", 49);
           put("v", 50);
           put("w", 51);
           put("x", 52);
           put("y", 53);
           put("z", 54);

           put("0", 7);
           put("1", 8);
           put("2", 9);
           put("3", 10);
           put("4", 11);
           put("5", 12);
           put("6", 13);
           put("7", 14);
           put("8", 15);
           put("9", 16);

           put("@", 77);
           put(".", 56);
           put("-", 69);
           put(" ", 62);


       }
   };



    Map<String, String> emailName = new HashMap<String, String>()
    {{
            put("General lead", "Ваш запрос на оформление тура принят");

            put("Surcharge", "Ваша ссылка на доплату за тур по заказу #%s!");
            put("Taken to work", "Ваш заказ №%s принят в работу (бронируется)!");
            put("Pending payment", "Ваш заказ №%s ожидает оплаты. Не упустите свой тур!");

            put("Online booking link", "Ваша ссылка на онлайн-бронирование тура по заказу #%s!");

        }};

    String newOrder = "Новый заказ";
    String employees = "Сотрудники";
    String sendingTourists = "Отправка туристов";
    String desktop = "Рабочий стол";
    String desktopTL = "Рабочий стол ТЛ";
    String statistics = "Статистика";
    String finance = "Финансы";
    String cashPayments = "Наличные платежи";
    String orderQueue = "Очередь заказов";
    String statementOfInsurance = "Выписка страховок";


    String statusNew = "Новый";
    String statusTakenBySeller = "Взят турагентом";
    String statusInProcess = "В работе";
    String statusPendingPayment = "Ожидание оплаты";
    String statusCanceled = "Отменен";
    String statusSold = "Продано";
    String statusPartiallySold = "Продано с частичной оплатой";
    String statusDuplicated = "Двойной заказ";
    String statusInvalid = "Неверный заказ";
    String statusCanceledBookedTourByClient = "Отмена бронированного тура клиентом";
    String statusBooking = "Бронируется";
    //String status = "Сторно ТО";
    String statusWillComeToOffice= "Придет в офис";
    //String status = "Запрос информации у ТО";
    //String status = "Сейчас не готов покупать-связаться позже";


    String statusPaymentAuthorized = "Авторизован";
    String statusPaymentReleased = "Средства разблокированы";

    String hotTours = "/tury";
    String calendar = "/calendar";
    String searchTours = "/search";
    String hotels = "/hotelOffer/search";
    String aboutUs = "/about";


}
