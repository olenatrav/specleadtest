package framework;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.URL;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.service.DriverService;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;



/**
 * Created by User on 25.07.2017.
 */
public class AndroidTestCase extends AndroidActions
{

    @BeforeMethod
    public void setUp() throws Exception
    {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("browserName", "Android");
        capabilities.setCapability("platformVersion", "6.0");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("newCommandTimeout", 500);
        capabilities.setCapability("appPackage", "ru.travelata.app");
        capabilities.setCapability("appActivity", "ru.travelata.app.activities.SplashActivity");
        capabilities.setCapability("unicodeKeyboard", "true");

        Runtime runtime = Runtime.getRuntime();
        runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a 127.0.0.1 -p 4723 --session-override -dc \"{\"\"noReset\"\": \"\"false\"\"}\"\"");
        Thread.sleep(5000);
        String avd = "\"C:/Program Files (x86)/SDK/tools/emulator\" -avd Nexus_S";
        runtime.exec(avd);
        Thread.sleep(15000);
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub/"), capabilities);

        WebDriverRunner.setWebDriver(driver);


    }

    @AfterMethod
    public void tearDown() throws Exception
    {

        getWebDriver().quit();
        Runtime.getRuntime().exec("taskkill /im cmd.exe") ;
        Runtime.getRuntime().exec("adb -s emulator-5554 emu kill") ;

    }
}
