package framework;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.util.concurrent.TimeUnit;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.*;
/**
 * Created by User on 26.08.2016.
 */
public class SeleniumTestCase extends WebDriverCommands {
    public static String fileName = "";
    Cookie cookie1 = new Cookie("threePrizes", "true");
    Cookie cookie2 = new Cookie("cheaperTours", "true");
    Cookie cookie3 = new Cookie("showUserBannerLottery", "true");
    Cookie cookie4 = new Cookie("promoBannerShowHotClosed", "true");
    Cookie cookie5 = new Cookie("promoBannerHotClosed", "true");
    Cookie cookie6 = new Cookie("searchTourPopupShowed", "true");
    Cookie cookie7 = new Cookie("showUserSubscribeBanner", "1");

    @BeforeMethod
    public void setUp() {
//        System.setProperty("webdriver.gecko.driver", "C:/projects/geckodriver-v0.17.0-win64/geckodriver.exe");
 //       WebDriver driver = new FirefoxDriver();

        //FirefoxProfile profile = new FirefoxProfile();
        //driver = new FirefoxDriver(profile);
//        driver = new ChromeDriver();
//        driver = new InternetExplorerDriver();
//        driver = new HtmlUnitDriver(BrowserVersion.INTERNET_EXPLORER_11);
        //driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
//        driver.manage().window().setSize(new Dimension(1100, 1100));
        //driver.manage().window().maximize();


        driver = new FirefoxDriver(profile);
        WebDriverRunner.setWebDriver(driver);
        getWebDriver().manage().window().maximize();
        getWebDriver().navigate().to("http://travnew.travellata.ru");


        getWebDriver().manage().addCookie(cookie1);
        getWebDriver().manage().addCookie(cookie2);
        getWebDriver().manage().addCookie(cookie3);
        getWebDriver().manage().addCookie(cookie4);
        getWebDriver().manage().addCookie(cookie5);
        getWebDriver().manage().addCookie(cookie6);
        getWebDriver().manage().addCookie(cookie7);


        getWebDriver().navigate().refresh();




//        getWebDriver().get("http://travnew.travellata.ru");

        //driver.get("http://demo.travellata.ru?ab=b");
        // driver.get("http://travelata.ru");
       /* driver.manage().addCookie(cookie1);
        driver.manage().addCookie(cookie2);
        driver.manage().addCookie(cookie3);
        driver.navigate().refresh();*/

    }

    @AfterMethod
    public void tearDown()
    {
        /*driver.manage().deleteCookie(cookie2);
        driver.manage().deleteCookie(cookie3);
        driver.manage().deleteCookie(cookie1);
        driver.quit();*/
        getWebDriver().manage().deleteAllCookies();
        getWebDriver().quit();

    }
}