package iOS;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import framework.IOSTestCase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

/**
 * Created by User on 07.09.2017.
 */
public class SpecLeadTest extends IOSTestCase
{
    @Test
    public void iOSSpecLeadTest() throws  Exception
    {
        AppiumDriver driver;

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "iPhone Simulator");
        //capabilities.setCapability("browserName", "");
        capabilities.setCapability("platformVersion", "6.0");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("newCommandTimeout", 500);

        capabilities.setCapability("unicodeKeyboard", "true");

        driver = new IOSDriver(new URL("http://5.149.215.116/wd/hub/"), capabilities);

        WebDriverRunner.setWebDriver(driver);
    }
}
