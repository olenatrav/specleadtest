package Android;

import framework.AndroidTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.AndroidApp.*;

/**
 * Created by User on 21.08.2017.
 */
public class FiltersTest extends AndroidTestCase
{
    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AndroidFiltersTest() throws Exception
    {
        FirstSplashScreen.getFirstSplashScreen().checkSplashScreen().skipSplash();

        MainScreen.getMainScreen().skipDialogs().selectDestination();
        CountryScreen.getCountryScreen().selectCountry("Таиланд");
        ResortScreen.getResortScreen().tapSubmitButton();

        MainScreen.getMainScreen().selectDate();
        DateScreen.getDateScreen().tapFlexibleDate().tapSubmitButton();
        MainScreen.getMainScreen().tapStartSearchButton();

        String[] resorts = {};
        String[] hotelClass = {"3"};
        String[] mealType = {};
        SerpScreen.getSerpScreen().waitUntilResultsShown()
                .tapFiltersButton()
                .tapClearFiltersButton()
                .selectFilterNearBeach(false)
                .selectFilterResorts(resorts)
                .selectFilterHotelClass(hotelClass)
                .selectFilterMealType(mealType)
                .selectFilterPrice(true);
        swipeFromBottomToTop(2);

        String rating = "3.5";
        SerpScreen.getSerpScreen().selectFilterRating(rating).tapFiltersAcceptButton().waitUntilResultsShown().selectHotel();
        HotelScreen.getHotelScreen().checkFilters(SerpScreen.nearBeachIsSelected, resorts, hotelClass, mealType, SerpScreen.priceValue, rating);



    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        String[] resorts = {};

        return new Object[][]
                {
                        {


                        }
                };

    }

}
