package Android;


import framework.AndroidTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.AndroidApp.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Set;

/**
 * Created by User on 21.07.2017.
 */

public class SpecLeadTest extends AndroidTestCase
{

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AndroidSpecLeadTest(String departureCity, String country, String resorts[], int dayFromCurrent, boolean flexibleDate,
                                    String nightsFrom, String nightsTo, String adults, String children, String infants,
                                    String foreignFirstName, String foreignLastName, String adultBirthday, String kidBirthday, String sex, String serialNumber, String expireDate,
                                    String internalFirstName, String internalLastName, String middleName, String issueDate) throws Exception
    {



      /*  Process process = Runtime.getRuntime().exec("adb logcat");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        String log = "";
        String line = "";
        int n = 0;
        while ((line = bufferedReader.readLine()) != null) {
            //log = log + "\n" + line;
            System.out.println(line);
        }
*/





        FirstSplashScreen.getFirstSplashScreen().checkSplashScreen().skipSplash();

        MainScreen.getMainScreen().skipDialogs()
                .selectDepartureCity();

        DepartureCityScreen.getDepartureCityScreen().checkTitle()
                .selectDepartureCity(departureCity);

        MainScreen.getMainScreen().selectDestination();


        CountryScreen.getCountryScreen().checkTitle().selectCountry(country);
        ResortScreen.getResortScreen().checkTitle().selectResorts(resorts);

        MainScreen.getMainScreen().selectDate();

        DateScreen.getDateScreen().checkTitle().selectDate(dayFromCurrent, flexibleDate);


        MainScreen.getMainScreen().getDate().selectNights(nightsFrom, nightsTo);
        swipeFromBottomToTop(1);
        MainScreen.getMainScreen().selectPassengers();

        TouristsScreen.getTouristsScreen().checkTitle().selectPassengers(adults, children, infants);

        MainScreen.getMainScreen().tapStartSearchButton();

        SerpScreen.getSerpScreen().waitUntilResultsShown(/*adults, children, infants*/)
                .checkCriteria(country, resorts, dayFromCurrent, flexibleDate, nightsFrom, nightsTo)
                .selectHotel();

        HotelScreen.getHotelScreen().checkHotelScreen(country, resorts)
                .scrollToTours()
                .checkTabs(flexibleDate,dayFromCurrent)
                .checkTour(nightsFrom, nightsTo, dayFromCurrent, flexibleDate, adults, children, infants)
                .selectTour();

        PreCheckoutScreen.getPreCheckoutScreen()
                .checkPreCheckoutScreen(HotelScreen.roomValue, HotelScreen.mealValue, HotelScreen.discountValue, HotelScreen.dateFromValue, HotelScreen.dateToValue, HotelScreen.nightsValue, HotelScreen.passengersValue, HotelScreen.originalPriceValue, HotelScreen.tourPriceValue)
                .tapBookTourButton();

        CustomerDataScreen.getCustomerDataScreen().editEmail("ttest-android@mailinator.com")
                .editPhone("666 666-66-66")
                .tapSubmitButton();

        CheckoutScreen.getCheckoutScreen().enterTouristData(country, adults, children, infants, foreignFirstName, foreignLastName, internalFirstName, internalLastName, middleName,
                adultBirthday, kidBirthday, sex, serialNumber, expireDate, issueDate);
       /* .enterCardData("4111111111111112", "1219", "123", "test");

        ConciergeScreen.getConciergeScreen().addConcierge(false);

        CheckoutScreen.getCheckoutScreen().checkPayment();

        PreCheckoutScreen.getPreCheckoutScreen().checkPaidBlock();
*/

    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        String[] resorts = {};

        return new Object[][]
                {
                        {
                            "Москва",//departure
                            "Россия",//country
                            resorts,//resorts
                            73,//day from current
                            true,//flexible date
                            "1",//nights from
                            "29",//nights to
                            "2",//adults
                            "",//children
                            "", //infants

                            "firstname",
                            "lastname",
                            "12 12 1990",//adult birthday
                            "10 10 2010",//kid birthday
                            "male",//sex
                            "11 11 11111111111",//serial number
                            "11 11 2020",//expire date
                            "тестимя",
                            "тестфамилия",
                            "тестотчество",
                            "11 11 2011"//issue date


                        }
                };

    }

}
