package Android;

import framework.AndroidTestCase;
import org.testng.annotations.Test;
import pageobjects.AndroidApp.*;

/**
 * Created by User on 04.08.2017.
 */
public class HotTourTest extends AndroidTestCase
{
    @Test
    public void AndroidHotTourTest() throws Exception
    {

        FirstSplashScreen.getFirstSplashScreen().checkSplashScreen().skipSplash();

        MainScreen.getMainScreen().skipDialogs().selectHotTourTab().selectDestination();
        String s[] = {"Кипр", "Израиль", "ОАЭ"};
        CountryHotToursScreen.getCountryHotToursScreen().selectCountries(s);
        MainScreen.getMainScreen().tapStartSearchButton();
    }
}
