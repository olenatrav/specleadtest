package Android;

import framework.AndroidTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.AndroidApp.*;

/**
 * Created by User on 17.08.2017.
 */
public class CountryHunterTest extends AndroidTestCase
{
    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AndroidCountryHunterTest(String country, String resorts[], int dayFromCurrent, boolean flexibleDate,
                                         String nightsFrom, String nightsTo, String adults, String children, String infants) throws Exception
    {
        FirstSplashScreen.getFirstSplashScreen().checkSplashScreen().skipSplash();

        MainScreen.getMainScreen().skipDialogs().selectDestination();

        CountryScreen.getCountryScreen().checkTitle().selectCountry(country);
        ResortScreen.getResortScreen().checkTitle().selectResorts(resorts);


        MainScreen.getMainScreen().selectDate();
        DateScreen.getDateScreen().checkTitle().selectDate(dayFromCurrent, flexibleDate);

        MainScreen.getMainScreen().selectNights(nightsFrom, nightsTo);
        MainScreen.getMainScreen().selectPassengers();

        TouristsScreen.getTouristsScreen().checkTitle().selectPassengers(adults, children, infants);

        MainScreen.getMainScreen().tapStartSearchButton();


        SerpScreen.getSerpScreen().waitUntilResultsShown()
                .addCountryToTourHunter("ttest@mailinator.com")
                .tapBackButton();
        MainScreen.getMainScreen().tapTourHunterButton();
        TourHunterScreen.getTourHunterScreen().tapCountryTab().checkCountry(country, SerpScreen.tourHunterPriceValue, dayFromCurrent, flexibleDate, nightsFrom, nightsTo, adults, children, infants);


    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        String[] resorts = {};

        return new Object[][]
                {
                        {
                                "������",//country
                                resorts,//resorts
                                60,//day from current
                                true,//flexible date
                                "1",//nights from
                                "29",//nights to
                                "2",//adults
                                "1",//children
                                "" //infants

                        }
                };

    }
}
