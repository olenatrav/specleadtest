package createOrderTests.Mobile;

import com.codeborne.selenide.WebDriverRunner;
import framework.Constants;
import framework.SeleniumTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.MailForSpamMainPage;
import pageobjects.TOM.AuthorizationPage;
import pageobjects.TOM.DesktopPage;
import pageobjects.TOM.OrderPage;
import pageobjects.TOM.QueuePage;
import pageobjects.Travelata.CheckoutPage;
import pageobjects.Travelata.HotelPage;
import pageobjects.Travelata.Mobile.*;

/**
 * Created by Lena on 11.07.2017.
 */
public class MobileSpecLeadTest extends SeleniumTestCase{

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void mobileSpecLeadTest(String cityFromValue, String countryToValue, String resortsToValue,
                                   boolean isFlexibleDays, int dayFromCurrent, String nightsFromValue,
                                   String nightsToValue, String touristGroupValue, String customerEmailValue, String customerPhoneValue,
                                   String foreignLastNameValue,
                                   String foreignFirstNameValue, String birthdayDateValue,
                                   String foreignSexValue, String passportNumberValue,
                                   String foreignPassportExpiredDateValue, boolean ifChildWithPassport, String foreignNationalityValue,
                                   String passportType, String internalLastNameValue, String internalFirstNameValue,
                                   String internalMiddleNameInput, String internalPassportIssuedDateValue,
                                   String cardNumberValue, String cardExpiredMonthValue, String cardExpiredYearValue,
                                   String cardCvvValue, String cardHolderValue, String TOMemail, String TOMpassword) throws Exception {
        MobileMainPage.getMobileMainPage()
                .goToMobileMainPage()
                .selectCityFrom(cityFromValue)
                .selectCountryResortsTo(countryToValue, resortsToValue)
                .selectDate(isFlexibleDays, cityFromValue, dayFromCurrent)
                .selectNightsAmount(nightsFromValue, nightsToValue)
                .clickSelectDatesNightsButton()
                .selectTouristGroup(touristGroupValue)
                .clickStartSearchToursButton()
                .waitForLoaderDissapear()
                .checkToCountry(countryToValue)
                .checkToResorts(resortsToValue)
                .checkDatesTo(isFlexibleDays, dayFromCurrent, cityFromValue)
                .checkNightsAmount(nightsFromValue, nightsToValue)
                .checkCountryResortByHotel(countryToValue)
                .gotoFirstSerpMobileHotelPage()
                ;
        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);

        MobileHotelPage.getMobileHotelPage()
                .waitForTourLoaderDissapear()
                .checkStartDate(isFlexibleDays, dayFromCurrent, cityFromValue)
                .checkNightsAmount(nightsFromValue, nightsToValue)
                .checkTouristGroup(touristGroupValue)
                .clickFirstTour()
                ;

        MobileTourDetailsPage.getMobileTourDetailsPage()
                .waitForTourLoaderDissapear()
                .checkTouristGroup(touristGroupValue)
                .gotoCheckOutPage()
                ;

        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);

        MobileCheckOutCustomerDataPage.getCheckOutCustomerDataPage()
                .setCustomerEmail(customerEmailValue)
                .setCustomerPhone(customerPhoneValue)
                .gotoCheckOutPage()
                ;

        MobileCheckOutTouristDataPage.getMobileCheckOutTouristDataPage()
                .closeOptionalFieldsAlert()
                .fillTouristData(countryToValue, foreignLastNameValue, foreignFirstNameValue, birthdayDateValue,
                        foreignSexValue, passportNumberValue, foreignPassportExpiredDateValue, ifChildWithPassport,
                        foreignNationalityValue, passportType, internalLastNameValue, internalFirstNameValue,
                        internalMiddleNameInput, internalPassportIssuedDateValue)
                .fillCardData(cardNumberValue, cardExpiredMonthValue, cardExpiredYearValue, cardCvvValue, cardHolderValue)
                .submitCheckOutPage()
                .checkIfPaymentSuccessful()
                ;

        MailForSpamMainPage.getMailForSpamPage()
                .gotoMailForSpamPage()
                .fillEmailValue(customerEmailValue)
                .clickCheckMailboxButton()
                .clickBookedOrderLetter()
                .getOrderNumber();

        AuthorizationPage.getAuthorizationPage().goToAuthorizationPage()
                .enterAccountCredentials(TOMemail, TOMpassword);

        DesktopPage.getDesktopPage().selectSideMenuOption(Constants.orderQueue);

        QueuePage.getQueuePage()
                .gotoCurrentOrderPage();

        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);



    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        return new Object[][]
                {
                        new Object[]{
                                "Москва",
                                "Таиланд",
                                "Паттайя",
                                false,
                                10,
                                "9",
                                "10",
                                "1, 2, 1",
                                "lenatest@mailforspam.com",
                                "1111111111",
                                "foreignLastName",
                                "foreignFirstName",
                                "11.11.2000",
                                "женский",
                                "1111111111",
                                "11.11.2020",
                                false,
                                "ukraine",
                                "Российский паспорт",
                                "Тестовый",
                                "Тест",
                                "Тестович",
                                "11.11.2010",
                                
                                "4111 1111 1111 1112",
                                "12",
                                "17",
                                "123",
                                "TEST TEST",
                                "olga.severina@travelata.ru",
                                "123"
                        }
                };

    }

}
