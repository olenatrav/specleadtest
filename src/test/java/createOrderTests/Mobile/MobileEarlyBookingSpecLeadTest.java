package createOrderTests.Mobile;

import com.codeborne.selenide.WebDriverRunner;
import framework.SeleniumTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.Mobile.*;
import pageobjects.Travelata.Mobile.EarlyBooking.MobileEarlyBookingMainPage;

/**
 * Created by Lena on 17.08.2017.
 */
public class MobileEarlyBookingSpecLeadTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void mobileEarlyBookingSpecLeadTest(String cityFromValue, String countryToValue, String resortsValue,
                                      boolean isFlexibleDays, int dayFromCurrent, String nightsFromValue,
                                      String nightsToValue, String touristGroupValue, String customerEmailValue, String customerPhoneValue,
                                      String foreignLastNameValue,
                                      String foreignFirstNameValue, String birthdayDateValue,
                                      String foreignSexValue, String passportNumberValue,
                                      String foreignPassportExpiredDateValue, boolean ifChildWithPassport, String foreignNationalityValue,
                                      String passportType, String internalLastNameValue, String internalFirstNameValue,
                                      String internalMiddleNameInput, String internalPassportIssuedDateValue,
                                      String cardNumberValue, String cardExpiredMonthValue, String cardExpiredYearValue,
                                      String cardCvvValue, String cardHolderValue) throws Exception{
        MobileMainPage.getMobileMainPage()
                .goToMobileMainPage()
                .gotoMobileEarlybookingMainPage();
        MobileEarlyBookingMainPage.getMobileEarlyBookingMainPage()
                .selectCityFrom(cityFromValue)
                .selectCountryResortsTo(countryToValue, resortsValue)
                .selectDate(isFlexibleDays, cityFromValue, dayFromCurrent)
                .selectNightsAmount(nightsFromValue, nightsToValue)
                .clickSelectDatesNightsButton()
                .selectTouristGroup(touristGroupValue)
                .clickStartSearchToursButton()
                .waitForLoaderDissapear()
                .checkToCountry(countryToValue)
                .checkToResorts(resortsValue)
                .checkDatesTo(isFlexibleDays, dayFromCurrent, cityFromValue)
                .checkNightsAmount(nightsFromValue, nightsToValue)
                .checkToursAvailability()
                .checkCountryResortByHotel(countryToValue)
                .gotoFirstSerpMobileHotelPage()
        ;
        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);

        MobileHotelPage.getMobileHotelPage()
                .waitForTourLoaderDissapear()
                .checkStartDate(isFlexibleDays, dayFromCurrent, cityFromValue)
                .checkNightsAmount(nightsFromValue, nightsToValue)
                .clickFirstTour()
        ;

        MobileTourDetailsPage.getMobileTourDetailsPage()
                .waitForTourLoaderDissapear()
                .gotoCheckOutPage()
        ;

        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);

        MobileCheckOutCustomerDataPage.getCheckOutCustomerDataPage()
                .setCustomerEmail(customerEmailValue)
                .setCustomerPhone(customerPhoneValue)
                .gotoCheckOutPage()
        ;

        MobileCheckOutTouristDataPage.getMobileCheckOutTouristDataPage()
                .fillTouristData(countryToValue, foreignLastNameValue, foreignFirstNameValue, birthdayDateValue,
                        foreignSexValue, passportNumberValue, foreignPassportExpiredDateValue, ifChildWithPassport,
                        foreignNationalityValue, passportType, internalLastNameValue, internalFirstNameValue,
                        internalMiddleNameInput, internalPassportIssuedDateValue)
                .fillCardData(cardNumberValue, cardExpiredMonthValue, cardExpiredYearValue, cardCvvValue, cardHolderValue)
                .submitCheckOutPage()
                .checkIfPaymentSuccessful()
        ;

    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        return new Object[][]
                {
                        new Object[]{
                                "Москва",
                                "Турция",
                                "Анталья",
                                false,
                                90,
                                "7",
                                "11",
                                "2, 0, 0",
                                "lenatest@mailforspam.com",
                                "1111111111",
                                "foreignLastName",
                                "foreignFirstName",
                                "11.11.1988",
                                "женский",
                                "1111111111",
                                "11.11.2020",
                                false,
                                "ukraine",
                                "Заграничный паспорт",
                                "Тестовый",
                                "Тест",
                                "Тестович",
                                "11.11.2000",
                                "4111 1111 1111 1112",
                                "12",
                                "17",
                                "123",
                                "TEST TEST"
                        }
                };
    }

}
