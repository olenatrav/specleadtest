package createOrderTests;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import framework.Constants;
import framework.SeleniumTestCase;
import pageobjects.TOM.AuthorizationPage;
import pageobjects.TOM.DesktopPage;
import pageobjects.TOM.OrderPage;
import pageobjects.TOM.QueuePage;
import pageobjects.Travelata.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by User on 27.09.2017.
 */

@Test(dataProvider = "tourCriteria", enabled = true)
public class RussiaSpecLeadTest extends SeleniumTestCase
{
    public void RussiaSpecLeadTest(int dayFromCurrent, String region, String departureCity, String country, String resort, boolean isFlexibleDate, String fromNights, String toNights,
                                   String adultsAmountValue, String childrenAmountValue, String infantsAmountValue, String hotelClassValue, String mealTypeValue,
                                   String phone, String internalLastName, String internalFirstName, String internalMiddleName, String adultBirthday, String kidBirthday, String internalIssueDate,
                                   String serialNumber, String foreignLastName, String foreignFirstName, String sex, String nationality, String expireDate, boolean conciergeBeforePay, boolean conciergeAfterPlay,
                                   String cardNumber, String cardMonth, String cardYear, String cardHolder, String CVV, String TOMemail, String TOMpassword) throws Exception
    {
        MainPage.getMainPage().selectRegion(region)
                .clickTopMenuOption("/russia");

        RussiaPage.getRussiaPage().selectResort();

        SearchPage.getSearchPage().selectDepartureCityRussia(departureCity)
                .selectResortRussia(resort)
                .selectDateRussia(dayFromCurrent)
                .selectFlexibleDatesRussia(isFlexibleDate)
                .selectNightsRussia(fromNights, toNights)
                .selectAdultsRussia(adultsAmountValue)
                .selectChildrenRussia(childrenAmountValue)
                .selectInfantsRussia(infantsAmountValue)
                .clickStartButton()

                .waitForResultShown()

            .selectHotelClass(hotelClassValue)
            .selectMealType(mealTypeValue)
           .clickMoreTours();
            //.selectRussianTour();

        switchToNewTab(0);  //switch to serp page
        WebDriverRunner.getWebDriver().close(); // close serp page
        switchToNewTab(0);  //switch to hotel page


        HotelPage.getHotelPage().checkHotelPageTourAssertions(dayFromCurrent, departureCity, isFlexibleDate, SearchPage.datesValue, SearchPage.nightsValue,
                adultsAmountValue, childrenAmountValue, infantsAmountValue, mealTypeValue, hotelClassValue)
                .clickSelectTourButton();

        Selenide.sleep(CONSTANT_3_SECONDS);

        switchToNewTab(1); //switch to checkout page

        CheckoutPage.getCheckoutPage().checkCheckoutPageAssertions(country, resort, HotelPage.hotelValue, HotelPage.departureDateValue, HotelPage.nightsValue, adultsAmountValue, childrenAmountValue,
                infantsAmountValue, HotelPage.mealTypeValue, HotelPage.hotelClassValue)
                .generateEmail()
                .enterCustomerData(CheckoutPage.customerEmailValue, phone)
                .enterTouristData(country, adultsAmountValue, childrenAmountValue, infantsAmountValue, internalLastName, internalFirstName, internalMiddleName, adultBirthday,
                        kidBirthday, internalIssueDate, serialNumber, foreignLastName, foreignFirstName, sex, nationality, expireDate)
                .selectConciergeCheckBox(conciergeBeforePay)
                .enterCardData(cardNumber, cardMonth, cardYear, cardHolder, CVV, conciergeBeforePay, conciergeAfterPlay);

        MailboxPage.getMailboxPage().goToMailboxPage()
                .enterTestEmail(CheckoutPage.customerEmailValue)
                .clickSubmitButton()
                .getOrderIDFromMail();

        AuthorizationPage.getAuthorizationPage().goToAuthorizationPage()
                .enterAccountCredentials(TOMemail, TOMpassword);

        switchToNewTab(0); //switch to hotel page
        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);  //switch to desktop page

        DesktopPage.getDesktopPage().selectSideMenuOption(Constants.orderQueue);

        QueuePage.getQueuePage().assignAndRedirectToCertainOrder();//.assignAndRedirectToLastOrder();

        WebDriverRunner.getWebDriver().close(); //close queue page
        switchToNewTab(0); //switch to order page
        OrderPage.getOrderPage()
                .checkOrderPageTourAssertions(departureCity, country, resort, HotelPage.hotelValue, HotelPage.hotelClassValue, adultsAmountValue, childrenAmountValue, infantsAmountValue, HotelPage.departureDateValue,
                        HotelPage.nightsValue, HotelPage.mealTypeValue)
                .checkExistingUpsells(true)
                .checkOrderPagePassportAssertions(adultsAmountValue, childrenAmountValue, infantsAmountValue, CheckoutPage.lastNameValue, CheckoutPage.firstNameValue,
                        CheckoutPage.serialNumberValue, CheckoutPage.birthdayAdultValue, CheckoutPage.birthdayKidValue, CheckoutPage.abroad, CheckoutPage.middleNameValue,
                        CheckoutPage.issueDateValue, CheckoutPage.nationalityValue, CheckoutPage.expireDateValue, CheckoutPage.sexValue)
                .checkAuthorizedPayment();

    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        return new Object[][]
                {
                        {
                                20,//day from current
                                "Москва",//region
                                "Москва",//departure city
                                "Россия",//country
                                "Красная Поляна",//resort
                                true,//flexible days
                                "7",//nights from
                                "10",//nights to
                                "2",//adults
                                "1",//kids
                                "1",//infants
                                "",//hotel class
                                "",//meal type

                                "6666666666",//phone

                                "автотестФамилия",//internal last name
                                "автотестИмя",//internal first name
                                "автотестОтчество",//internal middle name
                                "11.11.1990",//adult birthday
                                "11.11.2015",//kid birthday
                                "11.11.2016",//internal issue date
                                "111111111111111",//serial number
                                "autotestLastName",//foreign last name
                                "autotestFirstName",//foreign first name
                                "female",//sex
                                "RUSSIAN FEDERATION",//nationality
                                "11.11.2222",//expire date

                                false,//concierge before pay
                                true,//concierge after pay

                                "4111111111111112",//card number
                                "12",//card month
                                "17",//card year
                                "test cardholder",//cardholder
                                "123",//CVV


                                "olga.severina@travelata.ru",//tom email
                                "123",//tom password

                        }
                };

    }
}

