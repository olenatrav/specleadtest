package createOrderTests;

import com.codeborne.selenide.WebDriverRunner;
import framework.Constants;
import framework.SeleniumTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.TOM.*;
import pageobjects.Travelata.MailboxPage;
import pageobjects.Travelata.MainPage;

/**
 * Created by User on 28.09.2017.
 */
public class GenLeadTest extends SeleniumTestCase
{
    @Test(dataProvider = "tourCriteria", enabled = true)
    public void GenLeadTest(String region, String countries, int dayFromCurrent, boolean isFlexible, String nightsFrom, String nightsTo,
                            String adults, String children, String infants, String hotelClass, String name, String phone, String tomLogin, String tomPassword) throws Exception
    {


        MainPage.getMainPage().selectRegion(region)
                .clickGenLeadButton()
                .selectDestinationGenLead(countries)
                .clickFromWhereToWhenButton()

                .selectDateGenLead(dayFromCurrent, isFlexible)
                .selectNightsGenLead(nightsFrom, nightsTo)
                .clickFromWhenToWhomButton()

                .selectAdultsGenLead(adults)
                .selectChildrenGenLead(children)
                .selectInfantsGenLead(infants)
                .clickFromWhomToHotelClassButton()

                .selectHotelClassGenLead(hotelClass)
                .clickFromHotelClassToOfficeButton()

        .selectOfficeGenLead()
        .clickFromOfficeToContactsNextButton()

        .enterNameGenLead(name)
        .enterPhoneGenLead(phone)
        .generateEmail()
        .enterEmailGenLead(MainPage.customerEmailValue)
        .submitDataGenLead();

        MailboxPage.getMailboxPage().goToMailboxPage()
                .enterTestEmail(MainPage.customerEmailValue)
                .clickSubmitButton()
                .selectEmail("General lead")
                .getOrderIDFromGenLeadMail();

        AuthorizationPage.getAuthorizationPage().goToAuthorizationPage()
                .enterAccountCredentials(tomLogin, tomPassword);

        DesktopPage.getDesktopPage().selectSideMenuOption(Constants.orderQueue);

        QueuePage.getQueuePage().assignAndRedirectToCertainOrder();

        WebDriverRunner.getWebDriver().close(); //close queue page
        switchToNewTab(0);

        OrderPage.getOrderPage().checkGenLeadData(countries, isFlexible, dayFromCurrent, nightsFrom, nightsTo, adults, children, infants, hotelClass);




        

    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        return new Object[][]
                {
                        {
                                "Москва",//region
                                "Чехия",//countries
                                20,//day from current
                                true,//flexible days
                                "7",//nights from
                                "10",//nights to
                                "2",//adults
                                "",//kids
                                "",//infants
                                "",//hotel class

                                "autotest_general_lead",//name
                                "6666666666",//phone

                                "olga.severina@travelata.ru",//tom email
                                "123",//tom password

                        }
                };

    }
}
