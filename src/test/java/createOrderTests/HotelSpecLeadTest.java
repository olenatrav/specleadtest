package createOrderTests;

import com.codeborne.selenide.Selenide;
import framework.Constants;
import framework.SeleniumTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.HotelPage;
import pageobjects.Travelata.HotelSearchPage;
import pageobjects.Travelata.MainPage;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;

/**
 * Created by User on 29.06.2017.
 */
public class HotelSpecLeadTest extends SeleniumTestCase
{
    @Test(dataProvider =  "tourCriteria")
    public void HotelSpecLeadTest(String region, String country, String resort, int startDayFromCurrent, int endDayFromStart, String adults, String children, String infants, String hotelClass, String mealType) throws Exception
    {
        MainPage.getMainPage().selectRegion(region)
                              .clickTopMenuOption(Constants.hotels);
        HotelSearchPage.getHotelSearchPage().waitForResultShown()
                                            .selectCountryAndResort(country, resort)
                .selectDate(startDayFromCurrent, endDayFromStart)
                .selectAdults(adults)
                .selectChildren(children)
                .selectInfants(infants)
                .selectHotelClass(hotelClass)
                .selectMealType(mealType)
                .clickSearchButton()
                .waitForResultShown()
                .clickMoreHotelOffers();

        switchToNewTab(1);

       /* HotelPage.getHotelPage().checkHotelPageTourAssertions()
                .checkIfUnsuccessfulSearch()
                .*/

    }

    @DataProvider
    public Object[][] tourCriteria()
    {
        return new Object[][]
                {
                        {"Москва", "Турция", "Аланья", 40, 85, "2", "1", "1", "2, 3, 4", "HB"}
                };
    }
}
