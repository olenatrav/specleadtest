package TomTest;

import framework.SeleniumTestCase;
import framework.WebDriverCommands;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.TOM.*;

/**
 * Created by Lena on 26.06.2017.
 */
public class InsuranceTest extends SeleniumTestCase {

    @Test(dataProvider = "insuranceFilterData", enabled = false)
    private void insuranceFilterTest(String email, String password, String IDValue, String statusValue, String externalIDValue,
                                     String createDateFromValue, String createDateToValue, String cancelDateFromValue,
                                     String cancelDateToValue) throws Exception{

        AuthorizationPage.getAuthorizationPage()
                .goToAuthorizationPage()
                .enterAccountCredentials(email, password);

        InsurancePage.getInsurancePage()
                .goToInsurancePage()
                .fillID(IDValue)
                .fillStatus(statusValue)
                .fillExternalID(externalIDValue)
                .fillCreateDateFrom(createDateFromValue)
                .fillCreateDateTo(createDateToValue)
                .fillCancelDateFrom(cancelDateFromValue)
                .fillCancelDateTo(cancelDateToValue)
                .clickSearchButton()
                .checkFilteredInsuranceParameters(IDValue, statusValue, externalIDValue, createDateFromValue,
                        createDateToValue, cancelDateFromValue, cancelDateToValue)
                ;


    }

    @Test(dataProvider = "insuranceAddCycleData", enabled = true)
    private void insuranceAddCycleTest(String email, String password, String touristCountValue, String insurancePriceValue, String orderIDValue,
                                       String insuranceManagerNameValue, String insuranceExternalIDValue) throws Exception{

        AuthorizationPage.getAuthorizationPage()
                .goToAuthorizationPage()
                .enterAccountCredentials(email, password);

        OrderPage.getOrderPage()
                .goToOrderPage(orderIDValue)
                .selectUpsellProduct("Страхование", touristCountValue, insurancePriceValue,
                        insuranceManagerNameValue, "Нужно выписать полис");

        InsurancePage.getInsurancePage()
                .goToInsurancePage()
                .fillID(orderIDValue)
                .fillStatus("Нужно выписать полис")
                .clickSearchButton()
                .checkFilteredInsuranceParameters(orderIDValue, "Нужно выписать полис", "", "",
                        "", "", "")

                .setInsuranceExternalID(insuranceExternalIDValue)
                .setInsuranceCreateDate(WebDriverCommands.getCurrentDate())

                .fillID(orderIDValue)
                .fillStatus("Нужно выписать полис")
                .clickSearchButton()
                .checkFilteredInsuranceParameters(orderIDValue, "Нужно выписать полис", insuranceExternalIDValue, WebDriverCommands.getCurrentDate(),
                        WebDriverCommands.getCurrentDate(), "", "")

                .confirmInsuranceChangedStatus()

                .fillID(orderIDValue)
                .fillStatus("Полис выписан")
                .clickSearchButton()
                .checkFilteredInsuranceParameters(orderIDValue, "Полис выписан", insuranceExternalIDValue, WebDriverCommands.getCurrentDate(),
                        WebDriverCommands.getCurrentDate(), "", "");

        OrderPage.getOrderPage().goToOrderPage(orderIDValue)
                .setInsuranceStatus("Запрос на аннуляцию")
                .saveUpsellChanges();

        InsurancePage.getInsurancePage()
                .goToInsurancePage()
                .fillID(orderIDValue)
                .fillStatus("Запрос на аннуляцию")
                .clickSearchButton()
                .checkFilteredInsuranceParameters(orderIDValue, "Запрос на аннуляцию", insuranceExternalIDValue, WebDriverCommands.getCurrentDate(),
                        WebDriverCommands.getCurrentDate(), "", "")

                .setInsuranceCancelDate(WebDriverCommands.getCurrentDate())
                .confirmInsuranceChangedStatus()

                .fillID(orderIDValue)
                .fillStatus("Полис аннулирован")
                .clickSearchButton()
                .checkFilteredInsuranceParameters(orderIDValue, "Полис аннулирован", insuranceExternalIDValue, "",
                        "", "", "");

        OrderPage.getOrderPage().goToOrderPage(orderIDValue)
                .checkIfInsuranceCandcelled()
                .deleteInsurance();

    }

    @DataProvider
    public Object[][]  insuranceFilterData()
    {
        return new Object[][]
                {
                        {
                                "olga.severina@travelata.ru",
                                "123",
                                "1225",
                                "",
                                "",
                                "01.06.2017",
                                "15.06.2017",
                                "01.06.2017",
                                "15.06.2017"
                        }
                };

    }

    @DataProvider
    public Object[][]  insuranceAddCycleData()
    {
        return new Object[][]
                {
                        {
                                "olga.severina@travelata.ru",
                                "123",
                                "2",
                                "100",
                                "3422",
                                "Северина Ольга",
                                "11111"
                        }
                };

    }

}
