package TomTest;

import framework.SeleniumTestCase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.TOM.AuthorizationPage;
import pageobjects.TOM.FinancesPage;
import pageobjects.TOM.SendingTouristsPage;
import pageobjects.TOM.StatisticsPage;

/**
 * Created by User on 04.10.2017.
 */
public class DownloadFileTest extends SeleniumTestCase
{
    @Test(dataProvider = "tourCriteria", enabled = true)
    public void DownloadFileTest(String tomLogin, String tomPassword) throws Exception
    {
        AuthorizationPage.getAuthorizationPage().goToAuthorizationPage()
                .enterAccountCredentials(tomLogin, tomPassword);

        StatisticsPage.getStatisticsPage().goToStatisticsPage()
                .exportData();

        SendingTouristsPage.getSendingTouristsPage().goToSendingTouristsPage()
                .exportData();

        FinancesPage.getFinancesPage().goToFnancesPage()
                .exportData();
    }

    @DataProvider
    public Object[][]  tourCriteria()
    {
        return new Object[][]
                {
                        {

                                "olga.severina@travelata.ru",//tom email
                                "123",//tom password*/


                        }
                };

    }
}
