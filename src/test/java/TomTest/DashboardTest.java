package TomTest;

import com.codeborne.selenide.WebDriverRunner;
import framework.SeleniumTestCase;
import framework.WebDriverCommands;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.TOM.*;

/**
 * Created by Lena on 04.07.2017.
 */
public class DashboardTest extends SeleniumTestCase {

    @Test(dataProvider = "dashdoardData", enabled = true)
    private void dashboardTest(String email, String password, String orderIDValue, String nextActionTextValue) throws Exception{
        AuthorizationPage.getAuthorizationPage()
                .goToAuthorizationPage()
                .enterAccountCredentials(email, password);

        OrderPage.getOrderPage()
                .goToOrderPage(orderIDValue)
                .selectOrderStatus("В работе")
                .addNextAction(nextActionTextValue, WebDriverCommands.getCurrentDate());

        DashboardPage.getDashboardPage()
                .goToDashboardPage()
                .waitForOrdersLoad()
                .checkIfOrderIDExistInWorkQueue(orderIDValue)
                .checkIfOrderIDExistInActionsQueue(orderIDValue, nextActionTextValue);

        OrderPage.getOrderPage()
                .goToOrderPage(orderIDValue)
                .selectOrderStatus("Ожидание оплаты");

        DashboardPage.getDashboardPage()
                .goToDashboardPage()
                .waitForOrdersLoad()
                .checkIfOrderIDExistInPaymentQueue(orderIDValue);

        OrderPage.getOrderPage()
                .goToOrderPage(orderIDValue)
                .selectOrderStatus("Продано");

        DashboardPage.getDashboardPage()
                .goToDashboardPage()
                .waitForOrdersLoad()
                .gotoSoldOrdersPage();

        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);

        SoldOrdersPage.getSoldOrdersPage()
                .checkIfOrderIDExistInXancelledOrdersQueue(orderIDValue);

        OrderPage.getOrderPage()
                .goToOrderPage(orderIDValue)
                .selectOrderStatus("Неверный заказ");

        DashboardPage.getDashboardPage()
                .goToDashboardPage()
                .waitForOrdersLoad()
                .gotoCancelledOrdersPage();

        WebDriverRunner.getWebDriver().close(); //close hotel page
        switchToNewTab(0);

        CancelledOrdersPage.getCancelledOrdersPage()
                .checkIfOrderIDExistInСancelledOrdersQueue(orderIDValue);

    }

    @DataProvider
    public Object[][]  dashdoardData()
    {
        return new Object[][]
                {
                        {
                                "olga.severina@travelata.ru",
                                "123",
                                "3637",
                                "тестовое действие по заявке 3637"
                        }
                };

    }

}
